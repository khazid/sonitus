﻿-- Function: setcreated_at()

-- DROP FUNCTION setcreated_at();

CREATE OR REPLACE FUNCTION setcreated_at()
  RETURNS trigger AS
$BODY$
  DECLARE
    
        BEGIN
         
NEW.created_at=now();
        return NEW; 
        end;   

$BODY$
  LANGUAGE plpgsql VOLATILE STRICT
  COST 100;
ALTER FUNCTION setcreated_at()
  OWNER TO postgres;

  -- Function: setupdated_at()

-- DROP FUNCTION setupdated_at();

CREATE OR REPLACE FUNCTION setupdated_at()
  RETURNS trigger AS
$BODY$
  DECLARE
    
        BEGIN
         
NEW.updated_at=now();
        return NEW; 
        end;   

$BODY$
  LANGUAGE plpgsql VOLATILE STRICT
  COST 100;
ALTER FUNCTION setupdated_at()
  OWNER TO postgres;
-------------------------------------------------------

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON client
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON client
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

    CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON client_process
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON client_process
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

     CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON countries
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON countries
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON general_info
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON general_info
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON general_type
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON general_type
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON job_type
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON job_type
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON message_status
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON message_status
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON messages
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON messages
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON new_request
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON new_request
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON picture
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON picture
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON process
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON process
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

    CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON personal_visits
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON personal_visits
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

      CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON menu
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON menu
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  
      CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON menu_role
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON menu_role
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON password_resets
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON password_resets
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

    CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON permission_role
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON permission_role
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

      CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON permission_user
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON permission_user
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON permissions
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON permissions
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

    CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON personal
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON personal
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON visits_general_info
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON visits_general_info
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON visits_general_type
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON visits_general_type
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

    CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON visits_job_type
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON visits_job_type
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON process_messages
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON process_messages
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

    CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON visits_picture
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON visits_picture
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

      CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON process_status
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON process_status
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON role_user
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON role_user
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

    CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON roles
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON roles
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

      CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON status
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON status
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

      CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON visits
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON visits
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
        CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON billing_config
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON billing_config
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
          CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON events
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON events
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
          CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON holidays
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON holidays
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();

  
         CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON quality
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON quality
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
          CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON quote
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON quote
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
          CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON quote_items
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON quote_items
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
          CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON quote_status
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON quote_status
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
          CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON request_to_client
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON request_to_client
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();
  
           CREATE TRIGGER update_modified
  BEFORE UPDATE
  ON users
  FOR EACH ROW
  EXECUTE PROCEDURE setupdated_at();
  
  CREATE TRIGGER created
  BEFORE INSERT
  ON users
  FOR EACH ROW
  EXECUTE PROCEDURE setcreated_at();