﻿CREATE OR REPLACE FUNCTION dbcont_diaslab(date, date)
  RETURNS bigint AS
$BODY$
SELECT count(*) FROM
(SELECT extract('dow' FROM $1+x) AS dow
FROM generate_series(0,$2-$1) x WHERE $1+x NOT IN (SELECT date FROM holidays WHERE extract('dow' FROM date) NOT IN(0,6))) AS foo
WHERE dow BETWEEN 1 AND 5;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION dbcont_diaslab(date, date) OWNER TO postgres;

CREATE TABLE holidays
(
  id serial NOT NULL,
  date date,
  CONSTRAINT holidays_pkey PRIMARY KEY (id)
);
ALTER TABLE holidays OWNER TO postgres;

select dbcont_diaslab('2015-04-29', '2015-05-11')


insert into holidays(date,description) values('2016-02-8','Lunes Carnaval');--Carnaval
insert into holidays(date,description) values('2016-02-9','Martes Carnaval');--Carnaval
insert into holidays(date,description) values('2016-03-24','Jueves Santo');--jueves santo
insert into holidays(date,description) values('2016-03-25','Viernes Santo');--viernes santo
insert into holidays(date,description) values('2016-05-01','Dial del Trabajador');--dia del trabajador
insert into holidays(date,description) values('2016-06-24','Batalla de Carabobo');--Batalla de Carabobo
insert into holidays(date,description) values('2016-07-05','Dia de la Independencia');--Dia de la independencia
insert into holidays(date,description) values('2016-07-24','Natalicio Simon Bolivar');--natalicio simon bolivar
insert into holidays(date,description) values('2016-10-12','Día de la Resistencia Indígena');--Día de la Resistencia Indígena
insert into holidays(date,description) values('2016-12-24','Navidad');--navidad
insert into holidays(date,description) values('2016-12-25','Navidad');--navidad
insert into holidays(date,description) values('2016-12-31','Vispera de Ano Nuevo');--Año nuevo
insert into holidays(date,description) values('2017-01-01','Ano Nuevo');--año nuevo

insert into holidays(date,description) values('2017-02-27','Lunes Carnaval');--Carnaval
insert into holidays(date,description) values('2017-02-28','Martes Carnaval');--Carnaval
insert into holidays(date,description) values('2017-04-13','Jueves Santo');--jueves santo
insert into holidays(date,description) values('2017-04-24','Viernes Santo');--jueves santo
insert into holidays(date,description) values('2017-05-01','Dia del Trabajador');--jueves santo
insert into holidays(date,description) values('2017-06-24','Batalla de Carabobo');--jueves santo
insert into holidays(date,description) values('2017-07-05','Dia de la independencia');--jueves santo
insert into holidays(date,description) values('2017-07-24','Natalicio Simon Bolivar');--natalicio simon bolivar
insert into holidays(date,description) values('2017-10-12','Día de la Resistencia Indígena');--jueves santo
insert into holidays(date,description) values('2017-12-24','Navidad');
insert into holidays(date,description) values('2017-12-25','Navidad');
insert into holidays(date,description) values('2017-12-31','Vispera Ano Nuevo');
insert into holidays(date,description) values('2018-01-01','Ano Nuevo');

insert into holidays(date,description) values('2018-02-12','Lunes Carnaval');
insert into holidays(date,description) values('2018-02-13','Martes Carnaval');
insert into holidays(date,description) values('2018-03-29','Jueves Santo');
insert into holidays(date,description) values('2018-03-30','Viernes Santo');
insert into holidays(date,description) values('2018-05-01','dia del trabajador');
insert into holidays(date,description) values('2018-06-24','Batalla de Carabobo');
insert into holidays(date,description) values('2018-07-05','Dia de la independencia');
insert into holidays(date,description) values('2018-07-24','natalicio simon bolivar');
insert into holidays(date,description) values('2018-10-12','Día de la Resistencia Indígena');
insert into holidays(date,description) values('2018-12-24','navidad');
insert into holidays(date,description) values('2018-12-25','navidad');
insert into holidays(date,description) values('2018-12-31','Vispera de Ano Nuevo');
insert into holidays(date,description) values('2019-01-01','Ano nuevo');

insert into holidays(date,description) values('2019-02-4','Lunes Carnaval');
insert into holidays(date,description) values('2019-02-5','Martes Carnaval');
insert into holidays(date,description) values('2019-04-18','Jueves santo');
insert into holidays(date,description) values('2019-04-19','Viernes santo');
insert into holidays(date,description) values('2019-05-01','Dia del trabajador');
insert into holidays(date,description) values('2019-06-24','Batalla de Carabobo');
insert into holidays(date,description) values('2019-07-05','Dia de la independencia');
insert into holidays(date,description) values('2019-07-24','Natalicio Simon Bolivar');
insert into holidays(date,description) values('2019-10-12','Día de la Resistencia Indígena');
insert into holidays(date,description) values('2019-12-24','Navidad');
insert into holidays(date,description) values('2019-12-25','Navidad');
insert into holidays(date,description) values('2019-12-31','Vispera de Ano Nuevo');
insert into holidays(date,description) values('2020-01-01','Ano nuevo');

insert into holidays(date,description) values('2020-02-24','Lunes Carnaval');
insert into holidays(date,description) values('2020-02-25','Martes Carnaval');
insert into holidays(date,description) values('2020-04-09','Jueves santo');
insert into holidays(date,description) values('2020-04-10','Viernes santo');
insert into holidays(date,description) values('2020-05-01','Dia del trabajador');
insert into holidays(date,description) values('2020-06-24','Batalla de Carabobo');
insert into holidays(date,description) values('2020-07-05','Dia de la independencia');
insert into holidays(date,description) values('2020-07-24','Natalicio Simon Bolivar');
insert into holidays(date,description) values('2020-10-12','Día de la Resistencia Indígena');
insert into holidays(date,description) values('2020-12-24','Navidad');
insert into holidays(date,description) values('2020-12-25','Navidad');
insert into holidays(date,description) values('2020-12-31','Vispera de Ano Nuevo');
insert into holidays(date,description) values('2021-01-01','Ano nuevo');

1 Enero: Año Nuevo

27 Febrero: Lunes de Carnaval

28 Febrero: Martes de Carnaval

13 Abril: Jueves Santo (Semana Santa)

14 Abril: Viernes Santo (Semana Santa)

16 Abril: Domingo de Resurrección (Semana Santa)

19 Abril: Declaración de la Independencia

1 Mayo: Día del Trabajo

24 Junio: Batalla de Carabobo

5 Julio: Día de la Independencia

24 Julio: Natalicio de Simón Bolívar

12 Octubre: Día de la Resistencia Indígena

24 Diciembre: Víspera de Navidad

25 Diciembre: Navidad

31 Diciembre: Fiesta de Fin de Año




