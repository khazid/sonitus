## Sistema de Control de Procesos Sonitus

Sistema de control del Procesos para la empresa Isonitus ubicado en Venezuela y panama. 

## Lenguajes utilizados

Laravel 5.4
Jquery
Postgresql 9.5
Bootstrap
tema Gentella

## Description

Sonitus es una empresa dedicada a la instalacion e implementacion de equipos de seguridad y domotica, su idea principal es darle a sus clientes herramientas para la automatizacion de hogares hasta edificaciones para poder obtener la maxima seguridad y control total de los dispositivos contenidos. Son una empresa encargada de brindarles soluciones, comodidad, facilidad y seguridad al permitirle controlar todos los dispositivos de su oficina u hogar con tan solo un clic. Se ocupan de diseñar, instalar y automatizar sus espacios brindándoles la más alta calidad en servicios, productos, tecnología y responsabilidad.

Por ser una empresa enfocada en la seguridad y control, tenian la necesidad de un sistema que pudiera administrar de forma eficaz y en tiempo real los trabajos realizados para los diferentes clientes. Por esta razon el sistema contiene las siguientes funciones:

Administrador de Usuarios / Roles (Administrativo, tecnicos, jefe tecnico, jefe)
Administracion de Tareas (Creacion y asignacion de tareas y clientes)
Calendarios de Tareas
Administracion de tareas por Cliente y empleado
Panel administrativo para cada rol 
Panel administrativo para los clientes donde pueden realizar el seguimiento de los trabajos realizados y a realizar.
Envio de notificaciones por correo electronico
firma digital para la contratacion y finalizacion de un trabajo
Administracion de Facturacion y cotizaciones.


### License

The Laravel framework, Jquery is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

PostgreSQL is released under the PostgreSQL License, a liberal Open Source license, similar to the BSD or MIT licenses.
