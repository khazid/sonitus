<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quote_number');
            $table->integer('client_id')->unsigned()->index();
            $table->foreign('client_id')->references('id')->on('client');
            $table->integer('new_request_id')->unsigned()->index()->nullable();
            $table->foreign('new_request_id')->references('id')->on('new_request');
            $table->integer('process_id')->unsigned()->index()->nullable();
            $table->foreign('process_id')->references('id')->on('process');
            $table->integer('status_id')->unsigned()->index();
            $table->foreign('status_id')->references('id')->on('status');
            $table->integer('personal_id')->unsigned()->index();//personal que solicita la cotizacion
            $table->foreign('personal_id')->references('id')->on('personal');
            $table->integer('users_id')->unsigned()->index();//persona que crea la cotizacion
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('billing_config_id')->nullable();
            $table->string('observation',1000);//tipo de pago 50 % o total
            $table->string('pay_type',50);//tipo de pago 50 % o total
            $table->boolean('send');//cotizacion enviada o no
            $table->string('currency',50);//tipo de moneda
            $table->integer('tecnicians');//cantidad de tecnicos para el proyecto
            $table->integer('days');//cantidad de dias estimados para el proyecto los dias se basan en 8hrs
            $table->boolean('travel');//si tienen q viajar o no
            $table->boolean('night_work');//si aplica o no la hora nocturna
            $table->integer('night_work_hours');//cantidad de horas nocturnas
            $table->boolean('day_work');//si aplica o no la hora extra diurna
            $table->integer('day_work_hours');//cantidad de hora extras diurnas
            $table->double('night_work_price',15,2);//total horas nocturnas
            $table->double('day_work_price',15,2);//total horas diurnas
            $table->double('transportation',15,2);//pasaje 
            $table->double('viaticum',15,2);//comidas
            $table->double('lodgment',15,2);//hospedaje
            $table->doble('total',15,2);//porcentaje de iva para la cotizacion
            $table->date('date');//porcentaje de iva para la cotizacion
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('quote');
    }
}
