<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualityTable.php extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quality', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service',100);
            $table->string('punctuality',100);
            $table->string('attitude',100);
            $table->string('attention',100);
            $table->string('consultation',100);
            $table->string('professionalism',100);
            $table->string('job_quality',100);
            $table->string('equipment_quality',100);
            $table->string('solutions',100);
            $table->string('knowledge',100);
            $table->string('evaluation',100);
            $table->string('administrative',100);
            $table->boolean('work_with');
            $table->string('work_reason',1000);
            $table->string('service_satisfaction',1000);
            $table->string('less_satisfaction',1000);
            $table->string('comments',1000);
            $table->integer('process_id')->unsigned()->index();
            $table->foreign('process_id')->references('id')->on('process');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('quality');
    }
}
