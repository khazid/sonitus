<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_config', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->double('daily_salary',15,2);
            $table->double('month_salary',15,2);
            $table->double('biweekly_salary',15,2);//quincenal
            $table->integer('iva');
            $table->integer('workday');//cantidad de horas jornada laboral
            $table->string('night_hour_start');
            $table->string('night_hour_end');
            $table->string('extra_hour_start');
            $table->string('extra_hour_end');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billing_config');
    }
}
