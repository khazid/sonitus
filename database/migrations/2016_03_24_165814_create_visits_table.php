<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('process_id')->unsigned()->index();
            $table->foreign('process_id')->references('id')->on('process');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('assigned_id')->nullable();
            $table->foreign('assigned_id')->references('id')->on('personal');
            $table->integer('status_id');
            $table->string('description',350)->nullable();
            $table->date('date');
            $table->string('entrance_hr',10)->nullable();
            $table->string('exit_hr',10)->nullable();
            $table->boolean('have_request')->default(FALSE);
            $table->boolean('have_foto')->default(FALSE);
            $table->string('signature')->nullable();
            $table->string('close_type')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('visits');
    }
}
