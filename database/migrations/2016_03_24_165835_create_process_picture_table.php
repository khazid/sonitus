<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessPictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_picture', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('picture_id')->unsigned()->index();
            $table->foreign('picture_id')->references('id')->on('picture');
            $table->integer('visits_id')->unsigned()->index();
            $table->foreign('visits_id')->references('id')->on('visits');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visits_picture');
    }
}
