<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('quote_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quote_id')->unsigned()->index();
            $table->foreign('quote_id')->references('id')->on('quote');
            $table->string('description',1000);
            $table->integer('quantity');
            $table->double('unit_price',15,2);
            $table->double('total',15,2);//cantidad de tecnicos para el proyecto
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quote_items');
    }
}
