<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_job_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visits_id')->unsigned()->index();
            $table->foreign('visits_id')->references('id')->on('visits');
            $table->integer('job_type_id')->unsigned()->index();
            $table->foreign('job_type_id')->references('id')->on('job_type');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visits_job_type');
    }
}
