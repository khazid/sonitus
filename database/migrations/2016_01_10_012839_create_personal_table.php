<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',30);
            $table->string('identity_number')->unique();
            $table->string('address',600);
            $table->string('occupation', 100);
            $table->string('occupation_description',400);
            $table->string('telephone',50);
            $table->string('email',50);
            $table->string('profile_pic',50);
            $table->date('date_birth');
            $table->date('initial_date');
            $table->boolean('active',TRUE);
            $table->integer('users_id')->nullable();
            $table->foreign('users_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal');
    }
}
