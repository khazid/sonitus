<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quote_id')->unsigned()->index();
            $table->foreign('quote_id')->references('id')->on('process');
            $table->integer('status_id')->unsigned()->index();
            $table->foreign('status_id')->references('id')->on('status');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->string('observation',1000)->nullable();
            $table->date('date');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('quote_status');
    }
}
