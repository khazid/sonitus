<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessGeneralInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_general_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('general_id')->unsigned()->index();
            $table->foreign('general_id')->references('id')->on('general_info');
            $table->integer('visits_id')->unsigned()->index();
            $table->foreign('visits_id')->references('id')->on('visits');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visits_general_info');
    }
}
