<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',500);
            $table->string('description',600);
            $table->string('type');
            $table->timestamp('startdate');
            $table->timestamp('enddate');
            $table->boolean('allday');
            $table->integer('visits_id')->unsigned()->index()->nullable();
            $table->foreign('visits_id')->references('id')->on('visits');
            $table->integer('personal_id')->unsigned()->index()->nullable();
            $table->foreign('personal_id')->references('id')->on('personal');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('process_id')->unsigned()->index()->nullable();
            $table->foreign('process_id')->references('id')->on('process');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
