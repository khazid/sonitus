<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->index();
            $table->foreign('client_id')->references('id')->on('client');
            $table->integer('process_id')->nullable();
            $table->foreign('process_id')->references('id')->on('process');
            $table->integer('status_id');
            $table->foreign('status_id')->references('id')->on('status');
            $table->string('description',1000);
            $table->string('response',1000);
            $table->string('type',30)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('new_request');
    }
}
