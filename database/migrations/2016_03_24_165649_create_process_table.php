<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process', function (Blueprint $table) {
            $table->increments('id');
            $table->string('process_number',20);
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('assigned_id')->nullable();
            $table->date('assigned_date')->nullable();
            $table->integer('status_id')->unsigned()->index();
            $table->foreign('status_id')->references('id')->on('status');
            $table->date('ini_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('description',350);
            $table->string('job_type',300);
            $table->boolean('web');
            $table->boolean('quality');
            $table->string('priority')->default('Baja');
            $table->string('signature')->nullable();
            $table->boolean('maintenance')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('process');
    }
}
