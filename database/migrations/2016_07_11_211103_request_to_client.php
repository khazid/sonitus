<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RequestToClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::create('request_to_client', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visits_id')->unsigned()->index();
            $table->foreign('visits_id')->references('id')->on('visits');
            $table->integer('status_id')->unsigned()->index();
            $table->foreign('status_id')->references('id')->on('status');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->string('request',1000);
            $table->string('observation',800)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('process_status');
    }
}
