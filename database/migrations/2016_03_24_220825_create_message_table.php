<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_id')->unsigned()->index();
            $table->foreign('from_id')->references('id')->on('users');
            $table->integer('to_id')->unsigned()->index();
            $table->foreign('to_id')->references('id')->on('users');
            $table->integer('status_id')->unsigned()->index();
            $table->string('message',500);
            $table->string('tittle',250);
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
