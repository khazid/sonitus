<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('personal_id')->unsigned()->index();
            $table->foreign('personal_id')->references('id')->on('personal');
            $table->integer('visits_id')->unsigned()->index();
            $table->foreign('visits_id')->references('id')->on('visits');
            $table->integer('users_id')->unsigned()->index();
            $table->foreign('users_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_process');
    }
}
