<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->nullable();
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->string('name',150);
            $table->string('rif',20);
            $table->string('address',350);
            $table->string('applicant_name',150);
            $table->string('charge',50);
            $table->string('departmen',150)->nullable();
            $table->string('headquarters',150)->nullable();
            $table->string('phone',50);
            $table->string('email',50);
            $table->integer('master_client')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client');
    }
}
