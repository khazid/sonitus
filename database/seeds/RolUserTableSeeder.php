<?php

use Illuminate\Database\Seeder;

class RolUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // role: Superuser user:Kamaly 
        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 1,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        // role: Administrador user:Administrator 
        DB::table('role_user')->insert([
            'role_id' => 2,
            'user_id' => 2,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        // role: Superuser user:David Valera 
        DB::table('role_user')->insert([
            'role_id' => 3,
            'user_id' => 3,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        // role: Superuser user:Ronald Bermudez 
         DB::table('role_user')->insert([
            'role_id' => 4,
            'user_id' => 4,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         // role: Superuser user:Web 
         DB::table('role_user')->insert([
            'role_id' => 7,
            'user_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         // role: Superuser user:Tecnico 1 
          DB::table('role_user')->insert([
            'role_id' => 5,
            'user_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);

         
      
    }
}
