<?php

use Illuminate\Database\Seeder;

class HolidaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('holidays')->insert(['date' => '2016-02-8','description' =>'Lunes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-02-9','description' =>'Martes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-03-24','description' =>'Jueves Santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-03-25','description' =>'Viernes Santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-05-01','description' =>'Dial del Trabajador','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-06-24','description' =>'Batalla de Carabobo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-07-05','description' =>'Dia de la Independencia','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-07-24','description' =>'Natalicio Simon Bolivar','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-10-12','description' =>'Día de la Resistencia Indígena','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2016-12-24','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);--navidad
DB::table('holidays')->insert(['date' => '2016-12-25','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);--navidad
DB::table('holidays')->insert(['date' => '2016-12-31','description' =>'Vispera de Ano Nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);--Año nuevo
DB::table('holidays')->insert(['date' => '2017-01-01','description' =>'Ano Nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);--año nuevo

DB::table('holidays')->insert(['date' => '2017-02-27','description' =>'Lunes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-02-28','description' =>'Martes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-04-13','description' =>'Jueves Santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-04-24','description' =>'Viernes Santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-05-01','description' =>'Dia del Trabajador','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-06-24','description' =>'Batalla de Carabobo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-07-05','description' =>'Dia de la independencia','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-07-24','description' =>'Natalicio Simon Bolivar','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-10-12','description' =>'Día de la Resistencia Indígena','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-12-24','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-12-25','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2017-12-31','description' =>'Vispera Ano Nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-01-01','description' =>'Ano Nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

DB::table('holidays')->insert(['date' => '2018-02-12','description' =>'Lunes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-02-13','description' =>'Martes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-03-29','description' =>'Jueves Santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-03-30','description' =>'Viernes Santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-05-01','description' =>'dia del trabajador','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-06-24','description' =>'Batalla de Carabobo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-07-05','description' =>'Dia de la independencia','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-07-24','description' =>'natalicio simon bolivar','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-10-12','description' =>'Día de la Resistencia Indígena','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-12-24','description' =>'navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-12-25','description' =>'navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2018-12-31','description' =>'Vispera de Ano Nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-01-01','description' =>'Ano nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

DB::table('holidays')->insert(['date' => '2019-02-4','description' =>'Lunes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-02-5','description' =>'Martes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-04-18','description' =>'Jueves santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-04-19','description' =>'Viernes santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-05-01','description' =>'Dia del trabajador','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-06-24','description' =>'Batalla de Carabobo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-07-05','description' =>'Dia de la independencia','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-07-24','description' =>'Natalicio Simon Bolivar','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-10-12','description' =>'Día de la Resistencia Indígena','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-12-24','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-12-25','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2019-12-31','description' =>'Vispera de Ano Nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-01-01','description' =>'Ano nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

DB::table('holidays')->insert(['date' => '2020-02-24','description' =>'Lunes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-02-25','description' =>'Martes Carnaval','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-04-09','description' =>'Jueves santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-04-10','description' =>'Viernes santo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-05-01','description' =>'Dia del trabajador','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-06-24','description' =>'Batalla de Carabobo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-07-05','description' =>'Dia de la independencia','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-07-24','description' =>'Natalicio Simon Bolivar','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-10-12','description' =>'Día de la Resistencia Indígena','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-12-24','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-12-25','description' =>'Navidad','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2020-12-31','description' =>'Vispera de Ano Nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('holidays')->insert(['date' => '2021-01-01','description' =>'Ano nuevo','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
    }
}
