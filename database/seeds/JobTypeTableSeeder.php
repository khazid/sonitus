<?php

use Illuminate\Database\Seeder;

class JobTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_type')->insert(['name' => 'SOPORTE TÉCNICO','description' => 'Trabajo de Soporte tecnico de equipos, instalacion de software, reparacion de equipos','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
		DB::table('job_type')->insert(['name' => 'INSTALACIÓN','description' => 'Instalacion de equipos nuevos','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
		DB::table('job_type')->insert(['name' => 'ADIESTRAMIENTO','description' => 'Adiestramiento de personal en la utilizacion y manejo de los equipos instalados','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
		DB::table('job_type')->insert(['name' => 'MANTENIMIENTO','description' => 'Mantenimiento de los equipos instalados en un trabajo previo de la empresa','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        DB::table('job_type')->insert(['name' => 'VISITA TECNICA','description' => 'Visita para la revision del lugar o equipos de trabajo donde se llevara a cabo el proceso','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
		
    }
}
