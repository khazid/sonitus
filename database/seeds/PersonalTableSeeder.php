<?php

use Illuminate\Database\Seeder;

class PersonalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('personal')->insert([
            'name' => 'Sistema',
            'address' => 'Web',
            'identity_number' => '1',
            'occupation' => 'Admin',
            'occupation_description' => 'sistema',
            'telephone' => '0',
            'email' => '-',
            'profile_pic' => 'user.png',
            'date_birth' => date('Y-m-d'),
            'initial_date' => date('Y-m-d'),
            'active' => TRUE,
            'users_id' => 5,
        ]);
           

        DB::table('personal')->insert([
            'name' => 'Administrador',
            'address' => 'Web',
            'identity_number' => '0',
            'occupation' => 'Administrador',
            'occupation_description' => 'Adminitrador del sistema',
            'telephone' => '0',
            'email' => 'admin@isonitus.com',
            'profile_pic' => 'user.png',
            'date_birth' => date('Y-m-d'),
            'initial_date' => date('Y-m-d'),
            'active' => TRUE,
            'users_id' => 2,
        ]);

        DB::table('personal')->insert([
            'name' => 'Kamaly Acevedo',
            'address' => 'Los teques, Edo. Miranda',
            'identity_number' => '16524801',
            'occupation' => 'Superuser',
            'occupation_description' => 'Super usario del sistema',
            'telephone' => '0',
            'email' => 'jose.sira@isonitus.com',
            'profile_pic' => 'user.png',
            'date_birth' => date('Y-m-d'),
            'initial_date' => date('Y-m-d'),
            'active' => TRUE,
            'users_id' => 1,

        ]);

        DB::table('personal')->insert([
            'name' => 'Ronald Bermudez',
            'address' => 'Caracas, Distrito Capital',
            'identity_number' => '19555669',
            'occupation' => 'Jefe Tecnico',
            'occupation_description' => 'Jefe tecnico de Inversiones Sonitus area tecnologia',
            'telephone' => '0',
            'email' => 'ronald.bermudez@isonitus.com',
            'profile_pic' => 'user.png',
            'date_birth' => date('Y-m-d'),
            'initial_date' => date('Y-m-d'),
            'active' => TRUE,
            'users_id' => 4,
        ]);

        DB::table('personal')->insert([
            'name' => 'David Valera',
            'address' => 'Caracas, Distrito Capital',
            'identity_number' => '12345679',
            'occupation' => 'Jefe',
            'occupation_description' => 'Jefe de Inversiones Sonitus',
            'telephone' => '0',
            'email' => 'david.valera@isonitus.com',
            'profile_pic' => 'user.png',
            'date_birth' => date('Y-m-d'),
            'initial_date' => date('Y-m-d'),
            'active' => TRUE,
            'users_id' => 3,
        ]);

        DB::table('personal')->insert([
            'name' => 'Tecnico 1',
            'address' => 'Caracas, Distrito Capital',
            'identity_number' => '15151515',
            'occupation' => 'Tecnico',
            'occupation_description' => 'Tecnico especialista en redes',
            'telephone' => '0',
            'email' => 'tecnico@isonitus.com',
            'profile_pic' => 'user.png',
            'date_birth' => date('Y-m-d'),
            'initial_date' => date('Y-m-d'),
            'active' => TRUE,
            'users_id' => 6,
        ]);

    }
}
