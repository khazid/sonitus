<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Status for Process
        DB::table('status')->insert(['name' => 'Nuevo','description' => 'Nueva solicitud generada por el sistema','type' =>1,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
        DB::table('status')->insert(['name' => 'Nuevo WEB','description' => 'Nueva solicitud generada por la pagina web','type' =>1,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
		
        DB::table('status')->insert(['name' => 'En proceso','description' => 'En proceso por parte del tecnico, no se ha terminado de cargar toda la informacion','type' =>1,'status_value' =>20,'status_value' =>1,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
		
        DB::table('status')->insert(['name' => 'Terminado','description' => 'proceso Terminado por el tecnico','type' =>1,'status_value' =>100,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
        DB::table('status')->insert(['name' => 'Cerrado','description' => 'proceso cerrado por el jefe despues de verificar la informacion del proceso terminado','type' =>1,'status_value' =>100,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
		
        DB::table('status')->insert(['name' => 'Asignado','description' => 'Proceso asignado a un tecnico','type' =>1,'status_value' =>10,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        DB::table('status')->insert(['name' => 'Espera por el cliente','description' => 'En espera por el cliente para poder continuar con el proceso','type' =>1,'status_value' =>30,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        DB::table('status')->insert(['name' => 'Espera de Visita','description' => 'Trabajo terminado por parte del cliente en espera del visita del tecnico para continuar el proceso','type' =>1,'status_value' =>50,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        DB::table('status')->insert(['name' => 'Continuacion del proceso','description' => 'Se continua el proceso despues de realizada la solicitud de adecuacion al cliente','type' =>1,'status_value' =>60,'status_value' =>1,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        //status for Messages
        DB::table('status')->insert(['name' => 'read','description' => 'Mensaje Leido ','type' =>2,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
        DB::table('status')->insert(['name' => 'new','description' => 'Mensaje no leido','type' =>2,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        
        DB::table('status')->insert(['name' => 'answered','description' => 'Mensaje Respondido por el destinatario','type' =>2,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
        DB::table('status')->insert(['name' => 'deleted','description' => 'Mensaje borrado','type' =>2,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        
        //Status for Request_to_client

        DB::table('status')->insert(['name' => 'New','description' => 'Nueva solicitud al cliente por el tecnico','type' =>3,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Approved','description' => 'Solicitud Aprobada y enviada al cliente','type' =>3,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Rejected','description' => 'Solicitud Rechazada','type' =>3,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Done','description' => 'Solicitud Realizada por el cliente','type' =>3,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);

        //Status for Visits

        DB::table('status')->insert(['name' => 'New','description' => 'New visit added','type' =>4,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Loading','description' => 'Loading visit info','type' =>4,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Not made','description' => 'the visits wasnt made it','type' =>4,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Done','description' => 'Visit Done','type' =>4,'status_value' =>0,'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);

        //status for Web Request
        
        DB::table('status')->insert(['name' => 'Nuevo','description' => 'Nueva solicitud web','type' =>5,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Respuesta enviada','description' => 'Respuesta enviada al cliente','type' =>5,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Cotizacion enviada','description' => 'Cotizacion enviada al cliente','type' =>5,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Nuevo proceso','description' => 'Se crea nuevo proceso en base a la solicitud del cliente','type' =>5,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Visita Mantenimiento' => 'Se crea nueva visita por mantenimiento','type' =>5,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('status')->insert(['name' => 'Visita Soporte' => 'Se crea nueva visita por soporte','type' =>5,'status_value' =>0,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);





    }
}
