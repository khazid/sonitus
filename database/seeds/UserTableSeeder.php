<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   //1
       DB::table('users')->insert([
            'name' => 'Kamaly Acevedo',
            'email' => 'kamalyacevedo@gmail.com',
            'password' => bcrypt('123'),
            'created_at' => date('Y-m-d'),
            'active' => TRUE,
            'updated_at' => date('Y-m-d'),
            
        ]);
       //2
       DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@isonitus.com',
            'password' => bcrypt('123'),
            'created_at' => date('Y-m-d'),
            'active' => TRUE,
            'updated_at' => date('Y-m-d'),
            
        ]);
       //3
       DB::table('users')->insert([
            'name' => 'David Valera',
            'email' => 'david.valera@isonitus.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
            
        ]);
       //4
       DB::table('users')->insert([
            'name' => 'Ronald Bermudez',
            'email' => 'ronald.bermudez@isonitus.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
            
        ]);
       
        //5
        DB::table('users')->insert([
            'name' => 'Web',
            'email' => 'x',
            'password' => bcrypt('x404'),
            'active' => TRUE,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
           
        ]);

        //6
        DB::table('users')->insert([
            'name' => 'Tecnico 1',
            'email' => 'tecnico@isonitus.com',
            'password' => bcrypt('x404'),
            'active' => TRUE,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
           
        ]);
       

      
    }
}
