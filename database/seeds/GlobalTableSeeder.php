<?php

use Illuminate\Database\Seeder;

class GlobalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
    	$this->call('UserTableSeeder');
        $this->call('PersonalTableSeeder');
        $this->call('RolTableSeeder');
    	$this->call('PermissionTableSeeder');
        $this->call('MenuTableSeeder');
        $this->call('MenuRolTableSeeder');
        $this->call('PermissionRolTableSeeder');
        $this->call('RolUserTableSeeder');
        $this->call('CountrySeeder');
        $this->call('GeneralTypeTableSeeder');
        $this->call('GeneralInfoTableSeeder');
        $this->call('JobTypeTableSeeder');
        $this->call('StatusTableSeeder');
        
        
    }
}
