<?php

use Illuminate\Database\Seeder;

class GeneralTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('general_type')->insert(['name' => 'No Enciende','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Se Reinicia','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Mensaje de Error','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Bloqueo / Lentitud','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Revisión y / o Mantenimiento','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Instalar Programa','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Reinstalar Programa','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Virus','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Daño Físico','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Redes','type' => 'Asistencia Técnica / Computación','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);


DB::table('general_type')->insert(['name' => 'Televisor','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Proyector','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Pantalla','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Microfono','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Video Conferencia','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Altoparlante en Techo','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Altoparlante en Pared','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Platina de Pared','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Cables HDMI','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Cables VGA','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Base de Proyector','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Consola','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Equipo de Audio','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Fuente de Audio','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Fuente de Video','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Rack Metálico','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Rack Madera','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Seguridad','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Teclado Inalámbrico','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Mouse Inalámbrico','type' => 'EQUIPOS ','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);


DB::table('general_type')->insert(['name' => 'Tabiquería','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Canalización de Audio','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Cableado de Audio','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Canalización de Video','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Cableado de Video','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Piso','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Electricidad','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Albañilería','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Cielo Raso / Drywall','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Pintura','type' => 'REMODELACIÓN','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);


DB::table('general_type')->insert(['name' => 'Automatización','type' => 'INSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Equipo de Audio','type' => 'INSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Equipo de Video','type' => 'INSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Equipo de Computación / Redes','type' => 'INSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);


DB::table('general_type')->insert(['name' => 'Automatización','type' => 'REINSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Equipo de Audio','type' => 'REINSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Equipo de Video','type' => 'REINSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);
DB::table('general_type')->insert(['name' => 'Equipo de Computación / Redes','type' => 'REINSTALAR','created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),]);

    }
}
