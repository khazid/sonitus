<?php

use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	//1
     	DB::table('roles')->insert([
			'name' => 'Superuser',
			'slug' => 'Superuser',
			'description' => 'superusuario del sistema', // optional
			'level' => 1, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);
		//2
     	DB::table('roles')->insert([
			'name' => 'Administrador',
			'slug' => 'Admin',
			'description' => 'Administrador del sistema', // optional
			'level' => 2, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);
     	//3
		DB::table('roles')->insert([
			'name' => 'Jefe',
			'slug' => 'Jefe',
			'description' => 'Jefe', // optional
			'level' => 3, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);
		//4
		DB::table('roles')->insert([
			'name' => 'Jefe tecnico',
			'slug' => 'Jefe tecnico',
			'description' => 'Jefe de tecnicos', // optional
			'level' => 4, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);
		//5
		DB::table('roles')->insert([
			'name' => 'Tecnico',
			'slug' => 'Tecnico',
			'description' => 'Personal Tecnico', // optional
			'level' => 5, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);
		//6
		DB::table('roles')->insert([
			'name' => 'Administrativo',
			'slug' => 'Administrativo',
			'description' => 'Personal Administrativo', // optional
			'level' => 6, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);
		//7
		DB::table('roles')->insert([
			'name' => 'web',
			'slug' => 'web',
			'description' => 'web', // optional
			'level' => 7, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);
		//8
		DB::table('roles')->insert([
			'name' => 'Client',
			'slug' => 'Client',
			'description' => 'client', // optional
			'level' => 8, // optional, set to 1 by default
			'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),
		]);

		
    }
}
