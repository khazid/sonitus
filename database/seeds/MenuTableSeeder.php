<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([
            'name' => 'Reports',
            'position' => 1,
            'icon' => 'fa fa-area-chart',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Staff',
            'position' => 2,
            'icon' => 'fa fa-users',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Clients',
            'position' => 3,
            'icon' => 'fa fa-users',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Inbox',
            'position' => 4,
            'icon' => 'fa fa-envelope',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Process',
            'position' => 5,
            'icon' => 'glyphicon glyphicon-cog',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       
        DB::table('menu')->insert([
            'name' => 'Configuration',
            'position' => 10,
            'icon' => 'glyphicon glyphicon-wrench',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);

        DB::table('menu')->insert([
            'name' => 'Visits',
            'position' => 6,
            'icon' => 'fa fa-cogs',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Request',
            'position' => 7,
            'icon' => 'glyphicon glyphicon-hand-up',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
