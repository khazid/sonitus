<?php

use Illuminate\Database\Seeder;

class MenuRolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Superuser
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 1,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 2,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 3,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 4,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 5,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 6,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 7,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        //Administrator
        DB::table('menu_role')->insert(['role_id' => 2,'menu_id' => 1,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 2,'menu_id' => 2,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 2,'menu_id' => 3,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 2,'menu_id' => 4,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 2,'menu_id' => 5,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 2,'menu_id' => 6,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 2,'menu_id' => 7,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        //Jefe
        DB::table('menu_role')->insert(['role_id' => 3,'menu_id' => 1,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 3,'menu_id' => 2,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 3,'menu_id' => 3,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 3,'menu_id' => 4,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 3,'menu_id' => 5,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 3,'menu_id' => 6,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 3,'menu_id' => 7,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        //Jefe Tecnico
        DB::table('menu_role')->insert(['role_id' => 4,'menu_id' => 1,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 4,'menu_id' => 2,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 4,'menu_id' => 3,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 4,'menu_id' => 4,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 4,'menu_id' => 5,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 4,'menu_id' => 6,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 4,'menu_id' => 7,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
        //Tecnico
        DB::table('menu_role')->insert(['role_id' => 5,'menu_id' => 1,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 5,'menu_id' => 2,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 5,'menu_id' => 3,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 5,'menu_id' => 4,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 5,'menu_id' => 5,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 5,'menu_id' => 6,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 5,'menu_id' => 7,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        //Administrativo
        DB::table('menu_role')->insert(['role_id' => 6,'menu_id' => 1,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 6,'menu_id' => 2,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 6,'menu_id' => 3,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 6,'menu_id' => 4,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 6,'menu_id' => 5,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 6,'menu_id' => 6,'active' => FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 6,'menu_id' => 7,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
    }
}
