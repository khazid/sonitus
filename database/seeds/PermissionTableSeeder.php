<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //1
        DB::table('permissions')->insert([
            'name' => 'Menu',
            'slug' => 'menu.index',
            'description' => 'Principal menu of the application',
            'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        //2
        DB::table('permissions')->insert([
            'name' => 'Roles',
            'slug' => 'rol.index',
            'description' => 'Sistem roles',
             'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        //3
        DB::table('permissions')->insert([
            'name' => 'Sub-Menu',
            'slug' => 'perm.index',
            'description' => 'sub menu options',
             'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        //4
        DB::table('permissions')->insert([
            'name' => 'Staff list',
            'slug' => 'personal.index',
            'description' => 'List of teh staff members',
            'menu_id' => 2,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        //5
        DB::table('permissions')->insert([
            'name' => 'Process List',
            'slug' => 'process.index',
            'description' => 'List of the projects',
            'menu_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        //6
        DB::table('permissions')->insert([
            'name' => 'New Process',
            'slug' => 'process.create',
            'description' => 'create a new project ',
            'menu_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        //7
        DB::table('permissions')->insert([
            'name' => 'WEB Request',
            'slug' => 'process/webprocess',
            'description' => 'Request made by the web page',
            'menu_id' => 3,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        //8
        DB::table('permissions')->insert([
            'name' => 'Inbox',
            'slug' => 'mess.index',
            'description' => 'messages inbox',
            'menu_id' => 4 ,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        
        //9
         DB::table('permissions')->insert([
            'name' => 'Reports charts',
            'slug' => 'report.index',
            'description' => 'Report charts examples',
            'menu_id' => 1,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         //10
         DB::table('permissions')->insert([
            'name' => 'Request to clients',
            'slug' => 'client/newrequest',
            'description' => 'Request that are made by tecnicians to the client to make instalations in the area',
            'menu_id' => 3,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         //11
         DB::table('permissions')->insert([
            'name' => 'Deleted Staff',
            'slug' => 'personal/searchdeleted',
            'description' => 'list of the deleted staff members',
            'menu_id' => 2,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         //12
         DB::table('permissions')->insert([
            'name' => 'Client list',
            'slug' => 'client.index',
            'description' => 'clients list',
            'menu_id' => 3,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         //13
         DB::table('permissions')->insert([
            'name' => 'Visit calendar',
            'slug' => 'visits/calendar',
            'description' => 'visits Schedule',
            'menu_id' => 7,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         //14
         DB::table('permissions')->insert([
            'name' => 'Visit list',
            'slug' => 'visits.index',
            'description' => 'visits list',
            'menu_id' => 7,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);

        //15
         DB::table('permissions')->insert([
            'name' => 'Deleted Clients',
            'slug' => 'client/searchdeleted',
            'description' => 'Deleted Clients',
            'menu_id' => 3,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
          //16
         DB::table('permissions')->insert([
            'name' => 'List',
            'slug' => 'report.create',
            'description' => 'Listas de reportes',
            'menu_id' => 1,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         //17
         DB::table('permissions')->insert([
            'name' => 'Quote List',
            'slug' => 'quote.index',
            'description' => 'Quote List',
            'menu_id' => 8,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
          //18
         DB::table('permissions')->insert([
            'name' => 'Internal Request',
            'slug' => '#',
            'description' => 'Solicitudes Internas',
            'menu_id' => 8,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         //19
         DB::table('permissions')->insert([
            'name' => 'New Request',
            'slug' => '#',
            'description' => 'Nueva Solicitudes Internas',
            'menu_id' => 8,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
  
       
    }
}
