<?php

use Illuminate\Database\Seeder;

class GeneralInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('general_info')->insert([
            'name' => 'INSTALACIÓN NUEVA',
            'description' => 'Instalacion de equipos nuevos en el lugar',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('general_info')->insert([
            'name' => 'REINSTALAR',
            'description' => 'Reinstalacion de equipos en el lugar debido a falla o actualizacion del mismo',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('general_info')->insert([
            'name' => 'COMPUTACIÓN Y REDES',
            'description' => 'Trabajo relacionado a instalacion configuracion o adaptacion de equipos de computacion, desktops, laptops, cableado de red, etc.',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('general_info')->insert([
            'name' => 'ELECTROACÚSTICO',
            'description' => 'Trabajo relacionado a la instalacion, configuracion o adaptacion de equipos electroacusticos',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('general_info')->insert([
            'name' => 'OBRA CIVIL',
            'description' => 'Trabajo realcionado a la instalacion o construccion de salas, cuartos, espacios publicos etc.',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
