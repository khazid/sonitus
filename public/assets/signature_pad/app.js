 $(document).ready(function() {
var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = wrapper.querySelector("[data-action=save]"),
    canvas = wrapper.querySelector("canvas"),
    visitid = $("#visitid").val(),
    processid = $("#processid").val(),
    $type=$("#type").val(),
    signaturePad;
    //console.log("signaturePad visit="+visitid+' / process: '+processid);

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad = new SignaturePad(canvas);

clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

saveButton.addEventListener("click", function (event) {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
    } else {
         //console.log("signaturePad",signaturePad.toDataURL());
         signurl=signaturePad.toDataURL();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

         if($type=='visit'){
            $sigurl=APP_URL+"/savesignature";
         }else{
            $sigurl=APP_URL+"/process/savesignature";
         } 
         //console.log('info send signature' + signurl+'visitid'+visitid+'processid'+processid) ;
        $.ajax({
            type: 'POST', // Type of response and matches what we said in the route
            url: $sigurl, // This is the url we gave in the route
            data: {'signature' : signurl,'visitid':visitid,'processid':processid}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
               //console.log("signaturePad",response);

               location.href = response.urlresponse;
            }
        }); 
        
    }
    //window.open(signaturePad.toDataURL());
});
});
