$(document).ready(function() {
  var ul="";
  var APP_URL = {!! json_encode(url('/')) !!};
    // $('#dataTables-visits').DataTable({
    //             "responsive": true,
    //             "order": [[ 5, "desc" ]]
    // });
   $('#datatable-buttons tbody').on('click', '.foto', function () { 
    var idvisit=$(this).attr('data-id');
    var picurl = APP_URL+"/buscarphotos";
    var fileurl = APP_URL+"/images/equipment/";
    //alert(idvisit);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

       
    $.ajax({
        type: 'POST', // Type of response and matches what we said in the route
        url: picurl, // This is the url we gave in the route
        data: {'idvisit' : idvisit}, // a JSON object to send back
        success: function(response){ // What to do if we succeed
        console.log("Visita: " +idvisit);
        console.log("Photo: " +response.photo);
        console.log("Bool: " +response.bool);
            
            if(response.bool){
            $.each(response.photo, function(i, item) {
            ul +='<div class="thumbnail"><img src="'+fileurl+'/'+response.photo[i]["process_id"]+'/'+idvisit+'/'+response.photo[i]["file"]+'" alt="'+response.photo[i]["description"]+'"><div class="caption"><h3>Descripción:</h3> <p>'+ response.photo[i]["description"]+'</p></div></div>';
            
            });
            
            $.confirm({
                columnClass: 'col-md-8 col-md-offset-3',
                title: 'Fotos Capturadas en la visita # '+idvisit,
                content: ul,
                confirmButton: false,
                cancelButton: false,
                closeIcon: true,
                onClose: function(){
                    ul="";
                }
                
            });

           }else{
                 //console.log("historias null: " +response.historias);
                 $.alert({
                  icon: 'fa fa-warning',
                  title:'La visita # '+idvisit,
                  content: 'No tiene fotos guardadas',
               });

            }
             
          }
    });  
    
  });

   $('#datatable-buttons tbody').on('click', '.solicitud', function () {
     var idvisit=$(this).attr('data-id');
     var requesturl = APP_URL+"/buscarsolicitudes";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

       
    $.ajax({
        method: 'POST', // Type of response and matches what we said in the route
        url: requesturl, // This is the url we gave in the route
        data: {'idvisit' : idvisit}, // a JSON object to send back
        success: function(response){ // What to do if we succeed
        // console.log("Visita: " +idvisit);
        // console.log("Estado: " +response.estado);
        // console.log("Request: " +response.request);
        // console.log("Bool: " +response.bool);
            
            if(response.bool){
            $.confirm({
                columnClass: 'col-md-8 col-md-offset-3',
                title: '<b>Status de la solicitud de la visita #'+idvisit+':</b> <span class="label label-warning">'+response.estado+'</span>',
                content: response.request,
                confirmButton: false,
                cancelButton: false,
                closeIcon: true
                
            });
            
            }else{
             $.alert({
                  icon: 'fa fa-warning',
                  title:'La visita # '+idvisit,
                  content: 'No tiene solicitudes para el cliente',
               });
            }

           }
    });  
    
  });

        

});