$(document).ready(function() {
 
        $('.showjob').click(function(e) {
            var contenido= $(this).attr("data-content");
            var cliente= $(this).attr("data-client");

            $.alert({
                columnClass: 'col-md-6 col-md-offset-3',
                title: 'Trabajo a realizar al cliente "'+cliente+'"',
                content: contenido,
            });
        });

        $('.message').click(function(e) {
            var contenido= $(this).attr("data-content");
            var cliente= $(this).attr("data-client");
            var processid= $(this).attr("id");
            var titulo = "Proceso #: "+contenido+" Cliente: "+cliente;

            $('#tittle').val(titulo);  
            $('#process_id').val(processid);
            $('#formmessage').attr('action',APP_URL+'/messageprocess');  
            
        });

        $('.endtask').click(function(e) {
            var idprocess= $(this).attr("id");
            var contenido= $(this).attr("data-content");
            var cliente= $(this).attr("data-client");
            var proceso= $(this).attr("data-process");
            var procurl = APP_URL+"/endtask";
            var status=4;
            $.confirm({
                            title: '<strong>Cerrar proceso!!</strong>',
                            content: 'Seguro que desea marcar como Terminado: </br> <b>Proceso # '+proceso+'</br> Cliente: '+cliente+'</b> al cerrar el proceso el mismo sera enviado al jefe tecnico para su revision.',
                            confirmButton:'Si',
                            cancelButton:'No',
                            confirm: function(){
                               $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: procurl, // This is the url we gave in the route
                                        data: {'idprocess' : idprocess,'status':status}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                            //console.log('respuesta ',response);
                                            location.href=response.urlresponse;
                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });


    });