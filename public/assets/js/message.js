 $(document).ready(function() {
      
      $("#messageModal").on("shown.bs.modal", function (e) {
              var button = $(e.relatedTarget); // Botón que activó el modal
              console.log(button.data('id'));
              
              if(button.data('id')=='readmess')
              {   
                            var contenido= button.data('content');
                            var titulo= button.data("tittle");
                            var frommess= button.data("name");
                            var idmessage=button.data("idmess");
                            var idstatus=button.data("status");
                            var status=10;
                            var route="message";
              
                            $('#tittle').val(titulo);  
                            $('#messagetext').val(contenido); 
                            $('#frommess').val(frommess);
                            $('#frommess').show();
                            $('#addressee').hide();
                            $('#Send').hide();
                            $('#messagetext').attr('readonly','readonly');
                            $('#tittle').attr('readonly','readonly');
                            $('#recipientname').text('Remitente');
                            $('#formmessage').attr('action',APP_URL+'/messages/send'); 

                      if(idstatus==11)
                      {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
      
                               
                            $.ajax({
                                type: 'POST', // Type of response and matches what we said in the route
                                url: APP_URL+"/changestatus", // This is the url we gave in the route
                                data: {'idmessage' : idmessage,'idstatus' : status, 'route' : route}, // a JSON object to send back
                                success: function(response){ // What to do if we succeed
                                   // console.log("success changing status message");
                                   $('#dataTables-message tbody tr').attr('class','read'); 
                                }
                            });  
                      }
               }else{
                            $('#tittle').val('');  
                            $('#messagetext').val(''); 
                            $('#frommess').val('');
                            $('#frommess').hide();
                            $('#addressee').show();
                            $('#Send').show();
                            $('#messagetext').removeAttr('readonly');
                            $('#tittle').removeAttr('readonly');
                            $('#recipientname').text('Destinatario');
                }

          });//messageModal
          // $("#messageModal").on("hide.bs.modal", function (e) {
          //   location.reload();
          //  });//messageModal

          

          $('.new').click(function(){
              location.reload();
          });//.new

          $('.deleted').click(function(){
              var ul="";

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                   
                $.ajax({
                    type: 'POST', // Type of response and matches what we said in the route
                    url: APP_URL+"/deleted", // This is the url we gave in the route
                    data: {}, // a JSON object to send back
                    success: function(response){ // What to do if we succeed
                        console.log("success deleted messages",response.message);
                        if(response.message){
                        $.each(response.message, function(i, item) {
                        ul+='<tr><td><input type="checkbox" class="checkbox" name="" id="'+response.message[i]["id"]+'"/></td><td>'+response.message[i]["id"]+'</td><td>'+response.message[i]["name"]+'</td><td><a href="#" data-id="readmess" class="showmess" data-toggle="modal" data-target="#messageModal" data-name="'+response.message[i]["name"]+'" data-content="'+response.message[i]["message"]+'" data-idmess="'+response.message[i]["id"]+'" data-tittle="'+response.message[i]["tittle"]+'" data-status="'+response.message[i]["status_id"]+'">'+response.message[i]["tittle"]+'</a></td><td>'+response.message[i]["created_at"]+'</td></tr>';
                          $('#dataTables-message tbody tr').remove();
                          $('#dataTables-message tbody ').append(ul);
                        });
                       }
                     }
                });  
          });//.read

          if($('.delete').length > 0) {
              $('.delete').click(function(){
                  var c = false;
                  var cn = 0;
                  var o = new Array();
                  var objid=new Array();
                  $('.mailinbox input:checkbox').each(function(){
                      if($(this).is(':checked')) {
                          c = true;
                          o[cn] = $(this);
                          objid[cn]=$(this).attr('id');
                          cn++;
                      }
                  });
                  
                  if(!c) {
                      alert('No selected message'); 
                  } else {
                      var msg = (o.length > 1)? 'messages' : 'message';
                      var route="message";
                      if(confirm('Delete '+o.length+' '+msg+'?')) {
                          for(var a=0;a<cn;a++) {
                              $(o[a]).parents('tr').remove(); 
                          }
                          
                          //console.log("Objetos delete:",objid);
                          $.ajaxSetup({
                          headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          }
                          });

                             
                          $.ajax({
                              type: 'POST', // Type of response and matches what we said in the route
                              url: APP_URL+"/delete", // This is the url we gave in the route
                              data: {'objetos' : objid,'route' : route}, // a JSON object to send back
                              success: function(response){ // What to do if we succeed
                                 // console.log("Success delete: ");

                              }//success
                          });//ajax


                      }//confirm
                  }

              });//.delete
          }// if .delete

           // check all checkboxes in table
        if($('.checkall').length > 0) {

            $('.checkall').click(function(){
                var parentTable = $(this).parents('table');                                           
                var ch = parentTable.find('.checkbox');                                      
                if($(this).is(':checked')) {
                 
                    //check all rows in table
                    ch.each(function(){ 
                        $(this).attr('checked',true);
                        $(this).parent().addClass('checked');  //used for the custom checkbox style
                        $(this).parents('tr').addClass('selected'); // to highlight row as selected
                    });

                } else {
                    
                    //uncheck all rows in table
                    ch.each(function(){ 
                        $(this).attr('checked',false); 
                        $(this).parent().removeClass('checked');   //used for the custom checkbox style
                        $(this).parents('tr').removeClass('selected');
                    }); 
                    
                }
            });
        }
      });
    

   



        //add class selected to table row when checked
        jQuery('.mailinbox tbody input:checkbox').click(function(){
            if(jQuery(this).is(':checked'))
                jQuery(this).parents('tr').addClass('selected');
            else
                jQuery(this).parents('tr').removeClass('selected');
        });
        
        // trash

        
        
        // mark as read
jQuery('.mark_read').click(function(){
    var c = false;
    var cn = 0;
    var o = new Array();
    jQuery('.mailinbox input:checkbox').each(function(){
        if(jQuery(this).is(':checked')) {
            c = true;
            o[cn] = jQuery(this);
            cn++;
        }
    });
    if(!c) {
        alert('No selected message');   
    } else {
        var msg = (o.length > 1)? 'messages' : 'message';
        if(confirm('Mark '+o.length+' '+msg+' to read')) {
            for(var a=0;a<cn;a++) {
                jQuery(o[a]).parents('tr').removeClass('unread');
            }
        }
    }
});
            
       // make messages to unread
jQuery('.mark_unread').click(function(){
      var c = false;
      var cn = 0;
      var o = new Array();
      jQuery('.mailinbox input:checkbox').each(function(){
          if(jQuery(this).is(':checked')) {
              c = true;
              o[cn] = jQuery(this);
              cn++;
          }
      });
      if(!c) {
          alert('No selected message'); 
      } else {
          var msg = (o.length > 1)? 'messages' : 'message';
          if(confirm('Mark '+o.length+' '+msg+' to unread')) {
              for(var a=0;a<cn;a++) {
                  jQuery(o[a]).parents('tr').addClass('unread');
              }
          }
      }
});