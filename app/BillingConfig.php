<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillingConfig extends Model
{
     use SoftDeletes;
    protected $table = 'billing_config';
    protected $guarded = ['id']; 
  	protected $fillable = ['daily_salary', 'month_salary', 'biweekly_salary','iva','workday','night_hour_start','night_hour_end','extra_hour_start','extra_hour_end'];
  	protected $dates = ['deleted_at'];
}
