<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Picture extends Model
{
    use SoftDeletes;
    protected $table = 'picture';
    protected $guarded = ['id']; 
  	protected $fillable = ['file'];
  	protected $dates = ['deleted_at'];

  	public function visits()
    {
        return $this->belongsToMany('App\Visits', 'visits_picture','picture_id','visits_id');
    }
}
