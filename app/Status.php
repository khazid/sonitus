<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use SoftDeletes;
    protected $table = 'status';
    protected $guarded = ['id']; 
  	protected $fillable = ['name','description'];
  	protected $dates = ['deleted_at'];

  	public function process()
    {
        return $this->belongsToMany('App\Process');
    }

    public function quote()
    {
        return $this->belongsToMany('App\Quote');
    }
}
