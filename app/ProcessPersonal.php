<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcessPersonal extends Model
{
     use SoftDeletes;
    protected $table = 'personal_process';
    protected $guarded = ['id']; 
  	protected $fillable = ['process_id','personal_id'];
  	protected $dates = ['deleted_at'];
}
