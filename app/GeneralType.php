<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralType extends Model
{
    use SoftDeletes;
    protected $table = 'general_type';
    protected $guarded = ['id']; 
  	protected $fillable = ['name', 'type'];
  	protected $dates = ['deleted_at'];

  	public function visits()
    {
        return $this->belongsToMany('App\Visits', 'visits_general_type','general_type_id','visits_id');
    }
}
