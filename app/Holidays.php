<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holidays extends Model
{
    use SoftDeletes;
    protected $table = 'holidays';
    protected $guarded = ['id']; 
  	protected $fillable = ['date', 'description'];
  	protected $dates = ['deleted_at'];
}
