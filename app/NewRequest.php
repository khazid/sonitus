<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewRequest extends Model
{
    use SoftDeletes;
    protected $table = 'new_request';
    protected $guarded = ['id']; 
  	protected $fillable = ['description','process_id','type','client_id','status_id'];
  	protected $dates = ['deleted_at'];
}
