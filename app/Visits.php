<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visits extends Model
{
    use SoftDeletes;
    protected $table = 'visits';
    protected $guarded = ['id']; 
  	protected $fillable = ['description','date'];
  	protected $dates = ['deleted_at'];

  	 public function jobs()
    {
        return $this->belongsToMany('App\Jobtype', 'visits_job_type','visits_id','job_type_id');
    }

    public function generalinfo()
    {
        return $this->belongsToMany('App\GeneralInfo', 'visits_general_info','visits_id','general_id');
    }

    public function generaltype()
    {
        return $this->belongsToMany('App\GeneralType', 'visits_general_type','visits_id','general_type_id');
    }

    public function pictures()
    {
        return $this->belongsToMany('App\Picture', 'visits_picture','visits_id','picture_id');
    }

    public function requestclient()
    {
        return $this->hasOne('App\RequestClient');
    }
    public function personal()
    {
        return $this->belongsToMany('App\Personal', 'personal_visits','visits_id','personal_id');
    }

}
