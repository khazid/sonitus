<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralInfo extends Model
{
    use SoftDeletes;
    protected $table = 'general_info';
    protected $guarded = ['id']; 
  	protected $fillable = ['name', 'description'];
  	protected $dates = ['deleted_at'];

  	 public function visits()
    {
        return $this->belongsToMany('App\Visits', 'visits_general_info','general_id','visits_id');
    }

  	
}
