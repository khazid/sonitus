<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Process extends Model
{
    use SoftDeletes;
    protected $table = 'process';
    protected $guarded = ['id']; 
  	protected $fillable = ['process_number', 'ini_date', 'end_date','description','jobtype'];
  	protected $dates = ['deleted_at'];

  	public function client()
    {
        return $this->belongsToMany('App\Client', 'client_process','process_id','client_id');
    }
    public function message()
    {
        return $this->belongsToMany('App\Messages', 'process_messages','process_id','messages_id');
    }

    public function status()
    {
        return $this->belongsToMany('App\Status', 'process_status','process_id','status_id');
    }

   

    
}
