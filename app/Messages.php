<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Messages extends Model
{
    use SoftDeletes;
    protected $table = 'messages';
  	protected $guarded = ['id'];
  	protected $fillable = ['from_id','to_id','message_status_id','tittle','message'];
  	protected $dates = ['deleted_at'];

  	public function process()
    {
        return $this->belongsToMany('App\Process', 'process_messages','process_id','messages_id');
    }
}
