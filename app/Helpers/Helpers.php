<?php 
namespace App\Helpers;
use Mail;
use App\User;
function sendmail($info,$name,$email,$subject,$path)
    {   
    	$view='emails.plantilla';
       	$chief=User::join('role_user','user_id','=','users.id')
                     ->select('email')
                     ->whereRaw('role_id',['3','4'])
                     ->get();
          foreach($chief as $c){
              $address+=$c->email.',';
          }           
       //se envia el email
             $mail= Mail::send($view, ['info' => $info ], function ($m) use ($name,$email,$subject) {
                $m->from(env('SENDER_ADDRES'), 'Informacion Sonitus');
                $m->to($email, $name)->subject($subject);
                $m->bcc($address, 'chief');
                if($path!='')$m->attach($path);

               
            });
       return $mail;

    }