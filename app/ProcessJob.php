<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcessJob extends Model
{
   use SoftDeletes;
    protected $table = 'process_job';
    protected $guarded = ['id']; 
  	protected $fillable = ['process_id','job_id'];
  	protected $dates = ['deleted_at'];
}
