<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quality extends Model
{
    use SoftDeletes;
    protected $table = 'quality';
    protected $guarded = ['id']; 
  	protected $fillable = ['punctuality', 'attitude', 'attention','consultation','professionalism','job_quality','equipment_quality','contributions','knowledge','evaluation','administrative','work_with','work_reason','service_satisfaction','less_satisfaction','process_id'];
  	protected $dates = ['deleted_at'];
}
