<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuoteStatus extends Model
{
    use SoftDeletes;
    protected $table = 'quote_status';
    protected $guarded = ['id']; 
  	protected $fillable = ['quote_id', 'status_id', 'observation','users_id'];
  	protected $dates = ['deleted_at'];
}
