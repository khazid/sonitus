<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quote extends Model
{
     use SoftDeletes;
    protected $table = 'quote';
    protected $guarded = ['id']; 
  	protected $fillable = ['client_id', 'status_id', 'new_request_id','pay_type','send','currency','tecnicians','days','travel','night_work','night_work_hours','night_work_price','transportation','viaticum','lodgment','total','observation'];
  	protected $dates = ['deleted_at'];

  	public function status()
    {
        return $this->belongsToMany('App\Status', 'quote_status','quote_id','status_id');
    }
}
