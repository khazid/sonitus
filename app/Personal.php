<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Personal extends Model
{
     use SoftDeletes;
    protected $table = 'personal';
    protected $guarded = ['id','users_id']; 
  	protected $fillable = ['first_name', 'last_name', 'slug','active','local_number','cel_number','identity_number','users_id'];
  	protected $dates = ['deleted_at'];

  	public function role_user(){
		return $this->belongsTo('Role_user', 'user_id');
	}

	public function user(){
		return $this->hasOne('App\User', 'id', 'users_id');
	}

	public function visit(){
		 return $this->belongsToMany('App\Visits', 'personal_visits','personal_id','visits_id');
	}


	public function scopeInactive($query) 
	{
		return $this->withActive($query, false);
	}

	public function scopeActive($query) 
	{
		return $this->withActive($query, true);
	}

	public function scopeWithIdentityNumber($query, $identity) 
	{
		return $this->where('identity_number','=', $identity);
	}

	private function withActive($query, $active)
	{
		return $query->where('active','=', $active);
	}
}
