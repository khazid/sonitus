<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermRol extends Model
{
    use SoftDeletes;
    protected $table = 'permission_role';
  	protected $guarded = ['id'];
  	protected $dates = ['deleted_at'];
  	
}
