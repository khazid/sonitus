<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobType extends Model
{
    use SoftDeletes;
    protected $table = 'job_type';
    protected $guarded = ['id']; 
  	protected $fillable = ['name','description'];
  	protected $dates = ['deleted_at'];

  	 public function visits()
    {
        return $this->belongsToMany('App\Visits', 'visits_job_type','job_type_id','visits_id');
    }

    
}
