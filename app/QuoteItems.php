<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuoteItems extends Model
{
    use SoftDeletes;
    protected $table = 'quote_items';
    protected $guarded = ['id']; 
  	protected $fillable = ['quote_id', 'description', 'quantity','unit_price','total'];
  	protected $dates = ['deleted_at'];
}
