<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcessStatus extends Model
{
    use SoftDeletes;
    protected $table = 'process_status';
    protected $guarded = ['id']; 
  	protected $fillable = ['process_id', 'status_id', 'observation','users_id'];
  	protected $dates = ['deleted_at'];
}
