<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestClient extends Model
{
   use SoftDeletes;
    protected $table = 'request_to_client';
  	protected $guarded = ['id'];
  	protected $fillable = ['visits_id','status_id','users_id','request','observation'];
    protected $dates = ['deleted_at'];

    public function visits()
    {
        return $this->belongsTo('App\Visits');
    }
}
