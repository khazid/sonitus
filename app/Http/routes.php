<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {
//webpage


Route::get('/contactanos', function () {
      return view('webpage/contactanos');  
    });
Route::post('saveweb', ['as' =>'web/store', 'uses' => 'WebController@store']);
Route::get('accept/{id}', ['as' => 'web/accept', 'uses'=>'WebController@accept']);
Route::get('reject/{id}', ['as' => 'web/reject', 'uses'=>'WebController@reject']);
Route::post('resetpass', ['as' => 'web/resetpass', 'uses'=>'WebController@resetpass']);

//HOME
Route::get('/', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);

//DASHBOARD
Route::resource('dashboard', 'DashboardController');
//idioma
Route::get('lang/{lang}', function ($lang) {
        session(['lang' => $lang]);
        return \Redirect::back();
    })->where([
        'lang' => 'en|es'
    ]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);

//Dashboard routes...
Route::get('app/dashboard', ['as' => 'app/dashboard', 'uses' => 'DashboardController@index']);
Route::post('app/creategraphajax', ['as' => 'app/creategraphajax', 'uses' => 'DashboardController@creategraphajax']);
Route::post('app/alertas', ['as' => 'app/alertas', 'uses' => 'DashboardController@alertas']);

//rutas para la asignacion del rol del usuario
Route::resource('rol_user','RoleUserController');



//Report routes
Route::post('report/visitCharts', ['as' => 'report/visitCharts', 'uses'=>'ReportController@visitCharts']);
Route::post('report/listCharts', ['as' => 'report/listCharts', 'uses'=>'ReportController@listCharts']);
Route::post('report/proyectCharts', ['as' => 'report/proyectCharts', 'uses'=>'ReportController@proyectCharts']);
Route::post('report/hourCharts', ['as' => 'report/hourCharts', 'uses'=>'ReportController@hourCharts']);

Route::get('report/vList/{start}/{end}', ['as' => 'report/vList', 'uses'=>'ReportController@vList']);
Route::get('report/hourList/{start}/{end}', ['as' => 'report/hourList', 'uses'=>'ReportController@hourList']);
Route::get('report/vListTotal/{start}/{end}', ['as' => 'report/vListTotal', 'uses'=>'ReportController@vListTotal']);
Route::get('report/proyectList/{start}/{end}', ['as' => 'report/proyectList', 'uses'=>'ReportController@proyectList']);
Route::get('report/solicitudesWebList/{start}/{end}', ['as' => 'report/solicitudesWebList', 'uses'=>'ReportController@solicitudesWebList']);
Route::get('report/clientProjectList/{start}/{end}', ['as' => 'report/clientProjectList', 'uses'=>'ReportController@clientProjectList']);

Route::resource('report','ReportController');

//User routes
Route::post('user/checkemail', ['as' => 'user/checkemail', 'uses'=>'UserController@checkemail']);
Route::post('user/updatepass', ['as' => 'user/updatepass', 'uses'=>'UserController@updatepass']);
Route::post('user/savenewuser', ['as' => 'user/savenewuser', 'uses'=>'UserController@savenewuser']);
Route::resource('user','UserController');


//Uploads routes..
    
Route::get('upload', 'UploadsController@index');
Route::post('upload/create', 'UploadsController@save');
Route::get('upload/{archivo}', function ($archivo) {
     $public_path = public_path();
     $url = $public_path.'/storage/'.$archivo;
     //verificamos si el archivo existe y lo retornamos
     if (Storage::exists($archivo))
     {
       return response()->download($url);
     }
     //si no se encuentra lanzamos un error 404.
     abort(404);

});

//Registro de Admin
Route::get('auth/registerAdm', 'Auth\AuthController@getRegisterAdm');
Route::post('auth/registerAdm', ['as' => 'auth/registerAdm', 'uses' => 'Auth\AuthController@postRegisterAdm']);

// Password reset link request routes...
Route::get('password/email', ['as' => 'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'password/postEmail', 'uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', ['as' => 'password/postReset', 'uses' =>  'Auth\PasswordController@postReset']);


//Personal Staff
//ruta para realizar activacion de personal eliminado
Route::get('personal/searchdeleted', ['as' => 'personal/searchdeleted', 'uses'=>'PersonalController@searchdeleted']);
//ruta para realizar busqueda de registros.
Route::get('personal/activestaff/{id}', ['as' => 'personal/activestaff', 'uses'=>'PersonalController@activestaff']);
Route::post('personal/update/{id}', ['as' => 'personal/update', 'uses'=>'PersonalController@update']);
//una nueva ruta para eliminar registros con el metodo get
Route::post('personal/destroypersonal', ['as' => 'personal/destroypersonal', 'uses'=>'PersonalController@destroypersonal']);
Route::post('personal/searchpersonal', ['as' => 'personal/searchpersonal', 'uses'=>'PersonalController@searchpersonal']);
Route::post('personal/morrisbarajax', ['as' => 'personal/morrisbarajax', 'uses'=>'PersonalController@morrisbarajax']);

//rutas para el recurso del Personal
Route::resource('personal','PersonalController');
//ruta para realizar busqueda de registros.
Route::post('personal/search', ['as' => 'personal/search', 'uses'=>'PersonalController@search']);
//ruta para realizar busqueda de registros en la Tabla Personal.

//Pdf routes...
Route::resource('pdf', 'PdfController');



include_once("Routes/process.php");
include_once("Routes/menu.php");
include_once("Routes/messages.php");
include_once("Routes/client.php");
include_once("Routes/admin.php");

});


