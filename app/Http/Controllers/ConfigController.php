<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BillingConfig;
use App\Quote;
use App\Holidays;
use Response;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = BillingConfig::all();
                return \View::make('menu/config',compact('config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ruta='config.store';
        return \View::make('menu/new_config',compact('ruta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $config = new BillingConfig;
       $config->date=date('Y-m-d');    
        $config->daily_salary=str_replace(',', '', $request->daily_salary);
        $config->month_salary=str_replace(',', '', $request->month_salary);
        $config->biweekly_salary=str_replace(',', '', $request->biweekly_salary);
        $config->iva=$request->iva;
        $config->workday=$request->workday;
        $config->night_hour_start=$request->night_hour_start;
        $config->night_hour_end=$request->night_hour_end;
        $config->extra_hour_start=$request->extra_hour_start;
        $config->extra_hour_end=$request->extra_hour_end;
        $config->save();

        
       $config = BillingConfig::all();
                return \View::make('menu/config',compact('config'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $days = Holidays::all();
        
                return \View::make('menu/holidays',compact('days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ruta='config/update';
        $config = BillingConfig::find($id);
        return \View::make('menu/new_config',compact('ruta','config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $config = BillingConfig::find($request->idbilling);
        $config->daily_salary=str_replace(',', '', $request->daily_salary);
        $config->month_salary=str_replace(',', '', $request->month_salary);
        $config->biweekly_salary=str_replace(',', '', $request->biweekly_salary);
        $config->iva=$request->iva;
        $config->workday=$request->workday;
        $config->night_hour_start=$request->night_hour_start;
        $config->night_hour_end=$request->night_hour_end;
        $config->extra_hour_start=$request->extra_hour_start;
        $config->extra_hour_end=$request->extra_hour_end;
        $config->save();

        $config = BillingConfig::all();
                return \View::make('menu/config',compact('config'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyconfig(Request $request)
    {
        if($request->ajax()){
            //verificar que la configuracion no este asociada a una cotizacion
                $quotes= Quote::where('billing_config_id','=',$request->id)->first();
                if($quotes){
                    return Response::json(['destroy'=>'NO SE PUDO ELIMINAR LA CONFIGURACION!!, La configuración de facturación esta asociada a una cotización, puede modificarla (Esto afectara a todas las Cotizaciones creadas con el ID de esta configuracion) o crear una nueva para las nuevas cotizaciones a realizar.!! ']);
                }else{
                    // se procede a eliminar la configuracion
                        $config = BillingConfig::find($request->id);
                        $config->delete();
                        return Response::json(['destroy'=>'Eliminacion Exitosa.']);
                }
            
        }
       
    }

    
    public function storeholiday(Request $request)
    {
       $config = new Holidays;
        $config->date=$request->date;
        $config->description=$request->description;
        $config->save();

        
       $days = Holidays::all();
                return \View::make('menu/holidays',compact('days'));
    }

     public function updateholiday(Request $request)
    {
        if($request->ajax()) {
        $days = Holidays::find($request->id);
                $days->date = $request->date;
                $days->description = $request->description;
                
        $days->save();
        }
               return Response::json(['update'=>$days]);

    }
    
    public function destroyholiday($id)
    {
        $config = Holidays::find($id);
        $config->delete();
         $days = Holidays::all();
        
                return \View::make('menu/holidays',compact('days'));
    }
}
