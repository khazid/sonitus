<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Response;
use App\Personal;
use App\Menu;
use App\Process;
use App\Visits;
use App\NewRequest;
use App\Client;
use Khill\Lavacharts\Lavacharts as Lava;

class ReportController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';

        $semana['primer']=$start;
        $semana['ultimo']=$end;


        $line=$this->vCharts($start,$end);
        $visita=$this->vChartsTotal($start,$end);
        $bar=$this->proyectCharts($start,$end);
        $donut=$this->hourCharts($start,$end);
        $web=$this->solicitudesWeb($start,$end);
        $client=$this->clientProject($start,$end);
          
        
        return \View::make('reports/charts', compact('line','bar','donut','web','client','visita','semana'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 public function visitCharts(Request $request)
    {   
        $start=$request->start;
        $end=$request->end;

        $semana['primer']=$start;
        $semana['ultimo']=$end;
       
        $line=$this->vCharts($start,$end);
        $visita=$this->vChartsTotal($start,$end);
        $bar=$this->proyectCharts($start,$end);
        $donut=$this->hourCharts($start,$end);
        $web=$this->solicitudesWeb($start,$end);
        $client=$this->clientProject($start,$end);

        
        return \View::make('reports/charts', compact('line','bar','donut','web','client','visita','semana'));
      
    

    }


 public function vCharts($start,$end)
    {   
      if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
       $assigned= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('personal.name,
                                        SUM(CASE WHEN "visits"."status_id"=18 THEN 1  ELSE 0 END ) AS nuevas, 
                                        SUM(CASE WHEN "visits"."status_id"=20 THEN 1  ELSE 0 END ) AS nodone,
                                        SUM(CASE WHEN "visits"."status_id"=21 THEN 1  ELSE 0 END ) AS done'))
                        ->whereBetween('visits.date',[$start,$end])
                        ->whereRaw('("visits"."assigned_id" = personal.id or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = personal.id) )')
                        ->groupBy('personal.name')
                        ->orderBy('personal.name')
                        ->get();
       
        
        $personal='';
        $nuevas='';
        $nodone='';
        $done='';
        foreach($assigned as $c){
            $personal=$personal.'"'.$c->name.'",';
            $nuevas=$nuevas.$c->nuevas.',';
            $nodone=$nodone.$c->nodone.',';
            $done=$done.$c->done.',';
            

       }
        
        $personal = substr($personal, 0, -1);
        $nuevas=substr($nuevas, 0, -1);
        $nodone=substr($nodone, 0, -1);
        $done=substr($done, 0, -1);
        $data['personal']=$personal;
        $data['nuevas']=$nuevas;
        $data['nodone']=$nodone;
        $data['done']=$done;

        return $data;

   }
public function vChartsTotal($start,$end)
    {   
      if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
       $assigned= Visits::select(\DB::raw('SUM(CASE WHEN "visits"."status_id"=18 THEN 1  ELSE 0 END ) AS nuevas, 
                                        SUM(CASE WHEN "visits"."status_id"=20 THEN 1  ELSE 0 END ) AS nodone,
                                        SUM(CASE WHEN "visits"."status_id"=21 THEN 1  ELSE 0 END ) AS done'))
                        ->whereBetween('visits.date',[$start,$end])
                        ->get();
       
       return $assigned;

   }

public function proyectCharts($start,$end)
    {   
       if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
        
        $pro= Process::select(\DB::raw('SUM(CASE WHEN process.status_id= 3 THEN 1 ELSE 0 END) as enproceso,
                                            SUM(CASE WHEN process.status_id= 5 THEN 1 ELSE 0 END) as cerrados,
                                            SUM(CASE WHEN process.status_id= 6 THEN 1 ELSE 0 END) as asignados'))
                        ->whereRaw('process.ini_date is not null')
                        ->whereBetween('process.ini_date',[$start,$end])
                        ->get();
        return $pro;
    

    }
public function hourCharts($start,$end)
    {   
       
        if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
        
        $tecnicos="";
        $hr="";
        
        $staff= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('personal.name,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('visits.status_id',21)
                        ->whereBetween('visits.date',[$start,$end])
                        ->whereRaw('("visits"."assigned_id" = personal.id or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = personal.id) )')
                        ->groupBy('personal.name')
                        ->orderBy('personal.name')
                        ->get();
        
       foreach ($staff as $s) {
                $tecnicos=$tecnicos."'".$s->name."',";
                $horas=substr($s->total,0,2);
                $min=substr($s->total,3,2);
                $total=$horas.'.'.$min;
                $hr=$hr."'".$total."',";
                
        }
       
         $tecnicos= trim($tecnicos, ',');
         $hr= trim($hr, ',');      
        $data['data']=$hr;
        $data['label']=$tecnicos;
        $data['staff']=$staff;
        $data['count']=count($staff);
           

        
       return $data;
       
    

    } 

    public function solicitudesWeb($start,$end)
    {   
       
        if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
        
        $web= NewRequest::select(\DB::raw("SUM(CASE WHEN type='Presupuesto' THEN 1  ELSE 0 END ) AS presupuesto,
                                            SUM(CASE WHEN type='Proyecto' THEN 1  ELSE 0 END ) AS proyecto,
                                            SUM(CASE WHEN type='Informacion' THEN 1  ELSE 0 END ) AS informacion,
                                            SUM(CASE WHEN type='Soporte' THEN 1  ELSE 0 END ) AS soporte,
                                            SUM(CASE WHEN type='Mantenimiento' THEN 1  ELSE 0 END ) AS mantenimiento"))
                        ->whereBetween('created_at',[$start,$end])
                        ->get();
       //dd($web[0]);exit;
       return $web[0];
       
    

    } 

    public function clientProject($start,$end)
    {   
       
        if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
        $clientes='';
        $total='';
        $soporte='';
        $instalacion='';
        $adiestramiento='';
        $mantenimiento='';
        $visita='';

        $client= Client::join('client_process','client_process.client_id','=','client.id')
                        ->join('process','process.id','=','client_process.process_id')
                        ->select(\DB::raw("client.name,
                                            SUM(client_process.process_id ) AS total"))
                        ->whereBetween('process.ini_date',[$start,$end])
                        ->groupBy('client.name')
                        ->orderBy('client.name')
                        ->get();
       // foreach($client as $c){
       //      $clientes=$clientes."'".$c->name."',";
       //      $soporte=$soporte.$c->soporte.',';
       //      $instalacion=$instalacion.$c->instalacion.',';
       //      $adiestramiento=$adiestramiento.$c->adiestramiento.',';
       //      $mantenimiento=$mantenimiento.$c->mantenimiento.',';
       //      $visita=$visita.$c->visita.',';

       // }
       foreach($client as $c){
            $clientes=$clientes."'".$c->name."',";
            $total=$total.$c->total.',';
            

       }
       $data['nombres']=trim($clientes, ',');
       $data['total']=trim($total, ',');
       // $data['soporte']=trim($soporte, ',');
       // $data['instalacion']=trim($instalacion, ',');
       // $data['adiestramiento']=trim($adiestramiento, ',');
       // $data['mantenimiento']=trim($mantenimiento, ',');
       // $data['visita']=trim($visita, ',');
       return $data;
       
    

    } 
 
     

    function rangeWeek($datestr) {
            date_default_timezone_set(date_default_timezone_get());
            $dt = strtotime($datestr);
            $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
            $res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
            return $res;
    }
//--------------------------------------------------------------------------------------------------
public function listCharts(Request $request)
    {   
       

        $start=$request->start;
        $end=$request->end;
       
        
        if($request->lista=='vList'){ 
            return redirect()->action(
                    'ReportController@vList', ['start' => $start,'end'=>$end]
                );
        }else if($request->lista=='hourList'){ 
            return redirect()->action(
                    'ReportController@hourList', ['start' => $start,'end'=>$end]
                );    
        }else if($request->lista=='vListTotal'){
            return redirect()->action(
                    'ReportController@vListTotal', ['start' => $start,'end'=>$end]
                );     
        }else if($request->lista=='proyectList'){ 
            return redirect()->action(
                    'ReportController@proyectList', ['start' => $start,'end'=>$end]
                );   
        }else if($request->lista=='solicitudesWebList'){
            return redirect()->action(
                    'ReportController@solicitudesWebList', ['start' => $start,'end'=>$end]
                );    
        }else if($request->lista=='clientProjectList'){  
            return redirect()->action(
                    'ReportController@clientProjectList', ['start' => $start,'end'=>$end]
                );  
        }


    }

     public function create()
    {   
        
        return \View::make('reports/list');
      
    

    }

    public function vList($start,$end)
    {   
      if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
       $assigned= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.date,personal.name,
                                        SUM(CASE WHEN "visits"."status_id"=18 THEN 1  ELSE 0 END ) AS new, 
                                        SUM(CASE WHEN "visits"."status_id"=20 THEN 1  ELSE 0 END ) AS nodone,
                                        SUM(CASE WHEN "visits"."status_id"=21 THEN 1  ELSE 0 END ) AS done'))
                        ->whereBetween('visits.date',[$start,$end])
                        ->whereRaw('("visits"."assigned_id" = personal.id or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = personal.id) )')
                        ->groupBy('visits.date','personal.name')
                        ->orderBy('visits.date','personal.name')
                        ->get();
       $title[0]='date';
       $title[1]='name';
       $title[2]='new';
       $title[3]='nodone';
       $title[4]='done';

       $type='vlist';
        return \View::make('reports/list', compact('assigned','title','type'));
        

   }
public function vListTotal($start,$end)
    {   
      if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
       $assigned= Visits::select(\DB::raw('visits.date,SUM(CASE WHEN "visits"."status_id"=18 THEN 1  ELSE 0 END ) AS new, 
                                        SUM(CASE WHEN "visits"."status_id"=20 THEN 1  ELSE 0 END ) AS nodone,
                                        SUM(CASE WHEN "visits"."status_id"=21 THEN 1  ELSE 0 END ) AS done'))
                        ->whereBetween('visits.date',[$start,$end])
                        ->groupBy('visits.date')
                        ->orderBy('visits.date')
                        ->get();
       $title[0]='date';
       $title[1]='new';
       $title[2]='nodone';
       $title[3]='done'; 
        $type='vtotal';                
       return \View::make('reports/list', compact('assigned','title','type'));
       

   }

public function proyectList($start,$end)
    {   
       if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
        
        $assigned= Process::select(\DB::raw('process.ini_date as date,SUM(CASE WHEN process.status_id= 3 THEN 1 ELSE 0 END) as enpro,
                                            SUM(CASE WHEN process.status_id= 5 THEN 1 ELSE 0 END) as close,
                                            SUM(CASE WHEN process.status_id= 6 THEN 1 ELSE 0 END) as assign'))
                        ->whereRaw('process.ini_date is not null')
                        ->whereBetween('process.ini_date',[$start,$end])
                        ->groupBy('process.ini_date')
                        ->orderBy('process.ini_date')
                        ->get();

       $title[0]='date';
       $title[1]='enpro';
       $title[2]='close';
       $title[3]='assign';  

        $type='prolist';               
       return \View::make('reports/list', compact('assigned','title','type'));
    

    }
public function hourList($start,$end)
    {   
       
        if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
        
       $assigned= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('personal.name,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('visits.status_id',21)
                        ->whereBetween('visits.date',[$start,$end])
                        ->whereRaw('("visits"."assigned_id" = personal.id or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = personal.id) )')
                        ->groupBy('personal.name')
                        ->orderBy('personal.name')
                        ->get();
        
       $title[0]='name';
       $title[1]='total';
          $type='hour';             
       return \View::make('reports/list', compact('assigned','title','type'));
       
    

    } 

    public function solicitudesWebList($start,$end)
    {   
       
        if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }
        
        $assigned= NewRequest::select(\DB::raw("created_at as date,SUM(CASE WHEN type='Presupuesto' THEN 1  ELSE 0 END ) AS budget,
                                            SUM(CASE WHEN type='Proyecto' THEN 1  ELSE 0 END ) AS project,
                                            SUM(CASE WHEN type='Informacion' THEN 1  ELSE 0 END ) AS info,
                                            SUM(CASE WHEN type='Soporte' THEN 1  ELSE 0 END ) AS support,
                                            SUM(CASE WHEN type='Mantenimiento' THEN 1  ELSE 0 END ) AS maint"))
                        ->whereBetween('created_at',[$start,$end])
                        ->groupBy('created_at')
                        ->orderBy('created_at')
                        ->get();
       $title[0]='date';
       $title[1]='budget';
       $title[2]='project';
       $title[3]='info';
       $title[4]='support';
       $title[5]='maint';  

        $type='web';               
       return \View::make('reports/list', compact('assigned','title','type'));
       
    

    } 

    public function clientProjectList($start,$end)
    {   
       
        if($start==''){
        $start=date('Y').'-'.date('m').'-01';
        $end=date('Y').'-'.date('m').'-30';
        }

        $assigned= Client::join('client_process','client_process.client_id','=','client.id')
                        ->join('process','process.id','=','client_process.process_id')
                        ->select(\DB::raw("process.ini_date as date,client.name,
                                            SUM(CASE WHEN job_type='SOPORTE TECNICO' THEN 1  ELSE 0 END ) AS support,
                                            SUM(CASE WHEN job_type='INSTALACION' THEN 1  ELSE 0 END ) AS installation,
                                            SUM(CASE WHEN job_type='ADIESTRAMIENTO' THEN 1  ELSE 0 END ) AS training,
                                            SUM(CASE WHEN job_type='MANTENIMIENTO' THEN 1  ELSE 0 END ) AS maint,
                                            SUM(CASE WHEN job_type='VISITA TECNICA' THEN 1  ELSE 0 END ) AS visit"))
                        ->whereBetween('process.ini_date',[$start,$end])
                        ->groupBy('process.ini_date','client.name')
                        ->orderBy('process.ini_date','client.name')
                        ->get();
       
       $title[0]='date';
       $title[1]='name';
       $title[2]='support';
       $title[3]='installation';
       $title[4]='training';
       $title[5]='maint';  
       $title[6]='visit'; 

        $type='client';                
       return \View::make('reports/list', compact('assigned','title','type'));
       
    

    } 


}
