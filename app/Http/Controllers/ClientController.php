<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Client;
use App\Visits;
use App\User;
use App\Process;
use App\JobType;
use App\Personal;
use App\RequestClient;
use App\NewRequest;
use App\Quality;
use App\Quote;
use Input;
use Auth;
use Response;
use Mail;
use File;
use Session;

class ClientController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients=Client::whereNull('master_client')->orderBy('name')->get();
        
        return \View::make('client.list',compact('clients'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ruta='client.store';
        $update='no';
        $title='clientnew';
        return \View::make('client.info_client',compact('ruta','update','title'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//c=guardar cliente de web
    {   $userid= Auth::user()->id; 
        $rif=$request->riftype.$request->rif;
        $phone=$request->phone;
        
        
        $email=$request->email;

      //guardar informacion del cliente
            $cli = new Client;
                        $cli->name = $request->name;
                        $cli->rif = $rif;
                        $cli->address = $request->address;
                        $cli->applicant_name = $request->applicant_name;
                        $cli->charge = $request->charge;
                        $cli->departmen = $request->dpto;
                        $cli->headquarters = $request->sede;
                        $cli->phone = $request->telephone;
                        $cli->email = $request->email;
                        $cli->created_by = $userid;
                        if($request->clientid!='')$cli->master_client = $request->clientid;
                    $cli->save();
        $clients=Client::whereNull('master_client')->orderBy('name')->get();
        
        return \View::make('client.list',compact('clients'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $roles = User::find(Auth::user()->id)->role()->get();
        $rol=$roles->first()->pivot->role_id;

        $client = Client::withTrashed()->where('id','=',$id )->get();
        $client=$client[0];
        $projects= Process::join('client_process','client_process.process_id','=','process.id')
                            ->join('client','client_process.client_id','=','client.id')
                            //->leftJoin('visits','process.id','=','visits.process_id')
                            ->join('status','process.status_id','=','status.id')
                            //->select(\DB::raw('process.*,status.name as status,visits.have_request,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                            ->select('process.*','status.name as status')
                            ->where('client.id',$id)
                            //->groupBy('process.id','status.name','visits.have_request')
                            ->groupBy('process.id','status.name')
                            ->get();
       
        if($client->master_client!=null){
            $masterid=$client->master_client;
        }else{
            $masterid=$id;
        }
        
        $branches=Client::withTrashed()->where('master_client',$masterid)->lists('name','id')->prepend('Select...', 'null');
        return \View::make('client.profile',compact('client','projects','branches','rol','masterid'));
    }
//----------------------------------------------------------------------------------------------
    public function showQuote($id)
    {   
        $roles = User::find(Auth::user()->id)->role()->get();
        $rol=$roles->first()->pivot->role_id;

        $client = Client::withTrashed()->where('id','=',$id )->get();
        $client=$client[0];
        $quotes=Quote::join('client','client.id','=','quote.client_id')
                       ->join('status','status.id','=','quote.status_id')
                       ->leftJoin('new_request','new_request.id','=','quote.new_request_id')
                       ->join('personal','personal.id','=','quote.personal_id')
                       ->select('client.id as clientid','client.name as client','status.id as statusid','status.name as status','quote.*','personal.name as personal')
                       ->where('client.id',$id)
                       ->get();
       
        if($client->master_client!=null){
            $masterid=$client->master_client;
        }else{
            $masterid=$id;
        }
        
        $branches=Client::withTrashed()->where('master_client',$masterid)->lists('name','id')->prepend('Select...', 'null');
        return \View::make('client.client_quote',compact('client','quotes','branches','rol','masterid'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        $update='si';
        $ruta='client/update';
        $title='clientedit';
            return \View::make('client.info_client',compact('client','update','title','ruta'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //guardar informacion del cliente
            $cli = Client::find($request->client_id);
                $cli->name = $request->name;
                $cli->rif = $request->rif;
                $cli->address = $request->address;
                $cli->applicant_name = $request->applicant_name;
                $cli->charge = $request->charge;
                $cli->departmen = $request->dpto;
                $cli->headquarters = $request->sede;
                $cli->phone = $request->telephone;
                $cli->email = $request->email;
            $cli->save();

           if(trim($request->nuevaclave) !=''){

                $pers = User::where('email',$request->email)->get();
                if(count($pers)>0){

                        $pers=User::find($pers[0]->id);
                        $pers->password = bcrypt($request->nuevaclave);
                        
                        if($pers->save()){
                                //envio de correo para indicar el cambio de clave... 
                                
                                        
                                        $info['title']='Bienvenido '.$pers->name;
                                        
                                        $info['content'][0]= 'Su clave de ingreso ha sido cambiada con exito a continuacion vera la informacion actualizada de su usuario y contraseña';
                                        $info['content'][1]='Tu usuario: '.$request->email.'
                                                             Tu nueva clave: '.$request->nuevaclave;
                                        $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';
                                        $name=$pers->name;
                                        $email=$pers->email;
                                        $subject='Cambio de Clave Sistema Sonitus';
                                       
                                        $mail=$this->sendmail($info,$name,$email,$subject,'');
                                        
                    }
              }else{

                Session::flash('message','No se realiza cambio de clave ya que el Cliente no esta registrado como Usuario del sistema !!');

              }
           }
           return redirect()->route('client.show',['id'=>$request->client_id]);
    }
//----------------------------------------------------------------------------------------------
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->ajax()){
                //validar que no tenga procesos activos
        
                $process=Process::join('client_process','process.id','=','client_process.process_id')
                                  ->where('client_process.client_id','=',$request->id)
                                  ->whereRaw('process.status_id in (1,3,4,6)')
                                  ->select('process.id')
                                  ->get();
                if(count($process)==0){                  
                       
                        $client = Client::find($request->id);
                        
                        if($client->users_id != ''){
                            $client->users_id=null;
                            $client->save();

                            $user = User::find($client->users_id);
                            $user->delete();
                        }
                        $client->delete();
                
                                          
                        
               return Response::json(['deleted'=>$client]);
                }else{
                    
                return Response::json(['deleted'=>'404']);
                }
            }
        
    }
//----------------------------------------------------------------------------------------------
    public function activeclient($id)
    {   $client = Client::withTrashed()->where('id','=',$id )->restore();
        
        $client=Client::find($id);
        Session::flash('message','Activation of '.$client->name.' succed!!');
        return redirect()->back();
       
        
    }
//----------------------------------------------------------------------------------------------
    public function searchdeleted()
    {    $client = Client::onlyTrashed()->get();
        //dd($deleted);exit;
        return \View::make('client.deleted_client',compact('client'));
       
        
    }
//----------------------------------------------------------------------------------------------
    public function showdeleted($id)
    {   
        $roles = User::find(Auth::user()->id)->role()->get();
        $rol=$roles->first()->pivot->role_id;

        $client = Client::find($id);
        $projects= Process::join('client_process','client_process.process_id','=','process.id')
                            ->join('client','client_process.client_id','=','client.id')
                            ->leftJoin('visits','process.id','=','visits.process_id')
                            ->join('status','process.status_id','=','status.id')
                            ->select(\DB::raw('process.*,status.name as status,visits.have_request,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                            ->where('client.id',$id)
                            ->groupBy('process.id','status.name','visits.have_request')
                            ->get();
       
        if($client->master_client!=null){
            $masterid=$client->master_client;
        }else{
            $masterid=$id;
        }
        
        $branches=Client::where('master_client',$masterid)->lists('name','id')->prepend('Select...', 'null');
        return \View::make('client.profile',compact('client','projects','branches','rol','masterid'));
    }
//----------------------------------------------------------------------------------------------
    public function newproject($id)
    {
        $client = Client::find($id);
        $clientid = $client->id;
        $jobt= JobType::all();
        $from='webrequest';
        $tecnicos= Personal::where('occupation','Jefe tecnico')->lists('name','id')->prepend('Select', 'null');
        
        return \View::make('process/new_assign',compact('jobt','tecnicos','client','from','clientid'));
    }
//----------------------------------------------------------------------------------------------
    public function searchletter($id)
    {
        $clients = Client::whereRaw("upper(name) like ('".$id."%')")->whereNull('master_client')->orderBy('name')->get();
        
        return \View::make('client.list',compact('clients'));
    }
//----------------------------------------------------------------------------------------------
    public function newrequest()//Request to client
    {
        $roles = User::find(Auth::user()->id)->role()->get();
        $rol=$roles->first()->pivot->role_id;
       
        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->join('personal','personal.id','=','visits.assigned_id')
                                ->join('client','client.id','=','client_process.client_id')
                                ->select('request_to_client.id','request_to_client.request','request_to_client.users_id','status.name as status','status.id as statusid','visits.date','client_process.client_id','visits.id as visitid','process_number','personal.name','client.name as client');
        if($rol==5){
            $requestclient=$requestclient->where('visits.assigned_id',Auth::user()->id);
            
        } 
        $requestclient=$requestclient->get();                       
                                
        
        return \View::make('client.request_list',compact('requestclient','rol'));
    }
//----------------------------------------------------------------------------------------------
    public function listwebrequest()//new WEb request... to show the request form that will be in the web page 
    {
        
        $requestweb=NewRequest::leftJoin('process','process.id','=','new_request.process_id')
                                ->join('status','status.id','=','new_request.status_id')
                                ->join('client','client.id','=','new_request.client_id')
                                ->select('new_request.id','new_request.description','new_request.type','process_number','process.id as processid','client.name as client','client.email','client.id as clientid','status.name as status','new_request.status_id')
                                ->get();
        return \View::make('client.request_web_list',compact('requestweb'));
    }
//----------------------------------------------------------------------------------------------
    
 public function sendmail($info,$name,$email,$subject,$path)
    {   
       $view='emails.plantilla';
          
       //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject,$path) {
                $m->from(env('SENDER_ADDRES'), env('SENDER_NAME'));
                $m->to($email, $name)->subject($subject);
                $m->bcc(env('COPY_ADDRES'), env('COPY_NAME'));
                if($path!='')$m->attach($path);

               
            });
       return $mail;

    }
//----------------------------------------------------------------------------------------------
    public function request(Request $request)//new WEb request... to show the request form that will be in the web page 
    {   //dd($request->newrequest);exit;
        $new= new NewRequest;
        $new->client_id=$request->client_id;
        if($request->process_id!='') $new->process_id=$request->process_id;
        $new->description=$request->newrequest;
        $new->type=$request->tipo;
        $new->status_id=26;
        $new->save();
        
        return redirect(route('client.show',['id'=>$request->client_id]));
    }

//----------------------------------------------------------------------------------------------    

    public function responserequest(Request $request)
    {   
       if($request->ajax()) {
        $id=$request->requestid;
        $email=$request->email;
        $processid=$request->processid;
        $respuesta=$request->response;
        $tipo=$request->tipo;
        
        $webreq=NewRequest::find($id);
        $webreq->status_id=$tipo;
        $webreq->response=$respuesta;

        if($webreq->save()){
                        //envio de correo para indicar el cambio de clave... 
                        $client=Client::find($webreq->client_id);
                                
                                $info['title']='Estimado '.$client->name;
                                
                                $info['content'][0]= 'En respuesta a su solicitud ';
                                $info['content'][1]=$respuesta;
                                $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';
                                $name=$client->name;
                                $email=$client->email;
                                $subject='Respuesta Solicitud Web Sistema Sonitus';
                               
                                $mail=$this->sendmail($info,$name,$email,$subject,'');
                                
            }

       

       return Response::json(['respuesta'=>$webreq]);
       
      }
       

    }
//----------------------------------------------------------------------------------------------    

    public function findresponse(Request $request)
    {   
       if($request->ajax()) {
        $id=$request->requestid;
        $response=NewRequest::find($id);

        return Response::json(['respuesta'=>$response->response]);
       
      }
       

    }
//----------------------------------------------------------------------------------------------    
    public function quality($id)
    {   
       
        $process_id=$id;
        $client=Process::join('client_process','client_process.process_id','=','process.id')
                         ->select('client_id')
                         ->where('process.id','=',$process_id)
                         ->get();
        $clientid=$client[0]->client_id;
        return \View::make('client.quality',compact('process_id','clientid'));
    }
//----------------------------------------------------------------------------------------------
    public function savequality(Request $request)
    {   
       
        $table= new Quality;
            $table->service = $request->service;
            $table->punctuality = $request->punctuality;
            $table->attitude = $request->attitude;
            $table->attention = $request->attention;
            $table->consultation = $request->consultation;
            $table->professionalism = $request->professionalism;
            $table->job_quality = $request->job_quality;
            $table->equipment_quality = $request->equipment_quality;
            $table->solutions = $request->solutions;
            $table->knowledge = $request->knowledge;
            $table->evaluation = $request->evaluation;
            $table->administrative = $request->administrative;
            $table->work_with = $request->work_again;
            $table->work_reason = $request->work_reason;
            $table->service_satisfaction = $request->satisfaction;
            $table->less_satisfaction = $request->less_satisfaction;
            $table->comments = $request->comments;
            $table->process_id = $request->process_id;
           $table->save();

          $pro= Process::find($request->process_id); 
          $pro->quality = TRUE;
          $pro->save(); 

          $process= Process::leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','process.signature','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','status.id as statusid','client.*')
                                ->where('process.id','=',$request->process_id)
                                ->get();
        $process=$process[0];
        $counter= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->where('process_id',$request->process_id)->get();

      //-----INFORMACION PARA EL PDF -------------
         $viewpdf =  \View::make('emails/quality_report',compact('process','request','counter'))->render(); 
            $name= 'Quality_'.date('d-m-Y').'_'.$process->processid.'.pdf';
            $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            //return $pdf->stream('emails/visit_report',compact('process','request','counter'));
            $pdf->save($path);

       // envio de correo al cliente
       $client=Client::find($request->client_id);
                                $info['title']='Estimado '.$client->name;
                                
                                $info['content'][0]= 'Su Encuesta de calidad ha sido llenada con exito la misma se encuentra adjuntada en este correo para su revision';
                                $info['content'][1]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';
                                $name=$client->name;
                                $email=$client->email;
                                $subject='Encuesta de Calidad Sonitus Proyecto '.$process->process_number;
                               
                                $mail=$this->sendmail($info,$name,$email,$subject,$path);
                    
                    //envio al lider del proyecto
                              $lider=Personal::find($pro->assigned_id); 
                              $name=$lider->name;
                              $email=$lider->email;
                                $info['title']='Estimado '.$lider->name;
                                $info['content'][0]= 'Se ha adjuntado la encuesta de calidad llenada por el cliente '.$client->name.' del proyecto Nro. '.$process->process_number;
                             
                                $mail=$this->sendmail($info,$name,$email,$subject,$path);  
                                if($mail)File::Delete($path); 

        
        return redirect(route('client.show',['id'=>$client->id]));
       
     
       

    }

    //----------------------------------------------------------------------------------------------    
    public function showquality($id)
    {   
       
        $process_id=$id;
        $client=Process::join('client_process','client_process.process_id','=','process.id')
                         ->select('client_id')
                         ->where('process.id','=',$process_id)
                         ->get();
        $clientid=$client[0]->client_id;

         $process= Process::leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','process.signature','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','status.id as statusid','client.*')
                                ->where('process.id','=',$process_id)
                                ->get();
        $process=$process[0];
        $counter= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->where('process_id',$process_id)->get();
        $request = Quality::select('*')->where('quality.process_id',$process_id)->get();
        $request=$request[0];

        $viewpdf =  \View::make('emails/quality_report',compact('process','request','counter'))->render(); 
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($viewpdf);
        
            return $pdf->stream('emails/quality_report',compact('process','request','counter'));
            
        
    }
//----------------------------------------------------------------------------------------------    

    public function quoterequest(Request $request)
    {   
       if($request->ajax()) {
        $id=$request->requestid;
        $clientid=$request->clientid;
        $observation=$request->observation;
        $processid=$request->processid;
        $userid= Auth::user()->id; 

        $personal=Personal::join('users','users.id','=','personal.users_id')
                            ->select('personal.id')
                            ->where('users.id',$userid)
                            ->get();
        

        $quote=new Quote;
        $quote->client_id=$clientid;
        $quote->date=date('Y-m-d');

        if($id!='')$quote->new_request_id=$id; //viene de una solicitud web

        $quote->status_id=33; //nueva solicitud de cotizacion
        $quote->observation=$observation;
        $quote->personal_id=$personal[0]->id;
        if($processid!='')$quote->process_id=$processid;

        if($quote->save()){
            $quote->status()->attach(33,['observation'=>'ClientSys: Nueva solicitud de Cotizacion','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
            
            $client=Client::find($clientid);//obtener la info del cliente

                      if($id!=''){ //Cambia el estado de la solicitud web a Cotizando
                                  $req=NewRequest::find($id);
                                  $req->status_id=32;
                                  $req->save();
                      

                        //si es una cotizacion de una solicitud del cliente
                        //envio de correo al cliente para indicar la realizacion de la cotizacion  

                                    
                                            
                                            $info['title']='Estimado '.$client->name;
                                            
                                            $info['content'][0]= 'En respuesta a su solicitud ';
                                            $info['content'][1]='Se esta procesando la cotizacion correspondiente a su solicitud, en el transcurso de 24 a 48 horas estara recibiendo un correo con la misma.';
                                            $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';
                                            $name=$client->name;
                                            $email=$client->email;
                                            $subject='Respuesta Solicitud Web Sistema Sonitus';
                                           
                                            $mail=$this->sendmail($info,$name,$email,$subject,'');
                        }

                        //-----------envio de correo notificando al personal administrativo

                                    $perinfo = Personal::select('name','email')->where('occupation','=','Administrativo')->get();
                                    $pername ='';
                                    $peremail = '';
                                    $cant = count($perinfo);
                                    $i=0;

                                            $infoper['title']='Solicitud de Cotizacion para el Cliente '.$client->name;
                                            
                                            $infoper['content'][0]= 'Se realiza una nueva solicitud de cotizacion con la observacion: '.$observation.' para que la misma sea procesada';
                                            
                                            $subject='Solicitud de Cotizacion';

                                            for($i;$i<$cant;$i++){
                                                 
                                                 $mail=$this->sendmail($infoper,$perinfo[$i]['name'],$perinfo[$i]['email'],$subject,''); 
                                            }
                                           
                                                            
                                       
            }

       
        if($request->processid!='') $ruta= \URL::route('quote/addmaterials',['id'=>$quote->id]);
            else $ruta='';

       return Response::json(['respuesta'=>$quote,'ruta'=>$ruta,'perinfo'=>$perinfo]);
       
      }
       

    }

}
