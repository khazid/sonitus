<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\JobType;
use App\Personal;
use App\Client;
use App\Process;
use App\User;
use App\Visits;
use App\RequestClient;
use App\NewRequest;
use App\Status;
use App\ProcessStatus;
use Input;
use Auth;
use Response;
use DB;
use Mail;
use URL;
use File;

class ProcessController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personal= Auth::personal();
        $perid = $personal->id;
        $roluser= User::find(Auth::user()->id)->role;
        $rol=$roluser[0]->id;
        // dd($personal); exit;
        if($roluser[0]->id == 5){ 
            $process= Process::join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->select('process.id as processid','process.process_number','process.description','status.name as status','status.id as statusid','process.assigned_date','process.priority','process.ini_date','client.*','personal.name as staffname','personal.profile_pic')
                                ->where('process.assigned_id','=',$perid)
                                ->get();
                                
        } else { 
            $process= Process::join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->select('process.id as processid','process.process_number','process.description','status.name as status','status.id as statusid','process.assigned_date','process.priority','process.ini_date','client.*','personal.name as staffname','personal.profile_pic')
                                ->get();
        }
        
        $personal = Personal::whereRaw("occupation in ('Tecnico','Jefe tecnico')")->lists('name','id');                   
        $destinatary = User::lists('email','id'); 
        $status = Status::where('type',1)->lists('name','id');   
        //dd($status);exit;                   
        return \View::make('process/list',compact('process','personal','destinatary','rol','status'));
        
       
    }
//----------------------------------------------------------------------------------------------
    public function webprocess()
    {
        $process= Process::join('users', 'users.id', '=', 'process.users_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','status.name as status','client.*')
                                ->where('web','=',TRUE)
                                ->where('status_id','=',2)
                                ->get();
                                
        return \View::make('process/list_web',compact('process'));
        
       
    }
//----------------------------------------------------------------------------------------------
    
    /**
     * Show the new assign for the WEB process..
     *
     * @return \Illuminate\Http\Response
     */
    public function findprocessweb($id)
    {   $jobt= JobType::all();
        $tecnicos= Personal::select('personal.name','personal.id')
                                ->where('personal.occupation','=','Tecnico')
                                ->get(); 
        $process= Process::join('users', 'users.id', '=', 'process.users_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','users.id as iduser','users.name as username','process.ini_date','process.web','process.end_date','status.name as status','client.*')
                                ->where('process.id','=',$id)
                                ->get();
        return \View::make('process/new_assign',compact('process','jobt','tecnicos'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() //crear un nuevo proceso y asignarlo a un tecnico
    {
        $jobt= JobType::all();

        $tecnicos= Personal::where('occupation','Jefe tecnico')->lists('name','id')->prepend('Select', 'null');
        
        return \View::make('process/new_assign',compact('jobt','tecnicos'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Store a newly assigned process.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   $checkbox = Input::get('job');
        $userid= Auth::user()->id;
        $rif=$request->riftype.$request->rif;
        
       //verificacion proyecto para mismo cliente, nueva sucursal o nuevo cliente
        if($request->branches=='on' && $request->clientid!=''){//nueva sucursal de un cliente existente
            $new=TRUE;
            $clientid=$request->clientid;
            $newbranch='nueva sucursal';
        }else if($request->branches==false && $request->clientid!='' && $request->from==''){//mismo cliente
            $new=FALSE;
            $clientid=$request->clientid;
            $newbranch='mismo cliente';
        }else if($request->branches==false && $request->clientid==''){//nuevo cliente
            $new=TRUE;
            $clientid='';
            $newbranch='nuevo cliente';
        }else if($request->from=='webrequest' && $request->clientid!=''){//nuevo cliente web solo actualizar
            $new=false;
            $clientid=$request->clientid;
            $newbranch='nuevo cliente web';
        }
//dd($request->branches.'/'.$request->clientid.' new: '.$newbranch);exit;
        if($new){
               //guardar informacion del cliente
                    $cli = new Client;
                        $cli->name = $request->name;
                        $cli->rif = $rif;
                        $cli->address = $request->address;
                        $cli->applicant_name = $request->applicant_name;
                        $cli->charge = $request->charge;
                        $cli->departmen = $request->dpto;
                        $cli->headquarters = $request->sede;
                        $cli->phone = $request->telephone;
                        $cli->email = $request->email;
                        $cli->created_by = $userid;
                        if($clientid!='')$cli->master_client = $clientid;
                    $cli->save();

                    $clientid=$cli->id;
        }

        if($request->from=='webrequest'){
            $cli = Client::find($request->clientid);
                        $cli->name = $request->name;
                        $cli->rif = $rif;
                        $cli->address = $request->address;
                        $cli->applicant_name = $request->applicant_name;
                        $cli->charge = $request->charge;
                        $cli->departmen = $request->dpto;
                        $cli->headquarters = $request->sede;
                        $cli->phone = $request->telephone;
                        $cli->email = $request->email;
                        $cli->created_by = $userid;
                        
                    $cli->save();

                    $clientid=$cli->id;
        }
        //dd($clientid);exit;
        $procesnumber= $this->process_number($clientid);//se crea numero de proceso

       //guardar relaciones del job type
       
         $jobs="";
        foreach($checkbox as $c){
            if($jobs == "") $jobs = $c;
            else $jobs = $jobs.' / '.$c;
            
        } 
        
        //guardar informacion del proceso (status asignado)
            $pro = new Process;
                $pro->process_number = $procesnumber;
                $pro->users_id = $userid;
                $pro->maintenance = $request->maintenance;
                if($request->tecnico!='null'){
                $pro->assigned_id = $request->tecnico;
                $pro->assigned_date = date('Y-m-d');
                $pro->status_id = 6;
                $procstatus=6;
                $procstatname='Lider Asignado';
                }else{
                  $pro->status_id = 1;
                  $procstatus=1; 
                  $procstatname='Nuevo sin asignar lider'; 
                }
                
                $pro->description = $request->description;
                $pro->job_type = $jobs;
                $pro->web = FALSE;
            if($pro->save()){
                $pro->status()->attach($procstatus,['observation'=>'System: Creacion de Proceso con estado '.$procstatname,'users_id'=>$userid,'date'=>date('Y-m-d')]);
                //guardar relacion con la tabla client_process
                $clie=Client::find($clientid);
                $clie->process()->attach($pro->id,['users_id'=>$userid]);
              }
         
         //retornamos a la creacion de procesos con un mensaje de creacion exitoso

        Session::flash('message','La Creacion del Proceso Numero: "'.$procesnumber.'"  se realizo con exito!!');
        return redirect('process');

    }
//----------------------------------------------------------------------------------------------
    /**
     * Store a newly WEB process .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeWeb(Request $request)
    {   
        $clientid= 1;
        $procesnumber= $this->process_number($clientid); // personal_id=1 corresponde al del Sistema

        //guardar informacion del cliente
            $cli = new Client;
                $cli->name = $request->name;
                $cli->rif = $request->rif;
                $cli->address = $request->address;
                $cli->applicant_name = $request->applicant_name;
                $cli->charge = $request->charge;
                $cli->phone = $request->phone;
                $cli->celphone = $request->celphone;
                $cli->email = $request->email;
                $cli->users_id = $userid;
            $cli->save();
       
        //guardar informacion del request 
            $newrequest = new NewRequest;
                $newrequest->client_id = $cli->id;
                $newrequest->description = $request->description;
                $newrequest->type = $request->type;
            $newrequest->save();
         
        
        
        //return \View::make('process/new_assign',compact('jobt','tecnicos'));
    }

//----------------------------------------------------------------------------------------------
    /**
     * Save the information of the assigned process.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function save(Request $request) //guardar informacion de la visita por proceso (tecnico)
    {
        
    }

//----------------------------------------------------------------------------------------------
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$type){ 
       $roles = User::find(Auth::user()->id)->role()->get();
        $rol=$roles->first()->pivot->role_id;
       $process= Process::leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','status.id as statusid','client.*')
                                ->where('process.id','=',$id)
                                ->get();
        $process=$process[0];  

        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->join('status','visits.status_id','=','status.id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,personal.name,personal.profile_pic,status.name as status,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('process_id',$id)
                        //->where('visits.status_id',21)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic','status.name')
                        ->orderBy('visits.date')
                        ->get();

        $counter= Visits::where('process_id',$id)
                        ->get([ \DB::raw('(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total')]);
        $cantvisit= Visits::where('process_id',$id)
                        ->get([ \DB::raw('count(visits.id)  as total')]);
        //dd($process->end_date);exit;
        if($process->end_date!=null){
            $days= Process::where('process.id',$id)
                        ->get([ \DB::raw('EXTRACT(DAY FROM age(date(end_date),date(ini_date) ) ) as total')]);
        }else{
           $days= Process::where('process.id',$id)
                        ->get([ \DB::raw('EXTRACT(DAY FROM age(now(),date(ini_date) ) ) as total')]); 
        }
        
        //dd($counter);exit;

        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','request_to_client.users_id','status.name as status','status.id as statusid','visits.date','client_process.client_id','visits.id as visitid')
                                ->where('process.id','=',$id)
                                ->get();
                                

         $requestweb=NewRequest::leftJoin('process','process.id','=','new_request.process_id')
                                ->join('status','status.id','=','new_request.status_id')
                                ->join('client','client.id','=','new_request.client_id')
                                ->select('new_request.id','new_request.description','new_request.type','process_number','process.id as processid','client.name as client','client.email','client.id as clientid','status.name as status','new_request.status_id','new_request.created_at as date','new_request.response')
                                ->where('process.id','=',$id)
                                ->get();

       return \View::make('process.detail',compact('process','visits','requestclient','rol','counter','type','cantvisit','days','requestweb'));
    }
//----------------------------------------------------------------------------------------------
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
//----------------------------------------------------------------------------------------------
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
//----------------------------------------------------------------------------------------------
/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatetec(Request $request)
    {   

        $process= Process::find($request->processid);
        $process->assigned_id = $request->personal_id;
        $process->status_id = 6;//asignado
        $process->updated_at = date('Y-m-d');
        $process->assigned_date = date('Y-m-d');
        if($process->save()){
                $process->status()->attach(6,['observation'=>$request->statusob,'users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);

              }

       return redirect('process');
        
    }

//----------------------------------------------------------------------------------------------
/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateprio(Request $request)
    {   

        $process= Process::find($request->proprioid);
        $process->priority = $request->priority;
        $process->save();

       return redirect('process');
        
    }
//----------------------------------------------------------------------------------------------
/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatestatus(Request $request)
    {   

        $process= Process::find($request->prostatid);
        $process->status_id = $request->newstatus;
        if($process->save()){
                $process->status()->attach($request->newstatus,['observation'=>$request->statusob,'users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);

              }

       return redirect('process');
        
    }
//----------------------------------------------------------------------------------------------   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
//----------------------------------------------------------------------------------------------
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $idstatus=$request->search;
        $process= Process::join('users', 'users.id', '=', 'process.users_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->select('process.id as processid','process.id','process.process_number','users.id as iduser','users.name as username','status.name as status','process.assigned_date','client.*','personal.name')
                                ->where('status.id','=',$idstatus)
                                ->get();

        return \View::make('process/list',compact('process'));
    }
//----------------------------------------------------------------------------------------------
    public function process_number($id)
    {
        $procesos= Process::join('client_process','client_process.process_id','=','process.id')
                            ->select('client_process.client_id as clientid','process.id')
                            ->where('client_process.client_id','=',$id)->get();
        if(count($procesos)>0){
                $procesos=$procesos[0];                  
                $cantidad= count($procesos) +1;
                $number=date('Ymd').'P'.$procesos->id.'C'.$procesos->clientid.'N'.$cantidad;
            }else {
                 $number=date('Ymd').'C'.$id.'N1';
            }

        return $number;
    }
//----------------------------------------------------------------------------------------------    
    public function endtask(Request $request){
    if($request->ajax()) {
        $idprocess=$request->idprocess;
        $status=$request->status;
        
        $process = Process::find($idprocess);
        $process->status_id=$status;
        $process->end_date=date('Y-m-d');
        if($status==5)$process->end_date=date('Y-m-d');
        if($process->save()){
            if($status==5){ //proceso cerrado por el jefe y envia el pdf con el reporte del cierre del proceso
                $process->status()->attach($status,['observation'=>'PCendtask:Cierre de proceso','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
                $mail = $this->pdf($idprocess);
                if($mail) $mail='true';
                else $mail='false';
                return Response::json(['endtask'=>$process,'mail'=>$mail]);

            }else if($status==4){ //proceso cerrado por el tecnico y guarda la firma
                $process->status()->attach($status,['observation'=>'PCendtask:Culminacion de proceso','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
               
                
                return Response::json(['endtask'=>$process,'urlresponse'=> \URL::route('process/signature',['id'=>$idprocess])]);
            }
        }
        
     }   
       
    }
//----------------------------------------------------------------------------------------------
public function signature($id){

    $process=Process::find($id);
    $process_id=$process->id;
    $type='process';
    return \View::make('visits/signature',compact('type','process_id'));
}

//----------------------------------------------------------------------------------------------
public function savesignature(Request $request)
    {   
        if($request->ajax()) {
        
        $idprocess=$request->processid;
        $sing=$request->signature;
        $userid= Auth::user()->id;
        //$filemaker = new File;
        $sing = str_replace('data:image/png;base64,', '', $sing);
        $sing = str_replace(' ', '+', $sing);
        $fileData = base64_decode($sing);
        $fileName = $idprocess.'_client_sig.jpg';

        //se crea el directorio donde ira la imagen
        $directory = public_path().'/images/equipment/'.$idprocess;

        if(! File::isDirectory($directory)){//verifica que existe el directorio del proceso
         File::makeDirectory($directory, $mode = 0777, true, true);
         
        }else{
          $directory =$directory;
          
        }

        $directory =$directory.'/'.$fileName; //se le agrega el nombre del archivo a la ruta del directorio
        //--------------------------------
        $process = Process::find($idprocess);
        $process->signature = $fileName;
        
        if($process->save()){

            $result = file_put_contents($directory, $fileData);
            //$this->pdf($idprocess);
            
        }

        return Response::json(['signature'=>$result,'directory'=>$directory,'urlresponse'=>URL::route('process.index')]);
    }
        
}


//----------------------------------------------------------------------------------------------

public function pdf($id)
    {
         
        $process= Process::leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','process.signature','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','status.id as statusid','client.*')
                                ->where('process.id','=',$id)
                                ->get();
        $process=$process[0];
        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,personal.name,personal.profile_pic,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('process_id',$id)
                        ->where('visits.status_id',21)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic')
                        ->orderBy('visits.date')
                        ->get();

        $picturestake= Visits::join('visits_picture','visits_picture.visits_id','=','visits.id')
                                ->join('picture','picture.id','=','visits_picture.picture_id')
                                ->select('picture.description','picture.file','visits.date','visits.id as visitsid','visits.process_id as processid')
                                ->where('visits.process_id',$id)
                                ->orderBy('visits.date')
                                ->get();

        $counter= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->where('process_id',$id)
                        ->get([ \DB::raw('(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total')]);
        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','request_to_client.users_id','status.name as status','status.id as statusid','visits.date','client_process.client_id','visits.id as visitid')
                                ->where('process.id','=',$id)
                                ->get();
        
        //-----INFORMACION PARA EL PDF ------------------
            $viewpdf =  \View::make('emails/process_report',compact('process','visits','requestclient','counter','picturestake'))->render(); 
             $name= 'Proyect_'.$process->process_number.'.pdf';
             $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            $pdf->save($path);
            //return $pdf->stream('emails/process_report',compact('process','visits','requestclient','counter'));


        //-----INFORMACION PARA EL CORREO ------------------
            $personal=Client::find($process->id);
                    $name=$personal->name;
                    $email= $personal->email;

            $content['content'][0]='El siguiente correo tiene la finalidad de informarle sobre la culminacion del proyecto #'.$process->process_number.'.';
            $content['content'][1]='Adjunto encontrara el informe correspondiente, por favor recuerde llenar el Formulario de calidad de servicio para ayudarnos a mejorar cada dia, el mismo podra ser llenado al ingresar al sistema en Boton de Encuesta de Calidad al lado del proyecto finalizado.';
            $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';   
            $content['title']='Estimado Cliente '.$personal->name; 
            $subject='Reporte de culminacion de proyecto #'.$process->process_number; 
             $mail=$this->sendmail($content,$name,$email,$subject,$path);
           // if($mail)File::Delete($path);
           return $mail; 
        
         
    }

//----------------------------------------------------------------------------------------------
public function pdfshow($id)
    {
         
        $process= Process::leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','process.signature','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','status.id as statusid','client.*')
                                ->where('process.id','=',$id)
                                ->get();
        $process=$process[0];
        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,personal.name,personal.profile_pic,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('process_id',$id)
                        ->where('visits.status_id',21)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic')
                        ->orderBy('visits.date')
                        ->get();
        $picturestake= Visits::join('visits_picture','visits_picture.visits_id','=','visits.id')
                                ->join('picture','picture.id','=','visits_picture.picture_id')
                                ->select('picture.description','picture.file','visits.date','visits.id as visitsid','visits.process_id as processid')
                                ->where('visits.process_id',$id)
                                ->orderBy('visits.date')
                                ->get();               
        $counter= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->where('process_id',$id)
                        ->get([ \DB::raw('(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total')]);
        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','request_to_client.users_id','status.name as status','status.id as statusid','visits.date','client_process.client_id','visits.id as visitid')
                                ->where('process.id','=',$id)
                                ->get();
        // return \View::make('emails/process_report',compact('process','visits','requestclient','counter','picturestake'));
        //-----INFORMACION PARA EL PDF ------------------
            $viewpdf =  \View::make('emails/process_report',compact('process','visits','requestclient','counter','picturestake'))->render(); 
             $name= 'Proyect_'.$process->process_number.'.pdf';
             $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            $pdf->save($path);
            return $pdf->stream('emails/process_report',compact('process','visits','requestclient','counter','picturestake'));


         
    } 
//---------------------------------------------------------------------------------------------- 
    
public function searchclient(Request $request){
    if($request->ajax()) {
        $client = Client::where('rif',$request->rif)->get();
        if(count($client)>0){
            return Response::json(['client'=>$client[0]]); 
        }else{
            return Response::json(['client'=>'null']); 
        }
     }   
      
    }
//----------------------------------------------------------------------------------------------
    
 public function sendmail($info,$name,$email,$subject,$path)
    {   
       $view='emails.plantilla';
       
       
      
                
       //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject,$path) {
                $m->from(env('SENDER_ADDRES'), env('SENDER_NAME'));
                $m->to($email, $name)->subject($subject);
                $m->bcc(env('COPY_ADDRES'), env('COPY_NAME'));
                if($path!='')$m->attach($path);

               
            });
       return $mail;

    }
//----------------------------------------------------------------------------------------------    
 public function showstatus($id){ 
       $roles = User::find(Auth::user()->id)->role()->get();
        $rol=$roles->first()->pivot->role_id;
       $process= Process::leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','status.id as statusid','client.*')
                                ->where('process.id','=',$id)
                                ->get();
        $process=$process[0];  
        
        $counter= Visits::where('process_id',$id)
                        ->get([ \DB::raw('(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total')]);
        $cantvisit= Visits::where('process_id',$id)
                        ->get([ \DB::raw('count(visits.id)  as total')]);
        //dd($process->end_date);exit;
        if($process->end_date!=null){
            $days= Process::where('process.id',$id)
                        ->get([ \DB::raw('EXTRACT(DAY FROM age(date(end_date),date(ini_date) ) ) as total')]);
        }else{
           $days= Process::where('process.id',$id)
                        ->get([ \DB::raw('EXTRACT(DAY FROM age(now(),date(ini_date) ) ) as total')]); 
        }
        
        //dd($counter);exit;

        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','request_to_client.users_id','status.name as status','status.id as statusid','visits.date','client_process.client_id','visits.id as visitid')
                                ->where('process.id','=',$id)
                                ->get();
        $status=ProcessStatus::join('process','process.id','=','process_status.process_id')
                       ->join('status','status.id','=','process_status.status_id')
                       ->join('users','users.id','=','process_status.users_id')
                       ->select('users.name','status.name as status','process_status.observation','process_status.created_at')
                       ->where('process.id',$id)
                       ->get();   

       return \View::make('process.status',compact('process','status','requestclient','rol','counter','cantvisit','days'));
    }      
     

}
