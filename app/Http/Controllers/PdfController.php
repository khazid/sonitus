<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GeneralInfo;
use App\GeneralType;
use App\JobType;
use App\Visits;
use App\RequestClient;
use App\Picture;
use App\Process;
use App\Personal;
use App\Event;
use App\Client;
use Input;
use Auth;
use File;
use Response;
use Mail;
use URL;

class PdfController extends Controller
{
     protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //$idvisit=31;
        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,visits.process_id,visits.signature,personal.name,personal.profile_pic,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('visits.id',$idvisit)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic')
                        ->orderBy('visits.date')
                        ->get();
        
        $visits=$visits[0];
        $process= Process::join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','client.*')
                                ->where('process.id','=',$visits->process_id)
                                ->get();
        $process=$process[0];  

        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','status.name as status','visits.date')
                                ->where('visits.id','=',$idvisit)->get();
        
        if(count($requestclient)>0)$requestclient=$requestclient[0]; 

        $jobt= Visits::join('visits_job_type','visits.id','=','visits_job_type.visits_id')
                         ->join('job_type','job_type.id','=','visits_job_type.job_type_id')
                         ->select('job_type.id','job_type.name')
                         ->where('visits.id','=',$idvisit)->get();
        
        
        $general= Visits::join('visits_general_info','visits.id','=','visits_general_info.visits_id')
                         ->join('general_info','general_info.id','=','visits_general_info.general_id')
                         ->select('general_info.id','general_info.name')
                         ->where('visits.id','=',$idvisit)->get();

        $generaltype= Visits::join('visits_general_type','visits.id','=','visits_general_type.visits_id')
                         ->join('general_type','general_type.id','=','visits_general_type.general_type_id')
                         ->select('general_type.type','general_type.name')
                         ->where('visits.id','=',$idvisit)
                         ->orderBy('type')
                         ->get(); 

        $gentype= GeneralType::join('visits_general_type','general_type.id','=','visits_general_type.general_type_id')
                               ->select('type')
                               ->where('visits_general_type.visits_id','=',$idvisit)
                               ->groupBy('type')
                               ->orderBy('type')
                               ->get();

        $picturestake= Visits::find($idvisit)->pictures;

        //-----INFORMACION PARA EL PDF -------------
         $viewpdf =  \View::make('emails/process_report',compact('process','visits','requestclient','jobt','general','generaltype','gentype','picturestake'))->render(); 
            $name= 'Visita_'.$visits->date.'_'.$visits->process_id.'.pdf';
            $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            $pdf->save($path);
            return $pdf->stream('emails/visit_report',compact('process','visits','requestclient','jobt','general','generaltype','gentype','picturestake'));
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id=2;
        $process= Process::leftjoin('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','process.signature','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','status.id as statusid','client.*')
                                ->where('process.id','=',$id)
                                ->get();
        $process=$process[0];
        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,personal.name,personal.profile_pic,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('process_id',$id)
                        ->where('visits.status_id',21)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic')
                        ->orderBy('visits.date')
                        ->get(); 
        $counter= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->where('process_id',$id)
                        ->get([ \DB::raw('(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total')]);
        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','request_to_client.users_id','status.name as status','status.id as statusid','visits.date','client_process.client_id','visits.id as visitid')
                                ->where('process.id','=',$id)
                                ->get();
        //return \View::make('emails/process_report',compact('process','visits','requestclient','counter'));
        $viewpdf =  \View::make('emails/process_report',compact('process','visits','requestclient','counter'))->render(); 
             // $name= 'Visita_'.$visits->date.'_'.$visits->process_id.'.pdf';
             // $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            // $pdf->save($path);
            return $pdf->stream('emails/process_report',compact('process','visits','requestclient','counter'));
         //return \View::make('pdf.sign');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function invoice() 
    {
       
            
        $data = $this->getData();
        $date = date('Y-m-d');
        $invoice = "2222";
        $view =  \View::make('pdf.invoice', compact('data', 'date', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('pdf/invoice');
    }

    public function getData() 
    {
        $data =  [
            'quantity'      => '1' ,
            'description'   => 'some ramdom text',
            'price'   => '500',
            'total'     => '500'
        ];
        return $data;
    }
}
