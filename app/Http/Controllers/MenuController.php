<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MenuRol as MenuRol;    
use App\Menu as Menu;
use App\Roles as Roles;
use Response;

class MenuController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all(); 
        return \View::make('menu/list',compact('menus'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return \View::make('menu/new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu;
        $menu->create($request->all());
        
        //asignacion de la opcion del menu con estado inactivo a los roles del sistema
          $menus=Menu::all();
          $menusnew=Menu::find($menus->last()->id);        
          $rol=Roles::all();
       
        foreach($rol as $r){
            if($r->id == 1 || $r->id == 2 ) {
                $menusnew->roles()->attach($r->id,['active'=>true]);
           }else {
                $menusnew->roles()->attach($r->id,['active'=>false]);
           }
        }

        //----------------------------------------
        
        //******************************************
        // Al crear una nueva opcion de menu se le  
        //asignan todos los roles y desde el  
        //administrador de rol se le activan 
        //las necesarias
        //******************************************
        return redirect('menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                $menu = Menu::find($id);
                return \View::make('menu/update',compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatemenu(Request $request)
    {
        if($request->ajax()) {
        $menu = Menu::find($request->id);
                $menu->name = $request->name;
                $menu->position = $request->position;
                 $menu->icon = $request->icon;
        $menu->save();
        }
               return Response::json(['update'=>$menu]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $menu = Menu::find($id);
        $menu->delete();
        $rol=Roles::all();
       
        foreach($rol as $r){
           $menu->roles()->detach($r->id);
        }
        return redirect()->back();
    }

    public function search(Request $request){
         $menus = Menu::where('name','like','%'.$request->name.'%')->get();
         return \View::make('menu/list', compact('menus'));
        
    }
}
