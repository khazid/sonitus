<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GeneralInfo;
use App\GeneralType;
use App\JobType;
use App\Visits;
use App\RequestClient;
use App\Picture;
use App\Process;
use App\Personal;
use App\Event;
use App\Client;
use App\User;
use App\Holidays;
use Input;
use Auth;
use File;
use Response;
use Mail;
use URL;

class VisitsController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$process_id=$id;
        $roluser= User::find(Auth::user()->id)->role;
        $rol=$roluser[0]->id;
        $perid= Auth::personal()->id;
        $visits= Visits::join('process','process.id','=','visits.process_id')
                        ->join('client_process','client_process.process_id','=','visits.process_id')
                        ->join('client','client.id','=','client_process.client_id')
                        ->join('status','status.id','=','visits.status_id')
                        ->join('personal_visits','visits.id','=','personal_visits.visits_id')
                        ->join('personal','personal.id','=','visits.assigned_id')
                        ->select('process_number','visits.*','client.*','visits.id as visitid','status.name as status','personal.name as lider');
                       

        $visittec=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','personal.id as personalid','visits.id')
                          ->get();
                        
        if($rol != 5){ 
            $visits=$visits->groupBy("visits.id","process_number", "client.id", "status.name", "personal.name")->orderBy('visits.date')->get();
                      
        }else if($rol == 5){ //tecnico
            
            $visits=$visits->whereRaw('("visits"."assigned_id" = '.$perid.' or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = '.$perid.') )')
            ->groupBy("visits.id","process_number", "client.id", "status.name", "personal.name")
            ->orderBy('visits.date')->get();
        }  
         $personal = Personal::whereRaw("occupation in ('Tecnico','Jefe tecnico')")->whereNotNull('users_id')->lists('name','id')->prepend('Seleccione', 'null');
       

        return \View::make('visits/visits',compact('visits','personal','rol','visittec','perid')); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,visits.process_id,visits.signature,personal.name,personal.profile_pic,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('visits.id',$id)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic')
                        ->orderBy('visits.date')
                        ->get();
        $visits=$visits[0];
        $visittec=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','visits.id')
                          ->where('visits.id',$id)
                          ->get();
        $process= Process::join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','client.*')
                                ->where('process.id','=',$visits->process_id)
                                ->get();
        $process=$process[0];  

        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','status.name as status','visits.date')
                                ->where('visits.id','=',$id)->get();
         if(count($requestclient)>0)$requestclient=$requestclient[0]; 

        $jobt= Visits::join('visits_job_type','visits.id','=','visits_job_type.visits_id')
                         ->join('job_type','job_type.id','=','visits_job_type.job_type_id')
                         ->select('job_type.id','job_type.name')
                         ->where('visits.id','=',$id)->get();
        
        
        $general= Visits::join('visits_general_info','visits.id','=','visits_general_info.visits_id')
                         ->join('general_info','general_info.id','=','visits_general_info.general_id')
                         ->select('general_info.id','general_info.name')
                         ->where('visits.id','=',$id)->get();

        $generaltype= Visits::join('visits_general_type','visits.id','=','visits_general_type.visits_id')
                         ->join('general_type','general_type.id','=','visits_general_type.general_type_id')
                         ->select('general_type.type','general_type.name')
                         ->where('visits.id','=',$id)
                         ->orderBy('type')
                         ->get(); 

        $gentype= GeneralType::join('visits_general_type','general_type.id','=','visits_general_type.general_type_id')
                               ->select('type')
                               ->where('visits_general_type.visits_id','=',$id)
                               ->groupBy('type')
                               ->orderBy('type')
                               ->get();

        $picturestake= Visits::find($id)->pictures;                        
        return \View::make('visits.visit_report',compact('process','visits','requestclient','jobt','general','generaltype','gentype','picturestake','visittec'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $userid= Auth::user()->id;
        //dd($request->visits_id);exit;
        //guardar la visita
            $visit = Visits::find($request->visits_id);
                $visit->description = $request->jobobservation;
               // $visit->date = date('Y-m-d');
                $visit->entrance_hr = $request->entrada;
                if($request->salida!='00:00'){
                    $visit->exit_hr = $request->salida;
                    $visit->status_id = 50;
                }else{
                    $visit->status_id = 19;
                }
                
                $visit->users_id = $userid;
                
            $visit->save();

        //guardar relaciones del job type
        $jobstype = Input::get('job');
        foreach($jobstype as $c){
            $jobs=JobType::find($c);
            $jobs->visits()->attach($visit->id,['users_id'=>$userid]);
        }

        //guardar relaciones del general_info
        $general = Input::get('gen');
        //dd($general);exit;
        foreach($general as $c){
            $geneinfo=GeneralInfo::find($c);
            $geneinfo->visits()->attach($visit->id,['users_id'=>$userid]);
        }

        //guardar relaciones del general_type 
        $genertyp = Input::get('gendes');
        foreach($genertyp as $c){
            $genertype=GeneralType::find($c);
            $genertype->visits()->attach($visit->id,['users_id'=>$userid]);
        }

        //guardar fotos
        $photos = Input::get('photo');
        
         if($photos){//si existen fotos
          $descrip = Input::get('descript');
          $i=1;
           foreach($photos as $c){
              
                  $this->savefoto($c,$request->descript[$i],$request->process_id,$userid,$visit->id,$i);

            $i++; 
           }
            $visit->have_foto = TRUE;
            $visit->save();
        }
       
        //guardar solicitudes al cliente
        if($request->requestclient){
                  $reqcl = new RequestClient;
                        $reqcl->visits_id = $visit->id;
                        $reqcl->users_id = $userid;
                        $reqcl->status_id = 14;
                        $reqcl->request = $request->requestclient;
                       
                    $reqcl->save();

                     //se actualiza la tabla de visitas para incdicar que tiene una solicitud
                    $visit->have_request = TRUE;
                    $visit->save();
        }
        
        
        //Actualizar informacion del proceso (status en proceso)
          $visitold= Visits::where('process_id','=',$request->process_id)->whereRaw('status_id in (19,21)')->get();
          //dd(count($visitold));exit;
            if(count($visitold)==1){ //primera visita
              $pro = Process::find($request->process_id);
                  $pro->status_id = 3;
                  $pro->ini_date = date('Y-m-d');
              if($pro->save()){
                $pro->status()->attach(3,['observation'=>'System:Primera visita realizada cambio de estado En proceso','users_id'=>$userid,'date'=>date('Y-m-d')]);

              }
            }
        

        //retornamos al listado de procesos con un mensaje de creacion exitoso

        // Session::flash('message','Se guardo la informacion de la visita al proceso Numero: "'.$request->procesnumber.'" ');
        
        if($request->salida!='00:00'){
            $visit=Visits::find($request->visits_id);
            $type='visit';
            $process_id=$visit->process_id;
            return \View::make('visits/signature',compact('visit','type','process_id'));
        }else{
            return redirect(route('app/dashboard'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editvisit=true;
        $route="visits/update";
        $jobt= JobType::all();
        $general= GeneralInfo::all();
        $generaltype= GeneralType::orderBy('type','name')->get();
        $gentype= GeneralType::select('type')->groupBy('type')->orderBy('type')->get();

        $process= Visits::join('process', 'process.id', '=', 'visits.process_id')
                                ->join('users', 'users.id', '=', 'process.users_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','process.job_type','users.id as iduser','users.name as username','process.ini_date','process.web','process.end_date','status.name as status','client.*','visits.entrance_hr','visits.description as visitdescrp','visits.id as visitsid')
                                ->where('visits.id','=',$id)
                                ->get();
        $process= $process[0];

        $jobtold= Visits::join('visits_job_type','visits.id','=','visits_job_type.visits_id')
                         ->join('job_type','job_type.id','=','visits_job_type.job_type_id')
                         ->select('job_type.id')
                         ->where('visits.id','=',$id)->get();
        
        
        $generalold= Visits::join('visits_general_info','visits.id','=','visits_general_info.visits_id')
                         ->join('general_info','general_info.id','=','visits_general_info.general_id')
                         ->select('general_info.id')
                         ->where('visits.id','=',$id)->get();

        $generaltypeold= Visits::join('visits_general_type','visits.id','=','visits_general_type.visits_id')
                         ->join('general_type','general_type.id','=','visits_general_type.general_type_id')
                         ->select('general_type.id')
                         ->where('visits.id','=',$id)->get();
        $picturestake= Visits::find($id)->pictures; 
        $countake =count($picturestake);                
        //dd($generaltypeold);exit;

         return \View::make('process/new',compact('process','jobt','general','generaltype','gentype','jobtold','generalold','generaltypeold','editvisit','atributes','route','picturestake','countake'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $userid= Auth::user()->id;
       $idvisits= $request->visits_id;
        //guardar la visita

            $visit = Visits::find($idvisits);
                $visit->description = $request->jobobservation;
                if($request->salida!='00:00'){
                    $visit->exit_hr = $request->salida;
                    $visit->status_id = 50;//Done
                }
            $visit->save();
//-------------------------------------------------------------------------
        //eliminar relaciones anteriores del job type
        $jobt= JobType::all(); 
        foreach($jobt as $b){
            $visit->jobs()->detach($b->id);
        }
        //Guardar relacion nueva
        $jobstype = Input::get('job');
        foreach($jobstype as $c){
            $jobs=JobType::find($c);
            $jobs->visits()->attach($visit->id,['users_id'=>$userid]);
        }
//-------------------------------------------------------------------------
        //eliminar relaciones anteriores del general_info
        $geni= GeneralInfo::all(); 
        foreach($geni as $b){
            $visit->generalinfo()->detach($b->id);
        }
        //guardar relaciones del general_info
        $general = Input::get('gen');
        //dd($general);exit;
        foreach($general as $c){
            $geneinfo=GeneralInfo::find($c);
            $geneinfo->visits()->attach($visit->id,['users_id'=>$userid]);
        }
//-------------------------------------------------------------------------
        //eliminar relaciones anteriores del general_type
        $gent= GeneralType::all(); 
        foreach($gent as $b){
            $visit->generaltype()->detach($b->id);
        }
        //guardar relaciones del general_type 
        $genertyp = Input::get('gendes');
        foreach($genertyp as $c){
            $genertype=GeneralType::find($c);
            $genertype->visits()->attach($visit->id,['users_id'=>$userid]);
        }
//-------------------------------------------------------------------------
        //guardar fotos
        $photos = Input::get('photo');
        
         if($photos){//si existen fotos
          $descrip = Input::get('descript');
          $i=$request->countini +1;
           foreach($photos as $c){
              
                  $this->savefoto($c,$request->descript[$i],$request->process_id,$userid,$visit->id,$i);

            $i++; 
           }
            $visit->have_foto = TRUE;
            $visit->save();
        }
//-------------------------------------------------------------------------       
        //guardar solicitudes al cliente
        if($request->requestclient){
            
            if($visit->have_request){//si se tiene una solicitud ya guardada en la visita

                 $reqcl = Visits::find($visit->id)->requestclient;
                 //dd($reqcl);exit;
            }else{
                $reqcl = new RequestClient;
                  $reqcl->visits_id = $visit->id;
                  $reqcl->users_id = $userid;
                  $reqcl->status_id = 14;
                  $reqcl->request = $request->requestclient;
                 
              $reqcl->save();
              //se actualiza la tabla de visitas para incdicar que tiene una solicitud
              $visit->have_request = TRUE;
              $visit->save();
          }

        }
        
//-------------------------------------------------------------------------        
        
        //retornamos al listado de procesos con un mensaje de creacion exitoso
 
        //Session::flash('message','Se actualizo la informacion de la visita al proceso Numero: "'.$request->procesnumber.'" ');
      
        //return redirect('visits/show/'.$request->process_id);
        if($request->salida!='00:00'){
            $visit=Visits::find($idvisits);
            $type='visit';
            $process_id=$visit->process_id;
            return \View::make('visits/signature',compact('visit','type','process_id'));
        }else{
            return redirect(route('app/dashboard'));
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the new assign for the normal process..
     *
     * @return \Illuminate\Http\Response
     */
    public function findprocess($id)
    {   
        $route="visits/create";
        $editvisit=false;
        $jobt= JobType::all();
        $general= GeneralInfo::all();
        $generaltype= GeneralType::orderBy('type','name')->get();
        $gentype= GeneralType::select('type')->groupBy('type')->orderBy('type')->get();
        $process= Process::join('visits', 'visits.process_id', '=', 'process.id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','process.job_type','process.ini_date','process.end_date','client.*','visits.id as visitsid','visits.status_id as statusid')
                                ->where('visits.id','=',$id)
                                ->get();
        $process= $process[0];                     
        $countake =0;

        
        return \View::make('process/new',compact('process','jobt','general','generaltype','gentype','editvisit','atributes','route','countake'));
    }
//----------------------------------------------------------------------------------------------
     public function savefoto($photo,$description,$idprocess,$idsupport,$idvisit,$correlativo)
    {   $userid= Auth::user()->id;
        //$filemaker = new File;
        $photo = str_replace('data:image/png;base64,', '', $photo);
        $photo = str_replace(' ', '+', $photo);
        $description= $description;
        $fileData = base64_decode($photo);
        $fileName = $idprocess.$idvisit.$idsupport.$correlativo.'.jpg';

        //se crea el directorio donde ira la imagen
        $directory = public_path().'/images/equipment/'.$idprocess;

        if(! File::isDirectory($directory)){//verifica que existe el directorio del proceso
         File::makeDirectory($directory, $mode = 0777, true, true);
         $directory =$directory.'/'.$idvisit;

           if(! File::isDirectory($directory)){//verifica si existe el directorio de la visita
              File::makeDirectory($directory, $mode = 0777, true, true);
            }

        }else{
          $directory =$directory.'/'.$idvisit;
          
          if(! File::isDirectory($directory)){//verifica si existe el directorio de la visita
            File::makeDirectory($directory, $mode = 0777, true, true);
          }

        }

        $directory =$directory.'/'.$fileName; //se le agrega el nombre del archivo a la ruta del directorio
        //--------------------------------
        $picture = new Picture;
        $picture->file = $fileName;
        $picture->description = $description;
        
        if($picture->save()){

            $picture->visits()->attach($idvisit,['users_id'=>$userid]);
            $result = file_put_contents($directory, $fileData);
            

        }
        
    }
//----------------------------------------------------------------------------------------------    

    public function buscarphotos($id)
    {   
       
        $id_visita=$id;
        
        $photo= Visits::join('visits_picture','visits_picture.visits_id','=','visits.id')
                                ->join('picture','visits_picture.picture_id','=','picture.id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->join('client','client.id','=','client_process.client_id')
                                ->select('picture.file','picture.description','visits.process_id','visits.id as visitid','process_number', 'client.name as client','visits.date')
                                ->where('visits_id','=',$id_visita)
                                ->get();
       return \View::make('visits/visit_gallery',compact('photo'));
      
       

    }
//----------------------------------------------------------------------------------------------    

    public function deletephoto(Request $request)
    {   
       if($request->ajax()) {
        $id_visita=$request->visitid;
        $file=$request->file;
        $id_pic=$request->picid;

        $delete=File::delete(public_path().'/images/equipment'.$file);

       if($delete){
            
            $visit=Visits::find($id_visita);
            $visit->pictures()->detach($id_pic);

            $picture=Picture::find($id_pic);
            $picture->delete();

            
        }

       return Response::json(['deleted'=>public_path().'/images/equipment'.$file]);
       
      }
       

    }
//---------------------------------------------------------------------------------------------- 
public function buscarsolicitudes(Request $request)
    {   
       if($request->ajax()) {
        $id_visita=$request->idvisit;
        
        $request= Visits::join('request_to_client','request_to_client.visits_id','=','visits.id')
                                ->join('status','request_to_client.status_id','=','status.id')
                                ->select('status.name','request_to_client.request')
                                ->where('visits.id','=',$id_visita)
                                ->get();
        //dd($request);exit;
        if(count($request)>0){
            return Response::json(['estado'=>$request[0]->name,'request'=>$request[0]->request,'bool'=>TRUE]);
        }else{
            return Response::json(['bool'=>FALSE]);
        }
        
       }
       

    }
//----------------------------------------------------------------------------------------------
 public function saveouthr(Request $request)
    {   
       if($request->ajax()) {
        $id_visita=$request->idvisit;
        
        $visita= Visits::find($id_visita);
        $visita->exit_hr=$request->outhr;
        $visita->save();
        
        return Response::json(['save'=>$visita]);
        //return \View::make('video/video',compact('historias','id_user','aseg','edad'));
       }
       

    }
//----------------------------------------------------------------------------------------------
     public function calendar()
    {   
       $visita= Visits::all();
       $teclider= Personal::whereRaw("occupation in ('Jefe tecnico')")->whereNotNull('users_id')->lists('personal.name','personal.id')->prepend('Select', 'null');
       $tecnicos= Personal::whereRaw("occupation in ('Tecnico','Jefe tecnico')")->whereNotNull('users_id')->lists('personal.name','personal.id')->prepend('Select', 'null');
       $clients= Client::orderBy('name')->lists('name','name')->prepend('Select', 'null');
       
       return \View::make('visits/visit_calendar',compact('visita','tecnicos','projects','clients','teclider'));

    }
//----------------------------------------------------------------------------------------------
     public function searchclients(Request $request)
    {   
       if($request->ajax()) {

           $data = array(); //declaramos un array principal que va contener los datos
            $destinatary = Client::all(); //listamos todos los id de los eventos
            $i=0;
            foreach($destinatary as $eve){
                $data[$i] = array(
                    "data"=>$eve->id,
                    "value"=>$eve->name, 
                     
                );
             $i++;   
            }
                //$data=json_encode($data);
            return Response::json(['data'=>$data]); 
           
         }

    }
//----------------------------------------------------------------------------------------------
     public function searchprojects(Request $request)
    {   
       if($request->ajax()) {
        $projects= Process::join('client_process','client_process.process_id','=','process.id')
                            ->join('client','client_process.client_id','=','client.id')
                            ->select('process.id','process_number')
                           ->whereRaw('status_id in ('.$request->type.')')
                           ->where('client.id',$request->clientid)
                           ->get();
        if(count($projects)>0){
            return Response::json(['projects'=>$projects]);

        }else{
            return Response::json(['projects'=>404]);
        }
       }
      

    }

//----------------------------------------------------------------------------------------------
     public function calendarscheduled(Request $request)
    {   
       
      
        $data = array(); //declaramos un array principal que va contener los datos
        $event = Event::all(); //listamos todos los id de los eventos
        $i=0;
        foreach($event as $eve){
            if($eve->type=='visit'){
                $color='#01A9DB';
            }else if($eve->type=='general'){
                $color='#FE9A2E';
            }else{
                $color='#D8D8D8';
            }
            $data[$i] = array(
                "id"=>$eve->id,
                "title"=>$eve->title, //obligatoriamente "title", "start" y "url" son campos requeridos
                "start"=>$eve->startdate, //por el plugin asi que asignamos a cada uno el valor correspondiente
                "end"=>$eve->enddate, 
                "allDay"=>$eve->allday,
                "url"=>"",
                "description"=>$eve->description,
                "visitid"=>$eve->visits_id,
                "personalid"=>$eve->personal_id,
                "color"=>$color 
            );
         $i++;   
        }

        $holidays = Holidays::all(); //listamos todos los id de los eventos
        foreach($holidays as $h){
            $data[$i] = array(
                "id"=>$h->id,
                "title"=>$h->description, //obligatoriamente "title", "start" y "url" son campos requeridos
                "start"=>$h->date.' 00:00:00', //por el plugin asi que asignamos a cada uno el valor correspondiente
                "end"=>$h->date.' 23:59:00', 
                "allDay"=>true,
                "url"=>"",
                "description"=>'',
                "visitid"=>'',
                "personalid"=>'',
                "color"=>'#9A2EFE' 
            );
         $i++;   
        }
            $data=json_encode($data);
        
       return $data;
       //return Response::json(['all'=>$data]);
    

    }
//----------------------------------------------------------------------------------------------
    public function schedule(Request $request)
    {   $usersid=Auth::user()->id;
       if($request->ajax()) {
        
        if($request->eventtype=='visit'){
                $visit= new Visits;
                $visit->process_id=$request->processid;//numero de proceso
                $visit->users_id=$usersid;
                $visit->date=$request->start;
                $visit->status_id=18;
                $visit->assigned_id=$request->teclider;
                $visit->save();    
                //asignacion de tecnicos a la visita
                $tecnicos =$request->tecnician;
                foreach($tecnicos as $c){
                    $visitper=Personal::find($c);
                    $visitper->visit()->attach($visit->id,['users_id'=>$usersid]);
                }
                //Actualizar informacion del proceso 
                  $visitold= Visits::where('process_id','=',$request->processid)->get();
                  
                    if(count($visitold)==1){ //primera visita
                      $pro = Process::find($request->processid);
                          $pro->ini_date = $request->start;
                      $pro->save();
                    }
        }
        $event = new Event;
        $event->title=$request->title;
        $event->description=$request->description;
        $event->type=$request->eventtype;
        $event->startdate=$request->start;
        $event->enddate=$request->end;
        $event->allday=$request->allday;
        $event->users_id=$usersid;
        if($request->eventtype=='visit'){
                    $event->visits_id= $visit->id;
                    $event->personal_id=$request->teclider;
        }else if($request->eventtype=='cobranza'){
                    $event->process_id= $request->processid;
                    $event->personal_id=$request->teclider;
                   }  
        
        if($event->save()){ 

        //-----INFORMACION PARA EL CORREO ------------------
            $personal=Personal::find($request->teclider);
                    $name=$personal->name;
                    $email= $personal->email;

            $content['content'][0]='El siguiente correo tiene la finalidad de informarle sobre la asignacion de una nueva visita para el dia '.$request->start.' Titulo: '.$request->title.' Descripcion: '.$request->description;
            $info['content'][1]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';   
            $content['title']='Estimado '.$personal->name; 
            $subject='Notificacion de Asignacion de Nueva Visita'; 
             $mail=$this->sendmail($content,$name,$email,$subject,'');
           }


        }
       
        return Response::json(['save'=>$event]);
    } 
//---------------------------------------------------------------------------------------------
    public function deletevent(Request $request)
    {   
       if($request->ajax()) {
         $event = Event::find($request->id);
         $event->delete();
         if($event->type=='visit'){
            $visit=Visits::find($event->visits_id);
            $visit->delete();
         }
       }
      return Response::json(['deleted'=>$event]);

    }
//---------------------------------------------------------------------------------------------
    public function updateevent(Request $request) //guardar campos editados en el calendario
    {   
       if($request->ajax()) {
         $event = Event::find($request->id);
        $event->title=$request->title;
        $event->description=$request->description;
        $event->startdate=$request->start;
        $event->enddate=$request->end;
        $event->allday=$request->allday;
        $event->save();
       }
      return Response::json(['saved'=>$event]);

    }
//----------------------------------------------------------------------------------------------

 public function searchvisitdetail(Request $request) //editar evento de calendario
    {   
       if($request->ajax()) {
        
        $visittec=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','visits.id')
                          ->where('visits.id',$request->visitid)
                          ->get();
                       
        $client=Visits::join('process','process.id','=','visits.process_id')
                        ->join('client_process','process.id','=','client_process.process_id')
                        ->join('client','client.id','=','client_process.client_id')
                        ->select('client.name','process_number','visits.assigned_id')
                        ->where('visits.id',$request->visitid)
                        ->get();
        $teclider=Personal::find($client[0]->assigned_id);   
        }
      return Response::json(['tecnico'=>$visittec,'client'=>$client,'teclider'=>$teclider]);
       

    }

//----------------------------------------------------------------------------------------------
    public function requesttoclient(Request $request)
    {   
       if($request->ajax()) {
          $id=$request->id;
          $clientid=$request->clientid;
          $personalid=$request->personalid;
          //$personalid=3;//envio de correo a kamaly cambiar para que sea con el del personal
          $description=$request->description;
          $type=$request->type;
          $visitid=$request->visitid;
          

          $requestclient=RequestClient::find($id);

          switch($type){
            case 'accept': case 'edit': $requestclient->status_id=15;$requestclient->request=$description;
                            if($requestclient->save()){
                                $personal=Client::find($clientid);//cliente
                                        $name=$personal->name;
                                        $email= $personal->email;

                                $info=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                    ->join('status','status.id','=','request_to_client.status_id')
                                    ->join('process','process.id','=','visits.process_id')
                                    ->join('client_process','process.id','=','client_process.process_id')
                                    ->join('client','client.id','=','client_process.client_id')
                                    ->select('request_to_client.request','visits.date','client.name','process.process_number')
                                    ->where('request_to_client.id',$id)
                                    ->get(); 
                                $content['content'][0]='Se le ha realizado una solicitud durante la visita realizada el dia '.date('d/m/Y',strtotime($info[0]->date)).' con la descripcion: "'.$info[0]->request.'" del proyecto #'.$info[0]->process_number.'.';
                                $content['content'][1]='Por favor notificar la realizacion del mismo ingresando al portal de Inversiones Sonitus con su clave y contraseña previamente enviada, presionar el numero del proyecto correspondiente a la solicitud y seleccionando el boton de "Done" en el detalle del proyecto En la solicitud indicada. (Lado Derecho de la informacion del proyecto ) ';
                                $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';   
                                $content['title']='Estimado Cliente '.$info[0]->name; 
                                $subject='Nueva Solicitud al Cliente';           
                                
                                $this->sendmail($content,$name,$email,$subject,'');
                            }

                            break; 
            case 'denied':  $requestclient->status_id=16; $requestclient->observation=$description;
                            if($requestclient->save()){
                                $personal=Personal::find($personalid); //lider del proyecto
                                        $name=$personal->name;
                                        $email= $personal->email;

                                $info=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                    ->join('status','status.id','=','request_to_client.status_id')
                                    ->join('process','process.id','=','visits.process_id')
                                    ->join('client_process','process.id','=','client_process.process_id')
                                    ->join('client','client.id','=','client_process.client_id')
                                    ->select('request_to_client.request','visits.date','client.name','process.process_number')
                                    ->where('request_to_client.id',$id)
                                    ->get(); 
                                $content['content'][0]='La solicitud que se realizo al cliente '.$info[0]->name.'  en la visita  '.date('d/m/Y',strtotime($info[0]->date)).' con la descripcion "'.$info[0]->request.'" del proyecto # '.$info[0]->process_number.' fue "RECHAZADA" con la observacion "'.$decription.'".';
                                $content['title']='Atencion!!';
                                $subject='Actualizacion de Solicitud al Cliente';          
                                
                                $this->sendmail($content,$name,$email,$subject,'');
                            }
                            break;
            
            case 'done':$requestclient->status_id=17;
                        if($requestclient->save()){
                            $personal=User::find($personalid); //lider del proyecto y agregar jefe tecnico
                                    $name=$personal->name;
                                    $email= $personal->email;
                            $info=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->join('client','client.id','=','client_process.client_id')
                                ->select('request_to_client.request','visits.date','client.name','process.process_number')
                                ->where('request_to_client.id',$id)
                                ->get(); 
                            $content['content'][0]='La solicitud que se realizo al cliente '.$info[0]->name.'  en la visita  '.date('d/m/Y',strtotime($info[0]->date)).' con la descripcion "'.$info[0]->request.'" ya fue marcada como "REALIZADA" por el cliente.';
                            $content['content'][1]='Por favor verifique esta informaciion para continuar con el proyecto # '.$info[0]->process_number.'.';
                            $content['title']='Atencion!!';
                            $subject='Actualizacion de Solicitud al Cliente';           
                            
                            $this->sendmail($content,$name,$email,$subject,'');
                        }
                        break;
            case 'notify': 
                            $personal=Client::find($clientid);//cliente
                                        $name=$personal->name;
                                        $email= $personal->email;

                                $info=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                    ->join('status','status.id','=','request_to_client.status_id')
                                    ->join('process','process.id','=','visits.process_id')
                                    ->join('client_process','process.id','=','client_process.process_id')
                                    ->join('client','client.id','=','client_process.client_id')
                                    ->select('request_to_client.request','visits.date','client.name','process.process_number')
                                    ->where('request_to_client.id',$id)
                                    ->get(); 
                                $content['content'][0]='Le recordamos Realizar o Notificar la solicitud de la visita realizada el dia '.date('d/m/Y',strtotime($info[0]->date)).' con la descripcion: "'.$info[0]->request.'" del proyecto #'.$info[0]->process_number.'.';
                                $content['content'][1]='Por favor notificar la realizacion del mismo ingresando al portal de Inversiones Sonitus con su clave y contraseña previamente enviada, presionar el numero del proyecto correspondiente a la solicitud y seleccionando el boton de "Done" en el detalle del proyecto En la solicitud indicada. (Lado Derecho de la informacion del proyecto ) ';
                                $content['content'][2]='Si necesita informacion adicional puede enviar un mensaje a traves del portal en la opcion de "Envio de Mensajes" al ingresar al detalle del proyecto correspondiente o llamar a traves de los numeros telefonicos +(58)(212)5778891.';   
                                $content['title']='Estimado Cliente '.$info[0]->name; 
                                $subject='Recordatorio para la Realizacion o Notificacion de Solicitud realizada por Sonitus';           
                                
                                $this->sendmail($content,$name,$email,$subject,'');
                            

                            break; 
              default:$requestclient='Nothing';
                        break;           

          }

        
       
        return Response::json(['requestclient'=>$requestclient]);
        
        
       }
       

    }
//----------------------------------------------------------------------------------------------
    
public function sendmail($info,$name,$email,$subject,$path)
    {   
        $view='emails.email';
       
       //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject,$path) {
                $m->from(env('SENDER_ADDRES'), env('SENDER_NAME'));
                $m->to($email, $name)->subject($subject);
                $m->bcc(env('COPY_ADDRES'), env('COPY_NAME'));
                if($path!='')$m->attach($path);

               
            });
       return $mail;

    }
//----------------------------------------------------------------------------------------------
     public function savesignature(Request $request)
    {   
        if($request->ajax()) {
        
        $idvisit=$request->visitid;
        $idprocess=$request->processid;
        $sing=$request->signature;
        $userid= Auth::user()->id;
        //$filemaker = new File;
        $sing = str_replace('data:image/png;base64,', '', $sing);
        $sing = str_replace(' ', '+', $sing);
        $fileData = base64_decode($sing);
        $fileName = $idprocess.$idvisit.'_sig.jpg';

        //se crea el directorio donde ira la imagen
        $directory = public_path().'/images/equipment/'.$idprocess;

        if(! File::isDirectory($directory)){//verifica que existe el directorio del proceso
         File::makeDirectory($directory, $mode = 0777, true, true);
         $directory =$directory.'/'.$idvisit;

           if(! File::isDirectory($directory)){//verifica si existe el directorio de la visita
              File::makeDirectory($directory, $mode = 0777, true, true);
            }

        }else{
          $directory =$directory.'/'.$idvisit;
          
          if(! File::isDirectory($directory)){//verifica si existe el directorio de la visita
            File::makeDirectory($directory, $mode = 0777, true, true);
          }

        }

        $directory =$directory.'/'.$fileName; //se le agrega el nombre del archivo a la ruta del directorio
        //--------------------------------
        $visit = Visits::find($idvisit);
        $visit->signature = $fileName;
        $visit->status_id = 47;
        
        if($visit->save()){

            $result = file_put_contents($directory, $fileData);
            $this->pdf($idvisit);
            
        }

        return Response::json(['signature'=>$result,'urlresponse'=>URL::route('visits.index')]);
    }
        
}
//----------------------------------------------------------------------------------------------

public function pdf($idvisit)
    {
         
        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,visits.process_id,visits.signature,personal.name,personal.profile_pic,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('visits.id',$idvisit)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic')
                        ->orderBy('visits.date')
                        ->get();
        
        $visits=$visits[0];
        $visittec=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','visits.id')
                          ->where('visits.id',$idvisit)
                          ->get();
        $process= Process::join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','client.*')
                                ->where('process.id','=',$visits->process_id)
                                ->get();
        $process=$process[0];  

        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','status.name as status','visits.date')
                                ->where('visits.id','=',$idvisit)->get();
        
        if(count($requestclient)>0)$requestclient=$requestclient[0]; 

        $jobt= Visits::join('visits_job_type','visits.id','=','visits_job_type.visits_id')
                         ->join('job_type','job_type.id','=','visits_job_type.job_type_id')
                         ->select('job_type.id','job_type.name')
                         ->where('visits.id','=',$idvisit)->get();
        
        
        $general= Visits::join('visits_general_info','visits.id','=','visits_general_info.visits_id')
                         ->join('general_info','general_info.id','=','visits_general_info.general_id')
                         ->select('general_info.id','general_info.name')
                         ->where('visits.id','=',$idvisit)->get();

        $generaltype= Visits::join('visits_general_type','visits.id','=','visits_general_type.visits_id')
                         ->join('general_type','general_type.id','=','visits_general_type.general_type_id')
                         ->select('general_type.type','general_type.name')
                         ->where('visits.id','=',$idvisit)
                         ->orderBy('type')
                         ->get(); 

        $gentype= GeneralType::join('visits_general_type','general_type.id','=','visits_general_type.general_type_id')
                               ->select('type')
                               ->where('visits_general_type.visits_id','=',$idvisit)
                               ->groupBy('type')
                               ->orderBy('type')
                               ->get();

        $picturestake= Visits::find($idvisit)->pictures;

        //-----INFORMACION PARA EL PDF -------------
         $viewpdf =  \View::make('emails/visit_report',compact('process','visits','requestclient','jobt','general','generaltype','gentype','picturestake','visittec'))->render(); 
            $name= 'Visita_'.date('Ymd',strtotime($visits->date)).'_'.$visits->process_id.'.pdf';
            $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            //return $pdf->stream('emails/visit_report',compact('process','visits','requestclient','jobt','general','generaltype','gentype','picturestake'));
            $pdf->save($path);


        //-----INFORMACION PARA EL CORREO ------------------
           //  $personal=Client::find($process->id);
           //          $name=$personal->name;
           //          $email= $personal->email;

           //  $content['content'][0]='El siguiente correo tiene la finalidad de informarle sobre la visita realizada el dia de hoy del proyecto #'.$process->process_number.'.';
           //  $content['content'][1]='Adjunto encontrara el informe correspondiente con la informacion levantada en la visita';
           //  $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';   
           //  $content['title']='Estimado Cliente '.$personal->name; 
           //  $subject='Reporte de Visita'; 
           //   $mail=$this->sendmail($content,$name,$email,$subject,$path);
           // if($mail)File::Delete($path);

        
         
    }

    //----------------------------------------------------------------------------------------------

public function sendpdf($idvisit)
    {
         
        $visits= Visits::where('visits.id',$idvisit)
                        ->get();
        
        $visits=$visits[0];

        $process= Process::join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('client.id as clientid','process.*')
                                ->where('process.id','=',$visits->process_id)
                                ->get();
        $process=$process[0]; 
        

        //-----INFORMACION PARA EL PDF -------------
         
            $name= 'Visita_'.date('Ymd',strtotime($visits->date)).'_'.$visits->process_id.'.pdf';
            $path= public_path().'/pdf/'.$name;
            

        //-----INFORMACION PARA EL CORREO ------------------
            $personal=Client::find($process->clientid);
                    $name=$personal->name;
                    $email= $personal->email;

            $content['content'][0]='El siguiente correo tiene la finalidad de informarle sobre la visita realizada el dia de hoy del proyecto #'.$process->process_number.'.';
            $content['content'][1]='Adjunto encontrara el informe correspondiente con la informacion levantada en la visita';
            $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';   
            $content['title']='Estimado Cliente '.$personal->name; 
            $subject='Reporte de Visita'; 
             $mail=$this->sendmail($content,$name,$email,$subject,$path);
           if($mail){
                File::Delete($path);
                $visit = Visits::find($idvisit);
                $visit->status_id = 21;
                $visit->save();
           }

           return redirect(route('visits.index'));
        
         
    }

//----------------------- ver informe de visita
   public function pdftest($idvisit)
    {
         
        $visits= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.id,visits.date,visits.entrance_hr,visits.exit_hr,visits.description,visits.process_id,visits.signature,personal.name,personal.profile_pic,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('visits.id',$idvisit)
                        ->groupBy('visits.id' , 'personal.name', 'personal.profile_pic')
                        ->orderBy('visits.date')
                        ->get();
        
        $visits=$visits[0];
        $visittec=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','visits.id')
                          ->where('visits.id',$idvisit)
                          ->get();
        $process= Process::join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.id as processid','process.process_number','process.description','personal.name as staff','process.ini_date','process.web','process.end_date','status.name as status','client.*')
                                ->where('process.id','=',$visits->process_id)
                                ->get();
        $process=$process[0];  

        $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->select('request_to_client.id','request_to_client.request','status.name as status','visits.date')
                                ->where('visits.id','=',$idvisit)->get();
        
        if(count($requestclient)>0)$requestclient=$requestclient[0]; 

        $jobt= Visits::join('visits_job_type','visits.id','=','visits_job_type.visits_id')
                         ->join('job_type','job_type.id','=','visits_job_type.job_type_id')
                         ->select('job_type.id','job_type.name')
                         ->where('visits.id','=',$idvisit)->get();
        
        
        $general= Visits::join('visits_general_info','visits.id','=','visits_general_info.visits_id')
                         ->join('general_info','general_info.id','=','visits_general_info.general_id')
                         ->select('general_info.id','general_info.name')
                         ->where('visits.id','=',$idvisit)->get();

        $generaltype= Visits::join('visits_general_type','visits.id','=','visits_general_type.visits_id')
                         ->join('general_type','general_type.id','=','visits_general_type.general_type_id')
                         ->select('general_type.type','general_type.name')
                         ->where('visits.id','=',$idvisit)
                         ->orderBy('type')
                         ->get(); 

        $gentype= GeneralType::join('visits_general_type','general_type.id','=','visits_general_type.general_type_id')
                               ->select('type')
                               ->where('visits_general_type.visits_id','=',$idvisit)
                               ->groupBy('type')
                               ->orderBy('type')
                               ->get();

        $picturestake= Visits::find($idvisit)->pictures;

        //-----INFORMACION PARA EL PDF -------------
         $viewpdf =  \View::make('emails/visit_report',compact('process','visits','requestclient','jobt','general','generaltype','gentype','picturestake','visittec'))->render(); 
            $name= 'Visita_'.date('Ymd',strtotime($visits->date)).'_'.$visits->process_id.'.pdf';
            $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            return $pdf->stream('emails/visit_report',compact('process','visits','requestclient','jobt','general','generaltype','gentype','picturestake'));
            //$pdf->save($path);

        
         
    }

 //-------------------------------Cierre Administrativo listado de visitas---------------------------------------------------------------  

    public function endadmin(Request $request){

        if($request->ajax()) {
        
        $visit=Visits::find($request->idvisit);
        $visit->close_type=$request->status;
        if($visit->save()){
            
                
           //-----------envio de correo notificando al personal administrativo
                $proyect = Process::select('process_number')->where('process.id','=',$visit->process_id)->get();
                
                $perinfo = Personal::select('name','email')->where('occupation','=','Administrativo')->get();
                $pername ='';
                $peremail = '';
                $cant = count($perinfo);
                $i=0;

                        $infoper['title']='Tiene una nueva Solicitud de Cierre Administrativo';
                        
                        $infoper['content'][0]= 'Solicitud de Cierre Administrativo para '.$request->status.' de la visita con Fecha '.$visit->date.' del Proyecto Nro. '.$proyect[0]['process_number'].' para que la misma sea procesada';
                        
                        $subject='Solicitud de Cierre Administrativo';

                        for($i;$i<$cant;$i++){
                             
                             $mail=$this->sendmail($infoper,$perinfo[$i]['name'],$perinfo[$i]['email'],$subject,''); 
                        }
         //-------------------------------------------------------------------------------------------                                   
        
        return Response::json(['closeadmin'=>$visit,'process'=>$proyect[0]['process_number']]);

        }else{
            return Response::json(['closeadmin'=>404]);
        }
        }
      
    }
//-------------------------------Cierre Administrativo Realizado listado de visitas---------------------------------------------------------------  

    public function endadmindone(Request $request){

        if($request->ajax()) {
        
        $visit=Visits::find($request->idvisit);
        $visit->close_type=$request->status;
        if($visit->save()){
            
                
           //-----------envio de correo notificando al lider 
                $proyect = Process::select('process_number')->where('process.id','=',$visit->process_id)->get();
                
                $perinfo = Personal::find($visit->assigned_id);
                $pername ='';
                $peremail = '';
                $cant = count($perinfo);
                $i=0;

                        $infoper['title']='Solicitud de Cierre Administrativo Realizado';
                        
                        $infoper['content'][0]= 'La Solicitud de Cierre Administrativo de la visita con Fecha '.$visit->date.' del Proyecto Nro. '.$proyect[0]['process_number'].' ya fue realizado';
                        
                        $subject='Cierre Administrativo Realizado';

                        $mail=$this->sendmail($infoper,$perinfo->name,$perinfo->email,$subject,''); 
                       
         //-------------------------------------------------------------------------------------------                                   
        
        return Response::json(['closeadmin'=>$visit,'process'=>$proyect[0]['process_number']]);

        }else{
            return Response::json(['closeadmin'=>404]);
        }
        }
      
    }
//---------Agregar observacion cuando no se realizo la visita---------------------------------------------------------------  

    public function notmade(Request $request){

        if($request->ajax()) {
        
        $visit=Visits::find($request->idvisit);
        $visit->description=$request->description;
        if($visit->save()){
          return Response::json(['notmade'=>$visit]);

        }else{
            return Response::json(['notmade'=>404]);
        }
        }
      
    }
//----------------------------------para modal de cambio de tecnicos listado de visitas------------------------------------------------------------
     public function updatetec(Request $request)
    {   if($request->ajax()) {
        $usersid=Auth::user()->id;
            $visit= Visits::find($request->idvisit);
            if($request->lider_id!='null'){
                $visit->assigned_id = $request->lider_id;
                $visit->updated_at = date('Y-m-d');
                $visit->save();
            }

            if($request->tecnico!=''){
                $tecs= Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','personal.id')
                          ->where('visits.id',$request->idvisit)
                          ->get(); 

                foreach($tecs as $t){
                            $visit->personal()->detach($t->id);
                       }
                
                $newtecs=$request->tecnico;
               
                foreach($newtecs as $r){
                    
                   $visit->personal()->attach($r,['users_id'=>$usersid]);
                } 
            }
        }
        return Response::json(['visit'=>$visit]); 
        
    }
 //----------------------------------------------------------------------------------------------  
//buscar tecnicos asignados a la visita para mostrar en modal de cambio de tecnicos listado de visitas
    public function findtec(Request $request){

        if($request->ajax()) {
        
        $tecs=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','personal.id as personalid','visits.id')
                          ->where('visits.id',$request->idvisit)
                          ->get();
        
           return Response::json(['tecs'=>$tecs]); 
       
        }
      
    }  


    //--------------- listado de visitas por facturar o archivar para rol admnistrativo ----------------
 public function adminclose(){
   
    $roluser= User::find(Auth::user()->id)->role;
        $rol=$roluser[0]->id;
        $perid= Auth::personal()->id;
        $visits= Visits::join('process','process.id','=','visits.process_id')
                        ->join('client_process','client_process.process_id','=','visits.process_id')
                        ->join('client','client.id','=','client_process.client_id')
                        ->join('status','status.id','=','visits.status_id')
                        ->join('personal_visits','visits.id','=','personal_visits.visits_id')
                        ->join('personal','personal.id','=','visits.assigned_id')
                        ->select('process_number','visits.*','client.*','visits.id as visitid','status.name as status','personal.name as lider')
                        ->where('visits.status_id',21)
                        ->whereNotNull('close_type')
                        ->groupBy("visits.id","process_number", "client.id", "status.name", "personal.name")->orderBy('visits.date')->get();
                       

        $visittec=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','personal.id as personalid','visits.id')
                          ->get();
        $personal = Personal::whereRaw("occupation in ('Tecnico','Jefe tecnico')")->whereNotNull('users_id')->lists('name','id')->prepend('Seleccione', 'null');                
        
        return \View::make('visits/visits',compact('visits','personal','rol','visittec','perid'));  
    } 

}
