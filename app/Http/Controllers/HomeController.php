<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Process;

class HomeController extends Controller
{
    protected $redirectTo = 'auth/login';
    
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
            $process= Process::join('users', 'users.id', '=', 'process.users_id')
                                ->join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->select('process.process_number', 'personal.last_name','personal.first_name','users.id as iduser','personal.id','users.name','process.ini_date','process.end_date','status.name as status','client.name as clientname','client.applicant_name')
                                ->get();
                                
        return \View::make('app/dashboard',compact('process'));
    }
}