<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Visits;
use App\Messages;
use App\Event;
use Auth;
use DB;
use App\User;
use App\Personal;
use App\Client;
use App\Process;
use App\NewRequest;
use App\RequestClient;
use App\Quote;
use Carbon;
use Response;

class DashboardController extends Controller
{
    protected $redirectTo = 'auth/login';
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $userid= Auth::user()->id;
       //dd($userid);exit;
       $roles = User::find($userid)->role()->get();
       $rol=$roles->first()->pivot->role_id;

       //----- Actuailizacion de visitas no realizadas -----
       $visitnotmade=Visits::whereRaw('date < CURRENT_DATE')
                              ->where('status_id',18)
                              ->get();
            if(count($visitnotmade)>0) {                 
                foreach ($visitnotmade as $k) {
                  $visitact=Visits::find($k->id);
                  $visitact->status_id=20;
                  $visitact->save();
                }
              }
        //--------------------------------------------------                      
       if($rol!=8){
              //cantidad de mensajes nuevos o no leidos
              $mess= Messages:: where('to_id','=',$userid)->where('status_id','=',11)->get();
              $cantmess= count($mess);
              $rangmes=$this->rangemonth();
              
       
                   $visits= Event::join('visits', 'events.visits_id', '=', 'visits.id')
                                      ->join('process', 'process.id', '=', 'visits.process_id')
                                       ->join('personal', 'personal.id', '=', 'visits.assigned_id')
                                       ->join('status', 'status.id', '=', 'process.status_id')
                                       ->join('client_process', 'client_process.process_id', '=', 'visits.process_id')
                                       ->join('client', 'client.id', '=', 'client_process.client_id')
                                       ->join('personal_visits','visits.id','=','personal_visits.visits_id')
                                       ->select('process.process_number','process.id as processid', 'personal.name as staff','personal.id','process.ini_date','process.end_date','status.name as status','client.name as clientname','client.applicant_name','visits.date','visits.id as visitsid','visits.description','visits.status_id as visitstatus','events.description as event','events.title','events.type as eventtype')
                                       ->whereBetween('events.startdate', [$rangmes['start'],$rangmes['end']])
                                       ->whereRaw('visits.status_id in (18,19) ');

                  $visittec=Visits::join('personal_visits','visits.id','=','personal_visits.visits_id')
                          ->join('personal','personal.id','=','personal_visits.personal_id')
                          ->select('personal.name','visits.id')->orderBy('visits.date')->get();
                                            
                                       
                  
                   $visitsmade= Event::join('visits', 'events.visits_id', '=', 'visits.id')
                                       ->join('process', 'process.id', '=', 'visits.process_id')
                                       ->join('personal', 'personal.id', '=', 'visits.assigned_id')
                                       ->join('status', 'status.id', '=', 'process.status_id')
                                       ->join('client_process', 'client_process.process_id', '=', 'visits.process_id')
                                       ->join('client', 'client.id', '=', 'client_process.client_id')
                                       ->join('personal_visits','visits.id','=','personal_visits.visits_id')
                                       ->select('process.process_number','process.id as processid', 'personal.name as staff','personal.id','process.ini_date','process.end_date','status.name as status','client.name as clientname','client.applicant_name','visits.date','visits.id as visitsid','visits.description','visits.status_id as visitstatus','events.id','events.description as event','events.title','events.type as eventtype')
                                       ->whereBetween('events.startdate', [$rangmes['start'],$rangmes['end']])
                                       ->where('visits.status_id','=',21);
                                       
                  
                  if($rol < 5 || $rol == 6){ 
                      $visits=$visits->groupBy("visits.id","process_number","process.id", "client.id", "personal.name","personal.id","status.name","events.id")->orderBy('visits.date')->get();
                      $visitsmade=$visitsmade->groupBy("visits.id","process_number","process.id", "client.id", "personal.name","personal.id","status.name","events.id")->orderBy('visits.date')->get();
                      
                                
                  }else if($rol == 5){ //tecnico
                      $perid= Auth::personal()->id;

                      $visits=$visits->whereRaw('("visits"."assigned_id" = '.$perid.' or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = '.$perid.') )')
                      ->groupBy("visits.id","process_number","process.id", "client.id", "personal.name","personal.id","status.name","events.id")
                      ->orderBy('visits.date')->get();

                      $visitsmade=$visitsmade->whereRaw('("visits"."assigned_id" = '.$perid.' or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = '.$perid.') )')
                      ->groupBy("visits.id","process_number","process.id", "client.id", "personal.name","personal.id","status.name","events.id")
                      ->orderBy('visits.date')->get();

                      
                  }
              //---------------DATA PARA GRAFICO --------------------                          
                   $semana=$this->rangemonth(); 
                   $graphnew=$this->creategraph(18,$semana['start'],$semana['end']); 
                   $graphnotmade=$this->creategraph(20,$semana['start'],$semana['end']); 
                   $graphdone=$this->creategraph(21,$semana['start'],$semana['end']); 
                   $gtotal=$this->totalgraph($semana['start'],$semana['end']);
                   $visita=$this->vChartsTotal($semana['start'],$semana['end']);

                   
                  //dd($gtotal);exit;  
              //----------------CONTADORES DE CABECERA DASHBOARD---------------------------------------------------------
                       $newweb=NewRequest::where('status_id',26)
                                         // ->whereBetween('created_at',[$semana['primer'],$semana['ultimo']])
                                         ->get([ DB::raw('count(id) as total')]); 
                       $newvisit=Visits::where('status_id',18)
                                         // ->whereBetween('created_at',[$semana['primer'],$semana['ultimo']])
                                         ->get([ DB::raw('count(id) as total')]);
                       $newnotvisit=Visits::where('status_id',20)
                                         // ->whereBetween('created_at',[$semana['primer'],$semana['ultimo']])
                                         ->get([ DB::raw('count(id) as total')]);   
                                                                                        
                       $counter= Process::select([ DB::raw('SUM(CASE WHEN status_id=1 THEN 1  ELSE 0 END ) AS nuevo,
                                       SUM(CASE WHEN status_id=3 THEN 1  ELSE 0 END ) AS proceso,
                                       SUM(CASE WHEN status_id=4 THEN 1  ELSE 0 END ) AS terminado,
                                       SUM(CASE WHEN status_id=5 THEN 1  ELSE 0 END ) AS cerrado,
                                       SUM(CASE WHEN status_id=6 THEN 1  ELSE 0 END ) AS asignado')])->get();
                       
                       $quotes= Quote::select([ DB::raw('SUM(CASE WHEN status_id=33 THEN 1  ELSE 0 END ) AS nuevo,
                                       SUM(CASE WHEN status_id=34 THEN 1  ELSE 0 END ) AS aceptada,
                                       SUM(CASE WHEN status_id=38 THEN 1  ELSE 0 END ) AS solicitudmat,
                                       SUM(CASE WHEN status_id=39 THEN 1  ELSE 0 END ) AS matcargados,
                                       SUM(CASE WHEN status_id=46 THEN 1  ELSE 0 END ) AS factentregada')])->get();
                       $closeadminvisit=Visits::where('status_id',21)
                                         ->whereIn('close_type',['Facturar','Archivar'])
                                         ->get([ DB::raw('count(id) as total')]);
                    
                    $events= Event::select('events.description as event','events.title','events.type as eventtype','events.startdate')
                                       ->whereBetween('events.startdate', $rangmes)
                                       ->where('type','<>','visit')->get();
                      //dd($events);exit;
             
            
            //----------------------------------------------------------------------------

        return \View::make('app/dashboard',compact('visits','cantmess','visitsmade','events','visittec','perid','rol','newweb','newvisit','newnotvisit','counter','graphnew','graphnotmade','graphdone','gtotal','semana','visita','quotes','closeadminvisit'));

        }else{// si entra como cliente
            
            $clientid=Auth::client()->id;
            $client = Client::find($clientid);
             $projects= Process::join('client_process','client_process.process_id','=','process.id')
                            ->join('client','client_process.client_id','=','client.id')
                            //->leftJoin('visits','process.id','=','visits.process_id')
                            ->join('status','process.status_id','=','status.id')
                            //->select(\DB::raw('process.*,status.name as status,visits.have_request,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                            ->select('process.*','status.name as status')
                            ->where('client.id',$clientid)
                            //->groupBy('process.id','status.name','visits.have_request')
                            ->groupBy('process.id','status.name')
                            ->get();
         
        if($client->master_client!=null){
            $masterid=$client->master_client;
        }else{
            $masterid=$clientid;
        }
        
        $branches=Client::where('master_client',$masterid)->lists('name','id')->prepend('Client Branches...', 'null');
        
        return \View::make('client.profile',compact('client','projects','branches','rol','masterid'));
            //return redirect('client.show',['id' => $client]);
            

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function creategraph($statusid,$first,$last)//funcion para crear la grafica principal
    {
          
               $counter= Visits::whereBetween('created_at',["'".$first."'","'".$last."'"])
                                 ->select([ DB::raw('created_at,SUM(CASE WHEN status_id='.$statusid.' THEN 1  ELSE 0 END ) AS cant')])
                                 ->groupBy('created_at')
                                 ->orderBy('created_at')
                                 ->get();
               $data = [];
                
               foreach($counter as $co){
                   
                   $data[]= ["gd(".date('Y',strtotime($co->created_at)).','.date('m',strtotime($co->created_at)).','.date('d',strtotime($co->created_at)).'),'.$co->cant];
                 
               } 
               $data= json_encode($data); 
               $data=str_replace('"', '', $data);  
               return $data;
         
                         
    }

    public function creategraphajax(Request $request)//funcion para crear la grafica principal
    {
       
       if($request->ajax()){

        if($request->type=='datepicker'){
          $first=$request->first;
          $last=$request->last;
        }else if($request->type=='default'){
          $semana=$this->rangemonth();
          $first=$semana['start'];
          $last=$semana['end'];
        }
                   $graphnew=$this->creategraph(18,$first,$last);
                   $graphnotmade=$this->creategraph(20,$first,$last);
                   $graphdone=$this->creategraph(21,$first,$last);
                   $gtotal=$this->totalgraph($first,$last); 
               
         
        }
          return Response::json(['graphnew'=>$graphnew,'graphnotmade'=>$graphnotmade,'graphdone'=>$graphdone,'gtotal'=>$gtotal]);               
    }


    public function totalgraph($first,$last)//funcion para crear la grafica principal
    {
        $graphtotal= Visits::whereBetween('created_at',[$first,$last])
                          ->select([ DB::raw('created_at,count(id) AS cant')])
                          ->groupBy('created_at')
                          ->orderBy('created_at')
                          ->get();
       $gtotal = [];

        foreach($graphtotal as $co){
            
            $gtotal[]= ["gd(".date('Y',strtotime($co->created_at)).','.date('m',strtotime($co->created_at)).','.date('d',strtotime($co->created_at)).'),'.$co->cant];
          
        }   
        $data= json_encode($gtotal); 
        $data=str_replace('"', '', $data);
        return $data;              
    }

    public function vChartsTotal($start,$end)
    {   
      if($start==''){
        $start=date('Y').'-08-01';
        $end=date('Y').'-08-30';
        }
       $assigned= Visits::select(\DB::raw('count(id) as total,SUM(CASE WHEN "visits"."status_id"=18 THEN 1  ELSE 0 END ) AS nuevas, 
                                        SUM(CASE WHEN "visits"."status_id"=20 THEN 1  ELSE 0 END ) AS nodone,
                                        SUM(CASE WHEN "visits"."status_id"=21 THEN 1  ELSE 0 END ) AS done'))
                        ->whereBetween('visits.date',[$start,$end])
                        ->get();
       
        $nuevas='';
        $nodone='';
        $done='';
        $total='';
        foreach($assigned as $c){
            $nuevas=$nuevas.$c->nuevas.',';
            $nodone=$nodone.$c->nodone.',';
            $done=$done.$c->done.',';
            $total=$total.$c->total.',';
            

       }
        

        $data['nuevas']=$nuevas;
        $data['nodone']=$nodone;
        $data['done']=$done;
        $data['total']=$total;

        return $data;

   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function week()
    {
        
        $year=date('Y');
        $month=date('m');
        $day=date('d');
         
        # Obtenemos el numero de la semana
        $semana=date("W",mktime(0,0,0,$month,$day,$year));
         
        # Obtenemos el día de la semana de la fecha dada
        $diaSemana=date("w",mktime(0,0,0,$month,$day,$year));
         
        # el 0 equivale al domingo...
        if($diaSemana==0)
            $diaSemana=7;
         
        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $primerDia=date("Y-m-d",mktime(0,0,0,$month,$day-$diaSemana+1,$year));
        //$primerDia=date("l jS F Y",strtotime($primerDia)); 
        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $ultimoDia=date("Y-m-d",mktime(0,0,0,$month,$day+(7-$diaSemana),$year));
        //$ultimoDia=date("l jS F Y",strtotime($ultimoDia));
        $semana=[];
        $semana['primer']=$primerDia;
        $semana['ultimo']=$ultimoDia;
        return $semana;
       
    }
    public function previousweek()
    {
        
        $year=date('Y');
        $month=date('m');
        $day=date('d');
         
        # Obtenemos el numero de la semana
        $semana=date("W",mktime(0,0,0,$month,$day,$year));
        $semanaanterior=$semana-1;
         
        # Obtenemos el día de la semana de la fecha dada
        $diaSemana=date("w",mktime(0,0,0,$month,$day,$year));
         
        # el 0 equivale al domingo...
        if($diaSemana==0)
            $diaSemana=7;
         
        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $primerDia=date("Y-m-d",mktime(0,0,0,$month,$day-$diaSemana+1,$year));
        
        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $ultimoDia=date("Y-m-d",mktime(0,0,0,$month,$day+(7-$diaSemana),$year));
        
        $semana=[];
        $semana['primer']=$primerDia;
        $semana['ultimo']=$ultimoDia;
        return $semana;
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function alertas(Request $request)
    {
        if($request->ajax()){
          
          $birthday=array();
          $rqtocl=array();
          $proc=array();
          $requestweb=array();
          $quotesr=array();
          $matrequesttec=array();
          $matrequestadm=array();
          $visitsclose=array();
          $quoteaccept=array();
          $quotereject=array();
          $semana=$this->week();
          $rangmes=$this->rangemonth();
          
          //Cumpleaños------------
          $birth=Personal::whereBetween('date_birth',[$rangmes['start'],$rangmes['end']])->get();
          $i=0;
          foreach($birth as $b){
              $birthday[$i]=$b->name.' el dia '.date('d-m',$b->date_birth).'-'.date('Y');
              $i++;
          }
          
          //------------- Mostrar alerta en perfil del Cliente-----------------------------
          if($request->clientid!=''){
            //---solicitudes al cliente sin responder--------
                $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                      ->join('status','status.id','=','request_to_client.status_id')
                                      ->join('process','process.id','=','visits.process_id')
                                      ->join('client_process','process.id','=','client_process.process_id')
                                      ->join('personal','personal.id','=','visits.assigned_id')
                                      ->join('client','client.id','=','client_process.client_id')
                                      ->select('request_to_client.request')
                                      ->where('request_to_client.status_id',15)
                                      ->where('client.id','=',$request->clientid)
                                      ->get();
                $i=0;
                foreach($requestclient as $r){
                    
                        $rqtocl[$i]=$r->request;
                        $i++;
                   
                }

                 return Response::json(['requestclient'=>$rqtocl,'clientid'=>$request->clientid]); 

          // ----------------Mostrar alerta en dashboard del personal Sonitus -------------------
          }else{ 
          //---solicitudes al cliente sin responder--------
          $requestclient=RequestClient::join('visits','visits.id','=','request_to_client.visits_id')
                                ->join('status','status.id','=','request_to_client.status_id')
                                ->join('process','process.id','=','visits.process_id')
                                ->join('client_process','process.id','=','client_process.process_id')
                                ->join('personal','personal.id','=','visits.assigned_id')
                                ->join('client','client.id','=','client_process.client_id')
                                ->select([DB::raw('client.name as client')])
                                ->where('request_to_client.status_id',15)
                                ->groupBy('client.name')
                                ->get();
          $i=0;
          foreach($requestclient as $r){
              
                  $rqtocl[$i]=$r->client;
                  $i++;
             
          }
          
          //---Processos sin asignar visita--------
             $process= Process::select('process_number')
                        ->whereBetween('created_at',[$rangmes['start'],$rangmes['end']])
                        ->where('status_id',6)
                        ->get(); 
             $i=0;
          foreach($process as $p){
              $proc[$i]=$p->process_number;
              $i++;
          }        
          
          //---Solicitudes WEB para responder ---
          $rweb=NewRequest::leftJoin('process','process.id','=','new_request.process_id')
                                ->join('status','status.id','=','new_request.status_id')
                                ->join('client','client.id','=','new_request.client_id')
                                ->select('new_request.description','process_number','client.name as client')
                                ->where('new_request.status_id',26)
                                ->get();
          $i=0;
          foreach($rweb as $r){
              
                  $requestweb[$i]['cliente']=$r->client;
                  $requestweb[$i]['description']=$r->description;
                  $requestweb[$i]['proyecto']=$r->process_number;
                  $i++;
             
          }
          
          //---Solicitudes de cotizacion ---
           $quotes=Quote::join('client','client.id','=','quote.client_id')
                       ->join('status','status.id','=','quote.status_id')
                       ->leftJoin('new_request','new_request.id','=','quote.new_request_id')
                       ->join('personal','personal.id','=','quote.personal_id')
                       ->select('client.name as client','quote.observation','personal.name as personal')
                       ->where('quote.status_id',33)
                       ->whereBetween('quote.created_at',[$rangmes['start'],$rangmes['end']])
                       ->get(); 
            $i=0;
          foreach($quotes as $r){
              
                  $quotesr[$i]['cliente']=$r->client;
                  $quotesr[$i]['description']=$r->observation;
                  $quotesr[$i]['personal']=$r->personal;
                  $i++;
             
          }                    
          //---Solicitudes de cotizacion de Materiales tecnicos a admin ---
                        
            $mattec=Quote::join('client','client.id','=','quote.client_id')
                       ->join('status','status.id','=','quote.status_id')
                       ->leftJoin('new_request','new_request.id','=','quote.new_request_id')
                       ->join('personal','personal.id','=','quote.personal_id')
                       ->select('client.name as client','quote.observation','personal.name as personal')
                       ->where('quote.status_id',39)
                       ->whereBetween('quote.created_at',[$rangmes['start'],$rangmes['end']])
                       ->get(); 
          
           $i=0;
          foreach($mattec as $r){
              
                  $matrequesttec[$i]['cliente']=$r->client;
                  $matrequesttec[$i]['description']=$r->observation;
                  $matrequesttec[$i]['personal']=$r->personal;
                  $i++;
             
          }   
          //---Solicitudes de Materiales a usar en proyecto ---
                        
            $matadm=Quote::join('client','client.id','=','quote.client_id')
                       ->join('status','status.id','=','quote.status_id')
                       ->leftJoin('new_request','new_request.id','=','quote.new_request_id')
                       ->join('personal','personal.id','=','quote.personal_id')
                       ->select('client.name as client','quote.observation','personal.name as personal')
                       ->where('quote.status_id',38)
                       ->whereBetween('quote.updated_at',[$rangmes['start'],$rangmes['end']])
                       ->get(); 
            $i=0;
          foreach($matadm as $r){
              
                  $matrequestadm[$i]['cliente']=$r->client;
                  $matrequestadm[$i]['description']=$r->observation;
                  $matrequestadm[$i]['personal']=$r->personal;
                  $i++;
             
          }
          
          //---Visitas con Cierre Administrativo pendientes--
            $vclose= Visits::join('process','process.id','=','visits.process_id')
                        ->join('client_process','client_process.process_id','=','visits.process_id')
                        ->join('client','client.id','=','client_process.client_id')
                        ->join('status','status.id','=','visits.status_id')
                        ->join('personal_visits','visits.id','=','personal_visits.visits_id')
                        ->join('personal','personal.id','=','visits.assigned_id')
                        ->select('process_number','client.name','visits.id as visitid')
                        ->where('visits.status_id',21)
                        ->whereIn('close_type',['Archivar','Facturar'])
                        ->whereBetween('visits.updated_at',[$rangmes['start'],$rangmes['end']])
                        ->groupBy("visits.id","process_number","client.name")->orderBy('process_number')->get();
            $i=0;
          foreach($vclose as $r){
              
                  $visitsclose[$i]['cliente']=$r->name;
                  $visitsclose[$i]['proyecto']=$r->process_number;
                  $visitsclose[$i]['visita']=$r->visitid;
                  $i++;
             
          }

          //---Cotizaciones Aceptadas --
                        
            $qaccept=Quote::join('client','client.id','=','quote.client_id')
                       ->join('status','status.id','=','quote.status_id')
                       ->leftJoin('new_request','new_request.id','=','quote.new_request_id')
                       ->join('personal','personal.id','=','quote.personal_id')
                       ->select('client.name as client','quote.quote_number')
                       ->where('quote.status_id',34)
                       ->whereBetween('quote.updated_at',[$rangmes['start'],$rangmes['end']])
                       ->get(); 
            $i=0;
          foreach($qaccept as $r){
              
                  $quoteaccept[$i]['cliente']=$r->client;
                  $quoteaccept[$i]['quote_number']=$r->quote_number;
                  $i++;
             
          }

        //---Cotizaciones Rechazadas --
                        
            $qreject=Quote::join('client','client.id','=','quote.client_id')
                       ->join('status','status.id','=','quote.status_id')
                       ->leftJoin('new_request','new_request.id','=','quote.new_request_id')
                       ->join('personal','personal.id','=','quote.personal_id')
                       ->select('client.name as client','quote.quote_number')
                       ->where('quote.status_id',35)
                       ->whereBetween('quote.updated_at',[$rangmes['start'],$rangmes['end']])
                       ->get(); 
            $i=0;
          foreach($qreject as $r){
              
                  $quotereject[$i]['cliente']=$r->client;
                  $quotereject[$i]['quote_number']=$r->quote_number;
                  $i++;
          }
          
        }// if request ajax  
        
        return Response::json([
                            'birthday'=>$birthday,
                            'requestclient'=>$rqtocl,
                            'process'=>$proc,
                            'webrequest'=>$requestweb,
                            'newreqst'=>$quotesr,
                            'matrequesttec'=>$matrequesttec,
                            'matrequestadm'=>$matrequestadm,
                            'visitsadminclose'=>$visitsclose,
                            'quoteaccept'=>$quoteaccept,
                            'quotereject'=>$quotereject,

                            ]); 
      }//else if clientid
    
    }//public function alertas 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Search for the information of the process pass it by modal.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchprocess($id)
    {
        //
    }

    function rangeWeek($datestr) {
            date_default_timezone_set(date_default_timezone_get());
            $dt = strtotime($datestr);
            $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
            //$res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
            $res['end'] = date('Y-m-d', strtotime('next monday', $dt));
            return $res;
    }

    function rangemonth(){
      $month = date('m');
      $year = date('Y');
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
      $res['end'] = date('Y-m-d', mktime(0,0,0, $month, $day, $year));

      $res['start'] = date('Y-m-d', mktime(0,0,0, $month, 1, $year));

      return $res;
    }
}
