<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Personal;
use App\Process;
use App\Visits;
use App\RoleUser;
use App\Auth\AuthController;
use App\Insured;
use App\Roles;
use Input;
use URL;
use Auth;
use Storage;
use DB;
use App\User;
use Mail;
use File;
use Response;

class PersonalController extends Controller
{
     protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personals = Personal::leftJoin('users','users.id','=','personal.users_id')->select('personal.*')->whereNotIn('occupation',['Web'])->whereNotIn('personal.email',['kamalyacevedo@gmail.com'])->get();
        return \View::make('staff.list',compact('personals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $option= Roles::lists('name','name');
        $option->prepend('Seleccione un rol', 'null');
        $ruta='personal.store';
        $title='Registration';
        return \View::make('staff.new',compact('option','ruta','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
            
            if($request->registration==='on'){
                    $provisional= $this->generaPass();
                    $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->password = bcrypt($provisional);
                    $user->active = TRUE;
                if($user->save()){ 
                    //se asocia el rol al usuario
                    $rol= Roles::where('slug','=',$request->slug)->get();   

                    $roluser = new RoleUser;
                    $roluser->user_id = $user->id;
                    $roluser->role_id = $rol[0]->id;
                    $roluser->save();
                }

                //se envia el email
                $view='emails.plantilla';
                $info['title']='Bienvenido al Sistema de Control de Procesos de Inversiones Sonitus '.$request->name;
                $info['content'][0]='Tu usuario: '.$request->email.'
                                     Tu clave provisional: '.$provisional;
                $info['content'][1]= 'Por favor recuerde cambiar su clave provisional ingresando a la opcion de perfil de usuario, Cambio de clave.';
                $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo atencion@sonitus.com y con gusto te guiaremos con todo tu proceso.';
                $name=$request->name;
                $email=$request->email;
                $subject='Registro en Sistema Sonitus';
               
                $this->sendmail($info,$name,$email,$subject,'');
 
                 \Session::flash('message','The registration email send for "'.$request->name.'" succed!!');
            }
                

                if($request->hasFile('image')) {
                    

                    $imageName = $request->identity_number . '.' .$request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move(base_path() . '/public/images/personal/', $imageName);
                     
                }else{
                    $imageName='user.png';
                }

                //se guarda en tabla personal    
                $personal = new Personal;
                $personal->name = $request->name;
                $personal->address = $request->address;
                $personal->identity_number = $request->identity;
                $personal->occupation = $request->slug;
                $personal->occupation_description = $request->occupdescrip;
                $personal->telephone = $request->telephone;
                $personal->email = $request->email;
                $personal->date_birth = date('Y-m-d',strtotime($request->birthday));
                $personal->initial_date = date('Y-m-d',strtotime($request->initialdate));
                $personal->profile_pic = $imageName;
                $personal->active = TRUE;
                if($request->registration==='on'){$personal->users_id=$user->id;}
                $personal->save();

               \Session::flash('message','The registration of "'.$request->name.'" as Staff member succed!!');
            return redirect('personal');
        
    }

  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $per = Personal::find($id);
        $file= url('images/personal/'.$per->profile_pic);
        $range = \Carbon\Carbon::now()->subDays(7);
        $stats = User::where('created_at', '>=', $range)
                    ->groupBy('date')
                    ->orderBy('date', 'DESC')
                    ->get([
                        DB::raw('Date(created_at) as date'),
                        DB::raw('COUNT(*) as value')
                    ])
                    ->toJSON();
         $projects= Process::join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->join('personal', 'personal.id', '=', 'process.assigned_id')
                                ->select('process.id as processid','process.process_number','process.description','status.name as status','status.id as statusid','process.assigned_date','process.priority','client.*')
                                ->where('assigned_id','=',$id)
                                ->get();   
         $visits= Visits::join('process', 'process.id', '=', 'visits.process_id')
                                ->join('users', 'users.id', '=', 'process.users_id')
                                ->join('status', 'status.id', '=', 'process.status_id')
                                ->join('client_process', 'client_process.process_id', '=', 'process.id')
                                ->join('client', 'client.id', '=', 'client_process.client_id')
                                ->join('personal_visits','visits.id','=','personal_visits.visits_id')
                                ->join('personal','personal.id','=','visits.assigned_id')
                                ->select('process.id as processid','process.process_number','process.description','process.job_type','process.ini_date','process.end_date','status.name as status','client.*','visits.entrance_hr','visits.exit_hr','visits.description as visitdescrp','visits.id as visitsid','visits.date',\DB::raw('(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))

                                ->whereRaw('("visits"."assigned_id" = '.$id.' or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = '.$id.') )')
                                ->groupBy("visits.id","process.id","process_number", "client.id", "status.name", "personal.name")
                                ->orderBy('visits.date')
                                ->get();   
          $rangeweek=$this->rangeweek(date('Y-m-d'));
          $date['start']=date('Y').'-'.date('m').'-01';
          $lastday=date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1));
          $date['end']=date('Y').'-'.date('m').'-'.$lastday;
          $morris=$this->morrisbar($id,$date);
        return \View::make('staff.profile',compact('per','file','stats','projects','visits','morris'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $personal = Personal::find($id);
        $option= Roles::lists('name','name');
        $option->prepend('Select', 'null');
        $ruta='personal/update';
        $title='Edition';
        $file= url('images/personal/'.$personal->profile_pic);
        return \View::make('staff.new',compact('personal','option','ruta','file','title'));

        
    }

 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                
                if($request->hasFile('image')) {
                    
                    if($request->oldpic!='user.png')File::delete(public_path().'/images/personal/'.$request->oldpic);

                    $imageName = $request->identity . '.' .$request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move(base_path() . '/public/images/personal/', $imageName);
                     
                }else {
                    $imageName ='user.png';
                }

                //se guarda en tabla personal    
                $personal = Personal::find($request->id);
                $personal->name = $request->name;
                $personal->address = $request->address;
                $personal->identity_number = $request->identity;
                if($request->slug!='null')$personal->occupation = $request->slug;
                $personal->occupation_description = $request->occupdescrip;
                $personal->telephone = $request->telephone;
                $personal->email = $request->email;
                $personal->date_birth = date('Y-m-d',strtotime($request->birthday));
                $personal->initial_date = date('Y-m-d',strtotime($request->initialdate));
                $personal->profile_pic = $imageName;
                $personal->save();

               \Session::flash('message','The Update of "'.$request->name.'" succed!!');

            return redirect('personal');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroypersonal(Request $request)
    {
      if($request->ajax()) {

        $personal = Personal::find($request->id);
        $personal->delete();
        if($personal->users_id != ''){
            $user = User::find($personal->users_id);
            $user->delete();
        }                  
        //Session::flash('message','Deactivation of '.$personal->name.' succed!!');
        
        return Response::json(['deleted'=>$personal]);
        
       }
    
    }

    public function search(Request $request)
    {
         $personals = Personal::where('first_name','like','%'.$request->name.'%')
                                ->orWhere('last_name','like','%'.$request->name.'%')
                                ->orWhere('identity_number','like','%'.$request->name.'%')
                                ->get();
         return \View::make('staff.list', compact('personals'));
        
    }

    public function reset()
    {
        $id=Auth::user()->id;

         $user = User::where('id','=',$id)
                             ->get();
        
        return \View::make('auth.reset',compact('user'));
       
        
    }

    public function updatepass(Request $request)
    {   $id=Auth::user()->id;
        $user=User::find($id);
        $user->password = bcrypt($request->password);
        $user->save();
        return \View::make('home');
       
        
    }

    public function activestaff($id)
    {   $personal = Personal::withTrashed()->where('id','=',$id )->restore();
        $personal = Personal::find($id);
        $user=User::withTrashed()->where('id','=',$personal->users_id)->restore();
        
        
        Session::flash('message','Activation of '.$personal->name.' succed!!');
        return redirect()->back();
       
        
    }

    public function searchdeleted()
    {    $personals = Personal::onlyTrashed()->get();
        //dd($deleted);exit;
        return \View::make('staff.activeStaff',compact('personals'));
       
        
    }

    private function generaPass(){
        //Se define una cadena de caractares.
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        //Obtenemos la longitud de la cadena de caracteres
        $longitudCadena=strlen($cadena);
         
        //Se define la variable que va a contener la contraseña
        $pass = "";
        //Se define la longitud de la contraseña, en mi caso 6, pero puedes poner la longitud que quieras
        $longitudPass=6;
         
        //Creamos la contraseña
        for($i=1 ; $i<=$longitudPass ; $i++){
            //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
            $pos=rand(0,$longitudCadena-1);
         
            //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
            $pass .= substr($cadena,$pos,1);
        }
        return $pass;
    }

   //----------------------------------------------------------------------------------------------

 public function searchpersonal(Request $request)
    {   
       if($request->ajax()) {
        
        $personal=Personal::where('identity_number',$request->identity)->get();
          if(count($personal)==0){
            $personal='null';
          }
        }
      return Response::json(['personal'=>$personal]);

    }
//----------------------------------------------------------------------------------------------
     public function morrisbar($id,$date)
    {   
       
        
        $data = array();
        $staff= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.date,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('personal.id',$id)
                        ->where('visits.status_id',21)
                        ->whereBetween('visits.date',[$date['start'],$date['end']])
                        ->whereRaw('("visits"."assigned_id" = '.$id.' or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = '.$id.') )')
                        ->groupBy('visits.date')
                        ->orderBy('visits.date')
                        ->get();
        
                $i=0;
                foreach($staff as $s){
                    $data[$i] = array(
                        "period"=>date('M-d',strtotime($s->date)),
                        "Hours worked"=>$s->total, 
                    );
                 $i++;   
                }
                    $data=json_encode($data);
        
       return $data;
       //return Response::json(['all'=>$data]);
    

    }
//----------------------------------------------------------------------------------------------
    public function morrisbarajax(Request $request)
    {   
       if($request->ajax()){
        
        $data = array();
        $staff= Visits::join('personal','personal.id','=','visits.assigned_id')
                        ->select(\DB::raw('visits.date,(SUM(CAST( exit_hr AS TIME ))) - (SUM(CAST( entrance_hr AS TIME )))  as total'))
                        ->where('personal.id',$request->id)
                        ->where('visits.status_id',21)
                        ->whereBetween('visits.date',[$request->start,$request->end])
                        ->whereRaw('("visits"."assigned_id" = '.$request->id.' or "visits"."id" in (select visits_id from personal_visits where "personal_visits"."personal_id" = '.$request->id.') )')
                        ->groupBy('visits.date')
                        ->orderBy('visits.date')
                        ->get();
        
        $i=0;
        foreach($staff as $s){
            $data[$i] = array(
                "period"=>date('M-d',strtotime($s->date)),
                "Hours worked"=>$s->total, 
            );
         $i++;   
        }
            //$data=json_encode($data);
        
       }
       return Response::json(['data'=>$data]);
    

    }
//----------------------------------------------------------------------------------------------

    
 public function sendmail($info,$name,$email,$subject,$path)
    {   
        $view='emails.plantilla';
       
       
      
                
       //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject,$path) {
                $m->from(env('SENDER_ADDRES'), env('SENDER_NAME'));
                $m->to($email, $name)->subject($subject);
                $m->bcc(env('COPY_ADDRES'), env('COPY_NAME'));
                if($path!='')$m->attach($path);

               
            });
       return $mail;

    }
//----------------------------------------------------------------------------------------------
    function rangeWeek($datestr) {
            date_default_timezone_set(date_default_timezone_get());
            $dt = strtotime($datestr);
            $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
            $res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
            return $res;
    }

}
