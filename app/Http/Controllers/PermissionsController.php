<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Permissions as Perm;
use App\Menu as Menu;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roles as Roles;
use Response;
class PermissionsController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $perm= Perm::join('menu', 'menu.id', '=', 'permissions.menu_id')
                                ->select('permissions.name', 'permissions.id','permissions.slug','permissions.description','permissions.menu_id','menu.name as menuname')
                                ->get();
        $option= Menu::lists('name','id');
        $option->prepend('Seleccione el menu', 'null'); 
                     
        return \View::make('menu/listperm',compact('perm','option'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return \View::make('menu/newperm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $v = \Validator::make($request->all(), [
            
            'name' => 'required',
            'description' => 'required',
            'slug' => 'required|',
            'menu_id' => 'not_in:null',
        ]);
 
        if ($v->fails())
        {
            Session::flash('errors',$v->errors());
            return redirect(route('personal.create'));
        }else{ 
            $perm = new Perm;
            $perm->create($request->all());
            
            //asignacion del permiso a los roles 
              $perms=Perm::all();
              $permsnew=Perm::find($perms->last()->id);        
              $rol=Roles::all();
           
            foreach($rol as $r){
                
               if($r->id == 1 || $r->id == 2 ) {
                $permsnew->rolesp()->attach($r->id,['active'=>true]);
               }else {
                   $permsnew->rolesp()->attach($r->id,['active'=>false]);
               }
            }
        
            //----------------------------------------
            
            //******************************************
            // Al crear un nuevo permiso se le asignan a  
            //todos los roles y desde el administrador 
            //de rol se le activan las necesarias
            //******************************************
            return redirect('perm');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                
                $perm=Perm::join('menu', 'menu.id', '=', 'permissions.menu_id')
                                ->select('permissions.name', 'permissions.id','permissions.slug','permissions.description','permissions.menu_id','menu.name as menuname')
                                ->where('permissions.id','=',$id)
                                ->get();
                $option= Menu::lists('name','id');
                $option->prepend('Seleccione un menu', 'null');
                return \View::make('menu/updateperm',compact('perm','option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->ajax()){
                $perm = Perm::find($request->id);
                        $perm->name = $request->name;
                        $perm->slug = $request->slug;
                        $perm->description = $request->description;
                        if($request->newmenu != 'null')$perm->menu_id = $request->newmenu;
                $perm->save();
            }
                 return Response::json(['update'=>$perm]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $perm = Perm::find($id);
        $perm->delete();
        $rol=Roles::all();

        foreach($rol as $r){
           $perm->rolesp()->detach($r->id);
        }

        Session::flash('message','La eliminacion del Permiso'.$perm->name.' se realizo con exito!!');
        return redirect()->back();
    }

    public function search(Request $request){
         $perm = Perm::where('name','like','%'.$request->name.'%')->get();
         return \View::make('menu/listperm', compact('perm'));
        
    }

}
