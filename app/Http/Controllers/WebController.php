<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Client;
use App\NewRequest;
use App\Quote;
use App\User;
use App\Personal;
use Response;
use Mail;
use URL;
use Auth;

class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \View::make('webpage/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
                //guardar informacion del cliente
                $rif=$request->riftype.$request->rif;

                $exist=Client::where('rif',$rif)->get();

                if(count($exist)<=0){
                                $cli = new Client;
                                            $cli->name = $request->name;
                                            $cli->rif = $rif;
                                            $cli->address = $request->address;
                                            $cli->applicant_name = $request->applicant_name;
                                            $cli->charge = $request->charge;
                                            $cli->departmen = $request->dpto;
                                            $cli->headquarters = $request->sede;
                                            $cli->phone = $request->telephone;
                                            $cli->email = $request->email;
                                            $cli->created_by = 5;//user web
                                            
                                $cli->save();

                                $clientid=$cli->id;
                }else{
                    $clientid=$exist[0]->id;
                }
                $new= new NewRequest;
                        $new->client_id=$clientid;
                        $new->description=$request->message;
                        $new->type=$request->type;
                        $new->status_id=26;
                

                if($new->save()){
                    return Response::json(['send'=>true]);
                }else{
                    return Response::json(['send'=>false]);
                }
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accept($id)
    {
        $quote= Quote::find($id);
        $quote->status_id =34 ;
        $quote->save();

         $quote->status()->attach(34,['observation'=>'WebSys: Cotizacion aceptada por clientes','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);

        if($quote->new_request_id!=null){
            $newrequest=NewRequest::find($quote->new_request_id);
            $newrequest->status_id=34;
            $newrequest->save();
        }

        //-----------envio de correo notificando al personal administrativo 
        $personal = Personal::select('name','email')->where('occupation','=','Administrativo')->get();
        $email = '';
        $cant = count($personal);
        $i=0;

                                $view='emails.plantilla';
                                $info['title']='Cotizacion Nro '.$quote->quote_number.' Aceptada ';
                                
                                $info['content'][0]= 'La cotizacion  Nro '.$quote->quote_number.' por favor verifique la informacion en el sistema.';
                                
                                $subject='Cotizacion Aceptada por el Cliente';

                                for($i;$i<$cant;$i++){
                                                 
                                    $mail=$this->sendmail($info,$personal[$i]['name'],$personal[$i]['email'],$subject,$view); 
                                }
                               
                               
        //-------------------------------------------------------------------                        
        $currency=$quote->currency;
        $status='accept';
      
       return \View::make('quote.accept',compact('status','currency'));
    }

    public function reject($id)
    {
        $quote= Quote::find($id);
        $quote->status_id =35 ;
        $quote->save();

        if($quote->new_request_id!=null){
            $newrequest=NewRequest::find($quote->new_request_id);
            $newrequest->status_id=35;
            $newrequest->save();
        }

        $status='reject';
      
       return \View::make('quote.accept',compact('status'));
    }

    public function resetpass(Request $request)
    {   
        if($request->ajax()) { 
        
        $pers = User::where('email',$request->email)->get();
        if(count($pers)>0){
                $newpass=$this->generaPass();
                $pers=User::find($pers[0]->id);
                $pers->password = bcrypt($newpass);
                
                if($pers->save()){
                        //envio de correo para indicar el cambio de clave... 
                        
                                $view='emails.plantilla';
                                $info['title']='Bienvenido '.$pers->name;
                                
                                $info['content'][0]= 'Su clave de ingreso ha sido cambiada con exito a continuacion observara la informacion actualizada de su usuario y contraseña';
                                $info['content'][1]='Tu usuario: '.$pers->email.'
                                                     Tu nueva clave: '.$newpass;
                                $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';
                                $name=$pers->name;
                                $email=$pers->email;
                                $subject='Cambio de Clave Sistema Sonitus';
                               
                                $mail=$this->sendmail($info,$name,$email,$subject,$view);
                                
                        return Response::json(['updated'=>$pers,'urlresponse'=>URL::route('auth/logout')]);
                }
            }else{

                return Response::json(['updated'=>404]);
            }
       } 
    }

    private function generaPass(){
        //Se define una cadena de caractares.
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        //Obtenemos la longitud de la cadena de caracteres
        $longitudCadena=strlen($cadena);
         
        //Se define la variable que va a contener la contraseña
        $pass = "";
        //Se define la longitud de la contraseña, en mi caso 6, pero puedes poner la longitud que quieras
        $longitudPass=6;
         
        //Creamos la contraseña
        for($i=1 ; $i<=$longitudPass ; $i++){
            //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
            $pos=rand(0,$longitudCadena-1);
         
            //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
            $pass .= substr($cadena,$pos,1);
        }
        return $pass;
    }

    //----------------------------------------------------------------------------------------------
    
 public function sendmail($info,$name,$email,$subject,$view)
    {   
       
       //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject) {
                $m->from(env('SENDER_ADDRES'), 'Informacion Sonitus');
                $m->to($email, $name)->subject($subject);

               
            });
       return $mail;

    }
//----------------------------------------------------------------------------------------------
}
