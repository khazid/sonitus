<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Messages;
use App\Process;
use Auth;
use Response;
use Mail;
class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    
    public function index()
    {   $userid=Auth::user()->id;
        $messages = Messages::join('users','users.id','=','messages.from_id')
                            ->select('messages.*','users.name','users.email') 
                            ->where('to_id','=',$userid)
                            ->whereIn('status_id', array(11)) 
                            ->get();
        $new=Messages::where('status_id','=',11)->where('to_id','=',$userid)->get(); 
        $countnew=count($new);                  
        return \View::make('messages/messages',compact('messages','countnew'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   $userid=Auth::user()->id;
        $process_id=$request->process_id;
        $correo=User::where('email',$request->addressee)->get();
        $idto=$correo[0]->id;
        $message = new Messages;
            $message->from_id=$userid;
            $message->to_id=$idto;
            $message->status_id=11; //new message
            $message->message=$request->messagetext;
            $message->tittle=$request->subject;
        
        if($message->save()){

            $personalto=User::find($idto);
            $personalfrom=User::find($userid);
            //se envia el email
                $info['title']='Estimado '.$personalto->name;
                $info['content'][0]='Tiene un nuevo mensaje en su bandeja de entrada del sistema Sonitus de parte de '.$personalfrom->name;
                $name=$personalto->name;
                $email=$personalto->email;
                $subject='Mensaje Sistema Sonitus';
               
                $this->sendmail($info,$name,$email,$subject,'');
        }
         return redirect()->back();   
        
        

    }

    public function messageprocess(Request $request)//guardar mensaje cuando viene del proceso
    {   $userid=Auth::user()->id;
        $process_id=$request->process_id;
        $message = new Messages;
            $message->from_id=$userid;
            $message->to_id=$request->addressee;
            $message->status_id=11; //new message
            $message->message=$request->messagetext;
            $message->tittle=$request->tittle;
        
        if($message->save()){//guardar relacion con la tabla de procesos
                    $pro=Process::find($process_id);
                    $pro->message()->attach($pro->id,['users_id'=>$userid]);
        }else{
            Session::flash('status','No se pudo guardar el mensaje');  
        }
         return redirect('process');   
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changestatus(Request $request){
    if($request->ajax()) {
        $idmessage=$request->idmessage;
        $status=$request->idstatus;
        //$ruta=$request->route;

        $mess = Messages::find($idmessage);
        $mess->status_id=$status;
        $mess->save();
     }   
      // return redirect($ruta);  
     return Response::json(['message'=>$mess]);
    }

    public function read(Request $request){
        $userid=Auth::user()->id;
        
        
            //$destinatary = User::lists('email','id');
            $messages = Messages::join('users','users.id','=','messages.from_id')
                            ->select('messages.*','users.name','users.email') 
                            ->where('to_id','=',$userid)
                            ->where('status_id','=',10)
                            ->get();
             
         return \View::make('messages/messages',compact('messages')); 
        
       
     }

     public function deleted(Request $request){
        $userid=Auth::user()->id;
        
        
            //$destinatary = User::lists('email','id');
            $messages = Messages::onlyTrashed()
                            ->join('users','users.id','=','messages.from_id')
                            ->select('messages.*','users.name','users.email') 
                            ->where('to_id','=',$userid)
                            ->get();
            return \View::make('messages/messages',compact('messages')); 
        
       
     }

     public function delete(Request $request){
        if($request->ajax()) {
            $id=$request->idmessage;
            $mess = Messages::find($id);
                $mess->delete();
           
            
         }   
          return Response::json(['deleted'=>$mess]);
     }

    public function destinatary(Request $request){
        if($request->ajax()) {

           $data = array(); //declaramos un array principal que va contener los datos
            $destinatary = User::all(); //listamos todos los id de los eventos
            $i=0;
            foreach($destinatary as $eve){
                $data[$i] = array(
                    "data"=>$eve->name,
                    "value"=>$eve->email, //obligatoriamente "title", "start" y "url" son campos requeridos
                     
                );
             $i++;   
            }
                //$data=json_encode($data);
            return Response::json(['data'=>$data]); 
           
         }
    }
//----------------------------------------------------------------------------------------------

    
 public function sendmail($info,$name,$email,$subject,$path)
    {   
        $view='emails.plantilla';
       
       
      
                
       //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject,$path) {
                $m->from(env('SENDER_ADDRES'), env('SENDER_NAME'));
                $m->to($email, $name)->subject($subject);
                $m->bcc(env('COPY_ADDRES'), env('COPY_NAME'));
                if($path!='')$m->attach($path);

               
            });
       return $mail;

    }


}
