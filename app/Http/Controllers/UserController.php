<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Personal;
use App\Client;
use App\Roles;
use Auth;
use Response;
use Mail;
use URL;
class UserController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = Auth::personal();
        $usep= Auth::user();

        $file= url('images/personal').'/'.$per->identity_number.'.png';
         if (! \Storage::exists($file))
         {
           $file= url('images/img/no-image.png');
         }
        
        // echo "<pre>";
        // print_r($per);
        // echo "</pre>";exit;
        return \View::make('staff.show',compact('per','file','usep'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $pers = User::find($id);
       
        return \View::make('users.update',compact('pers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatepass(Request $request)
    {   
        if($request->ajax()) { 
        
        $pers = User::where('email',$request->email)->get();
        if(count($pers)>0){
                $pers=User::find($pers[0]->id);
                $pers->password = bcrypt($request->newpass);
                
                if($pers->save()){
                        //envio de correo para indicar el cambio de clave... 
                        
                                $view='emails.plantilla';
                                $info['title']='Bienvenido '.$pers->name;
                                
                                $info['content'][0]= 'Su clave de ingreso ha sido cambiada con exito a continuacion observara la informacion actualizada de su usuario y contraseña';
                                $info['content'][1]='Tu usuario: '.$pers->email.'
                                                     Tu nueva clave: '.$request->newpass;
                                $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';
                                $name=$pers->name;
                                $email=$pers->email;
                                $subject='Cambio de Clave Sistema Sonitus';
                               
                                $mail=$this->sendmail($info,$name,$email,$subject,$view);
                                if($mail){
                                  return Response::json(['updated'=>'ok','urlresponse'=>URL::route('auth/logout')]);  
                              }else{
                                return Response::json(['updated'=>'error envio email']);
                              }
                                
                        
                }
            }else{

                return Response::json(['updated'=>404]);
            }
       } 
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function savenewuser(Request $request)
    {   
        if($request->ajax()) {    
                $provisional= $this->generaPass();
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($provisional);
                $user->active = TRUE;
                

            if($user->save()){ 
                if($request->type=='staff'){
                                //se actualiza el users_id en la tabla personal
                                 $personal = Personal::find($request->id);
                                 $personal->users_id=$user->id;
                                 $personal->save();

                $rol=Roles::where('slug',$personal->occupation)->get();
                $user->role()->attach($rol[0]->id);   
                
                }else if($request->type=='client'){
                                //se actualiza el users_id en la tabla cliente
                                 $client = Client::find($request->id);
                                 $client->users_id=$user->id;
                                 $client->save();
                $user->role()->attach(8);                
                }



               //se envia el email
                $view='emails.plantilla';
                $info['title']='Bienvenido al Sistema de Control de Procesos de Inversiones Sonitus '.$request->name;
                $info['content'][0]='Tu usuario: '.$request->email.'
                                     Tu clave provisional: '.$provisional;
                $info['content'][1]= 'Por favor recuerde cambiar su clave provisional ingresando a la opcion de perfil de usuario, Cambio de clave.';
                $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto te guiaremos con todo tu proceso.';
                $name=$request->name;
                $email=$request->email;
                $subject='Registro en Sistema Sonitus';
               
                $mail=$this->sendmail($info,$name,$email,$subject,$view);
                    if($mail){
                                     
                        \Session::flash('message','The registration email send for "'.$request->name.'" succed!!');
                    }
                }
         }   
        return Response::json(['created'=>$user->id]); 
    }

     private function generaPass(){
        //Se define una cadena de caractares.
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        //Obtenemos la longitud de la cadena de caracteres
        $longitudCadena=strlen($cadena);
         
        //Se define la variable que va a contener la contraseña
        $pass = "";
        //Se define la longitud de la contraseña, en mi caso 6, pero puedes poner la longitud que quieras
        $longitudPass=6;
         
        //Creamos la contraseña
        for($i=1 ; $i<=$longitudPass ; $i++){
            //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
            $pos=rand(0,$longitudCadena-1);
         
            //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
            $pass .= substr($cadena,$pos,1);
        }
        return $pass;
    }
//----------------------------------------------------------------------------------------------
    
 public function sendmail($info,$name,$email,$subject,$view)
    {   
       
       //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject) {
                $m->from(env('SENDER_ADDRES'), 'Informacion Sonitus');
                $m->to($email, $name)->subject($subject);

               
            });
       return $mail;

    }
//----------------------------------------------------------------------------------------------
    public function checkemail(Request $request)
    {   
       if($request->ajax()) {
        
        $user=User::where('email',$request->email)->get();
          if(count($user)<=0){
            $user='null';
          }
        }
      return Response::json(['email'=>$user]);

    
    }
//----------------------------------------------------------------------------------------------
}
