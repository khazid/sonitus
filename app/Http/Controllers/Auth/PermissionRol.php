<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Role_user as Role_user;
use App\User as User;
use App\Personal as Personal;
use App\MenuRol;
use App\PermRol;
use App\Messages;
use App\Client;

trait PermissionRol
{
     public function options()
    {
       $mrol= MenuRol::join('menu', 'menu.id', '=', 'menu_role.menu_id')
                                ->join('role_user', 'role_user.role_id', '=', 'menu_role.role_id')
                                ->select('role_user.role_id as idrol', 'menu.name as menuname', 'menu.id as idmenu','menu_role.active','menu_role.id','menu.icon')
                                ->where('role_user.user_id','=',$this->user()->id)
                                ->where('menu_role.active','=',TRUE)
                                ->orderby('position')
                                ->get();
                
        
        return $mrol;
    }

    /**
     * Find the permissions of the rol.
     **/
    public function permission()
    {
       $mrol= PermRol::join('permissions', 'permissions.id', '=', 'permission_role.permission_id')
                                ->join('role_user', 'role_user.role_id', '=', 'permission_role.role_id')
                                ->select('role_user.id as idrol', 'permissions.name as permname', 'permissions.id as idperm','permission_role.active','permission_role.id','permissions.slug','permissions.menu_id')
                                ->where('role_user.user_id','=',$this->user()->id)
                                ->where('permission_role.active','=',TRUE)
                                ->get();
                
        
        return $mrol;
    }

    public function message()
    {
       $mrol= Messages::join('users','users.id','=','messages.from_id')
                            ->select('messages.*','users.name') 
                            ->where('to_id','=',$this->user()->id)
                            ->where('status_id','=',11)
                            ->get();
                
        
        return $mrol;
    }

    public function personal()
    {
      
       
              $mrol= Personal::join('users', 'users.id', '=', 'personal.users_id')
                               ->select('personal.*')
                               ->where('personal.users_id','=',$this->user()->id)
                               ->get();
           if(count($mrol)>0){           
               $mrol[0]['img']="images/personal/".$mrol[0]->profile_pic;
             return $mrol[0];  
           }

           
    }

    public function rol()
    {
       $mrol= User::join('role_user', 'role_user.user_id', '=', 'users.id')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->select('users.id as userid','roles.id as roleid','roles.name as rol')
                        ->where('users.id','=',$this->user()->id)
                        ->get();
                
        return $mrol[0];
    }

    public function client()
    {
       $mrol= Client::join('users', 'users.id', '=', 'client.users_id')
                        ->select('client.*')
                        ->where('client.users_id','=',$this->user()->id)
                        ->get();
                
        return $mrol[0];
    }
}