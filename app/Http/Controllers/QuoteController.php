<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Quote;
use App\QuoteItems;
use App\Client;
use App\NewRequest;
use App\Status;
use App\BillingConfig;
use App\Process;
use App\QuoteStatus;
use App\User;
use App\Personal;
use Response;
use Auth;
use Mail;
use File;

class QuoteController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid= Auth::user()->id;
       //dd($userid);exit;
       $roles = User::find($userid)->role()->get();
       $rol=$roles[0]->id;
        $quotes=Quote::join('client','client.id','=','quote.client_id')
                       ->join('status','status.id','=','quote.status_id')
                       ->leftJoin('new_request','new_request.id','=','quote.new_request_id')
                       ->leftJoin('process','process.id','=','quote.process_id')
                       ->join('personal','personal.id','=','quote.personal_id')
                       ->select('client.id as clientid','client.name as client','status.id as statusid','status.name as status','quote.*','personal.name as personal','process.process_number');
        if($rol==4){//jefe tecnico
          $quotes=$quotes->where('quote.status_id','=',38); //solicitud de materiales por el personal administrativo 
          $quotes=$quotes->get();
        }else{
          $quotes=$quotes->get();
        }              
         //dd($quotes);exit;

        $status=Status::where('type',6)->orderBy('name')->lists('name','id');               
        $config=BillingConfig::all();
        if(count($config)>0) $configload="true";
        else $configload="false";
        return \View::make('quote.list',compact('quotes','status','rol','configload'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
       $client=Quote::join('client','quote.client_id','=','client.id')
                    ->select('client.*','quote.observation','quote.id as quoteid','quote.quote_number')
                    ->where('quote.id',$id)
                    ->get();
        $materialnew=QuoteItems::where('quote_items.quote_id',$id) //materiales por cotizar
                    ->whereNull('quote_items.unit_price')
                    ->get();
        $materials=QuoteItems::where('quote_items.quote_id',$id)//materiales ya cotizados
                    ->whereNotNull('quote_items.unit_price')
                    ->get();
        $client=$client[0];

        //si la cotizacion ya fue creada se modifican los datos 
        if($client->quote_number=='') $correlativo=$this->correlativo();
        else $correlativo=$client->quote_number;

       return \View::make('quote.new',compact('client','materialnew','correlativo','materials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $userid= Auth::user()->id;
       //buscar configuracion de facturas  
        $config=BillingConfig::all();
        $config=$config->last();

       //Actualizar informacion de la cotizacion
        $quote=Quote::find($request->quoteid);
        $quote->quote_number=$request->quotenumber;
        $quote->pay_type=$request->paytype;
        $quote->currency=$request->currency;
        $quote->travel=$request->travel;
        $quote->status_id=41;
        $quote->users_id=$userid;
        $quote->tecnicians=$request->technicians;
        $quote->days=$request->days;
        $quote->billing_config_id=$config->id;
        
        //--------VIAJES--------------------
        if($request->travel=='true'){
            $trans=str_replace(',', '', $request->transportation);
            $vi=str_replace(',', '', $request->viaticum);
            $lo=str_replace(',', '', $request->lodgment);

            //se guarda cada item bruto por separado en la BD
            $quote->transportation=$trans;  
            $quote->viaticum=$vi;
            $quote->lodgment=$lo;

            //se calcula el item segun formula indicada
            //$trans=$trans * $request->technicians; //calculo de pasaje por cantidad de tecnicos
            $vi= $vi * $request->technicians; // calculo de viaticos por cantidad de tecnicos
            $lo=(($request->technicians / 2) * $lo) * $request->days; //precio del hospedaje por la cantidad tecnicos entre 2  por la cantidad de dias
            
            //suma total de los viaticos para colocarlo en un solo item en la cotizacion
            $traveltotal=$trans + $vi + $lo;
        
           
        }

        //---------Horas Extras Nocturnas------------------------
        $quote->night_work=$request->night;

        if($request->night=='true'){
          $nocturno=true;
          $quote->night_work_hours=$request->nighthour;  //BD item cantidad de horas 
          $quote->night_work_price=str_replace(',', '', $request->nighthourprice);  //BD item monto total horas nocturnas 
          
          $nighttech=str_replace(',', '', $request->nighthourprice);//para calcular el total de los viaticos
          /*$nighttotal=($config->daily_salary * $request->nighthour) * 1.8;
          $nighttech=$nighttotal * $request->technicians; */

        }else{
          $nighttech=0;
        }

        //---------Horas Extras Diurnas------------------------
        $quote->day_work=$request->day;

        if($request->day=='true'){
          $diurno=true;
          $quote->day_work_hours=$request->dayhour; //BD item
          $quote->day_work_price=str_replace(',', '', $request->dayhourprice);  //BD item monto total horas nocturnas 

          $daytech=str_replace(',', '', $request->dayhourprice);//para calcular el total de los viaticos
          /*$daytotal=($config->daily_salary * $request->dayhour) * 1.8;
          $daytech=$daytotal * $request->technicians; */

        }else{
          $daytech=0;
        }
                  

        if($request->travel=='true'){
            $total=$daytech + $nighttech + $traveltotal;  
            $nightitem=new QuoteItems;
                        $nightitem->quote_id=$request->quoteid;
                        $nightitem->description='Transporte, Hospedaje, Refrigerio Y Alimentacion para el personal tecnico';
                        $nightitem->quantity=1;
                        $nightitem->unit_price=$total;
                        $nightitem->total=$total;
                        $nightitem->save();
          }
        
        

        
        if($quote->save()){
          $quote->status()->attach(41,['observation'=>'QuoteSys: Creacion de cotizacion','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
            
                $i=0;
                foreach($request->mitexto as $m){
                    if($m!='' && $request->itemid[$i]==''){ // guardar nuevos items a la cotizacion 
                        $qitem=new QuoteItems;
                        $qitem->quote_id=$request->quoteid;
                        $qitem->description=$m;
                        $qitem->quantity=$request->cant[$i];
                        $qitem->unit_price=str_replace(',', '', $request->unitprice[$i]);
                        $qitem->total=str_replace(',', '', $request->total[$i]);
                        $qitem->save();
                    
                    }elseif($m!='' && $request->itemid[$i]!=''){ // actualizar materiales 
                        $qitemmod=QuoteItems::find($request->itemid[$i]);
                        $qitemmod->description=$m;
                        $qitemmod->quantity=$request->cant[$i];
                        $qitemmod->unit_price=str_replace(',', '', $request->unitprice[$i]);
                        $qitemmod->total=str_replace(',', '', $request->total[$i]);
                        $qitemmod->save();

                    }

                $i++;
                }


        
            // actualizar precios materiales solicitados por los tecnicos 
                if($request->cantmaterials > 0){
                    $i=0;
                    foreach($request->id as $m){
                        if($request->matprice[$i]!=''){
                            $qmat=QuoteItems::find($m);
                            $qmat->unit_price=str_replace(',', '', $request->matprice[$i]);
                            $qmat->total=str_replace(',', '', $request->mattotal[$i]);
                            $qmat->save();
                        }
                    $i++;
                    }
                }
        }

       return redirect(route('quote.index'));
    }

public function storematerial(Request $request)
    {
       
            // guardar nuevos items a la cotizacion 
                $i=0;
                foreach($request->mitexto as $m){
                    if($request->mitexto!=''){
                        $qitem=new QuoteItems;
                        $qitem->quote_id=$request->quoteid;
                        $qitem->description=$m;
                        $qitem->quantity=$request->cant[$i];
                        $qitem->save();
                    }
                $i++;
                }
        
        $quote=Quote::find($request->quoteid);
        $quote->status_id=39;
        $quote->save();
        $quote->status()->attach(39,['observation'=>'QuoteSys: Carga de Materiales y equipos de cotizacion','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);

        if($quote->process_id!=null){
          $process=Process::find($quote->process_id);
          $process->status_id=22;
          $process->save();
          $process->status()->attach(22,['observation'=>'QuoteSys:Carga de Materiales y equipos de cotizacion','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
          return redirect(route('process.index'));
        }else{
          return redirect(route('quote.index'));
        }    
        

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client=Quote::join('client','quote.client_id','=','client.id')
                     ->select('client.name','client.applicant_name','client.rif','client.phone','client.email','client.address','quote.*','quote.id as quoteid','quote.created_at as quotedate')
                    ->where('quote.id',$id)
                    ->get();
         $client=$client[0];           
        //buscar configuracion de facturas  
        $config=BillingConfig::find($client->billing_config_id);

        if(count($config)>0){/// verificar si existe la configuracion de la cotizacion
            // dd($config);exit;
            $material=QuoteItems::where('quote_items.quote_id',$id)
                        ->whereNotNull('quote_items.unit_price')
                        ->get();

            $subtotal=QuoteItems::selectRaw('SUM(total) as total')
                        ->where('quote_items.quote_id',$id)
                        ->whereNotNull('quote_items.unit_price')
                        ->get();

            $subtotal=$subtotal[0]->total;   

            if($client->currency=='Bs'){         
                    $iva=( $subtotal * $config->iva) / 100; 
            
                    $total= $subtotal + $iva;   
            }else{
                 $total= $subtotal;
            }        
            
            // return \View::make('pdf.invoice',compact('client','config','material','subtotal','iva','total'));
            //-----INFORMACION PARA EL PDF -------------
                if($client->currency=='Bs'){
                    $viewpdf =  \View::make('quote/quoteBs',compact('client','config','material','subtotal','iva','total'))->render(); 
                }else if($client->currency=='Dolar'){
                    $viewpdf =  \View::make('quote/quoteDol',compact('client','config','material','total'))->render();
                }
                $name= 'Quote_'.$client->quote_number.'.pdf';
                $path= public_path().'/pdf/'.$name;
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($viewpdf);
                
                if($client->currency=='Bs'){
                   return $pdf->stream('quote/quoteBs',compact('client','config','material','subtotal','iva','total'));
                }else if($client->currency=='Dolar'){
                    return $pdf->stream('quote/quoteDol',compact('client','config','material','total'));
                }

        }else{
          Session::flash('message','No se pudo encontrar la configuracion para esta cotizacion, veirifique en la lista de configuracion de Facturacion e intentelo nuevamente. ');
          return redirect(route('quote.index'));
        }
       
            
           
    }

public function send($id)
    {
        $client=Quote::join('client','quote.client_id','=','client.id')
                     ->select('client.name','client.applicant_name','client.rif','client.phone','client.email','client.address','quote.*','quote.id as quoteid','quote.created_at as quotedate')
                    ->where('quote.id',$id)
                    ->get();
         $client=$client[0];           
        //buscar configuracion de facturas  
        $config=BillingConfig::find($client->billing_config_id);
        
        // dd($config);exit;
        $material=QuoteItems::where('quote_items.quote_id',$id)
                    ->whereNotNull('quote_items.unit_price')
                    ->get();
        $subtotal=QuoteItems::selectRaw('SUM(total) as total')
                    ->where('quote_items.quote_id',$id)
                    ->whereNotNull('quote_items.unit_price')
                    ->get();
        $subtotal=$subtotal[0]->total;            
        if($client->currency=='Bs'){         
                $iva=( $subtotal * $config->iva) / 100; 
        
                $total= $subtotal + $iva;   
        }else{
             $total= $subtotal;
        }            
        
        //-----INFORMACION PARA EL PDF -------------
           if($client->currency=='Bs'){
                $viewpdf =  \View::make('quote/quoteBs',compact('client','config','material','subtotal','iva','total'))->render(); 
            }else if($client->currency=='Dolar'){
                $viewpdf =  \View::make('quote/quoteDol',compact('client','config','material','total'))->render();
            }
            $name= 'Quote_'.$client->quote_number.'.pdf';
            $path= public_path().'/pdf/'.$name;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($viewpdf);
            $pdf->save($path);
           
        //-----INFORMACION PARA EL CORREO ------------------
            
                    $name=$client->name;
                    $email= $client->email;

            $content['content'][0]='El siguiente correo tiene la finalidad de entregarle la Cotizacion realizada segun su solicitud';
            $content['content'][1]='Por favor verifique la informacion, En caso de "Aceptarla" puede notificarlo presionando el siguiente enlance '.url('/').'/accept/'.$client->quoteid.'. En Caso de "Rechazo" presione el siguiente enlace '.url('/').'/reject/'.$client->quoteid.' y nos pondremos en contacto con usted';
            $info['content'][2]='Si necesitas mas informacion por favor comunicate con nosotros a traves del correo info.sonitus@isonitus.com o por el telefono +(58)(212)5778891 y con gusto le ayudaremos.';   
            $content['title']='Estimado Cliente '.$client->name; 
            $subject='Cotizacion Sonitus'; 
             $mail=$this->sendmail($content,$name,$email,$subject,$path);
           
           if($mail){
            File::Delete($path);
            
            $quote=Quote::find($client->quoteid);
            $quote->status_id=36;
            $quote->save();
            $quote->status()->attach(36,['observation'=>'QuoteSys: Envio de cotizacion al cliente','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
            if($client->new_request_id!=null){
                        $newrequest=NewRequest::find($client->new_request_id);
                        $newrequest->status_id=28;
                        $newrequest->save();
            }

            Session::flash('message','Correo enviado con exito ');
            } 

           
           return redirect(route('quote.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updatestatus(Request $request)
    {
        $quote= Quote::find($request->quoteid);
        $quote->status_id = $request->newstatus;

        $statusinfo= Status::find($request->newstatus);

        if($quote->save()){
                $quote->status()->attach($request->newstatus,['observation'=>'QuoteSys: '.$request->statusob,'users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);

              }
        if($quote->process_id!=null){

          $process=Process::find($quote->process_id);
          $process->status_id = $request->newstatus;
          $process->save();
        
        //------- SE ENVIA CORREO AL JEFE TECNICO ASIGNADO AL PROYECTO PARA INFORMAR EL CAMBIO DE ESTADO
          $jefetecnico= Personal::find($process->assigned_id);
          $name=$jefetecnico->name;
          $email= $jefetecnico->email;
          $processnum=$process->process_number;

            $content['title']='Sr(a) '.$name; 

            $subject='Actualizacion de Estado del Proyecto '.$processnum; 

            $content['content'][0]='El siguiente correo tiene la finalidad de informar sobre el cambio de estado del Proyecto '.$processnum.'El cual ha cambiado a Estado "'.$statusinfo->description.'"';
            
            if($request->newstatus==44){ //Estado de Equipos entregados 
              $content['content'][1]='Por favor verifique el listado de proyectos y actualice el estado a "Continuar proceso" para poder agendar nuevas visitas al cliente';
             }  
            
             $mail=$this->sendmail($content,$name,$email,$subject,'');
           
           Session::flash('message','Correo enviado con exito a '.$name);
              
        //----------------------------------------------------------------------------------------------

        }      

       return redirect('quote');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function askmaterials(Request $request)
    {
        if($request->ajax()){

            $quote=Quote::find($request->requestid);
            $quote->status_id=38;
            
            if($quote->save()){
                $quote->status()->attach(38,['observation'=>'QuoteSys: Solicitud de materiales a tecnicos','users_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
                return Response::json(['send'=>'good']);
            }else{
                return Response::json(['send'=>404]);
            }
        }
    }

    public function addmaterials($id)
    {
        $client=Quote::join('client','quote.client_id','=','client.id')
                    ->select('client.*','quote.observation','quote.id as quoteid')
                    ->where('quote.id',$id)
                    ->get();
       $client=$client[0];
        
       return \View::make('quote.material',compact('client'));
    }

    public function correlativo()
    {
        $ano=substr(date('Y'),2,2);
        $cantidad=Quote::whereRaw("quote_number like ('%-".$ano."') ")->orderBy('quote_number','ASC')->get();

        if(count($cantidad)>0){
            $cantidad= $cantidad->last();
            
            $quotenumber=$cantidad->quote_number;

            $numero=substr($quotenumber,0,4)+1;
            if($numero < 10){
                $numero='000'.$numero;
            }else if($numero >= 10 || $numero < 100){
                $numero='00'.$numero;
            }else if($numero >= 100 || $numero < 1000){
                $numero='0'.$numero;
            }else if($numero >= 1000 || $numero <= 9999){
                $numero=$numero;
            }
            $numero=$numero.'-'.$ano;

        }else{
            $numero='0001-'.$ano;
        }
        
        return $numero;


    }
//----------------------------------------------------------------------------------------------
    
 public function sendmail($info,$name,$email,$subject,$path)
    {   
       $view='emails.email';
       
      //se envia el email
             $mail= Mail::queue($view, ['info' => $info ], function ($m) use ($name,$email,$subject,$path) {
                $m->from(env('SENDER_ADDRES'), env('SENDER_NAME'));
                $m->to($email, $name)->subject($subject);
                $m->bcc(env('COPY_ADDRES'), env('COPY_NAME'));
                if($path!='')$m->attach($path);

               
            });
       return $mail;

    }

    public function showstatus($id)
    {   
       
        
        $status=QuoteStatus::join('quote','quote.id','=','quote_status.quote_id')
                       ->join('status','status.id','=','quote_status.status_id')
                       ->join('users','users.id','=','quote_status.users_id')
                       ->select('users.name','status.name as status','quote_status.observation','quote_status.created_at','quote.client_id')
                       ->where('quote.id',$id)
                       ->orderBy('quote_status.created_at')
                       ->get();
        $client = Client::withTrashed()->where('id','=',$status[0]->client_id )->get();
        $client=$client[0];
       return \View::make('quote.status',compact('client','status'));
    }

}
