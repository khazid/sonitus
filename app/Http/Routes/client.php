<?
 //--------------- rutas de client -->
Route::post('savenewuser', ['as' => 'client/savenewuser', 'uses'=>'ClientController@savenewuser']);
//web request
Route::post('responserequest', ['as' => 'client/responserequest', 'uses'=>'ClientController@responserequest']);
Route::post('findresponse', ['as' => 'client/findresponse', 'uses'=>'ClientController@findresponse']);
Route::post('client/requestnotify', ['as' => 'client/requestnotify', 'uses'=>'ClientController@requestnotify']);
Route::get('client/newproject/{id}', ['as' => 'client/newproject', 'uses' => 'ClientController@newproject']);
Route::get('client/searchletter/{id}', ['as' => 'client/searchletter', 'uses' => 'ClientController@searchletter']);
Route::get('client/newrequest', ['as' => 'client/newrequest', 'uses' => 'ClientController@newrequest']);
Route::post('client/request', ['as' => 'client/request', 'uses' => 'ClientController@request']);
Route::get('client/quality/{id}', ['as' => 'client/quality', 'uses' => 'ClientController@quality']);
Route::get('client/showquality/{id}', ['as' => 'client/showquality', 'uses' => 'ClientController@showquality']);
Route::post('client/savequality', ['as' => 'client/savequality', 'uses' => 'ClientController@savequality']);
Route::get('client/listwebrequest', ['as' => 'client/listwebrequest', 'uses' => 'ClientController@listwebrequest']);
Route::post('client/destroy', ['as' => 'client/destroy', 'uses' => 'ClientController@destroy']);
Route::get('client/activeclient/{id}', ['as' => 'client/activeclient', 'uses' => 'ClientController@activeclient']);
Route::get('client/searchdeleted', ['as' => 'client/searchdeleted', 'uses' => 'ClientController@searchdeleted']);
Route::post('quoterequest', ['as' => 'client/quoterequest', 'uses' => 'ClientController@quoterequest']);
Route::get('client/showQuote/{id}', ['as' => 'client/showQuote', 'uses' => 'ClientController@showQuote']);
Route::post('client/update', ['as' => 'client/update', 'uses' => 'ClientController@update']);


Route::resource('client', 'ClientController', ['except' =>['update']]);