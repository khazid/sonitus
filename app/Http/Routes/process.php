<?php
//Process routes
Route::get('process/webprocess', ['as' => 'process/webprocess', 'uses'=>'ProcessController@webprocess']);
Route::post('endtask', ['as' => 'process/endtask', 'uses'=>'ProcessController@endtask']);
Route::post('process/searchclient', ['as' => 'process/searchclient', 'uses'=>'ProcessController@searchclient']);
Route::get('process/show/{id}/{type}', ['as' => 'process/show', 'uses'=>'ProcessController@show']);
Route::get('process/showstatus/{id}', ['as' => 'process/showstatus', 'uses'=>'ProcessController@showstatus']);
Route::get('process/pdfshow/{id}', ['as' => 'process/pdfshow', 'uses'=>'ProcessController@pdfshow']);
Route::resource('process','ProcessController');

//una nueva ruta para guardar las opciones de los roles 
Route::get('process/store/{id}', ['as' => 'process/store', 'uses'=>'ProcessController@store']);

//ruta para realizar busqueda de registros.
Route::post('process/search', ['as' => 'process/search', 'uses'=>'ProcessController@search']);

//una nueva ruta para eliminar registros con el metodo get
Route::get('process/destroy/{id}', ['as' => 'process/destroy', 'uses'=>'ProcessController@destroy']);

//una nueva ruta para buscar informacion del proceso web
Route::get('process/findprocessweb/{id}', ['as' => 'process/findprocessweb', 'uses'=>'ProcessController@findprocessweb']);

//Ruta para guardar la informacion del proceso cargado por el tecnico
Route::post('process/save', ['as' => 'process/save', 'uses'=>'ProcessController@save']);

//una nueva ruta para actualizar al tecnico del proceso
Route::post('process/updatetec', ['as' => 'process/updatetec', 'uses'=>'ProcessController@updatetec']);
//una nueva ruta para actualizar prioridad
Route::post('process/updateprio', ['as' => 'process/updateprio', 'uses'=>'ProcessController@updateprio']);
//una nueva ruta para actualizar status
Route::post('process/updatestatus', ['as' => 'process/updatestatus', 'uses'=>'ProcessController@updatestatus']);
//---firma para el cierre del proceso
Route::get('process/signature/{id}', ['as' => 'process/signature', 'uses'=>'ProcessController@signature']);
Route::post('process/savesignature', ['as' => 'process/savesignature', 'uses'=>'ProcessController@savesignature']);




//una nueva ruta para buscar informacion del proceso
Route::get('visits', ['as' => 'visits.index', 'uses'=>'VisitsController@index']);
Route::get('visits/findprocess/{id}', ['as' => 'visits/findprocess', 'uses'=>'VisitsController@findprocess']);
Route::post('visits/create/{id}', ['as' => 'visits/create', 'uses'=>'VisitsController@create']);
Route::post('visits/update/{id}', ['as' => 'visits/update', 'uses'=>'VisitsController@update']);
Route::get('visits/show/{id}', ['as' => 'visits.show', 'uses'=>'VisitsController@show']);
Route::get('visits/edit/{id}', ['as' => 'visits.edit', 'uses'=>'VisitsController@edit']);
Route::get('visits/calendar', ['as' => 'visits/calendar', 'uses'=>'VisitsController@calendar']);
Route::post('visits/schedule', ['as' => 'visits/schedule', 'uses'=>'VisitsController@schedule']);
Route::get('visits/calendarscheduled', ['as' => 'visits/calendarscheduled', 'uses'=>'VisitsController@calendarscheduled']);
Route::post('searchprojects', ['as' => 'visits/searchprojects', 'uses'=>'VisitsController@searchprojects']);
Route::post('visits/deletevent', ['as' => 'visits/deletevent', 'uses'=>'VisitsController@deletevent']);
Route::post('visits/updateevent', ['as' => 'visits/updateevent', 'uses'=>'VisitsController@updateevent']);
Route::post('visits/searchvisitdetail', ['as' => 'visits/searchvisitdetail', 'uses'=>'VisitsController@searchvisitdetail']);
Route::post('visits/requesttoclient', ['as' => 'visits/requesttoclient', 'uses'=>'VisitsController@requesttoclient']);
Route::get('visits/buscarphotos/{id}', ['as' => 'visits/buscarphotos', 'uses'=>'VisitsController@buscarphotos']);

//rutas de ajax para buscar info de visitas
Route::post('saveouthr', ['as' => 'visits/saveouthr', 'uses'=>'VisitsController@saveouthr']);
Route::post('buscarsolicitudes', ['as' => 'visits/buscarsolicitudes', 'uses'=>'VisitsController@buscarsolicitudes']);
Route::post('visits/deletephoto', ['as' => 'visits/deletephoto', 'uses'=>'VisitsController@deletephoto']);
Route::post('savesignature', ['as' => 'visits/savesignature', 'uses'=>'VisitsController@savesignature']);
Route::post('viewsignature', ['as' => 'visits/viewsignature', 'uses'=>'VisitsController@viewsignature']);
Route::post('endadmin', ['as' => 'visits/endadmin', 'uses'=>'VisitsController@endadmin']);
Route::post('endadmindone', ['as' => 'visits/endadmindone', 'uses'=>'VisitsController@endadmindone']);
Route::post('notmade', ['as' => 'visits/notmade', 'uses'=>'VisitsController@notmade']);
//una nueva ruta para actualizar al tecnico del proceso
Route::post('visits/updatetec', ['as' => 'visits/updatetec', 'uses'=>'VisitsController@updatetec']);
Route::post('findtec', ['as' => 'visits/findtec', 'uses'=>'VisitsController@findtec']);
Route::post('visits/searchclients', ['as' => 'visits/searchclients', 'uses'=>'VisitsController@searchclients']);

Route::get('visits/pdf', ['as' => 'visits/pdf', 'uses'=>'VisitsController@pdf']);
Route::get('visits/sendpdf/{id}', ['as' => 'visits/sendpdf', 'uses'=>'VisitsController@sendpdf']);

Route::get('visits/pdftest/{id}', ['as' => 'visits/pdftest', 'uses'=>'VisitsController@pdftest']);


