<?
//ruta para el envio de mensajes
Route::post('mess/destinatary', ['as' => 'mess/destinatary', 'uses'=>'MessagesController@destinatary']);
Route::post('mess/send', ['as' => 'mess/send', 'uses'=>'MessagesController@store']);
Route::post('messageprocess', ['as' => 'messages/messageprocess', 'uses'=>'MessagesController@messageprocess']);
Route::post('changestatus', ['as' => 'messages/changestatus', 'uses'=>'MessagesController@changestatus']);
Route::post('delete', ['as' => 'messages/delete', 'uses'=>'MessagesController@delete']);
Route::get('mess/deleted', ['as' => 'mess/deleted', 'uses'=>'MessagesController@deleted']);
Route::get('mess/read', ['as' => 'mess/read', 'uses'=>'MessagesController@read']);

Route::resource('mess', 'MessagesController', ['except' => [
    'destroy','create'
]]);