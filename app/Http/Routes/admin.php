<?
//------------------Rutas de Cotizacion ---------------
Route::post('quote/updatestatus', ['as' => 'quote/updatestatus', 'uses'=>'QuoteController@updatestatus']);
Route::post('askmaterials', ['as' => 'quote/askmaterials', 'uses'=>'QuoteController@askmaterials']);
Route::get('quote/addmaterials/{id}', ['as' => 'quote/addmaterials', 'uses'=>'QuoteController@addmaterials']);
Route::get('quote/addmaterialsproject/{id}', ['as' => 'quote/addmaterialsproject', 'uses'=>'QuoteController@addmaterialsproject']);
Route::post('quote/storematerial', ['as' => 'quote/storematerial', 'uses'=>'QuoteController@storematerial']);
Route::get('quote/create/{id}', ['as' => 'quote/create', 'uses'=>'QuoteController@create']);
Route::get('quote/send/{id}', ['as' => 'quote/send', 'uses'=>'QuoteController@send']);
Route::get('quote/showstatus/{id}', ['as' => 'quote/showstatus', 'uses'=>'QuoteController@showstatus']);

Route::resource('quote', 'QuoteController', ['except' => ['create']]);

Route::get('adminclose', ['as' => 'visits.adminclose', 'uses'=>'VisitsController@adminclose']);
