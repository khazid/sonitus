<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    protected $table = 'client';
    protected $guarded = ['id']; 
  	protected $fillable = ['name', 'rif', 'address','applicant_name','charge','departmen','headquarters','phone','celphone','email','users_id','created_by'];
  	protected $dates = ['deleted_at'];

  	public function process()
    {
        return $this->belongsToMany('App\Process', 'client_process','client_id','process_id');
    }
}
