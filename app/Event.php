<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $table = 'events';
    protected $guarded = ['id']; 
  	protected $fillable = ['title', 'description', 'type','personal_id','visits_id','startdate','enddate','allday','users_id'];
  	protected $dates = ['deleted_at'];
}
