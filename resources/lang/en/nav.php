<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */

   	'menu' => [
        'Welcome'     => 'Welcome',
        'Home'    => 'Home',
        'Reports'   => 'Reports',
        'Staff'   => 'Staff',
        'Clients'   => 'Clients',
        'Inbox'   => 'Inbox',
        'Projects'   => 'Projects',
        'Visits'   => 'Visits',
        'Request'   => 'Requests',
        'Configuration'   => 'Configuration',
        'Profile'   => 'Profile',
        'Logout'   => 'Log Out',
        'nomess'   => 'No Messages to show',
        'allalerts'=> 'See All Messages',
        'Calendar'=> 'Calendar',
        
    ],
    'submenu' => [
        'title'     => 'User Registration     ',
        'submit'    => 'Submit',
        'back'    => 'Back',
        'reports_charts'    => 'Report Charts',
        'reports_lists'    => 'Report Lists',
        'deleted_staff'    => 'Deleted Staff',
        'staff_list'    => 'Staff List',
        'request_client'    => 'Request To Clients',
        'web_request'    => 'Web Requests',
        'client_list'    => 'Clients List',
        'deleted_clients'    => 'Deleted Clients',
        'inbox'    => 'Inbox',
        'project_list'    => 'Project list',
        'new_project'    => 'New Project',
        'calendar'    => 'Calendar',
        'visit_list'    => 'Visits List',
        'quote_list'    => 'Quotes List',
        'configuration'    => 'Configuration',
        'billing'    => 'Billing',
        'holidays'    => 'Holidays',
        'menu'    => 'Menu',
        'sub_menu'    => 'Sub-Menu',
        'roles'    => 'Roles',
        'admin_close' => 'Admin Close',
    ],
    'dashboard' => [
        'newweb'     => 'New WEB Request',
        'lastweek'     => 'From last week',
        'newvisit'    => 'New Visits',
        'newprocess'    => 'New Projects',
        'finishpro'     => 'Finished Projects',
        'closepro'     => 'Closed Projects',
        'processpro'    => 'Projects in Progress',
        'graphh3'    => 'Visits Activity',
        'graphsmall' => 'Visits information in the month ',
        'graphh2'    => 'Visits Report',   
        'graphd1'    => 'Total Visits',
        'graphd2'    => 'Visits Assigned',
        'graphd3'    => 'Visits Made',
        'graphd4'    => 'Visits Not Made',
        'genevent'   => 'General Events',
        'eventdone'  =>'Visits Made In The Month',
        'visits'  =>'Visits',
        'eventtomade'=>'Visits To Made In The Month',
        'eventtomadesmall'=> 'Click the Tag',
        'seedetail'             => 'See More Detail ',
        'newquote'             => 'New Quote ',
        'acceptquote'             => 'Accepted Quote',
        'askmat'             => 'Quote Ask Materials ',
        'loadmat'             => 'Quote Loaded Materials ',
        'deliveredbill'             => 'Bill Delivered ',



    ],
 	
    
];
