<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */

   	'menu' => [
        'Welcome'     => 'Bienvenido',
        'Home'    => 'Principal',
        'Reports'   => 'Reportes',
        'Staff'   => 'Personal',
        'Clients'   => 'Clientes',
        'Inbox'   => 'Mensajes',
        'Projects'   => 'Procesos',
        'Visits'   => 'Visitas',
        'Request'   => 'Solicitudes',
        'Configuration'   => 'Configuracion',
        'Profile'   => 'Perfil',
        'Logout'   => 'Salir',
        'nomess'   => 'No hay mensajes que mostrar',
        'allalerts'=> 'Ver todos los mensajes',
        'Calendar'=> 'Calendario',
        
    ],
    'submenu' => [
        'title'     => 'Registro de usuario     ',
        'submit'    => 'Guardar',
        'back'    => 'Volver',
        'reports_charts'    => 'Graficos',
        'reports_lists'    => 'Listas',
        'deleted_staff'    => 'Personal Eliminado',
        'staff_list'    => 'Lista de Personal',
        'request_client'    => 'Solicitudes al Cliente',
        'web_request'    => 'Solicitudes Web',
        'client_list'    => 'Lista de Clientes',
        'deleted_clients'    => 'Clientes Eliminados',
        'inbox'    => 'Bandeja de Mensajes',
        'project_list'    => 'Lista de Proyectos',
        'new_project'    => 'Nuevo Proyecto',
        'calendar'    => 'Calendario',
        'visit_list'    => 'Lista de Visitas',
        'quote_list'    => 'Lista de Cotizaciones',
        'configuration'    => 'Configuracion',
        'billing'    => 'Facturacion',
        'holidays'    => 'Dias Feriados',
        'menu'    => 'Menu',
        'sub_menu'    => 'Sub-Menu',
        'roles'    => 'Roles',
        'admin_close' => 'Cierre Administrativo',
        

    ],
    'dashboard' => [
        'newweb'     => 'Nueva Solicitud WEB',
        'lastweek'     => 'De la semana pasada',
        'newvisit'    => 'Nuevas Visitas',
        'newprocess'    => 'Nuevos Proyectos',
        'finishpro'     => 'Proyectos Terminados',
        'closepro'     => 'Proyectos Cerrados',
        'processpro'    => 'Proyectos en Proceso',
        'graphh3'    => 'Actividad de las Visitas',
        'graphsmall' => 'Informacion de Visitas en el Mes',
        'graphh2'    => 'Reporte de Visitas',   
        'graphd1'    => 'Cantidad de visitas',
        'graphd2'    => 'Visitas assignadas',
        'graphd3'    => 'Visitas realizadas',
        'graphd4'    => 'Visitas No Realizadas',
        'genevent'   => 'Eventos Generales',
        'eventdone'  =>'Visitas hechas en el Mes',
        'visits'  =>'Visitas',
        'eventtomade'=>'Visitas Del Mes',
        'eventtomadesmall'=> 'Haga click sobre la pestaña',
        'seedetail'             => 'Ver Detalle ',
        'newquote'             => 'Nueva Solicitud Cotizacion ',
        'acceptquote'             => 'Cotizaciones Aceptadas',
        'askmat'             => 'Solicitud Materiales Cotizacion ',
        'loadmat'             => 'Materiales Cargados Cotizacion ',
        'deliveredbill'             => 'Facturas Pendientes ',


    ],
 	
    
];
