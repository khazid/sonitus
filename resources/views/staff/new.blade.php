@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}

    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Staff</h3>
              </div>

             </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.staffmember')!!} <small>{{$title}}</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                 
                    {!! Form::open(['route' => $ruta, 'method' => 'post','class'=>"form-horizontal form-label-left",'data-parsley-validate','id'=>'form-validation','files' => true]) !!}

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">{!!trans('form.label.name')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" required="required" type="text" value="{{@$personal->name}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">{!!trans('form.label.identity')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="identity" class="form-control col-md-7 col-xs-12" name="identity" placeholder="identity number without separators Example: 15458587" required="required" type="text" value="{{@$personal->identity_number}}" data-inputmask="'mask': '99999999'">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">{!!trans('form.label.address')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="address" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" value="{{@$personal->address}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">{!!trans('form.label.email')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" data-parsley-trigger="change" value="{{@$personal->email}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{!!trans('form.label.fecha_nac')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="birthday" name="birthday" type="text" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99/99/9999'" required="required" value="{{date('d/m/Y',strtotime(@$personal->date_birth))}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="initialdate">{!!trans('form.label.initday')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="initialdate" name="initialdate" type="text" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99/99/9999'" required="required" value="{{date('d/m/Y',strtotime(@$personal->initial_date))}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">{!!trans('form.label.phone')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="telephone" name="telephone" type="text" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '+(99999)999-9999'" required="required" value="{{@$personal->telephone}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">{!!trans('form.label.occupation')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        @if($ruta=='personal/update')
                        <input type="address" id="oldoccupation" name="oldoccupation" class="form-control col-md-7 col-xs-12" value="{{@$personal->occupation}}" readonly="readonly">
                        @endif
                         {!! Form::select('slug', $option,null,['class' => 'form-control']) !!}
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupdescrip">{!!trans('form.label.occupationdesc')!!}<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="textarea" required="required" name="occupdescrip" class="form-control col-md-7 col-xs-12">{{@$personal->occupation_description}}</textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">{!!trans('form.label.profilepic')!!}<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          @if($ruta=='personal/update')
                          <label>Old Picture:</label>
                          <img class="img-responsive avatar-view" src="{{$file}}" alt="Avatar" title="Change the avatar" >
                          <input type="hidden" name="oldpic" value="{{@$personal->profile_pic}}">
                          <input type="hidden" name="id" value="{{@$personal->id}}">
                          <label>Choose new profile picture:</label>
                          @endif
                          {!! Form::file('image',null) !!}
                        </div>
                      </div>
                      @if($ruta=='personal.store')
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="registration">{!!trans('form.label.userreg')!!}?<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="checkbox" class="js-switch" name="registration" id="registration"/>
                        </div>
                      </div>
                      @endif
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          
                           <a href="{{ URL::previous() }}" class="btn btn-primary">{!!trans('form.label.cancel')!!}</a>

                          <button id="send" type="submit" class="btn btn-success enviar">{!!trans('form.label.submit')!!}</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
@section('script')

   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
  
    <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
    <!-- Switchery -->
    {!! Html::script('/vendors/switchery/dist/switchery.min.js') !!}
    <!-- Parsley -->
    {!! Html::script('/vendors/parsleyjs/dist/parsley.min.js') !!}
    <!-- jQuery Tags Input -->
    {!! Html::script('/vendors/jquery.tagsinput/src/jquery.tagsinput.js') !!}
        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}
    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}


    <script>
    $(":input").inputmask();
    <!-- Parsley -->
      $(document).ready(function() {
        $.listen('parsley:field:validate', function() {
          validateFront();
        });
        $('#form-validation .btn').on('click', function() {
          $('#form-validation').parsley().validate();
          validateFront();
        });
        var validateFront = function() {
          if (true === $('#form-validation').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
          } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
          }
        };
      });

      try {
        hljs.initHighlightingOnLoad();
      } catch (err) {}

      document.querySelector("#identity").addEventListener("blur",function(e){
          var identity = $(this).val();
          var APP_URL = {!! json_encode(url('/')) !!};  
          //console.log('identity:'+identity)  
               $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                $.ajax({
                        type: 'POST', // Type of response and matches what we said in the route
                        url: APP_URL+'/personal/searchpersonal', // This is the url we gave in the route
                        data: {'identity' : identity,}, // a JSON object to send back
                        success: function(response){ // What to do if we succeed
                           //console.log('Personal:',response.personal)
                            if(response.personal!='null'){
                              $.alert({
                                  icon: 'fa fa-warning',
                                  title: 'Atention!',
                                  content: 'De "identity number" already exist, please check the information before continue!!'
                                  
                              });
                              $('#identity').val('');
                              return false;
                            }
                        }
                    });
             

        });
      document.querySelector("#email").addEventListener("blur",function(e){
          var email = $(this).val();
          var APP_URL = {!! json_encode(url('/')) !!};  
          //console.log('identity:'+identity)  
               $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                $.ajax({
                        type: 'POST', // Type of response and matches what we said in the route
                        url: APP_URL+'/user/checkemail', // This is the url we gave in the route
                        data: {'email' : email}, // a JSON object to send back
                        success: function(response){ // What to do if we succeed
                           //console.log('Email:',response.email)
                            if(response.email!='null'){
                              $.alert({
                                  icon: 'fa fa-warning',
                                  title: 'Atention!',
                                  content: 'De "Email" already exist, please check the information before continue!!'
                                  
                              });
                               $('#email').val('');
                              return false;
                            }
                        }
                    });
             

        });
    </script>
    <!-- /Parsley -->
    
@endsection