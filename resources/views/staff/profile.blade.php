@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.staffprofile')!!}</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" name="staff" id="staff" value="{{$per->id}}">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.activityreport')!!} </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{{$file}}" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>{{ $per->name}}</h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> {{ $per->address}}
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> {{ $per->occupation}}
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-external-link user-profile-icon"></i>
                          <a href="http://www.kimlabs.com/profile/" target="_blank">{{ $per->email}}</a>
                        </li>
                      </ul>
                      <a class="btn btn-success" title='Edit' href="{{ route('personal.edit',['id' => $per->id] )}}" ><i class="fa fa-edit m-right-xs"></i>{!!trans('form.label.editprofile')!!}</a>
                      <a class="btn btn-warning passwordchange" title='Change Password' href="#" data-toggle='modal' data-target="#passwordModal" data-email="{{$per->email}}"><i class="fa fa-edit m-right-xs"></i>{!!trans('form.label.changepass')!!}</a>
                      
                      <br />

                      <!-- start skills -->
                     
                      <!-- end of skills -->
                     <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!!trans('form.label.back')!!}!</a>
                      
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>{!!trans('form.label.memberactivity')!!}</h2>
                        </div>
                        <!-- <div class="col-md-6">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                          </div>
                        </div> -->
                      </div>
                      <!-- start of user-activity-graph -->
                     <!-- <div id="graph_bar" style="width:100%; height:280px;"></div>-->
                      <!-- end of user-activity-graph -->

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Visits made</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Projects as lider</a>
                          </li>
                          
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <div class="panel panel-default">
                              <div class="panel-heading col-md-12">
                                
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body">
                             @if(count($visits)>0) 
                            <div class="dataTable_wrapper">
                             <!-- start tecnician visits -->
                            <table class="data table table-striped no-margin" id="dataTables-visits">
                              <thead>
                                <tr>
                                  <th>{!!trans('form.table.project')!!} #</th>
                                  <th>{!!trans('form.table.client')!!}</th>
                                  <th>{!!trans('form.table.date')!!}</th>
                                  <th>{!!trans('form.label.visitinfo')!!}</th>
                                  <th class="hidden-phone">{!!trans('form.label.hourspend')!!}</th>
                                  <th>{!!trans('form.table.status')!!}</th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                
                                 @foreach($visits as $v)
                                 <? 
                                    if($v->total == 0) $counthr='New visit';
                                    else $counthr=$v->total;
                                 ?>
                                <tr>
                                  <td><a title='Ver Info.' href="{{ route('visits.show',['id' => $v->visitsid] )}}" >{{$v->process_number}} </a></td>
                                  <td>{{$v->name}}</td>
                                  <td>{{date('l jS \of F Y',strtotime($v->date))}}</td>
                                  <td>{{$v->visitdescrp}}</td>
                                  <td class="hidden-phone">{{$counthr}}</td>
                                  <td>{{$v->status}}</td>
                                  
                                </tr>
                               @endforeach 
                               
                              </tbody>
                            </table>
                            <!-- end user projects -->
                            </div>
                            <!-- /.table-responsive -->
                            @else
                              
                                  <span>No information to show</span>
                                
                               @endif
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->  
                            

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <!-- start user projects -->
                            <table class="data table table-striped no-margin">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>{!!trans('form.table.client')!!}</th>
                                  <th>{!!trans('form.label.projectdescription')!!}</th>
                                  <th>{!!trans('form.table.status')!!}</th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                               @if(count($projects)>0)
                                 @foreach($projects as $pro)
                                <tr>
                                  <td><a title='Ver Info.' href="{{ route('process/show',['id' => $pro->processid,'type'=>'process'] )}}" >{{$pro->process_number}} </a></td>
                                  <td>{{$pro->name}}</td>
                                  <td>{{$pro->description}}</td>
                                  <td>{{$pro->status}}</td>
                                  
                                </tr>
                               @endforeach 
                               @else
                               <tr>
                                  <td colspan="5">No information to show</td>
                                </tr>
                               @endif
                              </tbody>
                            </table>
                            <!-- end user projects -->

                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@include('users/update')        
@endsection 
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- morris.js -->
    {!! Html::script('/vendors/raphael/raphael.min.js') !!}
    {!! Html::script('/vendors/morris.js/morris.min.js') !!}
    <!-- bootstrap-progressbar -->
    {!! Html::script('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}
    <!-- bootstrap-daterangepicker -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/datepicker/daterangepicker.js') !!}
        <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}
        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <script>
      var APP_URL = {!! json_encode(url('/')) !!};
      /*$(function() {
        var data = {!! $morris !!};
        var staff = $('#staff').val();
        Morris.Bar({
          element: 'graph_bar',
          data: data,
          xkey: 'period',
          hideHover: 'auto',
          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          ykeys: ['Hours worked', 'sorned'],
          labels: ['Hours worked', 'SORN'],
          xLabelAngle: 60,
          resize: true,
        });

        $MENU_TOGGLE.on('click', function() {
          $(window).resize();
        });
      });*/
    </script>

    <!-- datepicker -->
    <script type="text/javascript">
      $(document).ready(function() {
        var staff = $('#staff').val();
        var datebar=[];
       /* var cb = function(start, end, label) {
          //console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          // alert("Callback has fired: [" + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD') + ", label = " + label + "]");
          var startdate=start.format('YYYY-MM-DD');
          var enddate=end.format('YYYY-MM-DD');
          // console.log(staff+' / '+startdate+' / '+enddate);
           $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
          $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: APP_URL+'/personal/morrisbarajax', // This is the url we gave in the route
                      data: {'id' : staff,'start':startdate,'end':enddate}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      //console.log('data: ',response.data);
                        $('#graph_bar').empty();
                        var data = response.data;
                        
                        Morris.Bar({
                          element: 'graph_bar',
                          data: data,
                          xkey: 'period',
                          hideHover: 'auto',
                          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                          ykeys: ['Hours worked', 'sorned'],
                          labels: ['Hours worked', 'SORN'],
                          xLabelAngle: 60,
                          resize: true,
                        });
                        
                      }
                  });
          


        }

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2016',
          maxDate: '12/31/2021',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {},
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });*/
      });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
      $('#dataTables-visits').DataTable({
                "responsive": true,
                "order": [[ 5, "desc" ]]
      });
$('.passwordchange').click(function(e) {
var modalemail= $(this).data('email');
    $('#emailpass').val(modalemail);


});
       //check validations
$(".save").click(function(e) {
          
           var pass=$('#nuevaclave').val(),
               confirm=$('#confirmclave').val(),
               email=$('#emailpass').val(),
               procurl = APP_URL+"/user/updatepass";
                           
            if(email!='' && pass!='')   {
                           
                            console.log('new pass:'+pass);
                       
                        //alert(checked);
                         if(pass != confirm){
                          
                              $.alert({
                                    icon: 'fa fa-warning',
                                    title: 'Atention!',
                                    content: 'The password confirmation does not match, please verify the information and try again!!'
                                    
                                });
                              return false;
                        }else{
                           
                        
            
                       
                              $.confirm({
                                        title: '<strong>Are you sure about the password update ?</strong>',
                                        content: 'The system will send an email of the user information with the new password to: <b>'+email+'</b>',
                                        confirmButton:'Yes',
                                        cancelButton:'No',
                                        confirm: function(){
                                           $.ajaxSetup({
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    }
                                                });
                                            $.ajax({
                                                    type: 'POST', // Type of response and matches what we said in the route
                                                    url: procurl, // This is the url we gave in the route
                                                    data: {'email':email,'newpass':pass}, // a JSON object to send back
                                                    success: function(response){ // What to do if we succeed
                                                       console.log('pass updated:'+response.updated)
                                                       if(response.updated=='ok'){
                                                         //location.href = response.urlresponse;
                                                         $.alert({
                                                              icon: 'fa fa-warning',
                                                              title: 'Cambio de Contraseña Exitoso!',
                                                              content: 'Verifique la bandeja de mensajes recibidos de su correo para obtener informacion de su nueva clave'
                                                              
                                                          });
                                                       }else if(response.updated=='error envio email'){
                                                         //location.href = response.urlresponse;
                                                         $.alert({
                                                              icon: 'fa fa-warning',
                                                              title: 'Cambio de Contraseña Exitoso!',
                                                              content: 'Verifique la bandeja de mensajes recibidos de su correo para obtener informacion de su nueva clave'
                                                              
                                                          });
                                                       }else if(response.updated==404){
            
                                                        $.alert({
                                                              icon: 'fa fa-warning',
                                                              title: 'Atention!',
                                                              content: 'The email is not in registrated, please verify the information and try again!!'
                                                              
                                                          });
                                                       }
                                                    }
                                                });
                                        },
                                        cancel:function(){
                                           
                                        }
                                        
                                    });
                        }
                        }else{
                          $.alert({
                                    icon: 'fa fa-warning',
                                    title: 'Atention!',
                                    content: 'There is empty information!!'
                                    
                                });
                              return false;
                        }
            
            
        }); 
    
    });
    </script>
    <!-- /datepicker -->
@endsection