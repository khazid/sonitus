@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}

  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.staff')!!} <small>{!!trans('form.label.stafflist')!!}</small></h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <a href="{{ route('personal.create') }}" class="btn btn-primary">{!!trans('form.label.newmember')!!}</a>
                    @include('partials.layout.errors')
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>{!!trans('form.table.name')!!}</th>
                                <th>{!!trans('form.table.identity')!!}</th>
                                <th>{!!trans('form.table.email')!!}</th>
                                <th>{!!trans('form.table.occupation')!!}</th>
                                <th>{!!trans('form.table.action')!!}</th> 

                          </tr>
                      </thead>
                      <tbody>
                          @if(count(@$personals)>0)
                          @foreach($personals as $personal)
                          <tr>
                              <td>{{ $personal->name}}</td>
                              <td>{{ $personal->identity_number }}</td>
                              <td>{{ $personal->email }}</td>
                              <td>{{ $personal->occupation }}</td>
                              <td>
                              
                              <a href="#" class="fa fa-trash fa-2x deletestaff" data-name="{{ $personal->name}}" data-id="{{$personal->id}}" title="Delete"></a>

                              <a href="{{ route('personal.show',['id' => $personal->id] )}}" class="fa fa-eye fa-2x" title="Show info.">  </a>

                             
                              @if($personal->users_id == null)
                              <a  class="fa fa-user fa-2x newuser" title='Create System user' href="#" id="{{$personal->id}}" data-content="{{$personal->name}}" data-client="{{$personal->email}}" > </a>   
                              </td>
                              @endif
                          </tr>
                          @endforeach
                       @endif 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <!-- Datatables -->
    <script>
    var APP_URL = {!! json_encode(url('/')) !!};

      $(document).ready(function() {

       
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      
//create a new user
      $('#datatable-buttons').on('click','.newuser',function(e) {
            var id= $(this).attr("id");
            var nombre= $(this).attr("data-content");
            var email= $(this).attr("data-client");
            var procurl = APP_URL+"/user/savenewuser";
            var type = "staff";

           
            $.confirm({
                            title: '<strong>User Registration!!</strong>',
                            content: 'Are you sure about the user creation for: <b>'+nombre+'</b> ?</br> the system will send an email of the user information with an random password to: <b>'+email+'</b>',
                            confirmButton:'Yes',
                            cancelButton:'No',
                            confirm: function(){
                               $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: procurl, // This is the url we gave in the route
                                        data: {'id' : id,'name': nombre,'email': email,'type':type}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                           console.log('User id:'+response.created)
                                            location.reload();
                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });

//delete staff member
$('#datatable-buttons').on('click','.deletestaff',function(e) {
            var name=$(this).data('name'), 
                deleteurl='personal/destroypersonal', 
                id=$(this).data('id');

           
            $.confirm({
                title: '<strong>Delete Staff member!!</strong>',
                content: 'Are you sure do you want to delete the staff member '+name,
                confirmButton:'Yes',
                cancelButton:'No',
                confirm: function(){
                   $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                    $.ajax({
                            type: 'POST', // Type of response and matches what we said in the route
                            url: deleteurl, // This is the url we gave in the route
                            data: {'id' : id}, // a JSON object to send back
                            success: function(response){ // What to do if we succeed
                               console.log('deleted :',response.deleted)
                                location.reload();
                            }
                        });
                },
                cancel:function(){
                   
                }
                
            });
             
            
        });

    });
    </script>
@endsection
