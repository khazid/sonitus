@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}

  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.quotes')!!}</h3>
                <input type="hidden" name="configload" id="configload" value="{{$configload}}">
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    @include('partials.layout.errors')
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   

                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>{!!trans('form.table.date')!!}</th>
                              <th>{!!trans('form.table.client')!!}</th>
                              <th>{!!trans('form.table.project')!!}</th>
                              <th>{!!trans('form.table.applicant')!!}</th>
                              <th>{!!trans('form.table.description')!!}</th>
                              <th >{!!trans('form.label.currency')!!}</th>
                              <th >Id Config</th>
                              <th >Nro</th>
                              <th >{!!trans('form.table.status')!!}</th>
                              <th>{!!trans('form.table.action')!!}</th> 

                          </tr>
                      </thead>
                      <tbody>
                        @if(count($quotes)>0)
                          @foreach($quotes as $q)
                          <tr>
                              <td>{{ $q->id}}</td>
                              <td>{{ date('d/m/Y',strtotime($q->date))}}</td>
                              <td>{{ $q->client}}</td>
                              <td>{{ $q->process_number}}</td>
                              <td>{{ $q->personal}}</td>
                              <td style="width: 20%">{{ $q->observation}}</td>
                              <td>{{ $q->currency}}</td>
                              <td>{{ $q->billing_config_id}}</td>
                              <td>{{ $q->quote_number}}</td>
                              <td> 
                                @if($rol<=3 || $rol==6)
                                <button type="button" class="btn btn-warning btn-xs statuschange" data-toggle="modal" data-target="#statusModal"  id="{{$q->id}}">{{$q->status}}</button>
                                @else
                                {{$q->status}}
                                @endif
                              </td>
                              <td>
                                  @if($q->statusid==33) <!-- Nueva cotizacion -->
                                 
                                  <a href="{{route('quote/create',['id'=>$q->id])}}" class="btn btn-success btn-xs ">{!!trans('form.label.quote')!!}</a>

                                  <a href="#" class="btn btn-warning btn-xs ask"  id="{{$q->id}}" >{!!trans('form.label.askmat')!!}</a>
                                  
                                  @elseif($q->statusid==39)<!-- Materiales cargados por tecnico para cotizar -->
                                   
                                   <a href="{{route('quote/create',['id'=>$q->id])}}" class="btn btn-success btn-xs ">{!!trans('form.label.quote')!!}</a>
                                  
                                  @elseif($q->statusid==36) <!-- Enviada al Cliente para aprobacion o rechazo -->
                                  
                                  <a href="{{route('quote/showstatus',['id'=>$q->id])}}" class="btn btn-primary btn-xs " id="{{$q->id}}" >{!!trans('form.label.status')!!}</a>
                                  
                                  <a href="{{route('quote.show',['id'=>$q->id])}}" class="btn btn-primary btn-xs " id="{{$q->id}}" target="_blank">{!!trans('form.label.show')!!}</a>
                                    @if(date('Y-m-d') - $q->date >=10)
                                    <a href="{{route('quote/send',['id'=>$q->id])}}" class="btn btn-warning btn-xs " id="{{$q->id}}" title="Notificar al cliente"><i class="glyphicon glyphicon-bell"></i></a>
                                    @endif
                                 
                                  @elseif($q->statusid==41) <!-- Cargada -->
                                  
                                  <a href="{{route('quote/showstatus',['id'=>$q->id])}}" class="btn btn-primary btn-xs " id="{{$q->id}}" >{!!trans('form.label.status')!!}</a>
                                  <a href="{{route('quote.show',['id'=>$q->id])}}" class="btn btn-primary btn-xs " id="{{$q->id}}" target="_blank">{!!trans('form.label.show')!!}</a>
                                  
                                  <a href="{{route('quote/send',['id'=>$q->id])}}" class="btn btn-success btn-xs " id="{{$q->id}}" >{!!trans('form.label.send')!!}</a>
                                 
                                  @elseif($q->statusid==38 && $rol<=4) <!-- Solicitar materiales del proyecto -->
                                  
                                  <a href="{{route('quote/addmaterials',['id'=>$q->id])}}" class="btn btn-success btn-xs " id="{{$q->id}}" >{!!trans('form.label.loadmat')!!}</a>
                                 
                                  @elseif($q->statusid==34 || $q->statusid==35 || $q->statusid==42) <!-- Aceptada o Rechazada -->
                                    <a href="{{route('quote/showstatus',['id'=>$q->id])}}" class="btn btn-primary btn-xs " id="{{$q->id}}" >{!!trans('form.label.status')!!}</a>
                                    @if($q->quote_number!='')
                                    <a href="{{route('quote.show',['id'=>$q->id])}}" class="btn btn-primary btn-xs " id="{{$q->id}}" target="_blank">{!!trans('form.label.show')!!}</a>
                                    @endif
                                    @if($q->statusid==34) <!-- Aceptada-->
                                        @if($q->process_number!='')
                                          <a href="{{route('visits/calendar')}}" class="btn btn-success btn-xs" >Agendar visita</a>
                                        @else
                                        <a href="{{ route('client/newproject',['id' => $q->clientid] )}}"  class="btn btn-success btn-xs">
                                          <i class="fa fa-cog"> </i> {!!trans('form.label.newproject')!!}
                                        </a>
                                        @endif
                                    @elseif($q->statusid==35)<!-- Rechazada-->
                                       <a href="{{route('quote/create',['id'=>$q->id])}}" class="btn btn-success btn-xs ">Nueva {!!trans('form.label.quote')!!}</a>    
                                        
                                    @endif

                                 @endif
                                 
                             </td>
                          </tr>
                          @endforeach
                       @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
{!! Form::open(['route' => 'quote/updatestatus', 'method' => 'post', 'validate']) !!}

<!-- MODAL PARA EL CAMBIO DE STATUS ---->
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel">
               {!! Form::hidden('quoteid',null,['id'=>'quoteid']) !!}
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">{!!trans('form.label.statuschange')!!}</h4>
                  </div>
                  <div class="modal-body">
                    
                      <div class="form-group">
                        <label for="newstatus" class="control-label">{!!trans('form.label.status')!!}:</label>
                        {!! Form::select('newstatus', $status,null,['class' => 'form-control']) !!}
                      
                      </div>
                      <div class="form-group">
                        <label for="newstatus" class="control-label">{!!trans('form.label.observation')!!}:</label>
                        <textarea name="statusob" id="statusob" required="required" class="form-control"></textarea>
                      
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{!!trans('form.label.close')!!}</button>
                    <button type="submit" class="btn btn-primary">{!!trans('form.label.submit')!!}</button>
                  </div>
                </div>
              </div>
            </div>
<!---------------------------------------->
{!! Form::close()!!}   

@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
        var configload = {!!$configload !!};
        if(configload==false){

          $.confirm({
                  title: '<strong>Debe cargar los valores de configuracion para las cotizaciones!!</strong>',
                  content:'Por favor ingrese a la opcion de configuracion para realizar la carga de los valores para generar cotizaciones, Gracias',
                  cancelButton:false,
                  confirm:function(){
                     window.location.href = APP_URL+'/config';
                     
                  }
                });
        }
       
        $('#datatable-buttons').on('click','.ask',function(){
           var id=$(this).attr('id');
            console.log('id'+id);
            
                              $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/askmaterials', // This is the url we gave in the route
                                      data: {'requestid' : id}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          $.confirm({
                                              title: '<strong>Solicitud de Materiales/Equipos Realizada!!</strong>',
                                              content:'La solicitud de materiales y equipos fue enviada a los tecnicos para su carga',
                                              cancelButton:false,
                                              confirm:function(){
                                                 location.reload();
                                              }
                                            });

                                      }
                              });

        });
        $('#datatable-buttons').on('click','.statuschange',function(){
          var procc = $(this).attr("id");
                $("#quoteid").val(procc);

      });
        

      });
</script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {

       
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      

    });
    </script>
@endsection
