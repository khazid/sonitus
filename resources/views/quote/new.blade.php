@extends('app/navin')
@section('css')
  
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
   
    <!-- Select2 -->
    {!! Html::style('/vendors/select2/dist/css/select2.min.css') !!}
    
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}
    
    <!-- starrr -->
    {!! Html::style('/vendors/starrr/dist/starrr.css') !!}
    

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')
<style type="text/css">
  .quoteconf{
    font-size: 10px;
  }
</style>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.quote')!!}</h3>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    
                    <div class="clearfix"></div>
                  </div>
<!--- INFORMACION SECCION IZQUIERDA -->                  
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      
                      <h2>{{ $client->name.' '.$client->rif}}</h3>
                      <h3>{{$client->applicant_name}}</h2>
                      <p><strong>{{$client->charge}} </strong> </p>
                      <ul class="list-unstyled">
                                <li><i class="fa fa-envelope"></i> {!!trans('form.label.email')!!}: {{$client->email}} </li>
                                <li><i class="fa fa-building"></i> {!!trans('form.label.address')!!}: {{$client->address}} </li>
                                <li><i class="fa fa-phone"></i> {!!trans('form.label.phone')!!} #: {{$client->phone}} </li>
                      </ul>
                     <p><strong>Observacion del proyecto </strong>: {{$client->observation}} </p>
                      <strong><label class="bs-callout-warning">--- ATENCION !! En caso de viaje ---</label> <br/> 
                      <p>Transporte:</strong> Coloque el monto Total correspondiente al pasaje Ida y Vuelta Total.<br /><strong>Comidas:</strong>  Coloque el monto del Desayuno, Almuerzo y Cena basado en 1 PERSONA.<br /> <strong>Hospedaje:</strong> (Monto por noche) basado en 1 PERSONA y en 1 DIA, el sistema lo multiplicara por la cantidad de tecnicos y la cantidad de dias que haya indicado. <br /> <strong>Horas Nocturas:</strong> Colocar la cantidad de Horas nocturnas aproximada y el monto Total de las mismas. <br /> <strong>Horas Extras Diurnas :</strong> Colocar la cantidad de Horas extras diurnas aproximada y el monto Total de las mismas. </p>
                   
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>{!!trans('form.label.newquote')!!} Nro.{{$correlativo}}</h2>
                        </div>
                      </div>
                      
<!--- INFORMACION SECCION DERECHA -->
              <div class="" role="tabpanel" data-example-id="togglable-tabs">
 {!! Form::open(['route' => 'quote.store', 'method' => 'post','class'=>'form-horizontal form-label-left','data-parsley-validate','id'=>'formproject']) !!}
                   <input type="hidden" name="quoteid" id="quoteid" value="{{@$client->quoteid}}">
                   <input type="hidden" name="quotenumber" id="quotenumber" value="{{@$correlativo}}">
                <!-- seccion derecha -->    
                  <div class="col-md-4 col-sm-4 col-xs-12 quoteconf" >  
                        <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.currency')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="radio" name="currency" id="currency" value="Bs" checked="checked"> Bs.
                          <input type="radio" name="currency" id="currency" value="Dolar"> Dolar
                        </div>
                        
                      </div>
                      
                        <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.paytype')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="radio" name="paytype" value="50"> {!!trans('form.label.50')!!}
                          <input type="radio" name="paytype" value="total" checked="checked"> {!!trans('form.label.total')!!}
                        </div>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.technicians')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="technicians" class="integer" required="required" />
                          
                        </div>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.days')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="days" class="integer" required="required"/>
                          
                        </div>
                        
                      </div>
                  </div>
                  <!-- seccion media -->
                  <div class="col-md-4 col-sm-4 col-xs-12 quoteconf">
                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.night')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="radio" name="night" value="true" class="nightwork"> {!!trans('form.label.yes')!!}
                          <input type="radio" name="night" value="false" class="nightwork" checked="checked"> {!!trans('form.label.no')!!}
                        </div>
                        
                      </div>
                      
                      <div class="form-group night">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.nighthour')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nighthour" class="integer" />
                          </div>
                      </div>
                      <div class="form-group night">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.nighthourprice')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nighthourprice" class="numeric monto" />
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">Horas extras Diurnas <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="radio" name="day" value="true" class="daywork"> {!!trans('form.label.yes')!!}
                          <input type="radio" name="day" value="false" class="daywork" checked="checked"> {!!trans('form.label.no')!!}
                        </div>
                        
                      </div>
                      
                      <div class="form-group day">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.dayhour')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="dayhour" class="integer" />
                          </div>
                      </div>
                      <div class="form-group day">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.dayhourprice')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="dayhourprice" class="numeric monto" />
                          </div>
                      </div>
                    
                    </div>
                <!-- seccion derecha -->
                    <div class="col-md-4 col-sm-4 col-xs-12 quoteconf">
                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.travel')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="radio" name="travel" value="true" class="traveloption"> {!!trans('form.label.yes')!!}
                          <input type="radio" name="travel" value="false" class="traveloption" checked="checked"> {!!trans('form.label.no')!!}
                        </div>
                        
                      </div>
                      <div class="form-group travel">
                       <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.transportation')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="transportation" class="numeric monto" title="{!!trans('form.label.placeholdertrans')!!}" placeholder="{!!trans('form.label.placeholdertrans')!!}" />
                          
                        </div>
                        
                      </div>
                      <div class="form-group travel">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.viaticum')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="viaticum" class="numeric monto" title="{!!trans('form.label.placeholderviat')!!}" placeholder="{!!trans('form.label.placeholderviat')!!}" onkeyup="format(this)" onchange="format(this)"/>
                          
                        </div>
                        
                      </div>
                      <div class="form-group travel">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name">{!!trans('form.label.lodgment')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="lodgment" class="numeric monto" title="{!!trans('form.label.placeholderlod')!!}" placeholder="{!!trans('form.label.placeholderlod')!!}" onkeyup="format(this)" onchange="format(this)"/>
                          
                        </div>
                        
                      </div>
                    </div>  
                      
                       <!--<a id="agregarCampo" class="btn btn-info" href="#">Agregar Campo</a> -->
      
      <!-- ----------- Seccion de Items  -------------- -->
                      <div id="contenedor">
                       
                       <table id="datatable-items" class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                <td colspan="5" style="background-color: #F5F7FA;"> <h2>{!!trans('form.label.material')!!}</h2></td>
                              </tr>
                              <tr>
                                  <th>#</th>
                                  <th style="width:40%">{!!trans('form.table.description')!!}</th>
                                  <th>{!!trans('form.table.quantity')!!}</th>
                                  <th>{!!trans('form.table.unitprice')!!}</th>
                                  <th>{!!trans('form.table.total')!!}</th>
                              </tr>
                          </thead>
                          <tbody>
                          @if(count(@$materials)>0)
                               <?php $cantm=0; ?>
                              @foreach($materials as $m) <!--- campos con items de la cotizacion -->
                              <tr>
                                  <td><span>{{ $cantm}} <input type="hidden" name="itemid[]" value="{{$m->id}}"></span></td>
                                  <td style="width:40%"><input type="text" name="mitexto[]" id="campo_{{$cantm}}" placeholder="Descripcion " style="width:100%" value="{{ $m->description}}" /></td>
                                  <td><input type="text" name="cant[]" id="cant_{{$cantm}}" placeholder="Cantidad " data-number="{{$cantm}}" value="{{ $m->quantity}}" /></td>
                                  <td><input type="text" name="unitprice[]" id="unitprice_{{$cantm}}" placeholder="Precio unidad " data-number="{{$cantm}}" class="unitprice numeric monto" value="{{ number_format($m->unit_price,2,'.', ',')}}" /></td>
                                  <td><input type="text" name="total[]" id="total_{{$cantm}}" placeholder="Total " readonly="readonly" value="{{ number_format($m->total,2,'.', ',')}}" /></td>
                              </tr> 
                               <?php $cantm++; ?>   
                              @endforeach

                              @for ($i=count(@$materials)+1; $i <= 10; $i++) <!--- campos vacios para agregar mas items -->
                                  <tr>
                                      <td><span>{{$i}} <input type="hidden" name="itemid[]"></span></td>
                                      <td style="width:40%"><input type="text" name="mitexto[]" id="campo_{{$i}}" placeholder="Descripcion " style="width:100%"/></td>
                                      <td><input type="text" name="cant[]" id="cant_{{$i}}" placeholder="Cantidad " data-number="{{$i}}"/></td>
                                      <td><input type="text" name="unitprice[]" id="unitprice_{{$i}}" placeholder="Precio unidad " data-number="{{$i}}" class="unitprice numeric monto"/></td>
                                      <td ><input type="text" name="total[]" id="total_{{$i}}" placeholder="Total " readonly="readonly"  />
                                      <!--<a href="#" class="btn btn-danger btn-xs eliminar"><i class="fa fa-remove"></i></a>--> 
                                      </td>
                                  </tr>
                              @endfor
                           
                           @else
                              
                              @for ($i = 0; $i < 10; $i++) <!--- campos vacios para agregar items maximo 10 -->
                                  <tr>
                                      <td><span>{{$i}}</span></td>
                                      <td style="width:40%"><input type="text" name="mitexto[]" id="campo_{{$i}}" placeholder="Descripcion " style="width:100%"/></td>
                                      <td><input type="text" name="cant[]" id="cant_{{$i}}" placeholder="Cantidad " data-number="{{$i}}"/></td>
                                      <td><input type="text" name="unitprice[]" id="unitprice_{{$i}}" placeholder="Precio unidad " data-number="{{$i}}" class="unitprice numeric monto"/></td>
                                      <td ><input type="text" name="total[]" id="total_{{$i}}" placeholder="Total " readonly="readonly"  />
                                      <!--<a href="#" class="btn btn-danger btn-xs eliminar"><i class="fa fa-remove"></i></a>--> 
                                      </td>
                                  </tr>
                              @endfor
                             
                           @endif

                          </tbody>
                        </table>
                      </div>    
                      
                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>{!!trans('form.label.material')!!} Adicionales solicitados por los tecnicos</h2>
                          <input type="hidden" name="cantmaterials" value="{{count(@$materialnew)}}">
                        </div>
                      </div>


                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th style="width:40%">{!!trans('form.table.description')!!}</th>
                              <th>{!!trans('form.table.quantity')!!}</th>
                              <th>{!!trans('form.table.unitprice')!!}</th>
                              <th>{!!trans('form.table.total')!!}</th>
                          </tr>
                      </thead>
                      <tbody>
                          @if(count(@$materialnew)>0)
                          @foreach($materialnew as $m)
                          <tr>
                              <td><input type="hidden" name="id[]" value="{{ $m->id}}" />{{ $m->id}}</td>
                              <td>{{ $m->description}}</td>
                              <td><input type="hidden" name="quantity" id="{{'quantity_'.$m->id}}" value="{{ $m->quantity}}">{{ $m->quantity}}</td>
                              <td><input type="text" name="matprice[]" id="{{ 'matprice_'.$m->id}}" placeholder="Precio unidad " data-number="{{$m->id}}" class="matprice numeric monto" required /></td>
                              <td><input type="text" name="mattotal[]" id="{{ 'mattotal_'.$m->id}}" placeholder="Total " readonly="readonly" /></td>
                              
                          @endforeach
                       @endif 
                      </tbody>
                    </table>
       
                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!!trans('form.label.cancel')!!}</a>
                          <button type="submit" class="btn btn-success">{!!trans('form.label.save')!!}</button>
                        </div>
                      </div> 
</form>            
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->



@endsection
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    {!! Html::script('/assets/js/jquery.numeric.min.js') !!}
    {!! Html::script('/assets/js/jquery.numeric.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
  
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}

   <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
  
    <!-- Parsley -->
     {!! Html::script('/vendors/parsleyjs/dist/parsley.min.js') !!}
   
    <!-- Autosize -->
     {!! Html::script('/vendors/autosize/dist/autosize.min.js') !!}
         <!-- Switchery -->
    {!! Html::script('/vendors/switchery/dist/switchery.min.js') !!}

       <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- Parsley -->
    <script>
    function format2(n) {
      n=parseFloat(n);
      
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    };

 $(document).ready(function() {
        $('.travel').hide();
        $('.traveloption').click(function(){
          if($(this).val()=='true'){
            $('.travel').show();

          }else{
             $('.travel').hide();
          }
        });

        $('.night').hide();
        $('.nightwork').click(function(){
          if($(this).val()=='true'){
            $('.night').show();
          }else{
             $('.night').hide();
          }
        });

        $('.day').hide();
        $('.daywork').click(function(){
          if($(this).val()=='true'){
            $('.day').show();
          }else{
             $('.day').hide();
          }
        });

        $('.monto').focusout(function(){
          var valor=$(this).val();
          var currency=$('#currency').val();
            valor= format2(valor);
            $(this).val(valor);
        });

    
//-----validacion parsley-----------------------------------------------------------------      
       
       $(function () {
        $('#formproject').parsley().on('field:validated', function() {
          var ok = $('.parsley-error').length === 0;
          $('.bs-callout-info').toggleClass('hidden', !ok);
          $('.bs-callout-warning').toggleClass('hidden', ok);
        })
        /*.on('form:submit', function() {
          return false;
        });*/
      });
  });
    </script>
    <!-- /Parsley -->

    <!-- Autosize -->
    <script>
      $(document).ready(function() {
        autosize($('.resizable_textarea'));
      });
    </script>
    <!-- /Autosize -->

<!-- valores de tablas -->
<script type="text/javascript">
  $(document).ready(function() {



    var MaxInputs       = 8; //Número Maximo de Campos
    var contenedor       = $("#datatable-items"); //ID del contenedor
    var AddButton       = $("#agregarCampo"); //ID del Botón Agregar

    //var x = número de campos existentes en el contenedor
    var x = $("#contenedor div").length + 1;
    var FieldCount = x-1; //para el seguimiento de los campos

    $(AddButton).on("click",function (e) {
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            //agregar campo
            
            /*$("#datatable-items tbody").append('<tr id="tr_'+ FieldCount +'"><td><span>'+ FieldCount +'</span></td>'+
                                  '<td style="width:40%"><input type="text" name="mitexto[]" id="campo_'+ FieldCount +'" placeholder="Descripcion " style="width:100%"/></td>'+
                                  '<td><input type="text" name="cant[]" id="cant_'+ FieldCount +'" placeholder="Cantidad " data-number="1"/></td>'+
                                  '<td><input type="text" name="unitprice[]" id="unitprice_'+ FieldCount +'" placeholder="Precio unidad " data-number="'+ FieldCount +'" class="unitprice numeric monto"/></td>'+
                                  '<td ><input type="text" name="total[]" id="total_'+ FieldCount +'" placeholder="Total " readonly="readonly" />'+
                                  '<a href="#" class="btn btn-danger btn-xs eliminar"><i class="fa fa-remove"></i></a>'+ 
                                  '</td></tr> ');*/
            x++; //text box increment
        }
        return false;
    });

    $("#datatable-items tbody").on("click",".eliminar", function(e){ //click en eliminar campo
        if( x > 1 ) {
            var id = $(this).id;
            $(this).parent().remove(); //eliminar el campo aun no funciona bien
            x--;
        }
        return false;
    });

    $(".unitprice").on('focusout',function() {
      //totalizar
      
      var number=$(this).data('number');
      var cant=$('#cant_'+number).val();
      var price=$(this).val();
      console.log('original price'+ price);
      price=price.replace(/,/g,'');
      var total= price * cant;
      console.log('cant'+cant+' price'+price);
      $('#total_'+number).val(format2(total));
    
    });
    $(".matprice").on('focusout',function() {
      //totalizar
      
      var number=$(this).data('number');
      var cant=$('#quantity_'+number).val();
      var price=$(this).val();
          price=price.replace(/,/g,'');
      var total= price * cant;

      $('#mattotal_'+number).val(format2(total));
    
    });
});

</script>

<!-- INPUTMASK-->
<script type="text/javascript">
  $(".numeric").numeric();
  $(".integer").numeric(false, function() { alert("Integers only"); this.value = ""; this.focus(); });
  $(".positive").numeric({ negative: false }, function() { alert("No negative values"); this.value = ""; this.focus(); });
  $(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("Positive integers only"); this.value = ""; this.focus(); });
    $(".decimal-2-places").numeric({ decimalPlaces: 2 });
  
  </script>

@endsection