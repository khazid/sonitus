<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_TITLE')}} |</title>

    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    
  <style type="text/css">
    
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
    }
    
    .footer {
        bottom: 20px;
        font-size: 11px;
    }
    .pagenum:before {
        content: counter(page);
    }
    *{
      background-color: white!important;

    }
    thead:before, thead:after { display: none; }
    tbody:before, tbody:after { display: none; }
  </style>  
  </head>

  <body >
  
  <div class="footer">
    <p class="pagenum">INVERSIONES SONITUS <br /> El Dorado, Galerias Miami, Primer Piso, Oficina 8, Ciudad de Panama, Republica de Panama<br />Telefono: 507.8338974 </p>
  </div>
    <div class="container body">
      <div class="main_container">
        
        <!-- page content -->
        <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                <div align="left" style="width:50%;display:inline-block;float:left; font-size:10px">
                  <h4>Compañia: {{$client->name}}</h4>
                  <h5>RIF: {{$client->rif}}</h5>
                            
                            <ul class="list-unstyled">
                                <li><b>Contacto:</b> {{$client->applicant_name}} </li>
                                <li><b>Correo:</b> {{$client->email}} </li>
                                <li><b>Direccion:</b> {{$client->address}} </li>
                                <li><b>Telefonos:</b> {{$client->phone}} </li>
                            </ul>
                </div>
                <div style="width:50%;display:inline-block;float:right" align="right">
                <img src="{{ url('images/email/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;">
                <br />
                <span style="font-size:10px">RUC: 155631178-2-2016</span>
                </div>
                 <br />
                  <br />
                  <div class="x_title">
                          <div align="center" class="x_content">
                          <h2>Fecha: {{date("d/m/Y",strtotime($client->quotedate))}} Cotizacion Nro.{{$client->quote_number}} </h2>
                          </div>

                          <div class="project_detail">
                          <div class="x_title">
                            <div class="clearfix"></div>
                          </div>
                            
                            <h2 class="green"> Descripcion del proyecto:</h2>
                            <p>{{$client->observation}}</p>
                            <span class="pull-right " style="color: grey;" > Validity: 3 days</span>
                            <span class="pull-right" style="color: grey;" > Currency: Dolar</span>
                        </div>
                         
                      
                  </div>

                  <div class="x_content">
                    <div>
                    
                      <!-- info de la visita -->
                    @if(count($material)>0)
                      
                        <table id="datatable-items" class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th width="20px">#</th>
                                  <th width="250px">{!!trans('form.table.description')!!}</th>
                                  <th width="30px">{!!trans('form.table.quantity')!!}</th>
                                  <th width="100px">{!!trans('form.table.unitprice')!!}</th>
                                  <th width="100px">{!!trans('form.table.total')!!}</th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                            </tr>
                            <? $i=1 ?>
                              @foreach($material as $m)
                              
                              <tr>
                                  <td>{{$i}}</td>
                                  <td>{{ $m->description}}</td>
                                  <td>{{ $m->quantity}}</td>
                                  <td>{{ number_format($m->unit_price,2,",",".")}}</td>
                                  <td>{{ number_format($m->total,2,",",".")}}</td>
                              </tr> 
                            <? $i++ ?>
                              @endforeach
                              <tr>
                                  <td colspan="3"></td>
                                  <td >Total</td>
                                  <td>{{ number_format($total,2,",",".")}}</td>
                              </tr> 
                          </tbody>
                        </table>
                     @endif
                 </div>     
              </div>

                       
        <!-- /page content -->
      </div>

      <div style="page-break-before: always;"></div>
      <div class="x_panel" >
              <div class="x_content ">
                <p>
                  CONDICIONES<br />
                  1) Emitir orden de compra.<br />
                  2) Pago de contado al 100% según orden de compra.<br />
                  3) Entrega en la ciudad de Caracas.<br />
                  4) Entrega en Miami.<br />
                  5) Disponibilidad de entrega de 2 a 6 semanas aproximadamente.<br />
                  6) Garantía original de los equipos estipulada por el fabricante.<br />
                  7) Servicio post-venta garantizado.<br />
                  8) Este presupuesto está calculado en Dólares.<br />

                  <br /><br />
                  Atentamente<br />
                  Inversiones Sonitus C.A<br />

                </p>
              </div>
            </div>  
    </div>
    </div>
    </div>
</div>

  </body>
</html>