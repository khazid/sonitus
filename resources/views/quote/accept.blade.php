<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v3.5.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v3.5.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="assets/images/executive-conference-space-solution-image-128x96-35.jpg" type="image/x-icon">
  <meta name="description" content="">

  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900">
  {!! Html::style('/assets/et-line-font-plugin/style.css') !!}
  {!! Html::style('/assets/bootstrap-material-design-font/css/material.css') !!}
  {!! Html::style('/assets/tether/tether.min.css') !!}
  {!! Html::style('/assets/bootstrap/css/bootstrap.min.css') !!}
  {!! Html::style('/assets/socicon/css/socicon.min.css') !!}
  {!! Html::style('/assets/animate.css/animate.min.css') !!}
  {!! Html::style('/assets/dropdown/css/style.css') !!}
  {!! Html::style('/assets/theme/css/style.css') !!}
  {!! Html::style('/assets/mobirise-gallery/style.css') !!}
  {!! Html::style('/assets/mobirise/css/mbr-additional.css') !!}
  <style type="text/css">
      .formcontact{
        color:#ddd;
        text-align: left!important;
      }
  </style>
  
  
</head>
<body>
<section id="ext_menu-0">

    <nav class="navbar navbar-dropdown navbar-fixed-top">
        <div class="container">

            <div class="mbr-table">
                <div class="mbr-table-cell">

                    <div class="navbar-brand">
                        <a href="http://{!! env('WEB_PAGE')!!}" class="navbar-logo"><img src="{{url('assets/images/logo-unico-vectorizado4-489x128-21-489x128-42.png')}}" alt="Mobirise"></a>
                        
                    </div>

                </div>
                
            </div>

        </div>
    </nav>

</section>

<section class="engine"><a rel="external" href="#"></a></section><section class="mbr-section mbr-section-hero mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-0" style="background-image: url('/')+'/assets/images/135567383-2000x1519-26.jpg;">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>

    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="mbr-section col-md-10 col-md-offset-2 text-xs-left">
           
                        <h1 class="mbr-section-title display-1">Bienvenido</h1>
                        @if($status=='accept')
                        <p class="mbr-section-lead lead">Gracias por su respuesta,  Le estaremos avisando sobre la fecha y hora de la visita de nuestros tecnicos para la inspeccion</p><br />
                            @if(@$currency=='Bs')

                            <p class="mbr-section-lead lead">Puede Realizar su pago con : Cheque NO ENDOSABLE a nombre de <br/>INVERSIONES SONITUS, C.A. <br />Transferencia o Deposito a la Cuenta Corriente: Nº 0134 0366 04 3661291059<br/> en el Banco BANESCO</p>

                            @elseif(@$currency=='Dolar')

                            <p class="mbr-section-lead lead">Puede Realizar su pago con : Transferencia a nombre de <br/>INVERSIONES SONITUS, C.A. <br/>a la Cuenta Nº 110800024128 <br/>CODIGO SWIFT: BANSPAPAXXX <br/>en el Banco BANESCO PANAMA</p>

                            @endif
                        
                        @elseif($status=='reject')
                         <p class="mbr-section-lead lead">Gracias por su respuesta, Procederemos a contactarlo via correo electronico o llamada telefonica para que nos informe la razon del rechazo de la cotizacion y proceder a asesorarlo con la misma para que se adapte a sus necesidades<br></p>
                        @endif 
                        <div><a href="{!! env('WEB_PAGE')!!}" class="btn btn-sm btn-black">Aceptar</a></div>
                   
                </div>
            </div>
        </div>
    </div>

</section>


<footer class="mbr-small-footer mbr-section mbr-section-nopadding mbr-parallax-background" id="footer1-0" style="background-image: url(assets/images/formatosonitushoja5-2000x2588-25.jpg); padding-top: 0.875rem; padding-bottom: 0.875rem;">
    <div class="mbr-overlay" style="opacity: 0.4; background-color: rgb(0, 0, 0);"></div>
    <div class="container">
        <p class="text-xs-center">&nbsp;(c) 2016 Inversiones Sonitus</p>
    </div>
</footer>

  
  {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
  {!! Html::script('assets/tether/tether.min.js') !!}
  {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
  {!! Html::script('assets/smooth-scroll/SmoothScroll.js') !!}
  {!! Html::script('assets/viewportChecker/jquery.viewportchecker.js') !!}
  {!! Html::script('assets/dropdown/js/script.min.js') !!}
  {!! Html::script('assets/touchSwipe/jquery.touchSwipe.min.js') !!}
  {!! Html::script('assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js') !!}
  {!! Html::script('assets/masonry/masonry.pkgd.min.js') !!}
  {!! Html::script('assets/imagesloaded/imagesloaded.pkgd.min.js') !!}
  {!! Html::script('assets/jarallax/jarallax.js') !!}
  {!! Html::script('assets/theme/js/script.js') !!}
  {!! Html::script('assets/mobirise-gallery/script.js') !!}
  {!! Html::script('assets/formoid/formoid.min.js') !!}

  
   
  
  
  <input name="animation" type="hidden">
  </body>
</html>