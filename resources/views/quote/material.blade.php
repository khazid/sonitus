@extends('app/navin')
@section('css')
  
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
   
    <!-- Select2 -->
    {!! Html::style('/vendors/select2/dist/css/select2.min.css') !!}
    
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}
    
    <!-- starrr -->
    {!! Html::style('/vendors/starrr/dist/starrr.css') !!}
    

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.quote')!!}</h3>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      
                      <h2>{{ $client->name.' '.$client->rif}}</h3>
                      <h3>{{$client->applicant_name}}</h2>
                      <p><strong>{{$client->charge}} </strong> </p>
                      <ul class="list-unstyled">
                                <li><i class="fa fa-envelope"></i> {!!trans('form.label.email')!!}: {{$client->email}} </li>
                                <li><i class="fa fa-building"></i> {!!trans('form.label.address')!!}: {{$client->address}} </li>
                                <li><i class="fa fa-phone"></i> {!!trans('form.label.phone')!!} #: {{$client->phone}} </li>
                      </ul>
                     <p><strong>Observacion del proyecto </strong>: {{@$client->observation}} </p>
                   
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>{!!trans('form.label.material')!!}</h2>
                        </div>
                      </div>
                      

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
 {!! Form::open(['route' => 'quote/storematerial', 'method' => 'post','class'=>'form-horizontal form-label-left','data-parsley-validate','id'=>'formproject']) !!}
                   <input type="hidden" name="quoteid" id="quoteid" value="{{@$client->quoteid}}">
                   <input type="hidden" name="quotenumber" id="quotenumber" value="{{@$correlativo}}">
                        
                      <a id="agregarCampo" class="btn btn-info" href="#">Agregar Campo</a>
                      
                       <table id="contenedor" class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th style="width:40%">{!!trans('form.table.description')!!}</th>
                                  <th>{!!trans('form.table.quantity')!!}</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                             <tr>
                                  <td><span>1 </span></td>
                                  <td style="width:40%"><input type="text" name="mitexto[]" id="campo_1" placeholder="Descripcion " style="width:100%" required /></td>
                                  <td><input type="text" name="cant[]" id="cant_1" placeholder="Cantidad " data-number="1" required/></td>
                                  
                              </tr> 
                              
                          </tbody>
                        </table>
                      </div>    
                      
       
                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!!trans('form.label.cancel')!!}</a>
                          <button type="submit" class="btn btn-success">{!!trans('form.label.save')!!}</button>
                        </div>
                      </div> 
</form>            
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->



@endsection
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    {!! Html::script('/assets/js/jquery.numeric.min.js') !!}
    {!! Html::script('/assets/js/jquery.numeric.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
  
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}

   <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
  
    <!-- Parsley -->
     {!! Html::script('/vendors/parsleyjs/dist/parsley.min.js') !!}
   
    <!-- Autosize -->
     {!! Html::script('/vendors/autosize/dist/autosize.min.js') !!}
         <!-- Switchery -->
    {!! Html::script('/vendors/switchery/dist/switchery.min.js') !!}

       <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- Parsley -->
    <script>
      $(document).ready(function() {
        $('.travel').hide();
        $('.traveloption').click(function(){
          if($(this).val()=='true'){
            $('.travel').show();
          }else{
             $('.travel').hide();
          }
        });

        $.listen('parsley:field:validate', function() {
          validateFront();
        });
        $('#formproject .btn').on('click', function() {
          $('#formproject').parsley().validate();
          validateFront();
        });
        var validateFront = function() {
          if (true === $('#formproject').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
          } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
          }
        };
      });

      try {
        hljs.initHighlightingOnLoad();
      } catch (err) {}
    </script>
    <!-- /Parsley -->

    <!-- Autosize -->
    <script>
      $(document).ready(function() {
        autosize($('.resizable_textarea'));
      });
    </script>
    <!-- /Autosize -->
<script type="text/javascript">
  $(document).ready(function() {

    var MaxInputs       = 30; //Número Maximo de Campos
    var contenedor       = $("#contenedor"); //ID del contenedor
    var AddButton       = $("#agregarCampo"); //ID del Botón Agregar

    //var x = número de campos existentes en el contenedor
    var x = $("#contenedor >tbody >tr").length + 1;
    var FieldCount = x-1; //para el seguimiento de los campos
    
    $(AddButton).on("click",function (e) {
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            //agregar campo
           $(contenedor).append('<tr id="tr_'+FieldCount+'">'+
                                  '<td><span>'+FieldCount+'</span></td>'+
                                '<td style="width:40%"><input type="text" name="mitexto[]" id="campo_'+FieldCount+'" placeholder="Descripcion " style="width:100%"/></td>'+
                                  '<td><input type="text" name="cant[]" id="cant_'+FieldCount+'" placeholder="Cantidad " data-number="'+FieldCount+'"/></td>'+
                                 '<td ></td></tr>');
            x++; //text box increment
        }
        return false;
    });

    $("#contenedor >tbody >tr").on("click",".eliminar", function(e){ //click en eliminar campo
        alert('eliminar');
        if( x > 1 ) {
            $(this).parent('>tr').remove(); //eliminar el campo
            x--;
        }
        return false;
    });

    $(".unitprice").on('focusout',function() {
      //totalizar
      
      var number=$(this).data('number');
      var cant=$('#cant_'+number).val();
      var price=$(this).val();

      var total= price * cant;
      console.log('cant'+cant+' price'+price);
      $('#total_'+number).val(total);
    
    });
    $(".matprice").on('focusout',function() {
      //totalizar
      
      var number=$(this).data('number');
      var cant=$('#cantmat'+number).val();
      var price=$(this).val();

      var total= price * cant;

      $('#mattotal_'+number).val(total);
    
    });
});

</script>
<script type="text/javascript">
  $(".numeric").numeric();
  $(".integer").numeric(false, function() { alert("Integers only"); this.value = ""; this.focus(); });
  $(".positive").numeric({ negative: false }, function() { alert("No negative values"); this.value = ""; this.focus(); });
  $(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("Positive integers only"); this.value = ""; this.focus(); });
    $(".decimal-2-places").numeric({ decimalPlaces: 2 });
  
  </script>

@endsection