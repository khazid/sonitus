<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_TITLE')}} |</title>

    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    
  <style type="text/css">
    
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
    }
    
    .footer {
        bottom: 20px;
        font-size: 11px;
    }
    .pagenum:before {
        content: counter(page);
    }
    *{
      background-color: white!important;

    }
    thead:before, thead:after { display: none; }
    tbody:before, tbody:after { display: none; }
  </style>  
  </head>

  <body >
  
  <div class="footer">
    <p class="pagenum">INVERSIONES SONITUS <br /> Av. Este, Edif. Torre Morelos, P.B. Local 9, Urb. Los Caobos Telf.: (58)212-5778891, Email: inversiones.sonitus@gmail.com </p>
  </div>
    <div class="container body">
      <div class="main_container">
        
        <!-- page content -->
        <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                <div align="left" style="width:50%;display:inline-block;float:left; font-size:10px">
                  <h4>Compañia: {{$client->name}}</h4>
                  <h5>RIF: {{$client->rif}}</h5>
                            
                            <ul class="list-unstyled">
                                <li><b>Contacto:</b> {{$client->applicant_name}} </li>
                                <li><b>Correo:</b> {{$client->email}} </li>
                                <li><b>Direccion:</b> {{$client->address}} </li>
                                <li><b>Telefonos:</b> {{$client->phone}} </li>
                            </ul>
                </div>
                <div style="width:50%;display:inline-block;float:right" align="right">
                <img src="{{ url('images/email/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;">
                <br />
                <span style="font-size:10px">RIF: J-40188436-9</span>
                </div>
                 <br />
                  <br />
                  <div class="x_title">
                          <div align="center" class="x_content">
                          <h2>Fecha: {{date("d/m/Y",strtotime($client->quotedate))}} Cotizacion Nro.{{$client->quote_number}} </h2>
                          </div>

                          <div class="project_detail">
                          <div class="x_title">
                            <div class="clearfix"></div>
                          </div>
                            
                            <h2 class="green"> Descripcion del proyecto:</h2>
                            <p>{{$client->observation}}</p>
                            <span class="pull-right " style="color: grey;" > Validez: 3 dias</span>
                            <span class="pull-right" style="color: grey;" > Moneda: Bolivares</span>
                        </div>
                         
                      
                  </div>

                  <div class="x_content">
                    <div>
                    
                      <!-- info de la visita -->
                    @if(count($material)>0)
                      
                        <table id="datatable-items" class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th width="20px">#</th>
                                  <th width="250px">{!!trans('form.table.description')!!}</th>
                                  <th width="30px">{!!trans('form.table.quantity')!!}</th>
                                  <th width="100px">{!!trans('form.table.unitprice')!!}</th>
                                  <th width="100px">{!!trans('form.table.total')!!}</th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                            </tr>
                            <? $i=1 ?>
                              @foreach($material as $m)
                              
                              <tr>
                                  <td>{{$i}}</td>
                                  <td>{{ $m->description}}</td>
                                  <td>{{ $m->quantity}}</td>
                                  <td>{{ number_format($m->unit_price,2,",",".")}}</td>
                                  <td>{{ number_format($m->total,2,",",".")}}</td>
                              </tr> 
                            <? $i++ ?>
                              @endforeach
                              <tr>
                                  <td colspan="3"></td>
                                  <td >Sub Total</td>
                                  <td>{{number_format($subtotal,2,",",".")}}</td>
                              </tr>
                              <tr>
                                  <td colspan="3"></td>
                                  <td >IVA {{$config->iva}}%</td>
                                  <td>{{ number_format($iva,2,",",".")}}</td>
                              </tr>
                              <tr>
                                  <td colspan="3"></td>
                                  <td >Total</td>
                                  <td>{{ number_format($total,2,",",".")}}</td>
                              </tr> 
                          </tbody>
                        </table>
                     @endif
                 </div>     
              </div>

                       
        <!-- /page content -->
      </div>

      <div style="page-break-before: always;"></div>
      <div class="x_panel" >
              <div class="x_content ">
                <p>
                  CONDICIONES<br />

                    1) Emitir orden de compra.<br />
                    2) La factura será entregada al finalizar el trabajo.<br />
                    3) La factura será emitida a la culminación de la venta.<br />
                    4) Anticipo del 70% al emitir orden de compra.<br />
                    5) Cancelación del 30% restante a la entrega de los equipos e instalación.<br />
                    6) Pago de contado al 100% según orden de compra.<br />
                    7) Los requerimientos adicionales serán procesados con una nueva cotización.<br />
                    8) El tiempo de la entrega de productos será de 5 días hábiles, si al momento de la aprobación hay disponibilidad.<br />
                    9) Garantía original de los equipos estipulada por el fabricante.<br />
                    10) Servicio post-venta garantizado.<br />
                    11) El retraso del pago en cualquiera de las obligaciones a cargo del cliente, dará lugar a la aplicación de la tasa máxima de interés compensatorio, así como a la aplicación, por concepto de interés moratorio, de la tasa del 3% (tres por ciento) mensual o la tasa más alta certificada por la Superintendencia Banca y Seguros (la que resulte mayor), ambas tasas aplicadas desde la fecha de vencimiento de la obligación y hasta la fecha en que INVERSIONES SONITUS, C.A, reciba el pago total de la suma adeudada.<br />
                    12) El presupuesto está calculado en una jornada mixta.<br />
                    13) Precios sujetos a cambio sin previo aviso.<br />
                    14) El presupuesto incluye IVA.<br />
                    15) Favor emitir cheque NO ENDOSABLE a nombre de INVERSIONES SONITUS, C.A. Transferencia o deposito a la Cuenta Corriente N° 0134 0366 04 3661291059 en el Banco BANESCO.<br /><br /><br />






                    Atentamente<br />

                    Inversiones Sonitus C.A

                </p>
              </div>
            </div>  
    </div>
    </div>
    </div>
</div>

  </body>
</html>