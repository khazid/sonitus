@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.status')!!}</h3>
              </div>

              
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.quotes')!!}</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      
                      <h2>{{ $client->name}}</h3>
                      <h3>{{$client->applicant_name}}</h2>
                      <p><strong>{{$client->charge}} </strong> </p>
                      <ul class="list-unstyled">
                                <li><i class="fa fa-envelope"></i> {!!trans('form.label.email')!!}: {{$client->email}} </li>
                                <li><i class="fa fa-building"></i> {!!trans('form.label.address')!!}: {{$client->address}} </li>
                                <li><i class="fa fa-phone"></i> {!!trans('form.label.phone')!!} #: {{$client->phone}} </li>
                      </ul>
                      
                      <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!!trans('form.label.back')!!}!</a>
                      
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>{!!trans('form.label.clientquote')!!}</h2>
                        </div>
                      </div>
                      

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <!-- start user projects -->
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>{!!trans('form.table.date')!!}</th>
                              <th>{!!trans('form.table.status')!!}</th>
                              <th>{!!trans('form.label.staff')!!}</th>
                              <th>{!!trans('form.table.description')!!}</th>
                              

                          </tr>
                      </thead>
                      <tbody>
                          @if(count(@$status)>0)
                          @foreach($status as $q)
                          <tr>
                              <td>{{ date('d/m/Y H:i',strtotime($q->created_at))}}</td>
                              <td>{{ $q->status}}</td>
                              <td>{{ $q->name}}</td>
                              <td style="width: 40%">{{ $q->observation}}</td>
                              
                          </tr>
                          @endforeach
                       @endif 
                      </tbody>
                    </table>
                            
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
 
@endsection 
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- morris.js -->
    {!! Html::script('/vendors/raphael/raphael.min.js') !!}
    {!! Html::script('/vendors/morris.js/morris.min.js') !!}
    <!-- bootstrap-progressbar -->
    {!! Html::script('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}
    <!-- bootstrap-daterangepicker -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/datepicker/daterangepicker.js') !!}
    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}


@endsection