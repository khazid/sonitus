{!! Form::open(['method' => 'post', 'validate','id'=>'formmessage']) !!}
<!-- MODAL PARA EL ENVIO DE MENSAJES ---->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel">

              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="messageModalLabel">New message</h4>
                  </div>
                  <div class="modal-body">
                     <input type="hidden" name="process_id" id="process_id">
                      <div class="form-group">
                        <label for="recipient-name" class="control-label" id="recipientname">Recipient:</label>
                        <input type="text" name="frommess" id="frommess" readonly="readonly" style="display: none;" class="form-control">
                        {!! Form::select('addressee', $destinatary,null,['class' => 'form-control','id'=>'addressee']) !!}
                      </div>
                      <div class="form-group">
                        <label for="recipient-name" class="control-label">Titulo:</label>
                        <input type="text" class="form-control" id="tittle" name="tittle" required="required">
                      </div>
                      <div class="form-group">
                        <label for="message-text" class="control-label" >Message:</label>
                        <textarea class="form-control" id="messagetext" name="messagetext" rows="20" required="required"></textarea>
                      </div>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="Send">Send message</button>
                  </div>
                </div>
              </div>
            </div>
<!---------------------------------------->
{!! Form::close()!!}