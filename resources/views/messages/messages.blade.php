@extends('app/navin')
@section('css')    
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
    <!-- Custom styling plus plugins -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')   
 
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.inbox.inbox')!!}<small></small></h3>
              </div>
             </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.inbox.inbox')!!}<small>{!!trans('form.inbox.newmail')!!}</small></h2>
                    <div class="btn-group" style="float:right">
                                <a href="{{route('mess.index')}}" class="btn btn-sm btn-success viewread" type="button"> {!!trans('form.inbox.new')!!}</a>
                                <a href="{{route('mess/read')}}" class="btn btn-sm btn-primary viewread" type="button"> {!!trans('form.inbox.read')!!}</a>
                                <a href="{{route('mess/deleted')}}" class="btn btn-sm btn-default" type="button"  data-placement="top" data-toggle="tooltip">{!!trans('form.inbox.deleted')!!}</a>
                                
                              </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">
                      <div class="col-sm-3 mail_list_column">
                        <button id="compose" class="btn btn-sm btn-success btn-block" type="button">{!!trans('form.inbox.compose')!!}</button>
                      
                       @if(count($messages)>0)
                        @foreach($messages as $mess)
                        <a href="#" class="read" data-content="{{$mess->message}}" data-from="{{$mess->name}}" data-date="{{$mess->created_at}}" data-title="{{$mess->tittle}}" data-email="{{$mess->email}}" id="{{$mess->id}}">
                          <div class="mail_list">
                            <div class="left">
                              <i class="fa fa-circle"></i>
                            </div>
                            <div class="right">
                              <h3>{{$mess->name}} <small>{{date('d/m/Y g:i a',strtotime($mess->created_at))}}</small></h3>
                              <p>{{$mess->tittle}}</p>
                            </div>
                          </div>
                        </a>
                        @endforeach
                        @else
                        <a href="#">
                          <div class="mail_list">
                            <div class="left">
                              <i class="fa fa-circle"></i> 
                            </div>
                            <div class="right">
                              <h3>{!!trans('form.inbox.nomessage')!!}</h3>
                              
                            </div>
                          </div>
                        </a>
                        @endif
                      </div>
                      <!-- /MAIL LIST -->

                      <div class="col-sm-9 mail_view">
                        <div class="jumbotron welcome">
                          <h1>{!!trans('form.inbox.welcome')!!}!</h1>
                          <p>Aqui podras ver los mensajes que te han enviado a traves del Sistema Sonitus<br />
                              Modo de uso:<br />
                              <ul>
                                <li>Nuevo Mensaje: Para crear un nuevo mensaje pulse el boton de "Componer"</li>
                                <li>Ver Mensaje : Para ver el contenido del mensaje debe hacer click sobre la linea del mensaje que quiere revisar en el listado de mensajes nuevo</li>
                                <li>Responder : Para responder algun mensaje solo debe pulsar el boton "Responder" el sistema se encargara de colocar la direccion de correo del remitente automaticamente</li>
                                <li>Eliminar : Para eliminar puede pulsar el boton con el icono de papelera, el mensaje sera borrado y podra ser recuperado en el listado de mensajes eliminados </li>
                                <li>Mensjaes leidos : Para ver los mensajes leidos pulse el boton "Leidos"  </li>
                              </ul>

                          </p>
                         
                        </div>
                        <div class="inbox-body" style="display:none">
                          <div class="mail_heading row" >
                            <div class="col-md-8">
                              <div class="btn-group">
                                <button class="btn btn-sm btn-primary reply" type="button"  data-title="" data-email="" id=""><i class="fa fa-reply"></i> {!!trans('form.inbox.reply')!!}</button>
                               
                                <button class="btn btn-sm btn-default trash" type="button" id="" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                              </div>
                            </div>
                            <div class="col-md-4 text-right">
                              <p class="date" id="messdate"> </p>
                            </div>
                            <div class="col-md-12">
                              <h4 id="title"> </h4>
                            </div>
                          </div>
                          <div class="sender-info">
                            <div class="row">
                              <div class="col-md-12">
                                <strong id="from"></strong>
                                
                                <a class="sender-dropdown"><i class="fa fa-chevron-down"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="view-mail" id="messagecontent">
                            
                          </div>

                          <!-- <div class="btn-group">
                            <button class="btn btn-sm btn-primary reply" type="button"><i class="fa fa-reply"></i> {!!trans('form.inbox.reply')!!}</button>
                           
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                          </div> -->
                        </div>

                      </div>
                      <!-- /CONTENT MAIL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

    <!-- compose -->
    <div class="compose col-md-6 col-xs-12">
      <div class="compose-header">
        New Message
        <button type="button" class="close compose-close">
          <span>×</span>
        </button>
      </div>
{!! Form::open(['route' => 'mess/send','method' => 'post', 'validate','id'=>'formmessage']) !!}
      <div class="compose-body">

        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">{!!trans('form.inbox.to')!!}:</span>
                            <input type="text" name="addressee" id="autocomplete-custom-append" class="form-control col-md-10 addressee" aria-describedby="basic-addon1"/>
                          </div>
        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">{!!trans('form.inbox.subject')!!}:</span>
                            <input type="text" name="subject" id="subject" class="form-control col-md-10"  aria-describedby="basic-addon1"/>
                          </div>
      
        <div id="alerts"></div>

        <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
            <ul class="dropdown-menu">
            </ul>
          </div>

          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>
                <a data-edit="fontSize 5">
                  <p style="font-size:17px">Huge</p>
                </a>
              </li>
              <li>
                <a data-edit="fontSize 3">
                  <p style="font-size:14px">Normal</p>
                </a>
              </li>
              <li>
                <a data-edit="fontSize 1">
                  <p style="font-size:11px">Small</p>
                </a>
              </li>
            </ul>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
            <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
          </div>

          <!-- <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
            <div class="dropdown-menu input-append">
              <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
              <button class="btn" type="button">Add</button>
            </div>
            <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
          </div> -->
        </div>

        <div id="editor" name="editor" class="editor-wrapper"></div>
        <input type="hidden" name="messagetext" id="messagetext">
      </div>

      <div class="compose-footer">
        <button id="send" class="btn btn-sm btn-success" type="submit">{!!trans('form.inbox.submit')!!}</button>
      </div>
    </div>
    {!! Form::close()!!}
    <!-- /compose -->        
@endsection
@section('script')        
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
<!-- bootstrap-wysiwyg -->
{!! Html::script('/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') !!}
{!! Html::script('/vendors/jquery.hotkeys/jquery.hotkeys.js') !!}
{!! Html::script('/vendors/google-code-prettify/src/prettify.js') !!}

    <!-- jQuery autocomplete -->
    {!! Html::script('/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') !!}
   
<!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}
    {!! Html::script('assets/js/message.js') !!} 
   
    <!-- bootstrap-wysiwyg -->
    <script>
     var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {

        function initToolbarBootstrapBindings() {
          var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'
            ],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
          $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
          });
          $('a[title]').tooltip({
            container: 'body'
          });
          $('.dropdown-menu input').click(function() {
              return false;
            })
            .change(function() {
              $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
            })
            .keydown('esc', function() {
              this.value = '';
              $(this).change();
            });

          $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this),
              target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
          });

          if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $('#editor').offset();

            $('.voiceBtn').css('position', 'absolute').offset({
              top: editorOffset.top,
              left: editorOffset.left + $('#editor').innerWidth() - 35
            });
          } else {
            $('.voiceBtn').hide();
          }
        }

        function showErrorAlert(reason, detail) {
          var msg = '';
          if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
          } else {
            console.log("error uploading file", reason, detail);
          }
          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        initToolbarBootstrapBindings();

        $('#editor').wysiwyg({
          fileUploadError: showErrorAlert
        });

        prettyPrint();
      });
    </script>
    <!-- /bootstrap-wysiwyg -->

    <!-- compose -->
    <script>
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
//compose --------------------------------------------
      $('#compose, .compose-close').click(function(){
        $('.compose').slideToggle();
        
        $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: APP_URL+'/mess/destinatary', // This is the url we gave in the route
                      data: {}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      console.log('Data',response.data);
                        var destinatary =response.data;
                          // initialize autocomplete with custom appendTo
                          $('#autocomplete-custom-append').autocomplete({
                            lookup: destinatary
                          });
                      }
                  }); 
      });
//Reply ------------------------------------------------
      $('.reply').click(function(){
        
        var title=$(this).data('title');
        var email=$(this).data('email');
        
        $('.addressee').val(email);
        $('#subject').val('Respuesta: '+title);
       
        $('.compose').slideToggle();
       }); 
        
//Send ------------------------------------------------
      $('#send').click(function(){

        var message=$('#editor').html();
        $('#messagetext').val(message);
        
      });

//Read ------------------------------------------------
      $('.read').click(function(){
        
        var message=$(this).data('content');
        var from=$(this).data('from');
        var date=$(this).data('date');
        var title=$(this).data('title');
        var email=$(this).data('email');
        var id=$(this).attr('id');
        var status=10;
        $('#from').text(from+"("+email+")");
        $('#email').text(email);
        $('#messdate').text(date);
        $('#title').text(title);
        $('#messagecontent').html(message);
        console.log('email'+email+' title');
        $('.reply').data('email',email);
        $('.reply').data('title',title);
        $('.reply').attr('id',id);
        $('.trash').attr('id',id);
        
        $('.inbox-body').show();
        $('.welcome').hide();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: APP_URL+'/changestatus', // This is the url we gave in the route
            data: {'idstatus':status,'idmessage':id}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
            console.log('Data',response.data);
              
            }
        });
//Delete ------------------------------------------------
      $('.trash').click(function(){
        
        var id=$(this).attr('id');
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: APP_URL+'/delete', // This is the url we gave in the route
            data: {'idmessage':id}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
            console.log('Data',response.data);
              location.reload();
            }
        }); 
  
      });
});

</script>
    <!-- /compose -->


@endsection
