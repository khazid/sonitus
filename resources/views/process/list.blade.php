@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
   
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')

<!-- page content -->
<div class="right_col" role="main">
      <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.project')!!} <small>{!!trans('form.label.list')!!}</small></h3>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.projectlist')!!}</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    

                    <!-- start project list -->
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 1%">#</th>
                          <th style="width: 20%">{!!trans('form.table.client')!!}</th>
                          <th>{!!trans('form.table.proleader')!!}</th>
                          <th>{!!trans('form.table.date')!!}</th>
                          <th>{!!trans('form.table.status')!!}</th>
                          <th>{!!trans('form.table.priority')!!}</th>
                          <th style="width: 20%">{!!trans('form.table.action')!!}</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if(count(@$process)>0)
                        @foreach($process as $pro)
                        <tr>
                          <td>{{$pro->process_number}}</td>
                          <td>
                            <a>{{$pro->name}}</a>
                            <br />
                            <small>{{date('d-m-Y',strtotime($pro->assigned_date))}}</small>
                          </td>
                          <td>
                            <? if($pro->profile_pic!='')$file=url('images/personal/'.$pro->profile_pic);
                                else $file='';

                            ?>
                            @if($rol < 5 && $pro->statusid!=5)
                            <a  class="changetec" title='Cambiar Tecnico Asignado' data-toggle="modal" data-target="#changeModal"  id="{{$pro->processid}}">
                                @if(@$file!='')
                                {{$pro->staffname}}
                                @else
                                <span>Asignar Lider</span>
                                @endif
                            </a>    

                            @else
                            <img src="{{$file}}" class="avatar" alt="Avatar">{{$pro->staffname}}
                            @endif
                            
                          </td>
                          <td class="project_progress">
                            @if($pro->ini_date!='')
                            {{date('d-m-Y',strtotime($pro->ini_date))}}
                            @else
                            No se ha realizado la primera visita
                            @endif
                          </td>
                          <td>
                          @if($rol < 5 && $pro->statusid!=5)
                            <button type="button" class="btn btn-primary btn-xs statuschange" data-toggle="modal" data-target="#statusModal"  id="{{$pro->processid}}">{{$pro->status}}</button>
                          @else
                            {{$pro->status}}
                          @endif  
                          </td>
                          <td>
                           @if($rol == 5)
                              {{$pro->priority}}
                            @elseif($rol!=5 && $pro->statusid!=5)
                            <button type="button" class="btn btn-warning btn-xs priority" data-toggle="modal" data-target="#priorityModal"  id="{{$pro->processid}}">{{$pro->priority}}</button>
                            
                            @elseif($rol<=5 && $pro->statusid==5)
                              {{$pro->priority}}
                            @endif
                            
                          </td>
                          <td>
                            <!-- <a  class="fa fa-envelope-o fa-mid message" title='Enviar mensaje' href="#" data-content="{{$pro->process_number}}" data-client="{{$pro->name}}" data-id="{{$pro->processid}}" id="compose"></a> -->
                             <a href="{{ route('process/show',['id' => $pro->processid,'type'=>'process'] )}}" class="fa fa-eye fa-2x" title="Show info."></a>
                             @if($rol < 5 )
                             <a href="{{route('process/showstatus',['id'=>$pro->processid])}}" class="fa fa-info-circle fa-2x" title="Show Status Detail" ></a>
                                 @if($pro->statusid!=5 && $pro->statusid!=22 && $pro->statusid!=24 && $pro->statusid!=25 )
                                 <button type="button" class="fa fa-tasks fa-2x quote" data-client="{{$pro->id}}" data-process="{{$pro->processid}}" data-toggle="modal" data-target="#QuoteModal" title="Solicitar cotizacion de nuevos equipos o materiales para el proyecto"></button>
                                 @endif
                             @endif
                            @if($pro->statusid==4 && $rol<=4)
                            <a  class="fa fa-exclamation-circle fa-2x" style="color: orange" title='Revision de proceso para cerrar' href="#" >  </a>
                            @endif
                            
                            
                            @if($pro->statusid==3 && $rol==5)
                            <a  class="fa fa-check-circle fa-2x endtask" style="color: red" title='Terminar proceso' href="#" id="{{$pro->processid}}" data-content="{{$pro->description}}" data-client="{{$pro->name}}" data-process="{{$pro->process_number}}">  </a>
                            @endif

                            @if($pro->statusid==5)
                             <a  class="btn btn-default" target="_blank" title='Ver Reporte' href="{{route('process/pdfshow',['id'=>$pro->processid])}}">Reporte</a> 
                            @endif

                            @if($pro->statusid==6)
                            <!-- <a  class="fa fa-tasks fa-2x sendpro" title='Enviar Proyecto' href="#" id="{{$pro->processid}}" data-content="{{$pro->description}}" data-client="{{$pro->name}}" data-process="{{$pro->process_number}}"></a> -->
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      @endif 
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>
 </div>
 <!-- ----------- MODAL QUOTE ------------------------ -->
  <div id="QuoteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="QuoteModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="QuoteModalLabel2">Solicitud de Cotizacion Nuevos Materiales/Equipos</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="clientidquote" id="clientidquote">
          <input type="hidden" name="processidquote" id="processidquote">
          
            
            <div class="form-group">
            
                  <label class=" control-label">Observacion para la Cotizacion</label>
                  <textarea class="form-control" style="height:55px;" id="descrcot" name="descrcot"></textarea>
                  
            </div>
              
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitquote">Save changes</button>
          </div>
        </div>
      </div>
    </div>  
  
    @include('process.modalchange')
  @endsection

   @section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- bootstrap-progressbar -->
    {!! Html::script('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}
    <!-- bootstrap-wysiwyg -->
    {!! Html::script('/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') !!}
    {!! Html::script('/vendors/jquery.hotkeys/jquery.hotkeys.js') !!}
    {!! Html::script('/vendors/google-code-prettify/src/prettify.js') !!}
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}
    {!! Html::script('assets/js/process.js') !!}
    {!! Html::script('assets/js/modal.js') !!}

       <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}

        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

        <!-- bootstrap-wysiwyg -->
    <script>
   var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
        function initToolbarBootstrapBindings() {
          var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'
            ],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
          $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
          });
          $('a[title]').tooltip({
            container: 'body'
          });
          $('.dropdown-menu input').click(function() {
              return false;
            })
            .change(function() {
              $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
            })
            .keydown('esc', function() {
              this.value = '';
              $(this).change();
            });

          $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this),
              target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
          });

          if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $('#editor').offset();

            $('.voiceBtn').css('position', 'absolute').offset({
              top: editorOffset.top,
              left: editorOffset.left + $('#editor').innerWidth() - 35
            });
          } else {
            $('.voiceBtn').hide();
          }
        }

        function showErrorAlert(reason, detail) {
          var msg = '';
          if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
          } else {
            console.log("error uploading file", reason, detail);
          }
          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        initToolbarBootstrapBindings();

        $('#editor').wysiwyg({
          fileUploadError: showErrorAlert
        });

        prettyPrint();
      });
    </script>
    <!-- /bootstrap-wysiwyg -->
<!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
    <!-- compose -->
    <script>
      $('#compose, .compose-close').click(function(){
        $('.compose').slideToggle();
      });
      $('#send').click(function(){
       //funcion ajax para guardar el mensaje
      });

      $('#datatable-buttons').on('click','.quote',function(e) {
          var clientid=$(this).data('client'),
              processid=$(this).data('process');
              $('#clientidquote').val(clientid);
              $('#processidquote').val(processid);
              
              
              
        });
      $('.submitquote').click(function(){
          var requestid='',
              processid=$('#processidquote').val(),
              clientid=$('#clientidquote').val(),
              observation=$('#descrcot').val();
              
              console.log('response id:'+requestid+' clientid: '+clientid+' observacion: '+observation+' process: '+processid);
              $.confirm({
                            title: '<strong>Solicitud de Cotizacion!!</strong>',
                            content: 'La solicitud sera enviada con la observacion agregada al departamento Administrativo.',
                            confirmButton:'Aceptar',
                            cancelButton:'Cancelar',
                            confirm: function(){
                               $.confirm({
                                           icon:'fa fa-spinner fa-spin',
                                          title:'En Proceso..',
                                          content:'La Solicitud esta en proceso, por favor espere...',
                                          confirmButton:false,
                                          cancelButton:false,
                                          closeIcon:false,
                                        });
                                         this.close(true);
                                         
                               $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                              });
                               $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/quoterequest', // This is the url we gave in the route
                                      data: {'requestid' : requestid,'clientid':clientid,'observation':observation,'processid':processid}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          location.href=response.ruta;

                                      }
                              });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
        });
    </script>
    <!-- /compose -->
    
   @endsection 
