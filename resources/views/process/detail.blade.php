@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!! trans('form.label.project') !!}  <small>{!! trans('form.label.detail') !!}</small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for..."> -->
                    <span class="input-group-btn">
                    <a href="{{ ($type=='client' ?  route('client.show',['id'=>$process->id]) : route('process.index')) }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!! trans('form.label.back') !!}!</a>
                      <!-- <button class="btn btn-default" type="button">Go!</button> -->
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!! trans('form.label.projectdetail') !!}</h2>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <ul class="stats-overview">
                        <li>
                          <span class="name"> {!! trans('form.label.visitcant') !!} </span>
                          <span class="value text-success"> {{@$cantvisit[0]->total}} </span>
                        </li>
                        <li>
                          <span class="name">{!! trans('form.label.days') !!}</span>
                          <span class="value text-success"> {{@$days[0]->total}} </span>
                        </li>
                        <li class="hidden-phone">
                          <span class="name"> {!! trans('form.label.duration') !!} </span>
                          <span class="value text-success"> {{@$counter[0]->total}} Hours</span>
                        </li>
                      </ul>
                      <br />

                      <!-- <div id="mainb" style="height:350px;"></div>
                      <div class="x_title">
                          <div class="clearfix"></div>
                        </div> -->
                      <div><!-- Seccion de detalle de visitas y solicitudes al cliente y cliente a sonitus -->
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Visits</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Requests</a>
                          </li>
                          
                        </ul>
                        <div id="myTabContent" class="tab-content">

                        <!-- VISITAS -->
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab"> 

                            <div class="panel panel-default">
                              <div class="panel-heading col-md-12">
                                
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body">
                            
                                    @if(count($visits)>0) 
                                    <div class="dataTable_wrapper">
                                     <!-- start tecnician visits -->
                                    <table class="data table table-striped no-margin" id="dataTables-visits">
                                      <thead>
                                        <tr>
                                          <th>{!!trans('form.table.tecnicians')!!}</th>
                                          <th>{!!trans('form.table.date')!!}</th>
                                          <th>{!!trans('form.label.visitinfo')!!}</th>
                                          <th class="hidden-phone">{!!trans('form.label.hourspend')!!}</th>
                                          <th>{!!trans('form.table.status')!!}</th>
                                                                                    
                                        </tr>
                                      </thead>
                                      <tbody>
                                        
                                         @foreach($visits as $v)
                                         <? 
                                            if($v->total == 0) $counthr='New visit';
                                            else $counthr=$v->total;
                                         ?>
                                        <tr>
                                          <td>{{$v->name}}</td>
                                          <td>{{date('l jS \of F Y',strtotime($v->date))}}</td>
                                          <td>{{$v->description}}</td>
                                          <td class="hidden-phone">{{$counthr}}</td>
                                          <td>{{$v->status}}</td>
                                          
                                          
                                        </tr>
                                       @endforeach 
                                       
                                      </tbody>
                                    </table>
                                    <!-- end user projects -->
                                    </div>
                                    <!-- /.table-responsive -->
                                    @else
                                      
                                          <span>No information to show</span>
                                        
                                    @endif
                              </div><!-- /.panel-body -->

                    </div><!-- /.panel -->  

                    </div> 

                    <!-- SOLICITUDES AL CLIENTE Y SOLICITUDES A SONITUS -->
                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                          
                              <h4 class="green"> {!! trans('form.label.requestclient') !!}</h4>
                              <table class="data table table-striped no-margin" id="dataTables-rclient">
                                    <thead>
                                      <tr>
                                        <th>Date</th>
                                        <th>{!!trans('form.label.request')!!}</th>
                                        <th>{!!trans('form.table.status')!!}</th>
                                        <th>{!!trans('form.table.action')!!}</th>
                                        
                                      </tr>
                                    </thead>
                                    <tbody>
                                     @if(count($requestclient)>0)
                                       @foreach($requestclient as $rtoclient)
                                      <tr>
                                        <td>{{date("D M j Y",strtotime($rtoclient->date))}}</td>
                                        <td>{{$rtoclient->request}}</td>
                                        <td>{{$rtoclient->status}}</td>
                                        <td>
                                          @if($rol < 5 && $rtoclient->statusid == 14)
                                          <div class="col-xs-12 col-sm-12 emphasis">
                                            <button type="button" class="btn btn-success btn-xs accept" id="{{$rtoclient->id}}" data-client="{{$rtoclient->client_id}}" data-personal="{{$rtoclient->users_id}}" data-visit="{{$rtoclient->visitid}}" data-content="{{$rtoclient->request}}"><i class="glyphicon glyphicon-thumbs-up"></i></button>
                                            
                                            <button type="button" class="btn btn-danger btn-xs denied" data-toggle="modal" data-target="#CalenderModalEdit" id="{{$rtoclient->id}}" data-client="{{$rtoclient->client_id}}" data-personal="{{$rtoclient->users_id}}" data-visit="{{$rtoclient->visitid}}"><i class="glyphicon glyphicon-thumbs-down"></i></button>
                                            
                                            <button type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#CalenderModalEdit" id="{{$rtoclient->id}}" data-content="{{$rtoclient->request}}" data-client="{{$rtoclient->client_id}}" data-personal="{{$rtoclient->users_id}}" data-visit="{{$rtoclient->visitid}}"><i class="glyphicon glyphicon-pencil"></i></button>
                                           
                                          </div>
                                          @elseif($rol == 8 && $rtoclient->statusid == 15)
                                            <button type="button" class="btn btn-primary btn-xs done" id="{{$rtoclient->id}}" data-content="{{$rtoclient->request}}" data-client="{{$rtoclient->client_id}}" data-personal="{{$rtoclient->users_id}}" data-visit="{{$rtoclient->visitid}}"><i class="glyphicon glyphicon-check"></i></button>
                                          
                                          @endif
                                        </td>
                                        
                                      </tr>
                                     @endforeach 
                                     @else
                                     <tr>
                                        <td colspan="5">No information to show</td>
                                      </tr>
                                     @endif
                                    </tbody>
                              </table>

                            <!-- start user projects -->
                            <h4 class="green"> Solicitudes del Cliente</h4>
                            <table class="data table table-striped no-margin" id="dataTables-client">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>{!!trans('form.label.request')!!}</th>
                                  <th>{!!trans('form.table.type')!!}</th>
                                  <th>{!!trans('form.table.status')!!}</th>
                                  <th>{!!trans('form.table.response')!!}</th>
                                  <th></th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                               @if(count($requestweb)>0)
                                 @foreach($requestweb as $pro)
                                <tr>
                                  <td>{{date("D M j Y",strtotime($pro->date))}} </a></td>
                                  <td>{{$pro->description}}</td>
                                  <td>{{$pro->type}}</td>
                                  <td>{{$pro->status}}</td>
                                  <td>{{$pro->response}}</td>
                                  <td>
                                      @if($rol < 5 && $pro->status_id==26)
                                                <button type="button" class="btn btn-success btn-xs response" id="{{$pro->id}}" data-process="{{$pro->processid}}" data-content="{{$pro->email}}" data-toggle="modal" data-target="#ResponseModal">Responder</button>
                                                
                                                <a href="{{route('client/newproject',['id'=>$pro->clientid])}}" class="btn btn-warning btn-xs newcreate"  id="{{$pro->id}}" data-process="{{$pro->processid}}" data-content="{{$pro->email}}" data-type="newpro">Crear Proceso</button>
                                                
                                                <a href="{{route('visits/calendar')}}" class="btn btn-primary btn-xs newcreate" id="{{$pro->id}}" data-process="{{$pro->processid}}" data-content="{{$pro->email}}" data-type="newvisit">Agendar visita</a>

                                                <button type="button" class="btn btn-default btn-xs quote" id="{{$pro->id}}" data-email="{{$pro->email}}" data-client="{{$pro->clientid}}" data-content="{{$pro->description}}" data-toggle="modal" data-target="#QuoteModal">Cotizacion</button>
                                      @endif
                                  </td>
                                  
                                </tr>
                               @endforeach 
                               @else
                               <tr>
                                  <td colspan="5">No information to show</td>
                                </tr>
                               @endif
                              </tbody>
                            </table>
                            <!-- end user projects -->

                          </div>
                          
                        </div>
                      </div>

                        

                      </div>


                    </div>

                    <!-- -------------- SIDE BAR INFORMATION ---------------->
                    <div class="col-md-3 col-sm-3 col-xs-12">

                      <section class="panel">

                        <div class="x_title">
                          <h2>{!! trans('form.label.projectdescription') !!}</h2>

                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                        <p class="title"><b>{!! trans('form.label.inidate') !!}:</b> {{date("j M Y",strtotime($process->ini_date))}} /
                        <b>{!! trans('form.label.enddate') !!}:</b> {{($process->end_date!=null ? date("j M Y",strtotime($process->end_date)) : '-')}}</p>
                          <div><span class="badge bg-gray">{!! trans('form.label.status') !!}: {{$process->status}}</span></div>
                          <h4 class="green"> {!! trans('form.label.project') !!}: {{$process->process_number}}</h4>
                          @if($rol <= 4 && $process->statusid==4)
                            <a  class="btn btn-warning checkprocess"  title='Revision' href="#" id="{{$process->processid}}" data-content="{{$process->description}}" data-client="{{$process->name}}" data-process="{{$process->process_number}}">Close Process</a>
                          @endif
                          <p>{{$process->description}}</p>
                          <br />

                          <div class="project_detail">

                            <p class="title">{!! trans('form.label.corpname') !!}: {{$process->name}}</p>
                            <p class="title">{!! trans('form.label.rif') !!}: {{$process->rif}}</p>
                            <p class="title">{!! trans('form.label.contactname') !!}: {{$process->applicant_name}}</p>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-envelope"></i> <b>{!! trans('form.label.email') !!}:</b> {{$process->email}} </li>
                                <li><i class="fa fa-building"></i> <b>{!! trans('form.label.address') !!}:</b> {{$process->address}} </li>
                                <li><i class="fa fa-phone"></i> <b>{!! trans('form.label.phone') !!} #:</b> {{$process->phone}} </li>
                            </ul>
                            <p class="title">{!! trans('form.label.proleader') !!}</p>
                            <p>{{$process->staff}}</p>

                          
                        </div>
                        </div>
                      </section>

                    </div>
                    <!-- end project-detail sidebar -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->  
        <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2"></h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="requestid" id="requestid">
          <input type="hidden" name="clientid" id="clientid">
          <input type="hidden" name="personalid" id="personalid">
          <input type="hidden" name="visitid" id="visitid">
          <input type="hidden" name="type" id="type">
      
            <div id="testmodal" style="padding: 5px 20px;">
              <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label reason">Description</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default closerequest" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitrequest">Save changes</button>
          </div>
        </div>
      </div>
    </div> 
    <!-- ----------- MODAL RESPONSE ------------------------ -->
  <div id="ResponseModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ResponseModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="ResponseModalLabel2">Respuesta de solicitud Web</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="requestid" id="requestid">
          <input type="hidden" name="processid" id="processid">
          <input type="hidden" name="email" id="email">
            <div class="form-group">
            
                  <label class=" control-label">Tipo de Respuesta</label>
                  <select name="tipo" id="tipo" class="form-control">
                    <option value="27"> Respuesta de informacion</option>
                    <option value="28"> Envio de cotizacion</option>
                    <option value="29"> Se crea Proceso</option>
                    <option value="30"> Se crea Visita por Mantenimiento</option>
                    <option value="31"> Se crea Visita por Soporte</option>
                  </select>
                  
            </div>
            <div class="form-group">
            
                  <label class=" control-label">Respuesta</label>
                  <textarea class="form-control" style="height:55px;" id="descresponse" name="descresponse"></textarea>
                  
            </div>
              
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default closerequest" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitresponse">Save changes</button>
          </div>
        </div>
      </div>
    </div>   
<!-- ----------- MODAL QUOTE ------------------------ -->
  <div id="QuoteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="QuoteModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="QuoteModalLabel2">Solicitud de Cotizacion de solicitud Web</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="requestidquote" id="requestidquote">
          <input type="hidden" name="clientidquote" id="clientidquote">
          
            
            <div class="form-group">
            
                  <label class=" control-label">Informacion del Cliente</label>
                  <textarea class="form-control" style="height:55px;" id="descrquote" name="descrquote" readonly="readonly"></textarea>
                  
            </div>
            <div class="form-group">
            
                  <label class=" control-label">Observacion para la Cotizacion</label>
                  <textarea class="form-control" style="height:55px;" id="descrcot" name="descrcot"></textarea>
                  
            </div>
              
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitquote">Save changes</button>
          </div>
        </div>
      </div>
    </div>       
@endsection
@section('script') 
    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}  
        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}   
<script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {

        function modrequest(id,description,clientid,personalid,type,visitid){
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              type: 'POST', // Type of response and matches what we said in the route
              url: APP_URL+'/visits/requesttoclient', // This is the url we gave in the route
              data: {'id' : id,'description': description,'clientid': clientid, 'personalid':personalid, 'type':type, 'visitid': visitid}, // a JSON object to send back
              success: function(response){ // What to do if we succeed
                 //console.log('request id:',response.requestclient)
                  //location.reload();
              }
            });
          $('.closerequest').click();
        }
        
        $('.submitrequest').click(function(){
           var id=$('#requestid').val(),
              clientid=$('#clientid').val(),
              personalid=$('#personalid').val(),
              description=$('#descr').val(),
              visitid=$('#visitid').val(),
              type=$('#type').val();
              //console.log('Submit id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type+' description: '+description);
            modrequest(id,description,clientid,personalid,type,visitid);
        });
    
        $('.accept').click(function(){
          var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              description=$(this).data('content'),
              visitid=$(this).data('visit'),
              type='accept';
              //console.log('Accept id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type+' description: '+description);
              $.confirm({
                            title: '<strong>Accept and Send Request to client!!</strong>',
                            content: 'Are you sure to send the request to the client?',
                            confirmButton:'Yes',
                            cancelButton:'No',
                            confirm: function(){
                               modrequest(id,description,clientid,personalid,type,visitid);
                            },
                            cancel:function(){
                               
                            }
                            
                        });
              
        });

        $('.edit').click(function(){
          var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              visitid=$(this).data('visit'),
              description=$(this).data('content'),
              type='edit';
              //console.log('Edit id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type+' description: '+description);
              $('#requestid').val(id);
              $('#clientid').val(clientid);
              $('#personalid').val(personalid);
              $('#descr').val(description);
              $('#visitid').val(visitid);
              $('#type').val(type);
              $('.modal-header h4').text('Edit Request to Client');
              $('.reason').text('Description');

        });
        
        $('.denied').click(function(){
             var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              visitid=$(this).data('visit'),
              type='denied';
              //console.log('Denied id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type);
              $('#requestid').val(id);
              $('#clientid').val(clientid);
              $('#personalid').val(personalid);
              $('#visitid').val(visitid);
              $('#type').val(type);
              $('.modal-header h4').text('Reject Request to Client');
              $('.reason').text('Observation');
            
        });

        $('.done').click(function(){
            var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              visitid=$(this).data('visit'),
              type='done';
              console.log('Done id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type);
              $.confirm({
                            title: '<strong>Solicitud realizada!!</strong>',
                            content: 'Esta seguro que desea marcar la solicitud como realizada?',
                            confirmButton:'Yes',
                            cancelButton:'No',
                            confirm: function(){
                                modrequest(id,'',clientid,personalid,type,visitid);
                                location.reload();
                            },
                            cancel:function(){
                               
                            }
                            
                        });
           
            
        });

        $('.checkprocess').click(function(e) {
            var idprocess= $(this).attr("id");
            var contenido= $(this).attr("data-content");
            var cliente= $(this).attr("data-client");
            var proceso= $(this).attr("data-process");
            var procurl = APP_URL+"/endtask";
            var status=5;
            $.confirm({
                            title: '<strong>Cerrar proceso!!</strong>',
                            content: 'Seguro que desea marcar como Cerrado: </br> <b>Proceso # '+proceso+'</br> Cliente: '+cliente+'</b> al cerrar el proceso el mismo sera enviado al departamento administrativo.',
                            confirmButton:'Si',
                            cancelButton:'No',
                            confirm: function(){
                              $.alert('Por favor espere mientras se realiza la creacion y envio del reporte...');
                               $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: procurl, // This is the url we gave in the route
                                        data: {'idprocess' : idprocess,'status':status}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                          // console.log('endtask',response.endtask);
                                          // console.log('mail',response.mail);
                                           $.confirm({
                                                  title: '<strong>Proceso Cerrado!!</strong>',
                                                  content: '<b>Proceso # '+proceso+'</br> Cliente: '+cliente+'</b> ya esta cerrado y se le envio el correo correspondiente con el reporte',
                                                  confirmButton:'Aceptar',
                                                  cancelButton:false,
                                                  confirm: function(){
                                                      location.reload();
                                                  },
                                                  
                                                  
                                              });
                                            

                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });

        

      });
</script> 

<!-- Solicitudes Del Cliente via WEB -->
<script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
        $('#dataTables-client').on('click','.response',function(){
          var id=$(this).attr('id'),
              processid=$(this).data('process'),
              email=$(this).data('content');
              console.log('Para el MODAL request id:'+id+' processid: '+processid+' email: '+email);
              $('#requestid').val(id);
              $('#processid').val(processid);
              $('#email').val(email);
              $('#descr').removeAttr('readonly');
              $('.closerequest').show();
              $('.submitresponse').show();
              
        });
        $('.submitresponse').click(function(){
          var requestid=$('#requestid').val(),
              processid=$('#processid').val(),
              email=$('#email').val(),
              tipo=$('#tipo').val(),
              response=$('#descresponse').val();
              
              console.log('Para el ENVIO response id:'+requestid+' processid: '+processid+' email: '+email+' respuesta: '+response+' tipo'+tipo);
              $.confirm({
                            title: '<strong>Enviar respuesta al cliente!!</strong>',
                            content: 'La respuesta se enviara al correo '+email+' del cliente',
                            confirmButton:'Enviar',
                            cancelButton:'Cancelar',
                            confirm: function(){

                               $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/responserequest', // This is the url we gave in the route
                                      data: {'requestid' : requestid,'processid':processid,'email':email,'response':response,'tipo':tipo}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          location.reload();

                                      }
                              });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
        });
       
        $('#dataTables-client').on('click','.respuesta',function(){
           var id=$(this).attr('id');
            console.log('id'+id);
            $('.closerequest').hide();
            $('.submitresponse').hide();
           $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/findresponse', // This is the url we gave in the route
                                      data: {'requestid' : id}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          $('#descr').val(response.respuesta);
                                          $('#descr').attr('readonly','readonly');

                                      }
                              });

        });
        
        $('#dataTables-client').on('click','.newcreate',function(e){
             
            alert('Recuerde que despues de creado el proceso o la visita debe responder la solicitud del cliente en el boton "Responder"');
        });

        $('#dataTables-client').on('click','.quote',function(){
          var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              content=$(this).data('content');
              
              $('#requestidquote').val(id);
              $('#clientidquote').val(clientid);
              $('#descrquote').text(content);
              
              
        });
        $('.submitquote').click(function(){
          var requestid=$('#requestidquote').val(),
              clientid=$('#clientidquote').val(),
              observation=$('#descrcot').val(),
              processid='';
              
              console.log('response id:'+requestid+' clientid: '+clientid+' observacion: '+observation);
              $.confirm({
                            title: '<strong>Solicitud de Cotizacion!!</strong>',
                            content: 'La solicitud sera enviada con la observacion agregada al departamento Administrativo, de igual forma se le enviara un correo al cliente indicando que su solicitud esta en proceso de cotizacion y la misma sera enviada en 24 a 48 horas a su correo.',
                            confirmButton:'Aceptar',
                            cancelButton:'Cancelar',
                            confirm: function(){

                               $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/quoterequest', // This is the url we gave in the route
                                      data: {'requestid' : requestid,'clientid':clientid,'observation':observation,'processid':processid}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          location.reload();

                                      }
                              });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
        });


        

      });
</script> 
    
      @endsection