{!! Form::open(['route' => 'process/updatetec', 'method' => 'post', 'validate']) !!}

<!-- MODAL PARA EL CAMBIO DE TECNICO ---->
<div class="modal fade" id="changeModal" tabindex="-1" role="dialog" aria-labelledby="changeModalLabel">
              {!! Form::hidden('processid',null,['id'=>'processid']) !!}
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">{!!trans('form.label.techchange')!!}</h4>
                  </div>
                  <div class="modal-body">
                    
                      <div class="form-group">
                        <label for="recipient-name" class="control-label">{!!trans('form.label.technicians')!!}:</label>
                        {!! Form::select('personal_id', $personal,null,['class' => 'form-control']) !!}

                      </div>
                      <div class="form-group">
                        <label for="newstatus" class="control-label">{!!trans('form.label.observation')!!}:</label>
                        <textarea name="statusob" id="statusob" required="required" class="form-control"></textarea>
                      
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{!!trans('form.label.close')!!}</button>
                    <button type="submit" class="btn btn-primary">{!!trans('form.label.submit')!!}</button>
                  </div>
                </div>
              </div>
            </div>
<!---------------------------------------->
{!! Form::close()!!}


{!! Form::open(['route' => 'process/updateprio', 'method' => 'post', 'novalidate']) !!}

<!-- MODAL PARA EL CAMBIO DE PRIORIDAD ---->
<div class="modal fade" id="priorityModal" tabindex="-1" role="dialog" aria-labelledby="priorityModalLabel">
              {!! Form::hidden('proprioid',null,['id'=>'proprioid']) !!}
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">{!!trans('form.label.prioritychange')!!}</h4>
                  </div>
                  <div class="modal-body">
                    
                      <div class="form-group">
                        <label for="recipient-name" class="control-label">{!!trans('form.label.priority')!!}:</label>
                        <select name="priority" id="priority" class="form-control">
                          <option value="Alta">{!!trans('form.label.high')!!}</option>
                          <option value="Media">{!!trans('form.label.half')!!}</option>
                          <option value="Baja">{!!trans('form.label.low')!!}</option>
                         </select>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{!!trans('form.label.close')!!}</button>
                    <button type="submit" class="btn btn-primary">{!!trans('form.label.submit')!!}</button>
                  </div>
                </div>
              </div>
            </div>
<!---------------------------------------->
{!! Form::close()!!}

{!! Form::open(['route' => 'process/updatestatus', 'method' => 'post', 'validate']) !!}

<!-- MODAL PARA EL CAMBIO DE STATUS ---->
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel">
               {!! Form::hidden('prostatid',null,['id'=>'prostatid']) !!}
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">{!!trans('form.label.statuschange')!!}</h4>
                  </div>
                  <div class="modal-body">
                    
                      <div class="form-group">
                        <label for="newstatus" class="control-label">{!!trans('form.label.status')!!}:</label>
                        {!! Form::select('newstatus', $status,null,['class' => 'form-control']) !!}
                      
                      </div>
                      <div class="form-group">
                        <label for="newstatus" class="control-label">{!!trans('form.label.observation')!!}:</label>
                        <textarea name="statusob" id="statusob" required="required" class="form-control"></textarea>
                      
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{!!trans('form.label.close')!!}</button>
                    <button type="submit" class="btn btn-primary">{!!trans('form.label.submit')!!}</button>
                  </div>
                </div>
              </div>
            </div>
<!---------------------------------------->
{!! Form::close()!!}   
