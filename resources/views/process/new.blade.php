@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/css/camera.css') !!}
    {!! Html::style('video/css/main.css') !!}
    {!! Html::style('assets/clockpicker/dist/bootstrap-clockpicker.css') !!}
    {!! Html::style('assets/clockpicker/dist/bootstrap-clockpicker.min.css') !!}
    {!! Html::style('assets/clockpicker/dist/jquery-clockpicker.css') !!}
    {!! Html::style('assets/clockpicker/dist/jquery-clockpicker.min.css') !!}

@endsection

@section('content')
{!! Form::open(['route' => $route, 'method' => 'post', 'validate']) !!}

 {!! Form::hidden('process_id', $process->processid) !!}
 {!! Form::hidden('processnumber', $process->process_number) !!}
 {!! Form::hidden('visits_id', @$process->visitsid) !!}
 {!! Form::hidden('editvisit', @$editvisit,['id'=>'editvisit']) !!}
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.visitinfo')!!}</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <!-- <h2>Visit <small>Add information about the visit</small></h2> -->
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <div class="col-md-4 col-sm-12 col-xs-12 profile_left">
                      <h3>{!!trans('form.label.corpname')!!}: {{$process->name}}</h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> {{$process->address}}
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> {{$process->applicant_name}}
                        </li>

                        <li >
                          <i class="fa fa-external-link user-profile-icon"> {{ $process->email}}</i>
                          
                        </li>
                        <li class="m-top-xs">
                          <i class="glyphicon glyphicon-earphone"> <label>{{ $process->phone}}</label></i>
                         
                        </li>
                      </ul>
                   
                    <!-- <div class="x_title">
                            <div class="clearfix"></div>
                            </div> -->
                      <strong><h4>{!!trans('form.label.projectdescription')!!}:</h4></strong>
                      <p>{{ $process->description}}</p>

                                   
                           
                    
                    {!! Form::label('takepiclabel', trans('form.label.pictures'),['class' => 'control-label col-xs-6 col-lg-6']) !!}
                    <div class="form-group col-sm-12 col-md-12 col-lg-12" id="picstaken">
                            {!! Form::hidden('count',($countake!=0 ? $countake : 0 ),['id'=>'count']) !!}
                            {!! Form::hidden('countini',($countake!=0 ? $countake : 0 ),['id'=>'countini']) !!}
                            @if($editvisit)
                            
                                     @if(@$picturestake)
                                     
                                          @foreach($picturestake as $foto)

                                          <div class="col-sm-4 col-md-4 col-lg-4" id="pic{{$foto->id}}" >
                                              <div class="thumbnail">
                                                  <img src="{{url('images/equipment/').'/'.$process->processid.'/'.@$process->visitsid.'/'.$foto->file}}" alt="{{$foto->description}}">
                                                  <div class="caption">
                                                      <p>{{$foto->description}}</p>
                                                  </div>
                                                  <a href="#"  id="deletepics" data-file="{{'/'.$process->processid.'/'.@$process->visitsid.'/'.$foto->file}}" data-id="{{$foto->id}}" data-visitid="{{$process->visitsid}}"><i class="fa fa-trash"></i></a>
                                              </div>
                                              
                                          </div>
                                          
                                          @endforeach
                                      @else
                                      <div class="alert alert-warning">
                                        <strong> No hay fotos que mostrar.</strong>
                                      </div>
                                        
                                      @endif 
                            @endif
                           
                               
                    </div>        
                     <div class="col-sm-12 col-md-12 col-lg-12" align="center">
                                <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!!trans('form.label.back')!!}!</a>
                                 <input type="button" class="btn btn-warning" data-toggle="modal" data-target="#cameraModal" value="{!!trans('form.label.takepic')!!}" id="takepics">
                                 <input type="submit" class="btn btn-success save" value="{!!trans('form.label.submit')!!}">
                                 
                                   
                    </div>      
                    </div>

                    
                    <div class="col-md-8 col-sm-12 col-xs-12">
                         <div class="form-inline" align="center" style="padding-top:10px; padding-bottom:10px;">
                                @if(!$editvisit)

                                    <div class="input-group clockentry" data-placement="bottom" data-align="top" data-autoclose="true" >
                                    <span class="input-group-addon" style="background-color: #6cd138;">{!!trans('form.label.entrance')!!}: </span>
                                    <input type="text" class="form-control" value="00:00" placeholder="hora de Entrada" name="entrada" id="entrada" required="required">
                                    
                                    </div>
                                @else
                                     {!! Form::hidden('jobsold', @$jobtold,['id'=>'jobsold']) !!} 
                                     {!! Form::hidden('generalinfo', @$generalold,['id'=>'generalinfo']) !!} 
                                     {!! Form::hidden('generaltype', @$generaltypeold,['id'=>'generaltype']) !!} 
                                    <div class="input-group">
                                    <span class="input-group-addon" style="background-color: #6cd138;">{!!trans('form.label.entrance')!!}: </span>
                                    <input type="text" class="form-control" value="{{$process->entrance_hr}}" name="entrada" readonly="readonly">
                                    </div>
                                 @endif

                                      <div class="input-group clockentry" data-placement="bottom" data-align="top" data-autoclose="true" >
                                      <span class="input-group-addon" style="background-color: #e37b15;"> {!!trans('form.label.exit')!!}: </span>
                                      <input type="text" class="form-control" value="00:00" name="salida" id="salida" required="required">
                                      </div>
                                       <!-- <a  class="fa fa-pencil fa-mid" title='Editar info' href="{{ route('visits.edit',['id' => @$visit->id] )}}"></a> -->
                                
                            </div>
                            <div class="x_title">
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group " >
                              {!! Form::label('jobtypelabel', trans('form.label.jobtype'),['class' => 'control-label col-xs-6 col-md-6 col-lg-2']) !!}
                                  <div class="col-xs-12 visit-info"> 
                                      <div class="checkbox">    
                                        @foreach($jobt as $job)
                                        
                                        <div class="col-xs-12 col-sm-4 col-lg-3"> 
                                        <label class="checkbox-inline">
                                             
                                          <input type="checkbox" name="job[{{ $job->id }}]" id="job{{ $job->id }}" value="{!! $job->id !!}" class="jobtype" />{!!$job->name!!}

                                         </label>  
                                         </div>               
                                        @endforeach
                                      </div>  
                                  </div>          
                            </div>
                            <div class="x_title">
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group ">
                              {!! Form::label('generallabel',trans('form.label.generalinfo'),['class' => 'control-label col-xs-6 col-md-6 col-lg-2']) !!}
                                  <div class="col-xs-12 visit-info"> 
                                      <div class="checkbox"> 

                                        @foreach($general as $gen)
                                        <div class="col-xs-12 col-sm-4 col-lg-3">  
                                          <label class="checkbox-inline">
                                           
                                                <input type="checkbox" name="gen[{{ $gen->id }}]" id="gen{{ $gen->id }}" value="{!! $gen->id !!}" class="generalinfo" />{!!$gen->name!!}
                                          </label>  
                                         </div>               
                                        @endforeach
                                      </div>  
                                  </div>          
                            </div>
                            <div class="x_title">
                            <div class="clearfix"></div>
                            </div> 
                            <div class="form-group ">
                            <label class="control-label col-xs-2">{!!trans('form.label.generaldescription')!!}</label>
                                <div class="col-xs-12 visit-info">
                                      
                                        @foreach($gentype as $gt) <!--- recorrido de los tipos --> 
                                          <div class="col-xs-12">
                                          <label class="control-label"> {{$gt->type}} </label>
                                              
                                                @foreach($generaltype as $gent) <!--- recorrido de los names -->
                                                    <div class="checkbox">
                                                    
                                                    @if($gt->type==$gent->type)
                                                    <div class="col-xs-12 col-sm-4 col-md-4"> 
                                                      
                                                      <label class="checkbox-inline">
                                                      
                                                       <input type="checkbox" name="gendes[{{ $gent->id }}]" id="gendes{{ $gent->id }}" value="{!! $gent->id !!}" class="generaldescrip" />{!!$gent->name!!}
                                                        
                                                      </label> 
                                                         
                                                    </div>
                                                    @endif
                                                    
                                                    </div>
                                                @endforeach
                                            
                                          </div>   
                                        @endforeach
                                </div>
                               <div class="x_title">
                            <div class="clearfix"></div>
                            </div> 
                              </div>  
                                <div class="form-group ">
                                  {!! Form::label('requestlabel', trans('form.label.requestclient'),['class' => 'control-label col-xs-6 col-md-6 col-lg-2']) !!}
                                  <div class="col-xs-9 visit-info"> 
                                        <textarea class="form-control" rows="3" name="requestclient" id="requestclient" placeholder="Solicitudes de trabajos que debe hacer el cliente para poder continuar el proyecto" ></textarea>
                                  </div>          
                                </div>
                                <div class="x_title">
                            <div class="clearfix"></div>
                            </div>
                                <div class="form-group">
                                  {!! Form::label('observationlabel', trans('form.label.observation'),['class' => 'control-label col-xs-6 col-md-6 col-lg-2']) !!}
                                  <div class="col-xs-9 visit-info"> 
                                        <textarea class="form-control" rows="3" name="jobobservation" id="jobobservation" required="required">{{@$process->visitdescrp}}</textarea>
                                  </div>          
                                </div>
                                

                          </div>
                          
{!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        <!-- /page content -->
            

@include('process.pictures_modal')

@endsection 
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- Clockpicker -->
    {!! Html::script('assets/clockpicker/dist/clockpicker.js') !!}
    {!! Html::script('assets/clockpicker/dist/bootstrap-clockpicker.js') !!}
    {!! Html::script('assets/clockpicker/dist/bootstrap-clockpicker.min.js') !!}
    {!! Html::script('assets/clockpicker/dist/jquery-clockpicker.js') !!}
    {!! Html::script('assets/clockpicker/dist/jquery-clockpicker.min.js') !!}
    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}
    
    <!-- Video Scripts -->
    {!! Html::script('video/js/adapter.js') !!}
      {!! Html::script('video/js/common.js') !!}
   {!! Html::script('video/js/main.js') !!}
   {!! Html::script('video/js/lib/ga.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

<script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!};
$(document).ready(function(e){
   var editvisit = $('#editvisit').val(),
        width = 1280,
        height = 720;

$('#entrada').focusout(function(e){
  var fotos=$('#count').val();
      
     if(fotos<=0){
        $.confirm({
              title: '<strong>Atencion!!</strong>',
              content: 'Debe Tomar las Fotos del lugar antes de finalizar la visita',
              confirmButton:false,
              cancelButton:'Ok',
              cancel:function(){
                 
              }
              
          });
      }


});
$('#salida').focusout(function(e){
  var entrada = $('#entrada').val(),
      salida=$(this).val();
      fotos=$('#count').val();
      
      //console.log(entrada+'/'+salida);
      if(salida < entrada){
         $.confirm({
              title: '<strong>Atencion!!</strong>',
              content: 'La Hora de Salida no puede ser menor a la Hora de entrada',
              confirmButton:false,
              cancelButton:'Ok',
              cancel:function(){
                 
              }
              
          });
      }

      if(fotos<=0){
        $.confirm({
              title: '<strong>Atencion!!</strong>',
              content: 'Debe Tomar las Fotos del lugar antes de finalizar la visita',
              confirmButton:false,
              cancelButton:'Ok',
              cancel:function(){
                 
              }
              
          });
      }


});
//check validations
$(".save").click(function(e) {
            var fotos=$('#count').val();
            var checkedjob=false,checkedgeninfo=false,checkedgeneral=false, titlecheck='',checked=true;
            $('.jobtype:checked').each(function() {

                    checkedjob=true;
                    
            });
            
            $('.generalinfo:checked').each(function() {

                    checkedgeninfo=true;
                    
            });
            
            $('.generaldescrip:checked').each(function() {

                    checkedgeneral=true;
                    
            });
            if(!checkedjob){titlecheck+='Tipo de Trabajo ';checked=false;}
            if(!checkedgeninfo){titlecheck+='Informacion General ';checked=false;}
            if(!checkedgeneral){titlecheck+='Descripcion General ';checked=false;} 
            //alert(checked);
             if(!checked){
              
                  $.alert({
                        icon: 'fa fa-warning',
                        title: 'Atention!',
                        content: 'You must check a value for: "'+titlecheck+'"!!'
                        
                    });
                  return checked;
            }else{
                //alert($('#salida').val());
                
                if($('#entrada').val()==''){
                  alert('You have to assign an Entrance Hour!!');
                  return false;
                }else if($('#jobobservation').val()==''){
                  alert('You have to write an observation of the visit!!');
                  return false;
                }else if($('#salida').val()=='00:00'){
                  checked=confirm('Are you sure you want to save without exit hour assigned? the visit will not be Done without the exit hour');
                  return checked;
                }else if(fotos<=0){
                  $.confirm({
                        title: '<strong>Atencion!!</strong>',
                        content: 'Debe Tomar las Fotos del lugar antes de finalizar la visita',
                        confirmButton:false,
                        cancelButton:'Ok',
                        cancel:function(){
                           
                        }
                        
                    });
                  return false;
                }else{
                  
                  
                  return checked;
                }
              
            }
            
            
            
        }); 

// Edit information        
  if(editvisit){ 
    var jobsold = $.parseJSON($('#jobsold').val());
    var generalinfo = $.parseJSON($('#generalinfo').val());
    var generaltype = $.parseJSON($('#generaltype').val());

    //alert($('#job2').val());

    var jobarray = $.map( jobsold, function( value) {
                      
                      $('#job'+value['id']).attr("checked", true);
                      //console.log( "jobs: " + $('#job'+value['id']).attr("value"));

                      });

    var generalinfoarray = $.map( generalinfo, function( value) {
                      //console.log( "jobs: " + value['id']);
                      $('#gen'+value['id']).attr('checked',true);
                      });

    var generaltypearray = $.map( generaltype, function( value) {
                      //console.log( "jobs: " + value['id']);
                      $('#gendes'+value['id']).attr('checked',true);
                      });
    
   } 

    $('#cameraModal').on('shown.bs.modal', function (e) { 
      // var streaming = false,
      //     video        = document.querySelector('#video'),
      //     cover        = document.querySelector('#cover'),
      //     canvas       = document.querySelector('#canvas'),
      //     photo        = document.querySelector('#photo'),
      //     startbutton  = document.querySelector('#startbutton'),
      //     savebutton   = document.querySelector('#savebutton');
         
         
      //     navigator.getMedia = ( navigator.getUserMedia || 
      //                            navigator.webkitGetUserMedia ||
      //                            navigator.mozGetUserMedia ||
      //                            navigator.msGetUserMedia);
          

      //     navigator.getMedia({ video: true, audio: false},function(stream) {
              
      //               if (navigator.mozGetUserMedia) { 
      //                 video.mozSrcObject = stream;
      //               } else {
      //                 var vendorURL = window.URL || window.webkitURL;
      //                 video.src = vendorURL ? vendorURL.createObjectURL(stream) : stream;
      //               }
      //             video.play();
      //           },

      //           function(err) {
      //             console.log("An error occured! " + err);
      //           }
      //       );

      //     video.addEventListener('canplay', function(ev){
      //       //console.log("streaming canplay"+streaming);
      //       if (!streaming) {
      //         height = video.videoHeight / (video.videoWidth/width);
      //         video.setAttribute('width', width);
      //         video.setAttribute('height', height);
      //         canvas.setAttribute('width', width);
      //         canvas.setAttribute('height', height);
      //         streaming = true;
      //       }
      //     }, false);

       }); //camera modal show

          function takepicture() {

            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(video, 0, 0, width, height);
            var data = canvas.toDataURL('image/png');
            photo.setAttribute('src', data);
          }

          startbutton.addEventListener('click', function(ev){
              takepicture();
            ev.preventDefault();
          }, false);

          

          // Upload image to sever 
          savebutton.addEventListener("click", function(){
          var dataUrl = canvas.toDataURL();
          var descript = $('#descripcionfoto').val();
          var count = $('#count').val();
              count =  parseInt(count);
              count +=1;
            

            var photo = "<input type='hidden' name='photo["+count+"]' value='"+dataUrl+"' class='form-inline'><input type='hidden' name='descript["+count+"]' value='"+descript+"' class='form-control'><input type='hidden' name='title["+count+"]' value='photo"+count+".jpg' class='form-control'>"; 
            var phototittle = '<div class="col-sm-4 col-md-4 col-lg-4"><div class="thumbnail"><img src="'+dataUrl+'" alt="'+descript+'"><div class="caption"><p>'+descript+'</p></div></div></div>';
            
            $("#picstaken").append(photo,phototittle);     // Append new elements
            
            $('#count').val(count);
            $('#photo').removeAttr('src');
            $('#descripcionfoto').val('');
            $('#cameraModal').modal('toggle');


          });//savebutton

//Eliminar foto
$('#deletepics').click(function(e) {
            var file= $(this).data("file"),
                picid=$(this).data("id"),
                visitid=$(this).data("visitid");
            console.log(file+' / '+picid +' / '+visitid);

                        $.confirm({
                            title: '<strong>Photo Delete!!</strong>',
                            content: 'Are you sure about deleting this photo?',
                            confirmButton:'Yes',
                            cancelButton:'No',
                            confirm: function(){
                               $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: APP_URL+'/visits/deletephoto', // This is the url we gave in the route
                                        data: {'file' : file,'picid':picid,'visitid':visitid}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                           console.log('pic deleted:'+response.deleted)
                                           $('#pic'+picid).remove(); 
                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });
    });//document ready


</script>

    
@endsection