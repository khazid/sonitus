@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!! trans('form.label.project') !!}  <small>{!! trans('form.label.detail') !!}</small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for..."> -->
                    <span class="input-group-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!! trans('form.label.back') !!}!</a>
                      <!-- <button class="btn btn-default" type="button">Go!</button> -->
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!! trans('form.label.projectdetail') !!}</h2>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <ul class="stats-overview">
                        <li>
                          <span class="name"> {!! trans('form.label.visitcant') !!} </span>
                          <span class="value text-success"> {{@$cantvisit[0]->total}} </span>
                        </li>
                        <li>
                          <span class="name">{!! trans('form.label.days') !!}</span>
                          <span class="value text-success"> {{@$days[0]->total}} </span>
                        </li>
                        <li class="hidden-phone">
                          <span class="name"> {!! trans('form.label.duration') !!} </span>
                          <span class="value text-success"> {{@$counter[0]->total}} Hours</span>
                        </li>
                      </ul>
                      <br />

                      <!-- <div id="mainb" style="height:350px;"></div>
                      <div class="x_title">
                          <div class="clearfix"></div>
                        </div> -->
                      <div>

                        <h4>{!! trans('form.label.status') !!}</h4>
                        @if(count($status)>0)
                        <ul class="messages">
                        @foreach($status as $v)
                          <li>
                            <div class="message_wrapper">
                              <h4 class="heading"><a href="{{ route('visits.show',['id' => $v->id] )}}">{{date("d/m/Y H:i",strtotime($v->created_at))}}</a>
                              {!! trans('form.label.staff') !!}: {{$v->name}}</h4>
                              <blockquote class="message">{{$v->observation}}</blockquote>
                              
                              
                            </div>
                          </li>
                         @endforeach 
                        </ul>
                        <!-- end of user messages -->
                        @endif

                      </div>


                    </div>

                    <!-- start project-detail sidebar -->
                    <div class="col-md-3 col-sm-3 col-xs-12">

                      <section class="panel">

                        <div class="x_title">
                          <h2>{!! trans('form.label.projectdescription') !!}</h2>

                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                        <p class="title"><b>{!! trans('form.label.inidate') !!}:</b> {{date("j M Y",strtotime($process->ini_date))}} /
                        <b>{!! trans('form.label.enddate') !!}:</b> {{($process->end_date!=null ? date("j M Y",strtotime($process->end_date)) : '-')}}</p>
                          <div><span class="badge bg-gray">{!! trans('form.label.status') !!}: {{$process->status}}</span></div>
                          <h4 class="green"> {!! trans('form.label.project') !!}: {{$process->process_number}}</h4>
                          
                          <p>{{$process->description}}</p>
                          <br />

                          <div class="project_detail">

                            <p class="title">{!! trans('form.label.corpname') !!}: {{$process->name}}</p>
                            <p class="title">{!! trans('form.label.rif') !!}: {{$process->rif}}</p>
                            <p class="title">{!! trans('form.label.contactname') !!}: {{$process->applicant_name}}</p>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-envelope"></i> <b>{!! trans('form.label.email') !!}:</b> {{$process->email}} </li>
                                <li><i class="fa fa-building"></i> <b>{!! trans('form.label.address') !!}:</b> {{$process->address}} </li>
                                <li><i class="fa fa-phone"></i> <b>{!! trans('form.label.phone') !!} #:</b> {{$process->phone}} </li>
                            </ul>
                            <p class="title">{!! trans('form.label.proleader') !!}</p>
                            <p>{{$process->staff}}</p>
                          

                          <br />
                          <p class="title">{!! trans('form.label.requestclient') !!}</p>
                          <ul class="list-unstyled project_files">
                           @foreach($requestclient as $r)

                            <li><span class="badge bg-gray">{{$r->status}}</span> {{date("D M j Y",strtotime($r->date))}}
                                <p>{{$r->request}}</p>
                                
                            </li>
                           @endforeach 

                          </ul>
                          <br />

                          
                        </div>
                        </div>
                      </section>

                    </div>
                    <!-- end project-detail sidebar -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->  
       
@endsection
@section('script') 
    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- ECharts -->
    {!! Html::script('/vendors/echarts/dist/echarts.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}  
 

    
      @endsection