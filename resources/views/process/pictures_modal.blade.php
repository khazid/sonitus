

<!------------- TOMAR FOTOS DE LOS EQUIPOS------- -->
<div class="modal fade" id="cameraModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
              <div class="modal-dialog modal-lg" >
                <div class="modal-content" >
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="cameraModalLabel">{!!trans('form.label.takepic')!!}</h4>
                  </div>
                  <div class="modal-body">
                        

    <div class="select">
      <label for="videoSource">Video source: </label><select id="videoSource"></select>
    </div>
                  {!! Form::hidden('visit_id') !!}
                      <div class="form-group">
                        <label for="recipient-name" class="control-label">{!!trans('form.label.description')!!}:</label>
                        {!! Form::text('descripcionfoto', null, ['class' => 'form-control' ,'id'=>'descripcionfoto']) !!}
                      </div>
                      <div class="col-md-6" >
                        <video id="video"></video>
                      </div>
                      <div class="col-md-6" >
                        <img src="" id="photo" alt="photo">        
                        <canvas id="canvas"></canvas>
                      </div>
                    

                  </div>
                  <div class="modal-footer">
                    <a href="#" id="startbutton" class="btn btn-primary">{!!trans('form.label.capture')!!}</a>
                    <a href="#" id="savebutton" class="btn btn-success">{!!trans('form.label.save')!!}</a>
                  </div>
                </div>
              </div>
</div>


