@extends('app/navin')
@section('content')
<div id="page-wrapper">
    <div class="row">
                  <div class="col-lg-12">
                      <h1 class="page-header">{!!trans('form.label.menuedit')!!}</h1>
                  </div>
                  <!-- /.col-lg-12 -->
    </div>
	<div class="row">
    <div class="col-md-10 col-md-offset-1">
      {!! Form::model($menu,['route' => 'menu.update', 'method' => 'put', 'validate']) !!}
            
                {!! Form::hidden('id', $menu->id) !!}
            
                <div class="form-group">
                      {!! Form::label('full_name', trans('form.label.name')) !!}
                      {!! Form::text('name', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                      {!! Form::label('newposition', trans('form.label.position')) !!}
                      {!! Form::text('position', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                      {!! Form::label('newicon', trans('form.label.icon')) !!}
                      {!! Form::text('icon', null, ['class' => 'form-control' , 'required' => 'required','placeholder' => 'nombre del icono bootstrap']) !!}
                  </div>
            
                <div class="form-group">
                      {!! Form::submit(trans('form.label.submit'), ['class' => 'btn btn-success ' ] ) !!}
                      <a href="{{ URL::previous() }}" class="btn btn-primary" role="button">{!! trans('form.label.cancel') !!}</a>
                  </div>
            {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection