@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}
  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.submenu')!!} <small>{!!trans('form.label.submenusmall')!!}</small></h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     {!! Form::open(['route' => 'perm.store', 'method' => 'post', 'validate', 'class' => 'form-inline']) !!}
                                <div class="form-group">
                                      <label for="newperm">{!!trans('form.label.newsubmenu')!!}</label>
                                      {!! Form::text('name', null, ['class' => 'form-control' , 'required' => 'required', 'placeholder'=>'Nombre del permiso']) !!}
                                      {!! Form::text('description', null, ['class' => 'form-control' , 'required' => 'required', 'placeholder'=>'Descripcion']) !!}
                                      {!! Form::text('slug', null, ['class' => 'form-control' , 'required' => 'required', 'placeholder'=>'ruta del controlador']) !!}
                                      {!! Form::select('menu_id', $option,null,['class' => 'form-control']) !!}
                                      
                                </div>
                                    {!! Form::submit('Crear', ['class' => 'btn btn-success ' ] ) !!}
                                      
                            {!! Form::close() !!}
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>{!! trans('form.table.name') !!}</th>
                                <th>{!! trans('form.table.description') !!}</th>
                                <th>{!! trans('form.table.route') !!}</th>
                                <th>{!! trans('form.table.menuprin') !!}</th>  
                                <th>{!! trans('form.table.action')  !!}</th> 

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($perm as $p)
                            <tr>
                                <td>{{ $p->id }}</td>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->description }}</td>
                                <td>{{ $p->slug }}</td>
                                <td>{{ $p->menuname }}</td>
                                <td>
                                    <a class="fa fa-pencil-square-o fa-2x edit" href="#" id="{{$p->id}}" data-name="{{ $p->name }}" data-slug="{{ $p->slug }}" data-menuname="{{ $p->menuname }}" data-description="{{ $p->description }}" ></a> 
                                    <a class="fa fa-trash fa-2x" href="{{ route('perm/destroy',['id' => $p->id] )}}" ></a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/datatables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}
    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
    <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
    $(document).ready(function() {
      
      $('#datatable-buttons').on( 'click','.edit',function(e){

        var editurl=APP_URL+'/perm/update',
            permid=$(this).attr('id'),
            oldname=$(this).data('name'),
            olddescription=$(this).data('description'),
            oldslug=$(this).data('slug');
            oldmenuname=$(this).data('menuname');
            newmenu={!!$option!!};
            

            ul='';
            
                        
        $.confirm({
          title:'Edit Sub-Menu information',
          content: ' <label name="full_name">Name</label>'+
                      '<input type="text" name="name" id="name" class="form-control name" value="'+oldname+'"></br>'+
                      '<label name="newposition">Description</label>'+
                      '<input type="text" name="description" id="description" class="form-control description" value="'+olddescription+'"></br>'+
                      '<label name="newicon">Route</label>'+
                      '<input type="text" name="slug" id="slug" class="form-control slug" placeholder = "nombre del icono bootstrap" value="'+oldslug+'">'+
                      '<label name="newicon">Belongs to:</label>'+
                      '<input type="text" name="menuname" id="menuname" class="form-control menuname" placeholder = "nombre del icono bootstrap" value="'+oldmenuname+'">'+
                      '<label name="newicon">New menu:</label><br />'+
                      '<select id="newmenu" name="newmenu" class="newmenu">'+
                      '<option value="null"/>Seleccione'+
                      '</select>',
          onOpen: function(){
            // alert('onopen');
            var self = this;
            var count=1;
            var menu=self.$content.find('select.newmenu');
                    $.each(newmenu, function(i, item) {

                      ul +="<option value='"+count+"'>"+newmenu[i]+"</option>";
                      count=count+1;
                      });

                      menu.append(ul);
                    
            },           
          confirm: function(action){
              
              var name=this.$content.find('input.name').val(),
                  description=this.$content.find('input.description').val(),
                  slug=this.$content.find('input.slug').val();
                  newmenu=this.$content.find('select.newmenu').val();

                  
              $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
              $.ajax({
                      type: 'POST', // Type of response and matches what we said in the route
                      url: editurl, // This is the url we gave in the route
                      data: {'id' : permid,'name': name,'description': description,'slug':slug,'newmenu':newmenu}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                         console.log('updated: '+response.update)
                          location.reload();
                      }
                  });
              
             

          }
      });

      });
      
    });
    </script>
@endsection


