@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}

    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.configuration')!!} </h3>
              </div>

             </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Informacion para la cotizacion y facturacion</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                 
                    {!! Form::open(['route' => @$ruta, 'method' => 'post','class'=>"form-horizontal form-label-left",'data-parsley-validate','id'=>'form-validation','files' => true]) !!}
                      <input type="hidden" name="idbilling" value="{{@$config->id}}">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">{!!trans('form.label.month_salary')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="month_salary" class="form-control col-md-7 col-xs-12 numeric" data-validate-length-range="6" data-validate-words="2" name="month_salary" placeholder="Salario mensual" required="required" type="text" value="{{@$config->month_salary}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">{!!trans('form.label.daily_salary')!!} <span class="required ">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="daily_salary" class="form-control col-md-7 col-xs-12 numeric" data-validate-length-range="6" data-validate-words="2" name="daily_salary" placeholder="Salario Diario" required="required" type="text" value="{{@$config->daily_salary}}" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">{!!trans('form.label.biweekly_salary')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="biweekly_salary" class="form-control col-md-7 col-xs-12 decimal-2-places" data-validate-length-range="6" data-validate-words="2" name="biweekly_salary" placeholder="Salario quincenal" required="required" type="text" value="{{@$config->biweekly_salary}}" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="email">{!!trans('form.label.iva')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="iva" class="form-control col-md-7 col-xs-12 integer" data-validate-length-range="6" data-validate-words="2" name="iva" placeholder="valor porcentaje iva" required="required" type="text" value="{{@$config->iva}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{!!trans('form.label.workday')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="workday" class="form-control col-md-7 col-xs-12 integer" data-validate-length-range="6" data-validate-words="2" name="workday" placeholder="Cantidad de horas" required="required" type="text" value="{{@$config->workday}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="initialdate">{!!trans('form.label.night_hour_start')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="night_hour_start" class="form-control col-md-7 col-xs-12" data-validate-length-range="5" data-validate-words="2" name="night_hour_start" placeholder="24 hour format Exp. 10:30" required="required" type="text" value="{{@$config->night_hour_start}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">{!!trans('form.label.night_hour_end')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="night_hour_end" class="form-control col-md-7 col-xs-12" data-validate-length-range="5" data-validate-words="2" name="night_hour_end" placeholder="24 hour format Exp. 10:30" required="required" type="text" value="{{@$config->night_hour_end}}">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">{!!trans('form.label.extra_hour_start')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       <input id="extra_hour_start" class="form-control col-md-7 col-xs-12" data-validate-length-range="5" data-validate-words="2" name="extra_hour_start" placeholder="24 hour format Exp. 10:30" required="required" type="text" value="{{@$config->extra_hour_start}}">
                         
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">{!!trans('form.label.extra_hour_end')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       <input id="extra_hour_end" class="form-control col-md-7 col-xs-12" data-validate-length-range="5" data-validate-words="2" name="extra_hour_end" placeholder="24 hour format Exp. 10:30" required="required" type="text" value="{{@$config->extra_hour_end}}">
                         
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          
                           <a href="{{ URL::previous() }}" class="btn btn-primary">{!!trans('form.label.cancel')!!}</a>

                          <button id="send" type="submit" class="btn btn-success">{!!trans('form.label.submit')!!}</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
@section('script')

   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    {!! Html::script('/assets/js/jquery.numeric.min.js') !!}
    {!! Html::script('/assets/js/jquery.numeric.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
  
    <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
    <!-- Switchery -->
    {!! Html::script('/vendors/switchery/dist/switchery.min.js') !!}
    <!-- Parsley -->
    {!! Html::script('/vendors/parsleyjs/dist/parsley.min.js') !!}
    <!-- jQuery Tags Input -->
    {!! Html::script('/vendors/jquery.tagsinput/src/jquery.tagsinput.js') !!}
        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}
    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}


    <script>

function format2(n) {
      n=parseFloat(n);
      
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
};
    <!-- Parsley -->
      $(document).ready(function() {
          $(".numeric").numeric();

          $('#month_salary').focusout(function(){
          var valor=$(this).val();
            valorformat= format2(valor);
            $(this).val(valorformat);

            var daily=valor / 30;
            dailyformat=format2(daily);
            $('#daily_salary').val(dailyformat);

            var biweekly=valor / 2;
            biweeklyformat=format2(biweekly);
            $('#biweekly_salary').val(biweeklyformat);
        });

         
//----------------------------------------------------------
        $.listen('parsley:field:validate', function() {
          validateFront();
        });
        $('#form-validation .btn').on('click', function() {
          $('#form-validation').parsley().validate();
          validateFront();
        });
        var validateFront = function() {
          if (true === $('#form-validation').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
          } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
          }
        };
      });

      try {
        hljs.initHighlightingOnLoad();
      } catch (err) {}

     
      
    </script>
    <!-- /Parsley -->

    
@endsection