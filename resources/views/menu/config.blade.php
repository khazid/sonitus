@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}
  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.configuration')!!}</h3>

              </div>
              <div class="x_content">
                <p>A) Configuracion Utilizada para la creacion de Cotizaciones.<br/> B) El sistema creara las nuevas cotizaciones a partir de la ultima Configuracion generada.<br/> C) Las cotizaciones anteriores se mantendran con la configuracion que estaba Activa para el momento de su creacion.<br/> D) Puede verificar la configuracion utilizada segun el ID CONFIG indicado en la lista de Cotizaciones, que correspondera al ID de esta Lista. </p>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <a href="{{ route('config.create') }}" class="btn btn-primary">{!!trans('form.label.new')!!}</a>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th width="10%">{{ trans('form.table.date') }}</th>
                          <th>{{ trans('form.label.daily_salary') }}</th>
                          <th>{{ trans('form.label.month_salary') }}</th>
                          <th>{{ trans('form.label.biweekly_salary') }}</th>
                          <th>{{ trans('form.label.iva') }}</th>
                          <th>{{ trans('form.label.workday') }}</th>
                          <th>{{ trans('form.label.night_hour_start') }}</th>
                          <th>{{ trans('form.label.night_hour_end') }}</th>
                          <th>{{ trans('form.label.extra_hour_start') }}</th>
                          <th>{{ trans('form.label.extra_hour_end') }}</th>
                          <th>{{ trans('form.table.action') }}</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($config as $c)
                          <tr>
                              <td>{{ $c->id }}</td>
                              <td>{{ date('d-m-Y',strtotime($c->date)) }}</td>
                              <td>{{ number_format($c->daily_salary,2,',','.') }}</td>
                              <td>{{ number_format($c->month_salary,2,',','.') }}</td>
                              <td>{{ number_format($c->biweekly_salary,2,',','.') }} </td>
                              <td>{{ $c->iva }} </td>
                              <td>{{ $c->workday }} </td>
                              <td>{{ $c->night_hour_start }} </td>
                              <td>{{ $c->night_hour_end }} </td>
                              <td>{{ $c->extra_hour_start }} </td>
                              <td>{{ $c->extra_hour_end }} </td>
                              <td>
                                  <a class="fa fa-pencil-square-o fa-2x " href="{{ route('config.edit',['id' => $c->id] )}}" id="{{$c->id}}" ></a> 
                                  <a class="fa fa-trash fa-2x delete" href="#" data-id="{{$c->id}}" ></a>
                              </td>

                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}
    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->

    <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
    
    $('#datatable-buttons').on('click','.delete',function(e) {
            var deleteurl='config/destroyconfig', 
                id=$(this).data('id');

           
            $.confirm({
                title: '<strong>Delete Configuration!!</strong>',
                content: 'Esta seguro de querer eliminar esta configuración?',
                confirmButton:'Yes',
                cancelButton:'No',
                confirm: function(){
                   $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                    $.ajax({
                            type: 'POST', // Type of response and matches what we said in the route
                            url: deleteurl, // This is the url we gave in the route
                            data: {'id' : id}, // a JSON object to send back
                            success: function(response){ // What to do if we succeed
                               console.log('deleted :',response.destroy)
                                $.confirm({
                                              title: '<strong>Eliminación de Configuración!!</strong>',
                                              content: response.destroy,
                                              cancelButton:false,
                                              confirm:function(){
                                                 location.reload();
                                              }
                                            });
                            }
                        });
                },
                cancel:function(){
                   
                }
                
            });
             
            
        });
     
      
   
    </script>
@endsection

                     