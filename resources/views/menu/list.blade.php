@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}
  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.menu')!!}<small>{!!trans('form.label.menusmall')!!}</small></h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    {!! Form::open(['route' => 'menu.store', 'method' => 'post', 'validate', 'class' => 'form-inline']) !!}
                                <div class="form-group">
                                  {!! Form::label('newoption', trans('form.label.newoption')) !!}
                                  {!! Form::text('name', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                                  {!! Form::label('newposition', trans('form.label.position')) !!}
                                  {!! Form::text('position', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                                  {!! Form::label('newicon', trans('form.label.icon')) !!}
                                  {!! Form::text('icon', null, ['class' => 'form-control' , 'required' => 'required','placeholder' => 'nombre del icono bootstrap']) !!}
                                </div>
                                    {!! Form::submit(trans('form.label.submit'), ['class' => 'btn btn-success ' ] ) !!}
                                      
                          {!! Form::close() !!}
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>{{ trans('form.table.name') }}</th>
                          <th>{{ trans('form.table.menuprin') }}</th>
                          <th>{{ trans('form.table.icon') }}</th>
                          <th>{{ trans('form.table.action') }}</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($menus as $menu)
                          <tr>
                              <td>{{ $menu->id }}</td>
                              <td>{{ $menu->name }}</td>
                              <td>{{ $menu->position }}</td>
                              <td>{{ $menu->icon }} <i class="{{ $menu->icon }}"></i></td>
                              <td>
                                  <a class="fa fa-pencil-square-o fa-2x edit" href="#" id="{{$menu->id}}" data-name="{{ $menu->name }}" data-position="{{ $menu->position }}" data-icon="{{ $menu->icon }}"></a> 
                                  <a class="fa fa-trash fa-2x" href="{{ route('menu/destroy',['id' => $menu->id] )}}" ></a>
                              </td>

                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}
    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->

    <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
    $(document).ready(function() {
      $('#datatable-buttons').on('click','.edit',function(e){

        var editurl=APP_URL+'/menu/updatemenu',
            menuid=$(this).attr('id'),
            oldname=$(this).data('name'),
            oldposition=$(this).data('position'),
            oldicon=$(this).data('icon');

        $.confirm({
          title:'Edit menu information',
          content: ' <label name="full_name">Nombre</label>'+
                      '<input type="text" name="name" id="name" class="form-control name" value="'+oldname+'"></br>'+
                      '<label name="newposition">newposition</label>'+
                      '<input type="text" name="position" id="position" class="form-control position" value="'+oldposition+'"></br>'+
                      '<label name="newicon">newposition</label>'+
                      '<input type="text" name="icon" id="icon" class="form-control icon" placeholder = "nombre del icono bootstrap" value="'+oldicon+'">',
          confirm: function(action){
              // action is either 'confirm', 'cancel' or 'close'
              var name=this.$content.find('input.name').val(),
                  position=this.$content.find('input.position').val(),
                  icon=this.$content.find('input.icon').val();
              $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
              $.ajax({
                      type: 'POST', // Type of response and matches what we said in the route
                      url: editurl, // This is the url we gave in the route
                      data: {'id' : menuid,'name': name,'position': position,'icon':icon}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                         console.log('updated: '+response.update)
                          location.reload();
                      }
                  });
              
             

          }
      });

      });
      
    });
    </script>
@endsection

                     