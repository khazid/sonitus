@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Role</h3>
              </div>

             </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.rol')!!} <small>{!!trans('form.label.editrol')!!}</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
      {!! Form::model($rol,['route' => 'rol.update', 'method' => 'put', 'novalidate']) !!}
            
                {!! Form::hidden('id', $rol->id) !!}
            
                  <div class="form-group">
                      {!! Form::label('full_name', trans('form.label.name')) !!}
                      {!! Form::text('name', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                      {!! Form::label('slug', trans('form.label.type')) !!}
                      {!! Form::text('slug', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                      {!! Form::label('descripcion', trans('form.label.description')) !!}
                      {!! Form::text('description', null, ['class' => 'form-control' , 'required' => 'required']) !!}
                  </div>
                  
                  <div class="form-group">
                      {!! Form::submit(trans('form.label.submit'), ['class' => 'btn btn-success ' ] ) !!}
                      <a href="{{ route('rol.index') }}" class="btn btn-primary" role="button">{!!trans('form.label.cancel')!!}</a>
                  </div>
      {!! Form::close() !!}
     </div> 
     <div class="col-sm-12">
     
         <!--Para modificar las opciones del Menu -->
          <div class="row">
            
          {!! Form::model($mrol,['route' => 'menurol.update', 'method' => 'put', 'novalidate']) !!}
                   {!! Form::hidden('id', $rol->id) !!}
            <div class="col-sm-6">    
              <div class="row"> 
                  <div class="page-header">
                    <h3>{!!trans('form.label.menu')!!}</h3>
                  </div>  
                        
                         @foreach($mrol as $mr)
                              <div class="col-sm-5">
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                      <input type='hidden' name="menu[{{ $mr->id }}]" value='m{{ $mr->id }}' />
                                      <input type="checkbox" name="menu[{{ $mr->id }}]" id="opcion" value="{!! $mr->id !!}" {{($mr->active ? "checked='checked'" : "") }} />
                                    </span>

                                      <input type="text" class="form-control" value="{{ $mr->menuname }}">
                                  </div>
                               </div>     
                          @endforeach
                </div>
                 <div class="col-sm-10">
                    {!! Form::submit(trans('form.label.submit'), ['class' => 'btn btn-success ' ] ) !!}
                 </div>
              </div> 
                    
            
          {!! Form::close() !!}
         

          <!--Para modificar los permisos del rol -->
          
          {!! Form::model($perm,['route' => 'permrol.update', 'method' => 'put', 'novalidate']) !!}
                   {!! Form::hidden('id', $rol->id) !!}
                     
              <div class="col-sm-6"> 
                <div class="row">
                  <div class="page-header">
                    <h3>{!!trans('form.label.submenu')!!}</h3>
                  </div>  
          
                       @foreach($perm as $per)

                            <div class="col-sm-5">
                              <div class="input-group">
                                   <span class="input-group-addon">
                                    <input type='hidden' name="permiso[{{ $per->id }}]" value='p{{ $per->id }}' />
                                    <input type="checkbox" name="permiso[{{ $per->id }}]" id="permisos" value="{!! $per->id !!}" {{($per->active ? "checked='checked'" : "") }} />
                                   </span>
                                    <input type="text" class="form-control" value="{!! $per->name !!}"><br/>
                              </div>
                            </div>  

                       @endforeach
                 </div>     
                      <div class="col-sm-10">
                          {!! Form::submit(trans('form.label.submit'), ['class' => 'btn btn-success ' ] ) !!}
                      </div>
              </div>      
          {!! Form::close() !!}
           
         </div>

     </div>
</div>
</div>
</div>
</div>
</div>
<!-- /page content -->
@endsection
@section('script')

   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}

    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}


    
@endsection