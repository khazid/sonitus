@extends('app/navin')
@section('content')
<div id="page-wrapper">
    <div class="row">
                  <div class="col-lg-12">
                      <h1 class="page-header">Editar Sub-Menu "{{$perm[0]->name}}"</h1>
                  </div>
                  <!-- /.col-lg-12 -->
    </div>
  
	<div class="row">
    <div class="col-md-10 col-md-offset-1">
      {!! Form::open(['route' => 'perm.update', 'method' => 'put', 'novalidate']) !!}
            
                {!! Form::hidden('id', $perm[0]->id) !!}
            
                <div class="form-group">
                      {!! Form::label('full_name', 'Nombre') !!}
                      {!! Form::text('name', $perm[0]->name, ['class' => 'form-control' , 'required' => 'required']) !!}
                       {!! Form::label('Description', 'Descripcion') !!}
                      {!! Form::text('description', $perm[0]->description, ['class' => 'form-control' , 'required' => 'required']) !!}
                       {!! Form::label('slug', 'Ruta') !!}
                      {!! Form::text('slug', $perm[0]->slug, ['class' => 'form-control' , 'required' => 'required']) !!}
                       {!! Form::label('menu', 'Menu actual') !!}
                       {!! Form::text('menu', $perm[0]->menuname, ['class' => 'form-control' , 'readonly' => 'readonly']) !!}
                        {!! Form::label('menu', 'Nuevo Menu') !!}
                      {!! Form::select('menu_id', $option,null,['class' => 'form-control']) !!}
                  </div>
            
                <div class="form-group">
                      {!! Form::submit('Enviar', ['class' => 'btn btn-success ' ] ) !!}
                      <a href="{{ URL::previous() }}" class="btn btn-primary" role="button">Volver</a>
                  </div>
            {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection