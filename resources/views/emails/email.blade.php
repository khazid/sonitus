<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<style type="text/css">
body{ margin:0px; padding:0px; width:100%; }    
</style>

  <body >
    <div class="container body">
      <div class="main_container">
        
        <!-- page content -->
        <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                <div style="width:50%;display:inline-block;float:left" align="left">
                <img alt="" src="{{$message->embed(public_path('images/email/LOGOVECTORIZADO3.png'))}}" style="display: block; width: 150px; height: 32px;float:left;margin-top:-8px;border:0;" />
                </div>
                 <br />
                  <br />
                  <div class="x_title">
                          <div align="left" class="x_content">
                          <h2>{{$info['title']}}</h2>
                           <br />
                          <div class="x_content">
                                <div class="x_title">
                                  <div class="clearfix"></div>
                                </div>
                              
                                  <ul class="stats-overview">
                  
                                      @foreach($info['content'] as $content)
                                      <p style="font-size:12px;">{{$content}}</p>
                                      @endforeach

                                      <p style="font-size:12px;">Gracias.</p>
                  
                                    <p style="text-align:right;">
                                      <a href="{{url('/')}}" style="background-image:url('{{$message->embed(public_path('images/email/boton.jpg'))}}');width:153px;line-height:52px;text-align:center;display:block;color:#000000;text-decoration:none;float:right;margin-bottom:10px;">Ingresar</a></a>
                                    </p>
                                  </ul>
                              
                          </div>
                  </div>
              </div>
        </div>
    </div>
  </div>

  </body>
</html>