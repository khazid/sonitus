
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<style type="text/css">
body{ margin:0px; padding:0px; width:100%; }		
</style>
	</head>
	<body>
		<table align="center" bgcolor="#F2F2F2" border="0" cellpadding="0" cellspacing="0" style="padding:0px; margin:0px;" width="100%">
			<tbody>
				<tr>
					<td>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tbody>
								  <tr>
									<td align="left" height="20" valign="top"><img alt="" src="{{$message->embed(public_path('images/email/top-shadow-basica.png'))}}" style="display: block; width: 600px; height: 20px;" /></td>
								  </tr>
	                              <tr>
										<td align="left" height="20" valign="top" bgcolor="#ffffff">
										<img alt="" src="{{$message->embed(public_path('images/email/LOGOVECTORIZADO3.png'))}}" style="display: block; width: 150px; height: 32px;float:left;margin-top:-8px;border:0;" />
										</td>
								  </tr>
	                              <tr>
										<td align="left" valign="top" bgcolor="ffffff" style="padding:18px 20px 10px 20px;color:#3d3d3d;font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:14px;border-top:1px solid #afafaf">
										</td>
								  </tr>
                                
							</tbody>
						</table>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tbody>

								<tr>
									<td align="left" bgcolor="#ffffff" valign="top" style="padding:0 100px;font-family:Arial, Helvetica, sans-serif;font-size:12px;">
									<p style="font-size:20px;font-weight:bold;padding-top:20px;">{{$info['title']}}</p>
									
									@foreach($info['content'] as $content)
									<p style="font-size:12px;">{{$content}}</p>
									@endforeach

									<p style="font-size:12px;">Gracias.</p>
									
                                    <p style="text-align:right;">
                                    	<a href="{{url('/')}}" style="background-image:url('{{$message->embed(public_path('images/email/boton.jpg'))}}');width:153px;line-height:52px;text-align:center;display:block;color:#000000;text-decoration:none;float:right;margin-bottom:10px;">Ingresar</a></a>
                                    </p>
                                    </td>
								</tr>
                            </tbody>
						</table>
                        
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tbody>
								<tr>
									<td align="left" valign="top" bgcolor="ffffff" style="padding:18px 20px 10px 20px;color:#3d3d3d;font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:14px;border-top:1px solid #afafaf">
										<a href="#">
                                      		<img alt="" src="{{$message->embed(public_path('images/email/twitter2.png'))}}" style="display: block; width: 32px; height: 32px;float:right;margin-top:-8px;border:0;" />
                                      	</a>
                                    	<a href="#">
                                    		<img alt="" src="{{$message->embed(public_path('images/email/facebook2.png'))}}" style="display: block; width: 32px; height: 32px;float:right;margin-right:10px;margin-top:-8px;border:0;" />
                                    	</a>										
                                    </td>
							  </tr>
                                <tr>
									<td align="left" height="15" valign="top">
										<img alt="" src="{{$message->embed(public_path('images/email/shadow-basica.png'))}}" style="display: block; width: 600px; height: 15px;" />
									</td>
								</tr>
                                
							</tbody>
						</table>
			      
						<table align="center" border="0" cellpadding="0" cellspacing="0" height="176" width="600">
							<tbody>
								<tr>
									<td style="display:block; padding-bottom:15px; font-size:12px; font-family:Helvetica, Arial, sans-serif; color:#777777;" valign="top">
										
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>