<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_TITLE')}} |</title>

    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    
  <style type="text/css">
    *{
      background-color: white!important;
    }
  </style>  
  </head>

  <body >
    <div class="container body">
      <div class="main_container">
        
        <!-- page content -->
        <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                <div align="left" style="width:50%;display:inline-block;float:left; font-size:8px">
                  <h4>{!!trans('form.label.corpname')!!}: {{$process->name}}</h4>
                            
                            <ul class="list-unstyled">
                                <li><b>{!!trans('form.label.contactname')!!}:</b> {{$process->applicant_name}} </li>
                                <li><b>{!!trans('form.label.email')!!}:</b> {{$process->email}} </li>
                                <li><b>{!!trans('form.label.address')!!}:</b> {{$process->address}} </li>
                                <li><b>{!!trans('form.label.phone')!!}:</b> {{$process->phone}} </li>
                            </ul>
                </div>
                <div style="width:50%;display:inline-block;float:right" align="right">
                <img src="{{ url('images/email/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;">
                </div>
                 <br />
                  <br />
                  <div class="x_title">
                          <div align="center" class="x_content">
                          <h2>Encuesta de Calidad del Proyecto {{$process->process_number}}</h2>
                           <br />
                          <div class="x_content">
                                <div class="x_title">
                                  <div class="clearfix"></div>
                                </div>
                              
                                  <ul class="stats-overview">
                                    <li>
                                      <span class=" value"> {!!trans('form.label.inidate')!!} </span>
                                      <span class=" name"> {{date("l jS F Y",strtotime($process->ini_date))}} </span>
                                    </li>
                                    <li>
                                      <span class="value"> {!!trans('form.label.enddate')!!} </span>
                                      <span class="name "> {{date("l jS F Y",strtotime($process->end_date))}} </span>
                                    </li>
                                    <li class="hidden-phone">
                                      <span class="value"> {!!trans('form.label.duration')!!} </span>
                                      <span class="name ">  {{$counter[0]->total}} Hours</span>
                                    </li>
                                  </ul>
                              
                              </div>
                          </div>

                          <div class="project_detail">
                          <div class="x_title">
                            <div class="clearfix"></div>
                          </div>
                            
                            <h2 class="green"> {!!trans('form.label.projectdescription')!!}:</h2>
                            <p>{{$process->description}}</p>
                            <p class="title">{!!trans('form.label.proleader')!!}: {{$process->staff}}</p>
                           
                  
                        </div>
                         
                      
                  </div>

                  <div class="x_content">

                    
                      <!-- info de la visita -->
                    
                      <div>
                        <h2 class="green"> Resultados Encuesta de Calidad:</h2>
                       
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">1 - {!!trans('form.label.service_quality')!!}: <span>{!!trans('form.label.'.$request->service)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">2 - {!!trans('form.label.puntuality')!!}: <span>{!!trans('form.label.'.$request->punctuality)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">3 - {!!trans('form.label.personal_attitude')!!}: <span>{!!trans('form.label.'.$request->attitude)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">4 - {!!trans('form.label.attention')!!}: <span>{!!trans('form.label.'.$request->attention)!!}</span>
                        </label>
                       
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">5 - {!!trans('form.label.consultation')!!}: <span>{!!trans('form.label.'.$request->consultation)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">6 - {!!trans('form.label.professionalism')!!}: <span class="required">{!!trans('form.label.'.$request->professionalism)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">7 - {!!trans('form.label.job_quality')!!}: <span>{!!trans('form.label.'.$request->job_quality)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">8 - {!!trans('form.label.equipment_quality')!!}: <span>{!!trans('form.label.'.$request->equipment_quality)!!}</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <label></label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">9 - {!!trans('form.label.solutions')!!}: <span>{!!trans('form.label.'.$request->solutions)!!}</span>
                        </label>
                       
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">10 - {!!trans('form.label.knowledge')!!}: <span class="required">{!!trans('form.label.'.$request->knowledge)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">11 - {!!trans('form.label.evaluation')!!}: <span>{!!trans('form.label.'.$request->evaluation)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">12 - {!!trans('form.label.administrative')!!}: <span>{!!trans('form.label.'.$request->administrative)!!}</span>
                        </label>
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">13 - {!!trans('form.label.work_again')!!}: <span> {!!($request->work_again ? 'Yes' : 'No')!!}</span>
                        </label>
                       
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">14 - {!!trans('form.label.why')!!}: <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            {{$request->work_reason}}
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">15 - {!!trans('form.label.satisfaction')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            {{$request->satisfaction}}
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">16 - {!!trans('form.label.less_satisfaction')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            {{$request->less_satisfaction}}
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">17 - {!!trans('form.label.comments')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            {{$request->comments}}
                          
                        </div>
                      </div>
                        <!-- End info Quality -->
                      </div>
                      

                      <div class="x_title">
                        <div class="clearfix"></div>
                      </div>  
            
           </div>
                     
        <!-- /page content -->
      </div>
    </div>

  </body>
</html>