<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_TITLE')}} |</title>

    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    
  <style type="text/css">
    *{
      background-color: white!important;
    }
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
        bottom: 20px;
        font-size: 11px;
    }
    
    .pagenum:before {
        content: counter(page);
    }
    .header {
        width: 100%;
        position: fixed;
        top: 10px;
    }
    
    
  </style>  
  </head>

  <body >
  <div class="header"></div>
  <div class="footer">
    <p class="pagenum">INVERSIONES SONITUS <br /> Av. Este, Edif. Torre Morelos, P.B. Local 9, Urb. Los Caobos Telf.: (58)212-5778891, Email: inversiones.sonitus@gmail.com </p>
  </div>
    <div class="container body" >
      <div class="main_container">
        
        <!-- page content -->
        <div class="row" >
              <div class="col-md-12">
                <div class="">
                   <div align="left" style="width:50%;display:inline-block;float:left; font-size:8px">
                                <h4>Compañia: {{$process->name}}</h4>
                                          
                                          <ul class="list-unstyled">
                                              <li><b>Contacto:</b> {{$process->applicant_name}} </li>
                                              <li><b>Correo:</b> {{$process->email}} </li>
                                              <li><b>Direccion:</b> {{$process->address}} </li>
                                              <li><b>Telefonos:</b> {{$process->phone}} </li>
                                          </ul>
                  </div>
                  <div style="width:50%;display:inline-block;float:right" align="right">
                              <img src="{{ url('images/email/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;">
                  </div>
                  <div class="x_title" >
                          <div align="center" class="x_content">
                          <h2>Reporte Final del Proyecto {{$process->process_number}}</h2>
                           <br />
                          <div class="x_content">
                                <div class="x_title">
                                  <div class="clearfix"></div>
                                </div>
                              
                                  <ul class="stats-overview">
                                    <li>
                                      <span class=" value"> Fecha de inicio </span>
                                      <span class=" name"> {{date("l jS F Y",strtotime($process->ini_date))}} </span>
                                    </li>
                                    <li>
                                      <span class="value"> Fecha de finalizacion </span>
                                      <span class="name "> {{date("l jS F Y",strtotime($process->end_date))}} </span>
                                    </li>
                                    <li class="hidden-phone">
                                      <span class="value"> Duracion del proyecto </span>
                                      <span class="name ">  {{$counter[0]->total}} Hours</span>
                                    </li>
                                  </ul>
                              
                              </div>
                          </div>

                          <div class="project_detail">
                          <div class="x_title">
                            <div class="clearfix"></div>
                          </div>
                            
                            <h2 class="green"> Descripcion del proyecto:</h2>
                            <p>{{$process->description}}</p>
                            <p class="title">Lider del Proyecto: {{$process->staff}}</p>
                           
                  
                        </div>
                         
                      
                  </div>

                  <div class="x_content">

                    
                      <!-- info de la visita -->
                    @if(count($visits)>0)
                      <div>
                        <h2 class="green"> Informacion de visitas:</h2>
                       
                        <ul class="messages">
                        @foreach($visits as $v)
                          <li>
                             <div class="message_wrapper">
                              <h4 class="heading"><a href="{{ route('visits.show',['id' => $v->id] )}}">{{date("l jS F Y",strtotime($v->date))}}</a>
                              Tecnico: {{$v->name}}</h4>
                              <blockquote class="message">{{($v->description != '' ? $v->description : 'No information to display')}}</blockquote>
                              <br />
                              <p class="url">
                              <span class="fs1 text-info" aria-hidden="true">Hora de inicio: {{$v->entrance_hr.' / Hora culminacion: '.$v->exit_hr}}</span>
                                <br /><span class="fs1 text-info" aria-hidden="true"><b>Tiempo total:</b> {{$v->total}}</span>
                              </p>
                            </div>
                          </li>
                         @endforeach 
                        </ul>
                        <!-- End info de la visita -->
                      </div>
                      @endif

                      <div class="x_title">
                        <div class="clearfix"></div>
                      </div>  
           <!-- Request to client info -->
                    @if(count($requestclient)>0)
                      <h2 class="green">Solicitudes al Cliente</h2>
                          <ul class="list-unstyled project_files">
                           @foreach($requestclient as $r)

                            <li>Estado: {{$r->status.' Fecha: '.date("D M j Y",strtotime($r->date))}}
                                <p>{{$r->request}}</p>
                            </li>
                           @endforeach 

                          </ul>
                    @endif 
                    <div style="page-break-before: always;"></div>
                    <!-- Fotos de la visita -->
                        <div class="x_title">
                            <h2>Pictures </h2>
                            
                            <div class="clearfix"></div>
                          </div>
                         
                          @if(count(@$picturestake)>0)
                              <div class="col-xs-12 col-md-12" >     
                              @foreach($picturestake as $foto)
                                <!-- <div class="thumbnail" style="width:150px; height:150px> -->
                                    <div class="col-xs-12 col-md-3" style="width:150px;display:inline-block;float:left">
                                          <p><b>Fecha Visita: </b><br />{{date("D M j Y",strtotime($foto->date))}}<br />
                                          <b>Description: </b>{{$foto->description}}
                                          <img src="{{url('images/equipment/').'/'.$foto->processid.'/'.$foto->visitsid.'/'.$foto->file}}" alt="" width="100px" height="100px" />
                                          </p>
                                      </div>
                                <!-- </div> -->
                                @endforeach
                               </div>
                          @else
                          <div class="alert alert-warning">
                            <strong> No hay fotos que mostrar.</strong>
                          </div>
                            
                          @endif      
           </div>
            <!-- Sign -->
            @if($process->signature!='')
                      <div align="center">
                          <div class="x_title">
                              <img src="{{url('images/equipment/').'/'.$process->processid.'/'.$process->signature}}" alt="" width='40%' />
                                  <div class="clearfix"></div>
                                </div>
                            <p class="title">Firma del Cliente</p>
                      </div>      
                      @endif               
        <!-- /page content -->
      </div>
    </div>

  </body>
</html>