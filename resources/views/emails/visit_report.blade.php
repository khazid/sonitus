<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_TITLE')}} |</title>

    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    
  <style type="text/css">
    *{
      background-color: white!important;
    }
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
    }
    
    .footer {
        bottom: 50px;
        font-size: 11px;
       border-top-style: solid;
    }
    .pagenum:before {
        content: counter(page);
    }
    .header {
        width: 100%;
        position: fixed;
    }
    
    .header {
        top: 10px;
        
    }
  </style>  
  </head>

  <body >

  <div class="footer">
    
    <div style="width:50%;display:inline-block;float:left; font-size:11px" align="left" >
       <p class="title">Client Sign:
        @if($visits->signature!='')
            <img src="{{url('images/equipment/').'/'.$process->processid.'/'.$visits->id.'/'.$visits->signature}}" alt="" width='100px' />
        @endif
        </p>
    </div>
      
    <div style="width:50%;display:inline-block;float:right; font-size:11px" align="left">
      <p class="pagenum"><b>INVERSIONES SONITUS</b> <br /> Av. Este, Edif. Torre Morelos, P.B. Local 9, Urb. Los Caobos.<br /> Telf.: (58)212-5778891.<br /> Email: inversiones.sonitus@gmail.com </p>
    </div>                  
    
  </div>

    <div class="container body">
      <div class="main_container">
        
        <!-- page content -->
        <div class="row">
              <!--<div class="col-md-12">-->
            <div class="x_panel"> <!--proporciona el margen -->

                <div align="left" style="width:50%;display:inline-block;float:left; font-size:8px">
                  <h4>Company: {{$process->name}}</h4>
                            
                            <ul class="list-unstyled">
                                <li><b>RIF:</b> {{$process->rif}} </li>
                                <li><b>Contact:</b> {{$process->applicant_name}} </li>
                                <li><b>Email:</b> {{$process->email}} </li>
                                <li><b>Address:</b> {{$process->address}} </li>
                                <li><b>Phone #:</b> {{$process->phone}} </li>
                            </ul>
                </div>
                <div style="width:50%;display:inline-block;float:right" align="right">
                <img src="{{ url('images/email/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;">
                </div>

                 <br /><br />

                  <div class="x_title">
                      
                      <div align="center" class="x_content">
                          <h2>Visit made on {{date("l jS F Y",strtotime($visits->date))}}</h2>
                           <br />
                              <div class="x_content">
                                <div class="x_title">
                                  <div class="clearfix"></div>
                                </div>
                              
                                  <ul class="stats-overview">
                                    <li>
                                      <span class="name"> Initial Hour </span>
                                      <span class="value text-success">{{$visits->entrance_hr}} </span>
                                    </li>
                                    <li>
                                      <span class="name"> Exit Hour </span>
                                      <span class="value text-success">{{$visits->exit_hr}}</span>
                                    </li>
                                    <li class="hidden-phone">
                                      <span class="name"> Visit Duration </span>
                                      <span class="value text-success"> {{$visits->total}} Hours </span>
                                    </li>
                                  </ul>
                              
                              </div>
                      </div>

                          
                        <!--<h2 class="green"> Proyect Information:</h2>-->
                        <div style="width:70%;display:inline-block;float:left; font-size:11px" align="left" >
                              <ul class="list-unstyled">
                                  <li><b>Project number:</b> {{$process->process_number}} </li>
                                  <li><b>Description:</b> {{$process->description}} </li>
                              </ul>
                        </div>
                            
                        <div style="width:30%;display:inline-block;float:right; font-size:11px" align="left">
                            <ul class="list-unstyled">
                                  <li><b>Project Leader:</b> <br>{{$process->staff}} </li>
                                  <li><b>Tech. Team:</b><br> {{$visits->name}}<br />
                                        @foreach($visittec as $tec)
                                          {{$tec->name}}<br />
                                        @endforeach 
                                  </li>
                              </ul>
                        </div>
                         
                      
                  </div>

                  

                    <!-- info de la visita -->
                    <h2 class="green"> Visit Information:</h2>
                    <div style="width:25%;display:inline-block;float:left; font-size:11px" align="left" >
                              <ul class="list-unstyled">
                                  <li><b>Job type:</b> <br/> 
                                      @foreach($jobt as $job)
                                      {{$job->name}}
                                      @endforeach
                                  </li>
                                  
                              </ul>
                    </div>
                            
                    <div style="width:25%;display:inline-block;float:right; font-size:11px" align="left">
                        <ul class="list-unstyled">
                                  <li><b>General Information:</b> <br/>
                                      @foreach($general as $gen)
                                      {{$gen->name}}
                                      @endforeach
                                  </li>
                        </ul>
                    </div>

                    <div style="width:25%;display:inline-block;float:right; font-size:11px" align="left">
                        <ul class="list-unstyled">
                                  <li><b>General Description:</b> <br/>
                                      @foreach($gentype as $gt) <!--- recorrido de los tipos --> 
                                    <span > {{$gt->type}} </span>
                                        
                                          @foreach($generaltype as $gent) <!--- recorrido de los names -->
                                              @if($gt->type==$gent->type)
                                              <span>{!!$gent->name!!}, </span>
                                              @endif
                                              
                                              
                                          @endforeach
                                      <br />
                                      
                                  @endforeach
                                  </li>
                        </ul>
                    </div>

                    <div style="width:25%;display:inline-block;float:right; font-size:11px" align="left">
                        <ul class="list-unstyled">
                                  <li><b>Observation:</b><br/>{{$visits->description}}</li>
                                  @if(count($requestclient)>0)
                                    <li><b>Visit Request to client:</b> <br/>{{$requestclient->request}}</li>
                                  @endif
                        </ul>
                    </div>
                        
                       <!-- End info de la visita -->

                        <!-- Fotos de la visita -->
                        <div class="x_title">
                            <h2>Pictures </h2>
                            
                            <div class="clearfix"></div>
                          </div>
                         
                          @if(count(@$picturestake)>0)
                              <div  >     
                              @foreach($picturestake as $foto)
                                <!-- <div class="thumbnail" style="width:150px; height:150px> -->
                                    <div style="width:150px;display:inline-block;float:left">
                                          <p><b>Description: </b>{{$foto->description}}</p>
                                          <img src="{{url('images/equipment/').'/'.$process->processid.'/'.$visits->id.'/'.$foto->file}}" alt="" width="100px" height="100px" />
                                      </div>
                                <!-- </div> -->
                                @endforeach
                               </div>
                          @else
                          <div class="alert alert-warning">
                            <strong> No hay fotos que mostrar.</strong>
                          </div>
                            
                          @endif 
                     


                  </div>
                
                 <!--@if($visits->signature!='')
                      <div align="center">
                          <div class="x_title">
                              <img src="{{url('images/equipment/').'/'.$process->processid.'/'.$visits->id.'/'.$visits->signature}}" alt="" width='20%' />
                                  <div class="clearfix"></div>
                                </div>
                            <p class="title">Client Sign</p>
                      </div>      
                      @endif-->

            </div> 
              
            <!--</div>-->
        <!-- /page content -->
      </div>
    </div>

  </body>
</html>