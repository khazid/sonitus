<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v3.5.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v3.5.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="assets/images/executive-conference-space-solution-image-128x96-35.jpg" type="image/x-icon">
  <meta name="description" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900">
  {!! Html::style('/assets/et-line-font-plugin/style.css') !!}
  {!! Html::style('/assets/bootstrap-material-design-font/css/material.css') !!}
  {!! Html::style('/assets/tether/tether.min.css') !!}
  {!! Html::style('/assets/bootstrap/css/bootstrap.min.css') !!}
  {!! Html::style('/assets/socicon/css/socicon.min.css') !!}
  {!! Html::style('/assets/animate.css/animate.min.css') !!}
  {!! Html::style('/assets/dropdown/css/style.css') !!}
  {!! Html::style('/assets/theme/css/style.css') !!}
  {!! Html::style('/assets/mobirise-gallery/style.css') !!}
  {!! Html::style('/assets/mobirise/css/mbr-additional.css') !!}
  <style type="text/css">
      .formcontact{
        color:#ddd;
        text-align: left!important;
      }
  </style>
  
  
</head>
<body>
<section id="ext_menu-0">

    <nav class="navbar navbar-dropdown navbar-fixed-top">
        <div class="container">

            <div class="mbr-table">
                <div class="mbr-table-cell">

                    <div class="navbar-brand">
                        <a href="http://{!! env('WEB_PAGE')!!}" class="navbar-logo"><img src="assets/images/logo-unico-vectorizado4-489x128-21-489x128-42.png" alt="Mobirise"></a>
                        
                    </div>

                </div>
                <!-- <div class="mbr-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="hamburger-icon"></div>
                    </button>

                    <ul class="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar"><li class="nav-item"><a class="nav-link link" href="index.html" aria-expanded="false">INICIO</a></li><li class="nav-item"><a class="nav-link link" href="index.html#msg-box5-3">QUIENES SOMOS</a></li><li class="nav-item"><a class="nav-link link" href="index.html#form1-9">CONTACTANOS</a></li><li class="nav-item dropdown open"><a class="nav-link link dropdown-toggle" href="index.html#form1-9" data-toggle="dropdown-submenu" aria-expanded="true">SERVICIOS</a><div class="dropdown-menu"><a class="dropdown-item" href="HOGAR.html">HOGAR</a><a class="dropdown-item" href="EMPRESAS.html">EMPRESAS<br></a><a class="dropdown-item" href="page14.html">PROFESIONAL</a></div></li></ul>
                    <button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="close-icon"></div>
                    </button>

                </div> -->
            </div>

        </div>
    </nav>

</section>


<section class="mbr-section mbr-section-hero mbr-section-full mbr-parallax-background mbr-after-navbar" id="header4-0" style="background-image: url(assets/images/135567383-2000x1519-26.jpg);">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>

    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="mbr-section col-md-10 col-md-offset-2 text-xs-left">
           
                    <div class="form-group">
                      <label class="control-label formcontact" for="first-name">Tipo Cliente: <span class="required">*</span>
                        </label>
                       
                          <label class="formcontact">
                              <input type="radio" class="flat clienttype" checked name="clienttype"  value="empresa"> Empresa
                          </label>
                          <label class="formcontact">
                            <input type="radio" class="flat clienttype"  name="clienttype"  value="persona"> Persona
                          </label>
                    </div>
                    <div class="form-group empresa">
                        <label class="control-label formcontact" for="first-name">Nombre de la Empresa <span class="required">*</span>
                        </label>
                       
                          <input id="name" class="form-control" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="" type="text" >
                        
                      </div>
                      <div class="form-group empresa">
                        <label class="control-label formcontact" for="last-name">RIF <span class="required">*</span>
                        </label>
                        
                         <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                              <select name="riftype" id="riftype" class="form-inline">
                              <option value="J">J</option>
                              <option value="V">V</option>
                              <option value="P">P</option>
                              <option value="G">G</option>
                              <option value="E">E</option>
                              <option value="C">C</option>
                              </select>
                            </span>
                            <input id="rif" name="rif"type="text" class="form-control" data-inputmask="'mask': '99999999-9'" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label formcontact" for="first-name">Nombre Solicitante <span class="required">*</span>
                        </label>
                        
                          <input type="text" id="applicant_name" name="applicant_name" required="required" class="form-control" required>
                        
                      </div>
                      <div class="form-group">
                        <label for="address" class="control-label formcontact">Direccion <span class="required">*</span></label>
                        
                          <input id="address" class="form-control" type="text" name="address" required >
                       
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label formcontact" for="first-name">Cargo <span class="required">*</span>
                        </label>
                       
                          <select name="charge" id="charge" class="form-control">
                              <option value="Dueño">Dueño</option>
                              <option value="Jefe">Jefe</option>
                              <option value="Encargado">Encargado</option>
                              <option value="Gerente">Gerente</option>
                              <option value="Empleado">Empleado</option>
                              <option value="Autorizado">Autorizado</option>
                              </select>
                        
                      </div>
                       <div class="form-group">
                        <label class="form-control-label formcontact" for="first-name">Correo <span class="required">*</span>
                        </label>
                       
                          <input type="email" id="email" name="email" class="form-control" name="email" data-parsley-trigger="change" required />
                       
                      </div>
                      <div class="form-group">
                        <label class="control-label formcontact" for="first-name">Telefonos <span class="required">*</span>
                        </label>
                        
                          <input id="telephone" name="telephone" type="text" class="form-control" data-inputmask="'mask': '+(99)(999)999-9999'" required="required" >
                        
                      </div>
                      <div class="form-group">
                        <label class="control-label formcontact" for="first-name">Tipo de Solicitud: <span class="required">*</span>
                        </label>
                       
                          <select name="type" id="type" class="form-control input-sm ">
                              <option value="Presupuesto">Presupuesto</option>
                              <option value="Proyecto">Cotizar Proyecto</option>
                              <option value="Informacion">Informacion</option>
                              <option value="Soporte">Soporte</option>
                              </select>
                       
                      </div>
                      <div class="form-group">
                        <label class="form-control-label formcontact" for="message">Solicitud: <span class="required">*</span>
                        </label>
                        
                          <textarea class="form-control" name="message" id="message" data-form-field="Message" rows="5" id="contacts3-0-message"></textarea>
                        
                      </div>
                        <div class="form-group"><button type="submit" class="btn btn-sm btn-black send">Enviar</button></div>
                   
                </div>
            </div>
        </div>
    </div>

</section>


<footer class="mbr-small-footer mbr-section mbr-section-nopadding mbr-parallax-background" id="footer1-0" style="background-image: url(assets/images/formatosonitushoja5-2000x2588-25.jpg); padding-top: 0.875rem; padding-bottom: 0.875rem;">
    <div class="mbr-overlay" style="opacity: 0.4; background-color: rgb(0, 0, 0);"></div>
    <div class="container">
        <p class="text-xs-center">&nbsp;(c) 2016 Inversiones Sonitus</p>
    </div>
</footer>

  
  {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
  {!! Html::script('assets/tether/tether.min.js') !!}
  {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
  <!--{!! Html::script('assets/smooth-scroll/SmoothScroll.js') !!}-->
  {!! Html::script('assets/viewportChecker/jquery.viewportchecker.js') !!}
  {!! Html::script('assets/dropdown/js/script.min.js') !!}
  {!! Html::script('assets/touchSwipe/jquery.touchSwipe.min.js') !!}
  {!! Html::script('assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js') !!}
  {!! Html::script('assets/masonry/masonry.pkgd.min.js') !!}
  {!! Html::script('assets/imagesloaded/imagesloaded.pkgd.min.js') !!}
  {!! Html::script('assets/jarallax/jarallax.js') !!}
  {!! Html::script('assets/theme/js/script.js') !!}
  {!! Html::script('assets/mobirise-gallery/script.js') !!}
  {!! Html::script('assets/formoid/formoid.min.js') !!}

  <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
  
  <script type="text/javascript">
       $(document).ready(function() {
       
        $(":input").inputmask();

        $(".clienttype").click(function(){
          var valor=$(this).val();
          // alert(valor);

          if(valor=='empresa'){
            $('.empresa').show();
          }else if(valor=='persona'){
            $('.empresa').hide();
          }
        });

        $('#rif').focusout(function(e){
          var rif = $('#riftype').val()+$(this).val();
          var APP_URL = {!! json_encode(url('/')) !!};    
               $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                $.ajax({
                        type: 'POST', // Type of response and matches what we said in the route
                        url: APP_URL+'/process/searchclient', // This is the url we gave in the route
                        data: {'rif' : rif}, // a JSON object to send back
                        success: function(response){ // What to do if we succeed
                           console.log('Client:',response.client)
                           if(response.client!='null'){
                               $('#name').val(response.client['name']);
                               $('#address').val(response.client['address']);
                               $('#applicant_name').val(response.client['applicant_name']);
                               $('#dpto').val(response.client['departmen']);
                               $('#email').val(response.client['email']);
                               $('#telephone').val(response.client['phone']);
                               
                               
                          }else{
                               //$('#name').val(response.client['name']);
                               $('#address').val(response.client['address']);
                               $('#applicant_name').val(response.client['applicant_name']);
                               $('#dpto').val(response.client['departmen']);
                               $('#email').val(response.client['email']);
                               $('#telephone').val(response.client['phone']);
                               
                               
                          }
                        }
                    });
             

        });

        $(".send").click(function(e){
         
        var name=$('#name').val(),
            rif=$('#rif').val(),
            riftype=$('#riftype').val(),
            address=$('#address').val(),
            applicant_name=$('#applicant_name').val(),
            charge=$('#charge').val(),
            telephone=$('#telephone').val(),
            email=$('#email').val(),
            message=$('#message').val(),
            type=$('#type').val(),
            clienttype=$(".clienttype").val();
            webpage='{!! env('WEB_PAGE')!!}';

            if(clienttype=='persona'){
            name=$('#applicant_name').val();
           }

            validation=true;
            /*if(rif=='') {validation=false;
            }else */
            if(address==''){validation=false;
            }else if(applicant_name==''){validation=false;
            }else if(telephone==''){validation=false;
            }else if(email==''){validation=false;
            }else if(message==''){validation=false;
            }//else if(name==''){validation=false;} 

            if(validation==false){ 
              alert('Debe llenar todos los campos del formulario, Gracias');
              return false;
            }
           



            console.log('name'+name+' rif'+rif+' riftype'+riftype+' address'+address+' applicant'+applicant_name+' charge'+charge+' telefono'+telephone+' email'+email+' message'+message+' type'+type);
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
        $.ajax({
                  method: 'POST', // Type of response and matches what we said in the route
                  url: 'saveweb', // This is the url we gave in the route
                  data: {'name':name,
                          'rif':rif,
                          'riftype':riftype,
                          'address':address,
                          'applicant_name':applicant_name,
                          'charge':charge,
                          'telephone':telephone,
                          'email':email,
                          'message':message,
                          'type':type}, // a JSON object to send back
                  success: function(response){ // What to do if we succeed
                  console.log('RespuestaServ: ',response.send);
                  if(response.send){
                    alert('Datos enviados con exito, en el transcurso de 24 horas le estaremos respondiendo su mensaje, Gracias');
                    
                   // return false;
                    $(location).attr('href',webpage);
                  }else{
                    alert('En estos momentos no podemos procesar sus solicitud por favor intente mas tarde, Diculpe las molestias ocasionadas');
                  }
                
                    
                    
                    
                  }
              });
        });
    });
  </script> 
  
  
  <!--<input name="animation" type="hidden"> efecto de hide smooth de los campos-->
  </body>
</html>