
<!------------- Cambio de contraseña------- -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel">
              <div class="modal-dialog modal-lg" >
              
                <div class="modal-content" >
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="passwordModalLabel">Change password</h4>
                  </div>
                  <div class="modal-body">

                     <p class='green' align="center">A Email will be send after the password change confirmation</p>
                      <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Email</span>
                        <input type="text" name="emailpass" class="form-control" id="emailpass" aria-describedby="basic-addon3" (Auth::user() ? 'readonly="readonly"' : '' )  >
                      </div>
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">New Password</span>
                        <input type="password" name="nuevaclave" class="form-control" id="nuevaclave" aria-describedby="basic-addon3" >
                      </div>
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Confirm Password</span>
                        <input type="password" name="confirmclave" class="form-control" id="confirmclave" aria-describedby="basic-addon3" >
                      </div>
                         
                      </div>
                  </div>
                 <div class="modal-footer">
                    
                    <input type="submit" class="btn btn-success save" value="Save">
                  </div>
                </div>
                
              </div> 
</div>

