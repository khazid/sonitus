<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Signature Pad demo</title>
  <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
{!! Html::style('assets/signature_pad/signature-test/signature-pad.css') !!}

</head>
<body onselectstart="return false">

  <div id="signature-pad" class="m-signature-pad">
    <div class="m-signature-pad--body">
      <canvas></canvas>
    </div>
    <div class="m-signature-pad--footer">
      <div class="description">Sign above</div>
      <button type="button" class="button clear" data-action="clear">Clear</button>
      <button type="button" class="button save" data-action="save">Save</button>
    </div>
  </div>

   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    {!! Html::script('assets/signature_pad/signature-test/signature_pad.js') !!}
    {!! Html::script('assets/signature_pad/signature-test/app.js') !!}
<script type="text/javascript">
  var APP_URL = {!! json_encode(url('/')) !!};
</script>
</body>
</html>
