<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_TITLE')}} |</title>

    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    
  <style type="text/css">
    
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
    }
    
    .footer {
        bottom: 10px;
        font-size: 11px;
    }
    .pagenum:before {
        content: counter(page);
    }
    *{
      background-color: white!important;

    }
  </style>  
  </head>

  <body >
  
  <div class="footer">
    <p class="pagenum">INVERSIONES SONITUS <br /> Av. Este, Edif. Torre Morelos, P.B. Local 9, Urb. Los Caobos Telf.: (58)212-5778891, Email: inversiones.sonitus@gmail.com </p>
  </div>
    <div class="container body">
      <div class="main_container">
        
        <!-- page content -->
        <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                <div align="left" style="width:50%;display:inline-block;float:left; font-size:10px">
                  <h4>Compañia: {{$client->name}}</h4>
                  <h5>RIF: {{$client->rif}}</h5>
                            
                            <ul class="list-unstyled">
                                <li><b>Contacto:</b> {{$client->applicant_name}} </li>
                                <li><b>Correo:</b> {{$client->email}} </li>
                                <li><b>Direccion:</b> {{$client->address}} </li>
                                <li><b>Telefonos:</b> {{$client->phone}} </li>
                            </ul>
                </div>
                <div style="width:50%;display:inline-block;float:right" align="right">
                <img src="{{ url('images/email/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;">
                <br />
                <span style="font-size:10px">RIF: J-40188436-9</span>
                </div>
                 <br />
                  <br />
                  <div class="x_title">
                          <div align="center" class="x_content">
                          <h2>Fecha: {{date("d/m/Y",strtotime($client->quotedate))}} Cotizacion Nro.{{$client->quote_number}} </h2>
                          </div>

                          <div class="project_detail">
                          <div class="x_title">
                            <div class="clearfix"></div>
                          </div>
                            
                            <h2 class="green"> Descripcion del proyecto:</h2>
                            <p>{{$client->observation}}</p>
                        </div>
                         
                      
                  </div>

                  <div class="x_content">
                    <div>
                    
                      <!-- info de la visita -->
                    @if(count($material)>0)
                      
                        <table id="datatable-items" class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th width="10%">#</th>
                                  <th width="40%">{!!trans('form.table.description')!!}</th>
                                  <th width="10%">{!!trans('form.table.quantity')!!}</th>
                                  <th width="10%">{!!trans('form.table.unitprice')!!}</th>
                                  <th width="10%">{!!trans('form.table.total')!!}</th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                            </tr>
                            <? $i=1 ?>
                              @foreach($material as $m)
                              
                              <tr>
                                  <td>{{$i}}</td>
                                  <td>{{ $m->description}}</td>
                                  <td>{{ $m->quantity}}</td>
                                  <td>{{ $m->unit_price}}</td>
                                  <td>{{ $m->total}}</td>
                              </tr> 
                            <? $i++ ?>
                              @endforeach
                              <tr>
                                  <td colspan="3"></td>
                                  <td >Sub Total</td>
                                  <td>{{ $subtotal}}</td>
                              </tr>
                              <tr>
                                  <td colspan="3"></td>
                                  <td >IVA {{$config->iva}}%</td>
                                  <td>{{ $iva}}</td>
                              </tr>
                              <tr>
                                  <td colspan="3"></td>
                                  <td >Total</td>
                                  <td>{{ $total}}</td>
                              </tr> 
                          </tbody>
                        </table>
                     @endif
                 </div>     
              </div>
                       
        <!-- /page content -->
      </div>
    </div>
    </div>
    </div>
</div>

  </body>
</html>