@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.report')!!} {{date('d-m-Y',strtotime($semana['primer']))}} al {{date('d-m-Y',strtotime($semana['ultimo']))}}</h3>

              </div>
              
              
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                {!! Form::open(['route' =>'report/visitCharts', 'method' => 'post', 'novalidate','id'=>'formdate']) !!}
                  <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                            <input type="hidden" name="start" id="start">
                            <input type="hidden" name="end" id="end">
                  </div>
                 {!! Form::close() !!}      
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.project')!!} <small>{!!trans('form.label.client')!!}</small></h2>
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        
                             
                      </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <canvas id="clientChart"></canvas>
                        
                  </div>
                </div>
              </div>

              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.project')!!} <small>Total</small></h2>
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        
                             
                      </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <canvas id="mybarChart"></canvas>
                        
                  </div>
                </div>
              </div>

              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.visit')!!} <small>Total</small></h2>
                    <div class="title_right">
                      <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div >
                        
                        
                          
                        </div>
                             
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <canvas id="visitCharttotal"></canvas>
                  </div>
                </div>
              </div>
              
             </div>
              
             <div class="row">

              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.visit')!!} <small>{!!trans('form.label.technician')!!} </small></h2>
                    <div class="title_right">
                      <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div >
                        
                         
                          
                        </div>
                             
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <canvas id="visitChart"></canvas>
                  </div>
                </div>
              </div>

              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Horas <small>{!!trans('form.label.technician')!!}</small></h2>
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                        <canvas id="canvas1" ></canvas>
                      
                  </div>
                </div>
              </div>
             

             <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.webrequest')!!} </h2>
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                     </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <canvas id="webChart"></canvas>
                        
                  </div>
                </div>
              </div>
              
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- /page content -->
    @endsection  
    @section('script')
    
    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- Chart.js -->
    {!! Html::script('/vendors/Chart.js/dist/Chart.min.js') !!}

    <!-- bootstrap-daterangepicker -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/datepicker/daterangepicker.js') !!}

        <!-- morris.js -->
    {!! Html::script('/vendors/raphael/raphael.min.js') !!}
    {!! Html::script('/vendors/morris.js/morris.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

        <!-- Random colors-->
    {!! Html::script('assets/js/randomcolor.js') !!}

    <!-- datepicker -->
    <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
        var staff = $('#staff').val();
        var datebar=[];
        var cb = function(start, end, label) {
          //console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
           //alert("Callback has fired: [" + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD') + ", label = " + label + "]");
          

           
        }

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2016',
          maxDate: '12/31/2021',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {},
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          // console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          // console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));

          var startdate=picker.startDate.format('YYYY-MM-DD');
          var enddate=picker.endDate.format('YYYY-MM-DD');

          $('#start').val(startdate);
          $('#end').val(enddate);
          $('#formdate').submit();
            

        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          // console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
    <!-- Doughnut Chart -->
    <script>
      $(document).ready(function(){
        var options = {
          legend: true,
          responsive: false
        };
        var ddata=[{!!$donut['data'] !!}];
        var dlabel=[{!!$donut['label'] !!}];
        var count=[{!!$donut['count'] !!}];
        //var colores=randomColor({count: count,hue: 'blue'});
        
        
        /*new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: dlabel,
            datasets: [{
              data: ddata,
              backgroundColor: ["#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });*/

      //Bar chart hrs tecnico
     var colores=randomColor({count: count,hue: 'blue'});
     var ctx = document.getElementById("canvas1");
     var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: dlabel,
          datasets: [{
            label: '# Hrs por tecnico',
            backgroundColor: colores[0],
            data: ddata
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
     });
     
      // Bar chart Process
       var asign=[{!!$bar[0]->asignados !!}];
       var enpro=[{!!$bar[0]->enproceso !!}];
       var cerr=[{!!$bar[0]->cerrados !!}];
       var colores=randomColor({count: 3,hue: 'blue'});

      var ctx = document.getElementById("mybarChart");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ['Total de Procesos'],
          datasets: [{
            label: '# de procesos Asignados',
            backgroundColor: colores[0],
            data: asign
          }, {
            label: '# de procesos En proceso',
            backgroundColor: colores[1],
            data: enpro
          },{
            label: '# de procesos Cerrados',
            backgroundColor: colores[2],
            data: cerr
          }]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });

    //Bar chart Visits
     var colores=randomColor({count: 3,hue: 'blue'});
      var ctx = document.getElementById("visitChart");
      var personal=[{!!$line['personal'] !!}];
      var nuevas=[{!!$line['nuevas']!!}];
       var nodone=[{!!$line['nodone']!!}];
       var done=[{!!$line['done']!!}];
       

      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: personal,
          datasets: [{
            label: '# Nuevas',
            backgroundColor: colores[0],
            data: nuevas
          }, {
            label: '# Realizadas',
            backgroundColor: colores[1],
            data: done
          }, {
            label: '# No Realizadas',
            backgroundColor: colores[2],
            data: nodone
          }]
        },
     });

      //Bar chart Visits total
      var vnuevas=[{!!$visita[0]->nuevas!!}];
       var vnodone=[{!!$visita[0]->nodone!!}];
       var vdone=[{!!$visita[0]->done!!}];
      var colores=randomColor({count: 3,hue: 'blue'});
      var ctx = document.getElementById("visitCharttotal");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["Total de Visitas"],
          datasets: [{
            label: '# Nuevas',
            backgroundColor: colores[0],
            data: vnuevas
          }, {
            label: '# Realizadas',
            backgroundColor: colores[1],
            data: vdone
          }, {
            label: '# No Realizadas',
            backgroundColor: colores[2],
            data: vnodone
          }]
        },
     });

      //Bar chart procesos por cliente
      var cantclient=[{!!count($client['nombres']) !!}]
      var colores=randomColor({count:cantclient,hue: 'blue'});
      var ctx = document.getElementById("clientChart");
      var clientes=[{!!$client['nombres'] !!}];
      var total=[{!!$client['total']!!}];
      

      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: clientes,
          datasets: [{
            label: '# Total Proyectos',
            backgroundColor: colores[0],
            data: total
          }]
        },
     });

      // Bar chart solicitudes web 
      var pres=[{!!$web->presupuesto !!}];
       var pro=[{!!$web->proyecto !!}];
       var info=[{!!$web->informacion !!}];
       var sopor=[{!!$web->soporte !!}];
       var man=[{!!$web->mantenimiento !!}];
      var colores=randomColor({count: 5,hue: 'blue'});
      var ctx = document.getElementById("webChart");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["rango"],
          datasets: [{
            label: '# Presupuesto',
            backgroundColor: colores[0],
            data: pres
          }, {
            label: '# Proyecto',
            backgroundColor: colores[1],
            data: pro
          }, {
            label: '# Informacion',
            backgroundColor: colores[2],
            data: info
          }, {
            label: '# Soporte',
            backgroundColor: colores[3],
            data: sopor
          }, {
            label: '# Mantenimiento',
            backgroundColor: colores[4],
            data: man
          }]
        },
     });


    });
    </script>
    

    @endsection        
 
 
