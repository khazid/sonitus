@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}

  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.report')!!}</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  {!! Form::open(['route' =>'report/listCharts', 'method' => 'post', 'novalidate','id'=>'formdate']) !!}
                  <div class="pull-right col-md-4 col-sm-4 col-xs-12">
                   <button type="submit" class="btn btn-primary pull-right ">Buscar</button>
                  </div>

                  <div id="reportrange" class="pull-right col-md-4 col-sm-4 col-xs-12" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                            
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>{{date("F j, Y")}} - {{date("F j, Y")}}</span> <b class="caret"></b>
                            <input type="hidden" name="start" id="start" value="{{date('Y-m-d')}}">
                            <input type="hidden" name="end" id="end" value="{{date('Y-m-d')}}">
                            
                  </div>
                  <div class="pull-left col-md-4 col-sm-4 col-xs-12">
                  
                  <select id="lista" name="lista" class="form-control">
                     <option value="vList">Visitas por Tecnico</option>
                     <option value="hourList">Horas de trabajo</option>
                     <option value="vListTotal">Visitas Totales</option>
                     <option value="proyectList">Proyectos Totales</option>
                     <option value="solicitudesWebList">Solicitudes Web</option>
                     <option value="clientProjectList">Proyectos por Cliente</option>
                   </select>
                   
                   </div>
                   
                {!! Form::close() !!}
                   
                   </div>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    @if(count(@$assigned)>0)
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                          <tr>
                            <th colspan="7"><h2>
                            @if($type=='vlist')
                            Visitas por Tecnico
                            @elseif($type=='vtotal')    
                           Visitas Totales
                            @elseif($type=='prolist')    
                          Proyectos Totales
                            @elseif($type=='hour')    
                           Horas de trabajo
                            @elseif($type=='web')    
                          Solicitudes Web
                            @elseif($type=='client')    
                          Proyectos por Cliente
                          @endif
                          </h2></th>
                          </tr>
                          <tr> 
                          @foreach(@$title as $t)
                              <th>{!!trans('form.table.'.$t)!!}</th>
                                
                          @endforeach
                          </tr>
                      </thead>
                      <tbody>
                        @if(count(@$assigned)>0)
                          @foreach($assigned as $r)
                          <tr>
                          @if($type=='vlist')
                            <td>{{ $r->date}}</td><td>{{ $r->name }}</td><td>{{ $r->new }}</td><td>{{ $r->nodone }}</td><td>{{ $r->done }}</td>
                          @elseif($type=='vtotal')    
                          <td>{{ $r->date}}</td><td>{{ $r->new }}</td><td>{{ $r->nodone }}</td><td>{{ $r->done }}</td>
                           @elseif($type=='prolist')    
                          <td>{{ $r->date}}</td><td>{{ $r->enpro }}</td><td>{{ $r->close }}</td><td>{{ $r->assign }}</td>
                           @elseif($type=='hour')    
                          <td>{{ $r->name}}</td><td>{{ $r->total }}</td>
                           @elseif($type=='web')    
                          <td>{{ $r->date}}</td><td>{{ $r->budget }}</td><td>{{ $r->project }}</td><td>{{ $r->info }}</td><td>{{ $r->support }}</td><td>{{ $r->maint }}</td>
                           @elseif($type=='client')    
                          <td>{{ $r->date}}</td><td>{{ $r->name }}</td><td>{{ $r->support }}</td><td>{{ $r->installation }}</td><td>{{ $r->training }}</td><td>{{ $r->maint }}</td><td>{{ $r->visit }}</td>
                          @endif
                          </tr>
                          @endforeach

                       @endif 
                      </tbody>
                    </table>
                    @else
                    <span>No hay informacion que mostrar</span>
                    @endif
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}

    <!-- bootstrap-daterangepicker -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/datepicker/daterangepicker.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <!-- Datatables -->
    <script>
    var APP_URL = {!! json_encode(url('/')) !!};

      $(document).ready(function() {
         
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      
        


    });
    </script>
    <!-- datepicker -->
    <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
        var staff = $('#staff').val();
        var datebar=[];
        var cb = function(start, end, label) {
          //console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
           //alert("Callback has fired: [" + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD') + ", label = " + label + "]");
          

           
        }

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2016',
          maxDate: '12/31/2021',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {},
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);

        $('#reportrange').on('show.daterangepicker', function() {
          // console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          // console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));

          var startdate=picker.startDate.format('YYYY-MM-DD');
          var enddate=picker.endDate.format('YYYY-MM-DD');

          $('#start').val(startdate);
          $('#end').val(enddate);
          // $('#formdate').submit();
            

        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          // console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
@endsection
