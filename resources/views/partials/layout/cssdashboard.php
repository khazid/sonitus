@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-progressbar -->
    {!! Html::style('/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') !!}
    <!-- JQVMap -->
    {!! Html::style('/vendors/jqvmap/dist/jqvmap.min.css') !!}
   
   <!-- Custom Theme Style -->
     {!! Html::style('/build/css/custom.min.css') !!}

         <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
@endsection