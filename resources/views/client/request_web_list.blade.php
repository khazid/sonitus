@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}

  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.client')!!} <small>{!!trans('form.label.webrequest')!!}</small></h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>{!!trans('form.table.project')!!}</th>
                              <th>{!!trans('form.table.client')!!}</th>
                              <th>{!!trans('form.table.type')!!}</th>
                              <th width="40%">{!!trans('form.table.request')!!}</th>
                              <th >{!!trans('form.table.status')!!}</th>
                              <th>{!!trans('form.table.action')!!}</th> 

                          </tr>
                      </thead>
                      <tbody>
                          @if(count(@$requestweb)>0)
                          @foreach($requestweb as $r)
                          <tr>
                              <td>{{ ($r->process_number!='' ? $r->process_number : 'Cliente Web')}}</td>
                              <td>{{ $r->client}}</td>
                              <td>{{ $r->type }}</td>
                              <td id="request">{{ $r->description }}</td>
                              <td>
                                @if($r->status_id==27)
                                <a href="#" class="respuesta" id="{{$r->id}}" data-toggle="modal" data-target="#ResponseModal">{{ $r->status }}</a>
                                @else
                                {{ $r->status }}
                                @endif
                              </td>
                              <td>
                              
                                  @if($r->status_id==26)
                                  <button type="button" class="btn btn-success btn-xs response" id="{{$r->id}}" data-process="{{$r->processid}}" data-content="{{$r->email}}" data-toggle="modal" data-target="#ResponseModal">Responder</button>
                                  
                                  <a href="{{route('client/newproject',['id'=>$r->clientid])}}" class="btn btn-warning btn-xs newcreate"  id="{{$r->id}}" data-process="{{$r->processid}}" data-content="{{$r->email}}" data-type="newpro">Crear Proceso</button>
                                  
                                  <a href="{{route('visits/calendar')}}" class="btn btn-primary btn-xs newcreate" id="{{$r->id}}" data-process="{{$r->processid}}" data-content="{{$r->email}}" data-type="newvisit">Agendar visita</a>

                                  <button type="button" class="btn btn-default btn-xs quote" id="{{$r->id}}" data-email="{{$r->email}}" data-client="{{$r->clientid}}" data-content="{{$r->description}}" data-toggle="modal" data-target="#QuoteModal">Cotizacion</button>
                                 @endif
                             
                          </tr>
                          @endforeach
                       @endif 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
<!-- ----------- MODAL RESPONSE ------------------------ -->
  <div id="ResponseModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ResponseModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="ResponseModalLabel2">Respuesta de solicitud Web</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="requestid" id="requestid">
          <input type="hidden" name="processid" id="processid">
          <input type="hidden" name="email" id="email">
            <div class="form-group">
            
                  <label class=" control-label">Tipo de Respuesta</label>
                  <select name="tipo" id="tipo" class="form-control">
                    <option value="27"> Respuesta de informacion</option>
                    <option value="28"> Envio de cotizacion</option>
                    <option value="29"> Se crea Proceso</option>
                    <option value="30"> Se crea Visita por Mantenimiento</option>
                    <option value="31"> Se crea Visita por Soporte</option>
                  </select>
                  
            </div>
            <div class="form-group">
            
                  <label class=" control-label">Respuesta</label>
                  <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                  
            </div>
              
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default closerequest" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitresponse">Save changes</button>
          </div>
        </div>
      </div>
    </div>   
<!-- ----------- MODAL QUOTE ------------------------ -->
  <div id="QuoteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="QuoteModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="QuoteModalLabel2">Solicitud de Cotizacion de solicitud Web</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="requestidquote" id="requestidquote">
          <input type="hidden" name="clientidquote" id="clientidquote">
          
            
            <div class="form-group">
            
                  <label class=" control-label">Informacion del Cliente</label>
                  <textarea class="form-control" style="height:55px;" id="descrquote" name="descrquote" readonly="readonly"></textarea>
                  
            </div>
            <div class="form-group">
            
                  <label class=" control-label">Observacion para la Cotizacion</label>
                  <textarea class="form-control" style="height:55px;" id="descrcot" name="descrcot"></textarea>
                  
            </div>
              
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitquote">Save changes</button>
          </div>
        </div>
      </div>
    </div>         
@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
        $('#datatable-buttons').on('click','.response',function(){
          var id=$(this).attr('id'),
              processid=$(this).data('process'),
              email=$(this).data('content');
              
              $('#requestid').val(id);
              $('#processid').val(processid);
              $('#email').val(email);
              $('#descr').removeAttr('readonly');
              $('.closerequest').show();
              $('.submitresponse').show();
              
        });
        $('.submitresponse').click(function(){
          var requestid=$('#requestid').val(),
              processid=$('#processid').val(),
              email=$('#email').val(),
              tipo=$('#tipo').val(),
              response=$('#descr').val();
              
              console.log('response id:'+requestid+' processid: '+processid+' email: '+email+' respuesta: '+response+' tipo'+tipo);
              $.confirm({
                            title: '<strong>Enviar respuesta al cliente!!</strong>',
                            content: 'La respuesta se enviara al correo '+email+' del cliente',
                            confirmButton:'Enviar',
                            cancelButton:'Cancelar',
                            confirm: function(){

                               $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/responserequest', // This is the url we gave in the route
                                      data: {'requestid' : requestid,'processid':processid,'email':email,'response':response,'tipo':tipo}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          location.reload();

                                      }
                              });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
        });
       
        $('#datatable-buttons').on('click','.respuesta',function(){
           var id=$(this).attr('id');
            console.log('id'+id);
            $('.closerequest').hide();
            $('.submitresponse').hide();
           $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/findresponse', // This is the url we gave in the route
                                      data: {'requestid' : id}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          $('#descr').val(response.respuesta);
                                          $('#descr').attr('readonly','readonly');

                                      }
                              });

        });
        
        $('#datatable-buttons').on('click','.newcreate',function(e){
             
            alert('Recuerde que despues de creado el proceso o la visita debe responder la solicitud del cliente en el boton "Responder"');
        });

        $('#datatable-buttons').on('click','.quote',function(){
          var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              content=$(this).data('content');
              
              $('#requestidquote').val(id);
              $('#clientidquote').val(clientid);
              $('#descrquote').text(content);
              
              
        });
  
  // envio de solicitud de cotizacion       
        $('.submitquote').click(function(){
          var requestid=$('#requestidquote').val(),
              clientid=$('#clientidquote').val(),
              observation=$('#descrcot').val(),
              processid='';
              
              console.log('response id:'+requestid+' clientid: '+clientid+' observacion: '+observation);
              $.confirm({
                            
                            title: '<strong>Solicitud de Cotizacion!!</strong>',
                            content: 'La solicitud sera enviada con la observacion agregada al departamento Administrativo, de igual forma se le enviara un correo al cliente indicando que su solicitud esta en proceso de cotizacion y la misma sera enviada en 24 a 48 horas a su correo.',
                            confirmButton:'Aceptar',
                            cancelButton:'Cancelar',
                            confirm: function(){
                                   

                                        $.confirm({
                                           icon:'fa fa-spinner fa-spin',
                                          title:'En Proceso..',
                                          content:'La Solicitud esta en proceso, por favor espere...',
                                          confirmButton:false,
                                          cancelButton:false,
                                          closeIcon:false,
                                        });
                                         this.close(true);
                                         $.ajax({
                                                type: 'POST', // Type of response and matches what we said in the route
                                                url: APP_URL+'/quoterequest', // This is the url we gave in the route
                                                data: {'requestid' : requestid,'clientid':clientid,'observation':observation,'processid':processid}, // a JSON object to send back
                                                success: function(response){ // What to do if we succeed
                                                    console.log('respuesta',response);
                                                    
                                                    location.reload();

                                                }
                                        });

                                
                              
                            },
                            cancel:function(){
                               
                            }
                            
                        });

              

        });


        

      });
</script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {

       
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      

    });
    </script>
@endsection
