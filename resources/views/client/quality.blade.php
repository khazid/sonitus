@extends('app/navin')
@section('css')
  
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
   
    <!-- Select2 -->
    {!! Html::style('/vendors/select2/dist/css/select2.min.css') !!}
    
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}
    
    <!-- starrr -->
    {!! Html::style('/vendors/starrr/dist/starrr.css') !!}
    

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')<!-- page content -->

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.quality')!!}</h3>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.qualityinfo')!!}</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     {!! Form::open(['route' => 'client/savequality', 'method' => 'post','class'=>'form-horizontal form-label-left','data-parsley-validate','id'=>'formproject']) !!}
                   <input type="hidden" name="process_id" id="process_id" value="{{$process_id}}">
                   <input type="hidden" name="client_id" id="client_id" value="{{$clientid}}">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.service_quality')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="service" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="service" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="service" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="service" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="service" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.puntuality')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="punctuality" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="punctuality" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="punctuality" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="punctuality" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="punctuality" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.personal_attitude')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="attitude" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attitude" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attitude" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attitude" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attitude" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.attention')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="attention" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attention" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attention" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attention" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="attention" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.consultation')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="consultation" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="consultation" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="consultation" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="consultation" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="consultation" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.professionalism')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="professionalism" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="professionalism" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="professionalism" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="professionalism" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="professionalism" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.job_quality')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="job_quality" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="job_quality" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="job_quality" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="job_quality" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="job_quality" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.equipment_quality')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="equipment_quality" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="equipment_quality" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="equipment_quality" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="equipment_quality" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="equipment_quality" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.solutions')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="solutions" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="solutions" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="solutions" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="solutions" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="solutions" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.knowledge')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="knowledge" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="knowledge" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="knowledge" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="knowledge" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="knowledge" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.evaluation')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="evaluation" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="evaluation" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="evaluation" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="evaluation" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="evaluation" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.administrative')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="administrative" value="excellent"> {!!trans('form.label.excellent')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="administrative" value="good"> {!!trans('form.label.good')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="administrative" value="regular"> {!!trans('form.label.regular')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="administrative" value="deficient"> {!!trans('form.label.deficient')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="administrative" value="bad"> {!!trans('form.label.bad')!!}
                            </label>
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.work_again')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <div class="radio">
                            <label>
                              <input type="radio" class="flat" checked name="work_again" value="yes"> {!!trans('form.label.yes')!!}
                            </label>
                            <label>
                              <input type="radio" class="flat"  name="work_again" value="no"> {!!trans('form.label.no')!!}
                            </label>
                            
                          </div>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.why')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <textarea class="form-control" rows="3" id="work_reason" name="work_reason" maxlength="1000" required></textarea>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.satisfaction')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <textarea class="form-control" rows="3" id="satisfaction" name="satisfaction" maxlength="1000" required></textarea>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.less_satisfaction')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <textarea class="form-control" rows="3" id="less_satisfaction" name="less_satisfaction" maxlength="1000" required></textarea>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.comments')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                            <textarea class="form-control" rows="3" id="comments" name="comments" maxlength="1000" required></textarea>
                          
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          
                          <button type="submit" class="btn btn-success">{!!trans('form.label.submit')!!}</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div> <!-- row -->
        </div>
      </div>
          
        <!-- /page content -->

@endsection
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
  
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}

   <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
  
    <!-- Parsley -->
     {!! Html::script('/vendors/parsleyjs/dist/parsley.min.js') !!}
   
    <!-- Autosize -->
     {!! Html::script('/vendors/autosize/dist/autosize.min.js') !!}
         <!-- Switchery -->
    {!! Html::script('/vendors/switchery/dist/switchery.min.js') !!}

    

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <script>
      $(document).ready(function() {
        $(":input").inputmask();
        //-----validacion  antes de guardar-----
       

        $("#save").click(function(e) {
          
            var checked=false;
            $('.jobcheck:checked').each(function() {

                    checked=true;
            });

             if(!checked){
              
                  $.alert({
                        icon: 'fa fa-warning',
                        title: 'Atention!',
                        content: 'You must to select a "Job type"!!'
                        
                    });
            }
            return checked;
        }); 

        
      });
    </script>

    <!-- Parsley -->
    <script>
      $(document).ready(function() {
        $.listen('parsley:field:validate', function() {
          validateFront();
        });
        $('#formproject .btn').on('click', function() {
          $('#formproject').parsley().validate();
          validateFront();
        });
        var validateFront = function() {
          if (true === $('#formproject').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
          } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
          }
        };
      });

      try {
        hljs.initHighlightingOnLoad();
      } catch (err) {}
    </script>
    <!-- /Parsley -->

    <!-- Autosize -->
    <script>
      $(document).ready(function() {
        autosize($('.resizable_textarea'));
      });
    </script>
    <!-- /Autosize -->

@endsection