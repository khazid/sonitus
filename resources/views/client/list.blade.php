@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection    
@section('content')

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.clientcontact')!!}</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="{{ route('client.create') }}" class="btn btn-primary">{!!trans('form.label.new')!!}</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        <ul class="pagination pagination-split">
                        <li><a href="{{ route('client.index')}}">All</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'A'] )}}">A</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'B'] )}}">B</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'C'] )}}">C</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'D'] )}}">D</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'E'] )}}">E</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'F'] )}}">F</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'G'] )}}">G</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'H'] )}}">H</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'I'] )}}">I</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'J'] )}}">J</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'K'] )}}">K</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'L'] )}}">L</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'M'] )}}">M</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'N'] )}}">N</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'O'] )}}">O</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'P'] )}}">P</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'Q'] )}}">Q</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'R'] )}}">R</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'S'] )}}">S</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'T'] )}}">T</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'U'] )}}">U</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'V'] )}}">V</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'W'] )}}">W</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'X'] )}}">X</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'Y'] )}}">Y</a></li>
                          <li><a href="{{ route('client/searchletter',['id' => 'Z'] )}}">Z</a></li>
                        </ul>
                      </div>

                      <div class="clearfix"></div>
                      @foreach($clients as $c)
                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>{{$c->name}}</i></h4>
                            <div class="left col-xs-12">
                              <h2>{{$c->applicant_name}}</h2>
                              <p><strong>{{$c->charge}} </strong> </p>
                              <ul class="list-unstyled">
                                <li><i class="fa fa-briefcase"></i> RIF: {{$c->rif}} </li>
                                <li><i class="fa fa-envelope"></i> email: {{$c->email}} </li>
                                <li><i class="fa fa-building"></i> Address: {{$c->address}} </li>
                                <li><i class="fa fa-phone"></i> Phone #: {{$c->phone}} </li>
                              </ul>
                            </div>
                            <!-- <div class="right col-xs-5 text-center">
                              <img src="images/img.jpg" alt="" class="img-circle img-responsive">
                            </div> -->
                          </div>
                          <div class="col-xs-12 bottom">
                            
                            <div class="col-xs-12 col-sm-12 emphasis">
                              <a  class="btn btn-primary btn-xs" href="{{ route('client.show',['id' => $c->id] )}}">
                                <i class="fa fa-cogs"> </i> {!!trans('form.label.viewproject')!!}
                              </a>
                              <a  class="btn btn-primary btn-xs" href="{{ route('client/showQuote',['id' => $c->id] )}}">
                                <i class="fa fa-cogs"> </i> {!!trans('form.label.viewquo')!!}
                              </a>
                              <a href="{{ route('client/newproject',['id' => $c->id] )}}"  class="btn btn-success btn-xs">
                                <i class="fa fa-cog"> </i> {!!trans('form.label.newproject')!!}
                              </a>
                              <a href="#" data-name="{{$c->name}}" id="{{$c->id}}" class="btn btn-danger btn-xs delete">
                                <i class="fa fa-remove"> </i> {!!trans('form.label.deleteclient')!!}
                              </a>
                              @if($c->users_id=='')
                              <button type="button" class="btn btn-warning btn-xs newuser" id="{{$c->id}}" data-content="{{$c->name}}" data-client="{{$c->email}}"><i class="fa fa-user"></i> {!!trans('form.label.userreg')!!}</button>
                              @endif 

                              <button type="button" class="btn btn-default btn-xs quote" data-client="{{$c->id}}"  data-toggle="modal" data-target="#QuoteModal">Cotizacion</button>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<!-- ----------- MODAL QUOTE ------------------------ -->
  <div id="QuoteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="QuoteModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="QuoteModalLabel2">Solicitud de Cotizacion Nuevo Cliente</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="clientidquote" id="clientidquote">
          
            
            <div class="form-group">
            
                  <label class=" control-label">Observacion para la Cotizacion</label>
                  <textarea class="form-control" style="height:55px;" id="descrcot" name="descrcot"></textarea>
                  
            </div>
              
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitquote">Save changes</button>
          </div>
        </div>
      </div>
    </div>        
@endsection        
@section('script')
   
    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    
    <script>
    var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
        
        //create a new user
      $('.newuser').click(function(e) {
            var id= $(this).attr("id");
            var nombre= $(this).attr("data-content");
            var email= $(this).attr("data-client");
            var procurl = APP_URL+"/user/savenewuser";
            var type = "client";

           
            $.confirm({
                            title: '<strong>User Registration!!</strong>',
                            content: 'Are you sure about the user creation for: <b>'+nombre+'</b> ?</br> the system will send an email of the user information with an random password to: <b>'+email+'</b>',
                            confirmButton:'Yes',
                            cancelButton:'No',
                            confirm: function(){
                               $.confirm({
                                          icon:'fa fa-spinner fa-spin',
                                          title:'En Proceso..',
                                          content:'La Solicitud esta en proceso, por favor espere...',
                                          confirmButton:false,
                                          cancelButton:false,
                                          closeIcon:false,
                                        });
                                this.close(true);
                               $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: procurl, // This is the url we gave in the route
                                        data: {'id' : id,'name': nombre,'email': email,'type':type}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                           console.log('User id:'+response.created)
                                            location.reload();
                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });

      $('.delete').click(function(e) {
           
          var name=$(this).data('name'), 
                deleteurl=APP_URL+'/client/destroy', 
                id=$(this).attr('id');

           
            $.confirm({
                title: '<strong>Delete Client!!</strong>',
                content: 'Are you sure do you want to delete the Client: '+name,
                confirmButton:'Yes',
                cancelButton:'No',
                confirm: function(){
                   $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                    $.ajax({
                            type: 'POST', // Type of response and matches what we said in the route
                            url: deleteurl, // This is the url we gave in the route
                            data: {'id' : id}, // a JSON object to send back
                            success: function(response){ // What to do if we succeed
                               console.log('deleted :',response.deleted)
                               if(response.deleted!='404'){
                                $.confirm({
                                  title: '<strong>Eliminacion de Cliente Realizada!!</strong>',
                                  content:'El cliente ya fue desactivado del sistema, puede activarlo nuevamente desde el listado de Clientes eliminados, y se le debera crear un nuevo usuario',
                                  cancelButton:false,
                                  confirm:function(){
                                     location.reload();
                                  }
                                });
                               
                              }else{
                                $.alert({
                                  title: '<strong>Eliminacion de Cliente Rechazado!!</strong>',
                                  content:'No se pudo eliminar el Cliente ya que el mismo tiene Procesos Activos, Cierre o culmine los procesos para que pueda ser eliminado',
                                });
                              }
                                
                            }
                              
                        });
                },
                cancel:function(){
                   
                }
                
            });
             
            
        });
      $('.quote').click(function(){
          var clientid=$(this).data('client');
              $('#clientidquote').val(clientid);
              
              
              
        });
        $('.submitquote').click(function(){
          var requestid='',
              processid='',
              clientid=$('#clientidquote').val(),
              observation=$('#descrcot').val();
              
              console.log('response id:'+requestid+' clientid: '+clientid+' observacion: '+observation);
              $.confirm({
                            title: '<strong>Solicitud de Cotizacion!!</strong>',
                            content: 'La solicitud sera enviada con la observacion agregada al departamento Administrativo.',
                            confirmButton:'Aceptar',
                            cancelButton:'Cancelar',
                            confirm: function(){
                               $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                              });
                               $.ajax({
                                      type: 'POST', // Type of response and matches what we said in the route
                                      url: APP_URL+'/quoterequest', // This is the url we gave in the route
                                      data: {'requestid' : requestid,'clientid':clientid,'observation':observation,'processid':processid}, // a JSON object to send back
                                      success: function(response){ // What to do if we succeed
                                          console.log('respuesta',response);
                                          location.reload();

                                      }
                              });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
        });
    });
    </script>
@endsection
