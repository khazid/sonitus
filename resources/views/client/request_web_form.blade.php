@extends('app/navin')
@section('css')
  
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
   
    <!-- Select2 -->
    {!! Html::style('/vendors/select2/dist/css/select2.min.css') !!}
    
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}
    
    <!-- starrr -->
    {!! Html::style('/vendors/starrr/dist/starrr.css') !!}
    

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')<!-- page content -->

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>New Project</h3>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>New Project <small>information about de project</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     {!! Form::open(['route' => 'process.store', 'method' => 'post','class'=>'form-horizontal form-label-left','data-parsley-validate','id'=>'formproject']) !!}
                   <input type="hidden" name="clientid" id="clientid">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Corporate Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" required="required" type="text" value="{{@$client->name}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">RIF <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                              <select name="riftype" id="riftype" class="form-inline">
                              <option value="J">J</option>
                              <option value="V">V</option>
                              <option value="P">P</option>
                              <option value="G">G</option>
                              <option value="E">E</option>
                              <option value="C">C</option>
                              </select>
                            </span>
                            <input id="rif" name="rif"type="text" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99999999-9'" value="{{@$client->rif}}">
                            
                            
                          </div><!-- /input-group -->
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="address" class="form-control col-md-7 col-xs-12" type="text" name="address" required value="{{@$client->address}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contact Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="applicant_name" name="applicant_name" required="required" class="form-control col-md-7 col-xs-12" required value="{{@$client->applicant_name}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Charge <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="charge" id="charge" class="form-control">
                              <option value="Dueño">Dueño</option>
                              <option value="Jefe">Jefe</option>
                              <option value="Encargado">Encargado</option>
                              <option value="Gerente">Gerente</option>
                              <option value="Empleado">Empleado</option>
                              <option value="Autorizado">Autorizado</option>
                              </select>
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" class="form-control" name="email" data-parsley-trigger="change" required value="{{@$client->email}}"/>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Phone <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="telephone" name="telephone" type="text" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '+(99999)999-9999 / +(99999)999-9999'" required="required" value="{{@$client->phone}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Request Type: <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="type" id="type" class="form-control">
                              <option value="Budget">Presupuesto</option>
                              <option value="Proyect">Projecto</option>
                              <option value="Encargado">Encargado</option>
                              <option value="Gerente">Gerente</option>
                              <option value="Empleado">Empleado</option>
                              <option value="Autorizado">Autorizado</option>
                              </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Request Description: <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="3" id="description" name="description" placeholder='Descripcion del trabajo a realizar' required></textarea>
                        </div>
                      </div>
      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>Cancel</a>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div> <!-- row -->
        </div>
      </div>
          
        <!-- /page content -->

@endsection
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
  
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}

   <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
  
    <!-- Parsley -->
     {!! Html::script('/vendors/parsleyjs/dist/parsley.min.js') !!}
   
    <!-- Autosize -->
     {!! Html::script('/vendors/autosize/dist/autosize.min.js') !!}
         <!-- Switchery -->
    {!! Html::script('/vendors/switchery/dist/switchery.min.js') !!}

    

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <script>
      $(document).ready(function() {
        $(":input").inputmask();
        //-----validacion  antes de guardar-----
       $('#rif').focusout(function(e){
          var rif = $('#riftype').val()+$(this).val();
          var APP_URL = {!! json_encode(url('/')) !!};    
               $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                $.ajax({
                        type: 'POST', // Type of response and matches what we said in the route
                        url: APP_URL+'/process/searchclient', // This is the url we gave in the route
                        data: {'rif' : rif}, // a JSON object to send back
                        success: function(response){ // What to do if we succeed
                           console.log('Client:',response.client)
                           if(response.client!='null'){
                               $('#name').val(response.client['name']);
                               $('#address').val(response.client['address']);
                               $('#applicant_name').val(response.client['applicant_name']);
                               $('#dpto').val(response.client['departmen']);
                               $('#email').val(response.client['email']);
                               $('#telephone').val(response.client['phone']);
                               $('#clientid').val(response.client['id']);
                               $('.branches').show();
                          }else{
                               //$('#name').val(response.client['name']);
                               $('#address').val(response.client['address']);
                               $('#applicant_name').val(response.client['applicant_name']);
                               $('#dpto').val(response.client['departmen']);
                               $('#email').val(response.client['email']);
                               $('#telephone').val(response.client['phone']);
                               $('#clientid').val(response.client['id']);
                               $('.branches').hide();
                          }
                        }
                    });
             

        });


        
      });
    </script>

    <!-- Parsley -->
    <script>
      $(document).ready(function() {
        $.listen('parsley:field:validate', function() {
          validateFront();
        });
        $('#formproject .btn').on('click', function() {
          $('#formproject').parsley().validate();
          validateFront();
        });
        var validateFront = function() {
          if (true === $('#formproject').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
          } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
          }
        };
      });

      try {
        hljs.initHighlightingOnLoad();
      } catch (err) {}
    </script>
    <!-- /Parsley -->

    <!-- Autosize -->
    <script>
      $(document).ready(function() {
        autosize($('.resizable_textarea'));
      });
    </script>
    <!-- /Autosize -->

@endsection