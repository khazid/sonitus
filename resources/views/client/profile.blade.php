@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}

    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}

@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.clientprofile')!!}</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  <input type="hidden" name="masterid" id="masterid" value="{{$masterid}}">
                  <input type="hidden" name="clientid" id="clientid" value="{{$client->id}}">
                    <span class="input-group-addon" >{!!trans('form.label.branche')!!}:</span>
                    {!! Form::select('branches', $branches,null,['class' => 'form-control','id'=>'branches']) !!}
                    <span class="input-group-btn">
                      <a  class="btn btn-primary" href="#" id="searchbranch">Go!</a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.report')!!} <small>{!!trans('form.label.activityreport')!!}</small></h2>
                    
                    <div class="clearfix"></div>
                    @include('partials.layout.errors')
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      
                      <h2>{{ $client->name}}</h3>
                      <h3>{{$client->applicant_name}}</h2>
                      <p><strong>{{$client->charge}} </strong> </p>
                      <ul class="list-unstyled">
                                <li><i class="fa fa-envelope"></i> {!!trans('form.label.email')!!}: {{$client->email}} </li>
                                <li><i class="fa fa-building"></i> {!!trans('form.label.address')!!}: {{$client->address}} </li>
                                <li><i class="fa fa-phone"></i> {!!trans('form.label.phone')!!} #: {{$client->phone}} </li>
                      </ul>
                      <a class="btn btn-success" title='Edit' href="{{ route('client.edit',['id' => $client->id] )}}" ><i class="fa fa-edit m-right-xs"></i> {!!trans('form.label.editprofile')!!}</a>
                      <a class="btn btn-warning" title='Nueva solicitud de Proyecto' href="#" data-toggle="modal" data-target="#requestModal"  data-client="{{$client->id}}" data-tipo="newproject">{!!trans('form.label.newrequest')!!}</a>
                      
                      <br />

                      <!-- start skills -->
                     
                      <!-- end of skills -->
                      <!-- si el rol no es de cliente -->
                      @if($rol!=8)
                     <a href="{{route('client.index')}}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!!trans('form.label.back')!!}!</a>
                      @endif
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
                          <h2>{!!trans('form.label.clientpro')!!}</h2>
                        </div>
                      </div>
                      

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <!-- start user projects -->
                            <table class="data table table-striped no-margin">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>{!!trans('form.label.projectdescription')!!}</th>
                                  <th>{!!trans('form.label.projectstatus')!!}</th>
                                  <!--<th>{!!trans('form.label.requestclient')!!}</th>
                                  <th class="hidden-phone">{!!trans('form.label.hourspend')!!}</th>-->
                                  
                                  <th>{!!trans('form.table.action')!!}</th>
                                  
                                  
                                </tr>
                              </thead>
                              <tbody>
                              @foreach($projects as $pro)
                                <tr>
                                  <td><a class="btn btn-primary" title='Ver Info.' href="{{ route('process/show',['id' => $pro->id,'type'=>'client'] )}}" >{{$pro->process_number}} </a></td>
                                  <td>{{$pro->description}}</td>
                                  <td>{{$pro->status}}</td>
                                  <!--<td><b style="color: red;">{{($pro->have_request? 'Yes':'No')}}</b></td>
                                  <td class="hidden-phone">{{$pro->total}} Hours</td>-->
                                  <td>
                                @if($rol==8)
                                  <a class="btn btn-success" title='Nueva solicitud de Mantenimiento o Soporte para el proyecto' href="#" data-toggle="modal" data-target="#requestModal" data-id="{{$pro->id}}" data-client="{{$client->id}}" data-tipo="newreq">{!!trans('form.label.newrequest')!!}</a>
                                    @if($pro->status_id==5 && (!$pro->quality || $pro->quality==''))
                                      <a class="btn btn-default" title='Encuesta de Calidad' href="{{ route('client/quality',['id' => $pro->id] )}}" data-id="{{$pro->id}}" data-client="{{$client->id}}">{!!trans('form.label.quality')!!}</a>
                                    @elseif($pro->status_id==5 && ($pro->quality))
                                      <a class="btn btn-success" title='ver Encuesta de Calidad' href="{{ route('client/showquality',['id' => $pro->id] )}}" data-id="{{$pro->id}}" data-client="{{$client->id}}" target="_blank">{!!trans('form.label.quality')!!}</a>
                                    @endif
                                @else
                                    @if($pro->status_id==5 && (!$pro->quality || $pro->quality==''))
                                      <span>Aun no se ha llenado la Encuesta de Calidad</span>
                                    @elseif($pro->status_id==5 && ($pro->quality))
                                      <a class="btn btn-success" title='ver Encuesta de Calidad' href="{{ route('client/showquality',['id' => $pro->id] )}}" data-id="{{$pro->id}}" data-client="{{$client->id}}" target="_blank">{!!trans('form.label.quality')!!}</a>
                                    @endif
                                @endif
                                  </td>
                                </tr>
                               @endforeach 
                              </tbody>
                            </table>
                            <!-- end user projects -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
 {!! Form::open(['route' => 'client/request', 'method' => 'post','class'=>'form-horizontal','validate']) !!}       
<!------------- ENVIO DE REQUERIMIENTO DE MANTENIMIENTO O SOPORTE------- -->
<div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="requestModalLabel">
              <div class="modal-dialog modal-lg" >
                <div class="modal-content" >
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="requestModalLabel"></h4>
                  </div>
                  <div class="modal-body">
                    
                  {!! Form::hidden('process_id',null,['id'=>'process_id']) !!}
                  {!! Form::hidden('client_id',null,['id'=>'client_id']) !!}
                      <div class="form-group">
                        <label for="recipient-name" class="control-label">{!!trans('form.label.type')!!}:</label>
                        <select name="tipo" id="tipo" class="form-control newreq" style="display: block;">
                          <option value="Informacion">{!!trans('form.label.information')!!}</option>
                          <option value="Soporte">{!!trans('form.label.techsupport')!!}</option>
                          <option value="Mantenimiento">{!!trans('form.label.mainteinance')!!}</option>

                        </select>

                        <select name="tipo" id="tipo" class="form-control input-sm newproject" style="display: none;">
                              <option value="Presupuesto">Presupuesto</option>
                              <option value="Proyecto">Cotizar Proyecto</option>
                              <option value="Informacion">Informacion</option>
                              <option value="Soporte">Soporte</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="recipient-name" class="control-label">{!!trans('form.label.request')!!}:</label>
                        {!! Form::text('newrequest', null, ['class' => 'form-control' ,'id'=>'request','required']) !!}
                      </div>
                  </div>
                  <div class="modal-footer">
                    <input type="submit" id="savebutton" class="btn btn-success" value="Enviar solicitud">
                  </div>
                </div>
              </div>
</div>
{!! Form::close() !!}
@endsection 
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- morris.js -->
    {!! Html::script('/vendors/raphael/raphael.min.js') !!}
    {!! Html::script('/vendors/morris.js/morris.min.js') !!}
    <!-- bootstrap-progressbar -->
    {!! Html::script('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}
    <!-- bootstrap-daterangepicker -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/datepicker/daterangepicker.js') !!}
    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}
     <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

<script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!};
 $(document).ready(function() {
  var masterid=$('#masterid').val();

    $('#branches').append('<option value='+masterid+'>Master Branch</option>');

    $('#branches').change(function(){
      var id=$('#branches').val(),
          ruta=APP_URL+"/client/"+id;
          //console.log('id branche:'+id);

       $('#searchbranch').attr('href',ruta);
       $('#searchbranch').trigger('click');
       //alert($('#searchbranch').attr('href'));
    });
   $('#requestModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Botón que activó el modal
      var id = button.data('id'); // Extraer la información de atributos de datos
      var client=button.data('client');
      var tipo=button.data('tipo');
      var modal = $(this);
      modal.find('#process_id').val(id);
      modal.find('#client_id').val(client);
      
      if(tipo=="newreq"){
        $(".newreq").show();
        $(".newproject").hide();
        $('#requestModalLabel').text("{!!trans('form.label.clientrequest')!!}");

      }else if(tipo=="newproject"){
        $(".newreq").hide();
        $(".newproject").show();
        
         $('#requestModalLabel').text("{!!trans('form.label.newproject')!!}");
      }

    });
var clientid={!! $client->id !!};
   $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
   $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: APP_URL+'/app/alertas', // This is the url we gave in the route
                      data: {clientid}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      console.log('Alerts: ',response);
                     
                          reqst='';
                      console.log(response.requestclient.length);
                          if(response.requestclient.length>0){
                            reqst='<h2>Solicitudes:</h2> <br />';
                              $.each(response.requestclient, function(i, item) {
                                reqst += i+")"+ response.requestclient[i]+"<br />";
                               
                              });   

                          //--- se arma el modal con la info de las alertas ----
                              $.confirm({
                                      title: '<strong>Notificacion de Solicitudes Pendiente!!</strong>',
                                      content:'Estimado Cliente tiene las siguientes solicitudes pendientes para la continuación del proyecto, presione el boton del numero del proyecto para ver la solicitud en la pestaña "Request"<br/> '+reqst,
                                      confirmButton:false,
                                      cancelButton:'OK',
                                      cancel:function(){
                                         
                                      }
                                      
                                  });
                          }
           
                         
                      }
                  });


  });
</script>

@endsection