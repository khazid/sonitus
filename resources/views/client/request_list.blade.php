@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}

  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.client')!!} <small>{!!trans('form.label.requestclient')!!}</small></h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                          <tr>
                              <th>{!!trans('form.table.project')!!}</th>
                              <th>{!!trans('form.table.client')!!}</th>
                                <th>{!!trans('form.table.date')!!}</th>
                                <th width="40%">{!!trans('form.table.request')!!}</th>
                                <th>{!!trans('form.table.tecnicians')!!}</th>
                                <th>{!!trans('form.table.status')!!}</th>
                                <th>{!!trans('form.table.action')!!}</th> 

                          </tr>
                      </thead>
                      <tbody>
                          @if(count(@$requestclient)>0)
                          @foreach($requestclient as $r)
                          <tr>
                              <td>{{ $r->process_number}}</td>
                              <td>{{ $r->client}}</td>
                              <td>{{ $r->date }}</td>
                              <td id="request">{{ $r->request }}</td>
                              <td>{{ $r->name }}</td>
                              <td>{{ $r->status }}</td>
                              <td>
                              
                              @if($r->statusid ==14 && $rol < 5)
                                  <button type="button" class="btn btn-success btn-xs accept" id="{{$r->id}}" data-client="{{$r->client_id}}" data-personal="{{$r->users_id}}" data-visit="{{$r->visitid}}" data-content="{{$r->request}}"><i class="glyphicon glyphicon-thumbs-up"></i></button>
                                  
                                  <button type="button" class="btn btn-danger btn-xs denied" data-toggle="modal" data-target="#CalenderModalEdit" id="{{$r->id}}" data-client="{{$r->client_id}}" data-personal="{{$r->users_id}}" data-visit="{{$r->visitid}}"><i class="glyphicon glyphicon-thumbs-down"></i></button>
                                  
                                  <button type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-target="#CalenderModalEdit" id="{{$r->id}}" data-content="{{$r->request}}" data-client="{{$r->client_id}}" data-personal="{{$r->users_id}}" data-visit="{{$r->visitid}}"><i class="glyphicon glyphicon-pencil"></i></button>
                              @endif

                              @if($r->statusid ==15 )
                                <button type="button" class="btn btn-warning btn-xs notify" id="{{$r->id}}" data-client="{{$r->client_id}}" data-personal="{{$r->users_id}}" data-visit="{{$r->visitid}}" data-content="{{$r->request}}"><i class="glyphicon glyphicon-bell"></i></button>
                              @endif   
                             
                          </tr>
                          @endforeach
                       @endif 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->
<div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2"></h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="requestid" id="requestid">
          <input type="hidden" name="clientid" id="clientid">
          <input type="hidden" name="personalid" id="personalid">
          <input type="hidden" name="visitid" id="visitid">
          <input type="hidden" name="type" id="type">
      
            <div id="testmodal" style="padding: 5px 20px;">
              <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label reason">Description</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default closerequest" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary submitrequest">Save changes</button>
          </div>
        </div>
      </div>
    </div> 
          
@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {

        function modrequest(id,description,clientid,personalid,type,visitid){
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              type: 'POST', // Type of response and matches what we said in the route
              url: APP_URL+'/visits/requesttoclient', // This is the url we gave in the route
              data: {'id' : id,'description': description,'clientid': clientid, 'personalid':personalid, 'type':type, 'visitid': visitid}, // a JSON object to send back
              success: function(response){ // What to do if we succeed
                 console.log('request id:',response.requestclient)
                  //location.reload();
                  $('#request').text(response.requestclient.request);

              }
            });
          $('.closerequest').click();
        }
        
        $('.submitrequest').click(function(){
           var id=$('#requestid').val(),
              clientid=$('#clientid').val(),
              personalid=$('#personalid').val(),
              description=$('#descr').val(),
              visitid=$('#visitid').val(),
              type=$('#type').val();
              console.log('Submit id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type+' description: '+description);
            modrequest(id,description,clientid,personalid,type,visitid);
        });
    
        $('#datatable-buttons').on('click','.accept',function(){
          var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              description=$(this).data('content'),
              visitid=$(this).data('visit'),
              type='accept';
              console.log('Accept id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type+' description: '+description);
              $.confirm({
                            title: '<strong>Aceptar y enviar Solicitud Al Cliente!!</strong>',
                            content: 'Esta seguro que desea enviar la solicitud al cliente?',
                            confirmButton:'Si',
                            cancelButton:'No',
                            confirm: function(){
                               modrequest(id,description,clientid,personalid,type,visitid);
                            },
                            cancel:function(){
                               
                            }
                            
                        });
        });

        $('#datatable-buttons').on('click','.edit',function(){
          var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              visitid=$(this).data('visit'),
              description=$(this).data('content'),
              type='edit';
              console.log('Edit id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type+' description: '+description);
              $('#requestid').val(id);
              $('#clientid').val(clientid);
              $('#personalid').val(personalid);
              $('#descr').val(description);
              $('#visitid').val(visitid);
              $('#type').val(type);
              $('.modal-header h4').text('Edit Request to Client');
              $('.reason').text('Description');

        });
        
        $('#datatable-buttons').on('click','.denied',function(){
             var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              visitid=$(this).data('visit'),
              type='denied';
              console.log('Denied id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type);
              $('#requestid').val(id);
              $('#clientid').val(clientid);
              $('#personalid').val(personalid);
              $('#visitid').val(visitid);
              $('#type').val(type);
              $('.modal-header h4').text('Reject Request to Client');
              $('.reason').text('Observation');
            
        });

        $('#datatable-buttons').on('click','.done',function(){
            var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              visitid=$(this).data('visit'),
              type='done';
              console.log('Done id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type);
            modrequest(id,'',clientid,personalid,type,visitid);
        });

        $('#datatable-buttons').on('click','.notify',function(){
            var id=$(this).attr('id'),
              clientid=$(this).data('client'),
              personalid=$(this).data('personal'),
              description=$(this).data('content'),
              visitid=$(this).data('visit'),
              type='notify';
              console.log('Accept id:'+id+' clientid: '+clientid+' personalid: '+personalid+' visitid: '+visitid+' type:'+type+' description: '+description);
              $.confirm({
                            title: '<strong>Notificar al Cliente!!</strong>',
                            content: 'Seguro que desa notificar al cliente sobre esta solicitud?',
                            confirmButton:'Si',
                            cancelButton:'No',
                            confirm: function(){
                               modrequest(id,description,clientid,personalid,type,visitid);
                            },
                            cancel:function(){
                               
                            }
                            
                        });
        });

        

      });
</script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {

       
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      

    });
    </script>
@endsection
