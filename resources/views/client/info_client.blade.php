@extends('app/navin')
@section('css')
  
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-wysiwyg -->
    {!! Html::style('/vendors/google-code-prettify/bin/prettify.min.css') !!}
   
    <!-- Select2 -->
    {!! Html::style('/vendors/select2/dist/css/select2.min.css') !!}
    
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}
    
    <!-- starrr -->
    {!! Html::style('/vendors/starrr/dist/starrr.css') !!}
    

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
@endsection
@section('content')<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Client</h3>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.'.$title)!!}</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     {!! Form::open(['route' => $ruta, 'method' => 'post','class'=>'form-horizontal form-label-left','data-parsley-validate','id'=>'formproject']) !!}
                    <input type="hidden" name="client_id" value="{{@$client->id}}">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.name')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" required="required" type="text" value="{{@$client->name}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{!!trans('form.label.rif')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="input-group">
                            
                            <span class="input-group-addon" id="basic-addon1">
                            @if($update!='si')
                              <select name="riftype" id="riftype" class="form-inline">
                              <option value="J">J</option>
                              <option value="V">V</option>
                              <option value="P">P</option>
                              <option value="G">G</option>
                              <option value="E">E</option>
                              <option value="C">C</option>
                              </select>
                            @endif  
                            </span>
                            
                            @if($update!='si')
                            <input id="rif" name="rif"type="text" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99999999-9'">
                            @else
                            <input id="rif" name="rif"type="text" class="form-control col-md-7 col-xs-12"  value="{{@$client->rif}}">
                            @endif  
                            
                          </div><!-- /input-group -->
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">{!!trans('form.label.address')!!} <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="address" class="form-control col-md-7 col-xs-12" type="text" name="address" required value="{{@$client->address}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.contactname')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="applicant_name" name="applicant_name" class="form-control col-md-7 col-xs-12" required value="{{@$client->applicant_name}}">
                        </div>
                      </div>
                      <div class="form-group" id="divcharge">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.charge')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="charge" name="charge" required="required" class="form-control col-md-7 col-xs-12" required value="{{@$client->charge}}">
                        </div>
                      </div>
                      <div class="form-group" id="divdepartment">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dpto/Centro/Proyecto: <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="dpto" name="dpto" required="required" class="form-control col-md-7 col-xs-12" value="{{@$client->departmen}}">
                        </div>
                      </div>
                      <div class="form-group" id="divheadquarters">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.headquarters')!!} <span class="required" >*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="sede" name="sede" required="required" class="form-control col-md-7 col-xs-12" value="{{@$client->headquarters}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.email')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" class="form-control" name="email" data-parsley-trigger="change" required value="{{@$client->email}}" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.label.phone')!!} <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="telephone" name="telephone" type="text" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '+(99999)999-9999 / +(99999)999-9999'" required="required" value="{{@$client->phone}}">
                        </div>
                      </div>

                      @if($update=='si')
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.email.newpass')!!} <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="password" name="nuevaclave" class="form-control" id="nuevaclave" aria-describedby="basic-addon3" >
                        </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{!!trans('form.email.confirmpass')!!} <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="password" name="confirmclave" class="form-control" id="confirmclave" aria-describedby="basic-addon3" >
                        </div>
                      </div>
                      @endif

                     <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>{!!trans('form.label.cancel')!!}</a>
                          <button type="submit" class="btn btn-success save">{!!trans('form.label.submit')!!}</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div> <!-- row -->
        </div>
      </div>
          
        <!-- /page content -->

@endsection
@section('script')
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
  
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}

   <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
  
    <!-- Parsley -->
     {!! Html::script('/vendors/parsleyjs/dist/parsley.min.js') !!}
   
    <!-- Autosize -->
     {!! Html::script('/vendors/autosize/dist/autosize.min.js') !!}

    

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}
<script type="text/javascript">
$(document).ready(function() {
   $(":input").inputmask();

   $( "#riftype" ).change(function() {
     var tipo= $( "#riftype" ).val();
     var nombre=$('#name').val();

     if(tipo=='V' || tipo=='E'){
        $("#applicant_name").val(nombre);
        $( "#charge" ).removeAttr('required');
        $( "#charge" ).val('-');
        $( "#divcharge" ).hide();
        
        
        $( "#dpto" ).removeAttr('required');
        $( "#dpto" ).val('-');
        $( "#divdepartment" ).hide();
        
       
        $( "#sede" ).removeAttr('required');
        $( "#sede" ).val('-');
        $( "#divheadquarters" ).hide();
        
     }else{

        $("#applicant_name").val('');
        $( "#charge" ).prop( "required", 'required');
        $( "#divcharge" ).show();
        $( "#charge" ).val('');
        
        $( "#dpto" ).prop( "required", 'required');
        $( "#dpto" ).val('');
        $( "#divdepartment" ).show();
       
        $( "#sede" ).prop( "required", 'required');
        $( "#sede" ).val('');
        $( "#divheadquarters" ).show();

     }
    });
});
</script>
@endsection