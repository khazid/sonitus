<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Inversiones Sonitus! | </title>

    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
     {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
   {!! Html::style('/build/css/custom.min.css') !!}
   {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            {!! Form::open(['route' => 'auth/login']) !!}
              <h1><a href="{!!env('WEB_PAGE') !!}"><img src="{{ url('build/css/img/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;"></a></h1>
              <div class="form-group">
                <input name="email" id="email" type="text" class="form-control" placeholder="{!!trans('form.login.username')!!}" required="" />
                <input name="password" id="password" type="password" class="form-control" placeholder="{!!trans('form.login.password')!!}" required="" />

              </div>
              {!! Recaptcha::render() !!}
              {!! Form::submit(trans('form.login.submit'),['class' => 'btn btn-default','style'=>'float:right']) !!}
              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />
                <a  title='Reset Password' href="#" data-toggle='modal' data-target="#passwordModal">Forgot Password?</a>
                <div>
                  <!-- <h1><img src="{{ url('build/css/img/LOGOVECTORIZADO3.png')}}" alt="logo_header" style="width:130px;height: 33px;"></h1> -->
                  <p>©2016 All Rights Reserved. Inversiones Sonitus. Privacy and Terms</p>
                </div>
              </div>
            {!! Form::close() !!}
          </section>
        </div>
      </div>
    </div>


<!------------- Cambio de contraseña------- -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel">
              <div class="modal-dialog modal-lg" >
              
                <div class="modal-content" >
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="passwordModalLabel">Reset Password</h4>
                  </div>
                  <div class="modal-body">

                    <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon3">Email</span>
                          <input type="text" name="emailpass" class="form-control" id="emailpass" aria-describedby="basic-addon3" >
                        </div>
                     
                         
                      </div>
                  </div>
                 <div class="modal-footer">
                    
                    <input type="submit" class="btn btn-success save" value="Save">
                  </div>
                </div>
                
              </div> 
</div>
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}
        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}  
<script type="text/javascript">
     var APP_URL = {!! json_encode(url('/')) !!};
$(document).ready(function() { 

   $(".save").click(function(e) {
          
           var email=$('#emailpass').val(),
               procurl = APP_URL+"/resetpass";

            if(email!=''){
                           
                        $.confirm({
                              title: '<strong>Seguro que desea actualizar su clave de acceso ?</strong>',
                              content: 'El sistema le enviara un correo con su nueva clave aleatoria a: <b>'+email+'</b>',
                              confirmButton:'Yes',
                              cancelButton:'No',
                              confirm: function(){
                                 $.ajaxSetup({
                                          headers: {
                                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                          }
                                      });
                                  $.ajax({
                                          type: 'POST', // Type of response and matches what we said in the route
                                          url: procurl, // This is the url we gave in the route
                                          data: {'email':email}, // a JSON object to send back
                                          success: function(response){ // What to do if we succeed
                                             console.log('pass updated:'+response.updated)
                                             if(response.updated!=404){
                                               location.href = response.urlresponse;
                                             }else{
  
                                              $.alert({
                                                    icon: 'fa fa-warning',
                                                    title: 'Atencion!',
                                                    content: 'El correo electronico no se encuentra registrado, por favor verifique la informacion e intente nuevamente!!'
                                                    
                                                });
                                             }
                                          }
                                      });
                              },
                              cancel:function(){
                                 
                              }
                              
                          });
                        
                        }else{
                          $.alert({
                                    icon: 'fa fa-warning',
                                    title: 'Atention!',
                                    content: 'There is empty information!!'
                                    
                                });
                              return false;
                        }
            
            
        }); 
    
    });
    </script>
  </body>
</html>
