@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}

    <!-- Add fancyBox main JS and CSS files -->
    {!! Html::style('assets/fancy/jquery.fancybox.css') !!}

<style type="text/css">


.thumbnail {
    position: relative;
    padding: 0px;
    margin-bottom: 20px;
    background-color: #333;
}

</style>  
@endsection

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.visit')!!}  <small>{!!trans('form.label.detail')!!}</small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for..."> -->
                    <span class="input-group-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>Back!</a>
                      <!-- <button class="btn btn-default" type="button">Go!</button> -->
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.visitmadeon')!!} {{date("l jS F Y",strtotime($visits->date))}}</h2>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <ul class="stats-overview">
                        <li>
                          <span class="name"> {!!trans('form.label.startevents')!!} </span>
                          <span class="value text-success">{{$visits->entrance_hr}} </span>
                        </li>
                        <li>
                          <span class="name"> {!!trans('form.label.endevents')!!} </span>
                          <span class="value text-success">{{$visits->exit_hr}}</span>
                        </li>
                        <li class="hidden-phone">
                          <span class="name"> {!!trans('form.label.visitduration')!!} </span>
                          <span class="value text-success"> {{$visits->total}} Hours </span>
                        </li>
                      </ul>
                      <br />
                      <!-- info de la visita -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            
                            <div class="form-group " >
                              {!! Form::label('jobtypelabel', trans('form.label.jobtype'),['class' => 'control-label col-xs-2 col-lg-2']) !!}
                                  <div class="col-xs-9 visit-info" > 
                                      <div class="checkbox">    
                                        @foreach($jobt as $job)
                                        <div class="col-xs-4 col-sm-4 col-lg-3"> 
                                        <p>{!!$job->name!!}</p>  
                                        </div>               
                                        @endforeach
                                      </div>  
                                  </div>          
                            </div>
                            <div class="form-group ">
                              {!! Form::label('generallabel', trans('form.label.generalinfo'),['class' => 'control-label col-xs-2 col-lg-2']) !!}
                                  <div class="col-xs-9 visit-info"> 
                                      <div class="checkbox"> 

                                        @foreach($general as $gen)
                                        <div class="col-xs-4 col-sm-4 col-lg-3"> 
                                        <p>{!!$gen->name!!}</p>  
                                        </div>             
                                        @endforeach
                                      </div>  
                                  </div>          
                            </div> 
                            <div class="form-group ">
                            <label class="control-label col-xs-2">{!! trans('form.label.generaldescription')!!}</label>
                                <div class="col-xs-9 visit-info">
                                        @foreach($gentype as $gt) <!--- recorrido de los tipos --> 
                                          <div class="col-xs-4 col-sm-4 col-lg-3">
                                          <label > {{$gt->type}} </label>
                                              
                                                @foreach($generaltype as $gent) <!--- recorrido de los names -->
                                                    @if($gt->type==$gent->type)
                                                    <div class="checkbox"> 
                                                      
                                                      {!!$gent->name!!}
                                                     </div>
                                                    @endif
                                                    
                                                    
                                                @endforeach
                                            
                                          </div>   
                                        @endforeach
                                </div>
                          </div>
                          <div class="form-group">
                                  {!! Form::label('observationlabel', trans('form.label.observation'),['class' => 'control-label col-xs-2 col-lg-2']) !!}
                                  <div class="col-xs-9 visit-info"> 
                                        {{$visits->description}}
                                  </div>          
                                </div>
                        </div><!-- class="col-md-12 col-sm-12 col-xs-12" -->
                        <!-- End info de la visita -->
                        </br>
                        <!-- Fotos de la visita -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                         
                         <div class="x_title">
                            <h2>{!!trans('form.label.pictures')!!} </h2>
                            
                            <div class="clearfix"></div>
                          </div>
                         
                          @if(count(@$picturestake)>0)
                                     
                              @foreach($picturestake as $foto)
                              
                              <div class="col-xs-18 col-sm-6 col-md-3" id="pic{{$foto->id}}" >
                                  <div class="thumbnail">
                                      <a class="fancybox" href="{{url('images/equipment/').'/'.$process->processid.'/'.$visits->id.'/'.$foto->file}}" data-fancybox-group="gallery" title="{{$foto->description}}">
                                      <img src="{{url('images/equipment/').'/'.$process->processid.'/'.$visits->id.'/'.$foto->file}}" alt="" width='70%' />
                                      </a>
                                      <div class="caption">
                                          <p><b>{!!trans('form.label.description')!!}: </b>{{$foto->description}}</p>
                                      </div>
                                  </div>
                                  
                              </div>
                              
                              @endforeach
                          @else
                          <div class="alert alert-warning">
                            <strong> No hay fotos que mostrar.</strong>
                          </div>
                            
                          @endif 
                        </div><!-- class="col-md-12 col-sm-12 col-xs-12" -->
                       <!-- End Fotos de la visita -->
                      </div>

                    <!-- start project-detail sidebar -->
                    <div class="col-md-3 col-sm-3 col-xs-12">

                      <section class="panel">

                        <div class="x_title">
                          <h2>{!!trans('form.label.projectdescription')!!}</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                          <h3 class="green"><i class="fa fa-paint-brush"></i> {!!trans('form.label.project')!!}: {{$process->process_number}}</h3>

                          <p>{{$process->description}}</p>
                          <br />

                          <div class="project_detail">

                            <p class="title">{!!trans('form.label.corpname')!!}: {{$process->name}}</p>
                            <p>{!!trans('form.label.contactname')!!}: {{$process->applicant_name}}</p>
                            <ul class="list-unstyled">
                                
                                <li><b>{!!trans('form.label.rif')!!}:</b> {{$process->rif}} </li>
                                <li><i class="fa fa-envelope"></i> <b>{!!trans('form.label.email')!!}:</b> {{$process->email}} </li>
                                <li><i class="fa fa-building"></i> <b>{!!trans('form.label.address')!!}:</b> {{$process->address}} </li>
                                <li><i class="fa fa-phone"></i> <b>{!!trans('form.label.phone')!!} #:</b> {{$process->phone}} </li>
                            </ul>
                            <p class="title">{!!trans('form.label.proleader')!!}</p>
                            <p>{{$process->staff}}</p>

                            <p class="title">Tecnician assign to visit</p>
                            
                            <p class="title">{!!trans('form.label.visitleader')!!}: {{$visits->name}}</p><br />
                            <p>
                            @foreach($visittec as $tec)
                                {{$tec->name}}<br />
                              @endforeach
                            </p>
                          <br />

                          @if(count($requestclient)>0)
                          <p class="title">{!!trans('form.label.requestclient')!!}</p>
                          <ul class="list-unstyled project_files">
                           <li><span class="badge bg-orange">{{$requestclient->status}}</span> 
                                <p>{{$requestclient->request}}</p>
                            </li>
                          </ul>
                          <br />
                          @endif

                          @if($visits->signature!='')
                          <p class="title">{!!trans('form.label.sign')!!}</p>
                            <p ><img src="{{url('images/equipment/').'/'.$process->processid.'/'.$visits->id.'/'.$visits->signature}}" alt="" width='100%' /></p>
                          @endif  
                        </div>
                        </div>
                      </section>

                    </div><!-- class="col-md-3 col-sm-3 col-xs-12 -->
                    <!-- end project-detail sidebar -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->  
            
@endsection
@section('script') 
    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- ECharts -->
    {!! Html::script('/vendors/echarts/dist/echarts.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!} 

    <!-- Add fancyBox main JS and CSS files -->
    {!! Html::script('assets/fancy/jquery.fancybox.js') !!}
     

<script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!};
$(document).ready(function() {
      
$('.fancybox').fancybox();
});
</script>
    
      @endsection