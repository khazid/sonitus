 @extends('app/navin')
 @section('css') 
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- FullCalendar -->
    {!! Html::style('/vendors/fullcalendar/dist/fullcalendar.min.css') !!}
    {!! Html::style('/vendors/fullcalendar/dist/fullcalendar.print.css',['media'=>"print"]) !!}
    <!-- bootstrap-clockpicker -->
    {!! Html::style('assets/clockpicker/dist/bootstrap-clockpicker.css') !!}
    {!! Html::style('assets/clockpicker/dist/bootstrap-clockpicker.min.css') !!}
    {!! Html::style('assets/clockpicker/dist/jquery-clockpicker.css') !!}
    {!! Html::style('assets/clockpicker/dist/jquery-clockpicker.min.css') !!}
    <!-- Switchery -->
    {!! Html::style('/vendors/switchery/dist/switchery.min.css') !!}
    <!-- Select2 -->
    {!! Html::style('/vendors/select2/dist/css/select2.min.css') !!}
   

    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
 @endsection
 @section('content') 
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.calendar')!!} <small>{!!trans('form.label.calendarclick')!!}</small></h3>
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{!!trans('form.label.events')!!}</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id='calendar'></div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

    <!-- calendar modal -->
    <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">{!!trans('form.label.newevents')!!}</h4>
          </div>
          <div class="modal-body">
            <div id="testmodal" style="padding: 5px 20px;">
              <form id="antoform" class="form-horizontal calender" role="form">
              <div class="form-group">
                        <label class="col-sm-3 control-label" for="allday">{!!trans('form.label.allevents')!!}?</label>
                        <div class="col-sm-9">
                          <input type="checkbox" class="form-control js-switch" name="allday" id="allday"/>
                        </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1" >{!!trans('form.label.startevents')!!}:</span>
                     <input type="text" class="form-control" placeholder="Start time 24hr format" aria-describedby="basic-addon1" data-inputmask="'mask': '99:99'" id="start">
                     <span class="input-group-addon endadd" id="basic-addon1">{!!trans('form.label.endevents')!!}</span>
                     <input type="text" class="form-control" placeholder="End time 24hr format" aria-describedby="basic-addon1" data-inputmask="'mask': '99:99'" id="end">
                  </div>
                </div>
              <div class="form-group">
                  <label class="col-sm-3 control-label">{!!trans('form.label.eventtype')!!}:</label>
                  <div class="col-sm-9">
                    <select name="eventtype" id="eventtype" class="form-control">
                      <option value='visit'>Visita</option>
                      <option value='cobranza'>Cobranza</option>
                      <option value='general'>Evento general</option>

                    </select>
                    
                  </div>
                </div>
                <div class="form-group client">
                  <label class="col-sm-3 control-label">{!!trans('form.label.client')!!}</label>
                  <div class="col-sm-9">
                    <input type="text" name="client" id="client" class="form-control col-md-10"/>
                    <!-- {!! Form::select('client', $clients,null,['class' => 'form-control','id'=>'client']) !!} -->
                    
                  </div>
                </div>
                <div class="form-group project">
                  <label class="col-sm-3 control-label">{!!trans('form.label.project')!!}</label>
                  <div class="col-sm-9">
                   <select id="project" name="project" class="form-control">
                     <option value='null'>Select</option>
                   </select>
                    
                    
                  </div>
                </div>
                <div class="form-group tecnician2">
                  <label class="col-sm-3 control-label">{!!trans('form.label.visitleader')!!}</label>
                  <div class="col-sm-9">
                    {!! Form::select('teclider', $teclider,null,['class' => 'form-control','id'=>'teclider']) !!}
                  </div>
                </div>
                <div class="form-group tecnician">
                  <label class="col-sm-3 control-label">{!!trans('form.label.technicians')!!}</label>
                  <div class="col-sm-9">
                    
                    {!! Form::select('tecnico', $tecnicos,null,['class' => 'select2_multiple form-control','id'=>'tecnico','multiple'=>'multiple','style'=>'width:100%']) !!}
                    
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">{!!trans('form.label.title')!!}</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title" name="title">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">{!!trans('form.label.description')!!}</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descrnew" name="descrnew"></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">{!!trans('form.label.close')!!}</button>
            <button type="button" class="btn btn-primary antosubmit">{!!trans('form.label.save')!!}</button>
          </div>
        </div>
      </div>
    </div>
    <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2">{!!trans('form.label.editevents')!!}</h4>
          </div>
          <div class="modal-body">
          <input type="hidden" name="eventid" id="eventid">
            <div id="testmodal" style="padding: 5px 20px;">
              <form id="antoform" class="form-horizontal calender" role="form">
              <div class="form-group">
                        <label class="col-sm-3 control-label" for="allday">{!!trans('form.label.allevents')!!}?</label>
                        <div class="col-sm-9">
                          <input type="checkbox" class="checkbox" name="allday2" id="allday2"/>
                        </div>
                      </div>
                <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1" >{!!trans('form.label.startevents')!!}:</span>
                   <input type="text" class="form-control" placeholder="Start time 24hr format" aria-describedby="basic-addon1" data-inputmask="'mask': '99:99'" id="start2">
                   <span class="input-group-addon endadd" id="basic-addon1">{!!trans('form.label.endevents')!!}</span>
                   <input type="text" class="form-control" placeholder="End time 24hr format" aria-describedby="basic-addon1" data-inputmask="'mask': '99:99'" id="end2">
                </div>
                </div>
                <div class="form-group client2">
                  <label class="col-sm-3 control-label">{!!trans('form.label.actualclient')!!}</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id="clienta" name="clienta" readonly="readonly">
                  </div>
                </div>
                <div class="form-group project2">
                  <label class="col-sm-3 control-label">{!!trans('form.label.project')!!}</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id="project2" name="project2" readonly="readonly">
                  </div>
                </div>
                <div class="form-group tecnician2">
                  <label class="col-sm-3 control-label">{!!trans('form.label.visitleader')!!}</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="teclider2" name="teclider2" readonly="readonly">
                  </div>
                </div>
                <div class="form-group tecnician2">
                  <label class="col-sm-3 control-label">{!!trans('form.label.technicians')!!}</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="tecnico2" name="tecnico2" readonly="readonly">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">{!!trans('form.label.title')!!}</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title2" name="title">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">{!!trans('form.label.description')!!}</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descrnew2" name="descrnew"></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">{!!trans('form.label.close')!!}</button>
            <button type="button" class="btn btn-primary antosubmit2">{!!trans('form.label.save')!!}</button>
          </div>
        </div>
      </div>
    </div>

    <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
    <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
    <!-- /calendar modal -->
@endsection
@section('script')        
   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- FullCalendar -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/fullcalendar/dist/fullcalendar.min.js') !!}

    <!-- Clockpicker -->
    {!! Html::script('assets/clockpicker/dist/clockpicker.js') !!}
    {!! Html::script('assets/clockpicker/dist/bootstrap-clockpicker.js') !!}
    {!! Html::script('assets/clockpicker/dist/bootstrap-clockpicker.min.js') !!}
    {!! Html::script('assets/clockpicker/dist/jquery-clockpicker.js') !!}
    {!! Html::script('assets/clockpicker/dist/jquery-clockpicker.min.js') !!}
    <!-- jquery.inputmask -->
    {!! Html::script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}
    <!-- Switchery -->
    {!! Html::script('/vendors/switchery/dist/switchery.min.js') !!}
    {!! Html::script('/vendors/select2/dist/js/select2.full.min.js') !!}
    

    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}
    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- jQuery autocomplete -->
    {!! Html::script('/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') !!}

    <!-- FullCalendar -->
    <script>
    var APP_URL = {!! json_encode(url('/')) !!};
    $(document).ready(function() {
      $(":input").inputmask();
      $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
function lookprojects(clientid,type){
           var ul='';
                    
            $.ajax({
                  method: 'POST', // Type of response and matches what we said in the route
                  url: APP_URL+'/searchprojects', // This is the url we gave in the route
                  data: {'clientid' : clientid,'type':type}, // a JSON object to send back
                  success: function(response){ // What to do if we succeed
                  console.log('projects',response.projects);

                    if(response.projects!=404){
                    
                    $.each(response.projects, function(i, item) {
                      ul +="<option value='"+response.projects[i]['id']+"'>"+response.projects[i]['process_number']+"</option>";
                      
                      });
                    $('#project').append(ul);
                    }else{

                      alert('The client does not have Process for this event');
                    }
                  }
              }); 
}
 //------Autocomplete para clientes------------------------
      $.ajax({
          method: 'POST', // Type of response and matches what we said in the route
          url: APP_URL+'/visits/searchclients', // This is the url we gave in the route
          data: {}, // a JSON object to send back
          success: function(response){ // What to do if we succeed
          console.log('Data',response.data);
            var destinatary =response.data;
                type=
              // initialize autocomplete with custom appendTo
              $('#client').autocomplete({
                lookup: destinatary,
                onSelect: function (suggestion) {
                    // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                    var clientid=suggestion.data, 
                    eventtype=$('#eventtype').val();


                  console.log('clienid: '+clientid+' event:'+eventtype);  
                   $('#project').empty().append('<option value="null">Select</option>');
                           if(eventtype=='visit'){
                            var type='3,6,8,9'; // En proceso,Asignado,Espera de Visita,Continuacion del proceso
                                    
                            }else if(eventtype=='cobranza'){
                              var type='5';
                                
                            }
                            lookprojects(clientid,type);
                }
              });
          }
      });

      
//--------------------Dinamic select ------------------------------------------------      
      $("#eventtype").change(function () {
          var event=$(this).val();
          if(event=='general'){
              $('.client').hide();
              $('.project').hide();
              $('.tecnician').hide();
              $('.tecnician2').hide();

          }else{
              $('.client').show();
              $('.project').show();
              $('.tecnician').show();
              $('.tecnician2').show();
          }
          
      });
      
 //--------------------------------------------------------------------------------     
      
       $('#project').change(function(e) {
          var client=$('#client').val(),
              eventtype=$('#eventtype').val();
              // project=$('#project').text();
            //alert($(this).val());
          if(eventtype=='visit'){   
              $('#title').val('Visita: '+client);
          }else if(eventtype=='cobranza'){   
              $('#title').val('Cobranza: '+client);
          }
       });

      $('#allday').change(function(e) {
      
          if($(this).is(":checked")){
            $('#end').attr('disabled','true');
            $('#end').val('00:00');
           
          }else{
            $('#end').removeAttr('disabled');
             $('#end').val('');
            
          }
      });
    });

//-------------CALENDAR LOAD AND FUNCTIONS --------------------------     
      $(window).load(function() {
        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;
            
            $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
 

        var calendar = $('#calendar').fullCalendar({
           
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'

          },
          selectable: true,
          selectHelper: true,
          
          select: function(start, end, allDay) {
             
             var check = moment(start).format('YYYY-MM-DD')
              var today = moment(new Date()).format('YYYY-MM-DD');
              $('#start').val('');
              $('#end').val('');
              $("#title").val('');
              $('#descrnew').val('');
              $("#project").val('');
              $("#client").val('');
              
            //console.log('start: '+check+' today:'+today+' yesterday:'+yesterday);
            if(check < today)
            {
                $.confirm({
                    title: '<strong>Atention!!</strong>',
                    content: "You can't set a new event in previous dates..",
                    confirmButton:false,
                    cancelButton:'Ok',
                    cancel:function(){}
                    
                });
                return false;
            }
            
            $('#fc_create').click();

            started = start;
            ended = end;
           
           
            $(".antosubmit").on("click", function() {
              var starthr=$('#start').val();
                  endedhr=$('#end').val();
                  title = starthr+' '+$("#title").val();
                  start=started;
                  ended=end;
                  teclider=$('#teclider').val();
                  tecnician=$('#tecnico').val();
                  description=$('#descrnew').val();
                  processid=$("#project").val();
                  eventtype=$("#eventtype").val();
                  requesturl='schedule';
                  console.log('antosubmit '+tecnician);

                  startdate=moment(start).format('YYYY-MM-DD');

                  startdb=moment(startdate+' '+starthr).local().format('YYYY-MM-DD HH:mm');
                  
                  if(endedhr != '00:00'){ 
                    ended=moment(startdate+' '+endedhr).local().format('YYYY-MM-DD HH:mm');
                    allDay=false;
                   }else{
                    enddate=moment(end).format('YYYY-MM-DD');
                    ended=moment(enddate+' 00:00').local().format('YYYY-MM-DD HH:mm');
                    allDay=true;
                   }
                   //console.log('title'+ title+'startdate'+ startdate+'start' + startdb+ 'end'+ended, 'tecnician'+tecnician+ 'allday' + allDay+'processid'+processid+'description'+description);
                  $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: requesturl, // This is the url we gave in the route
                      data: {'title' : title,'startdate' : startdate,'start' : startdb, 'end':ended, 'teclider':teclider, 'tecnician':tecnician, 'allday' : allDay,'processid':processid,'description':description,'eventtype':eventtype}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      console.log('event saved:',response.save);
                        location.reload();
                      }
                  });  
                  
              

              categoryClass = $("#event_type").val();

              if (title) {
                calendar.fullCalendar('renderEvent', {
                    title: title,
                    start: started,
                    end: end,
                    allDay: allDay,
                    description: description
                  },
                  true // make the event "stick"
                );
              }

              $('#title').val('');

              calendar.fullCalendar('unselect');

              $('.antoclose').click();

              return false;
            });


          },//select
          // ---- EDIT EVENT --------------------------------
          eventClick: function(calEvent, jsEvent, view) {
             //console.log('calEvent',calEvent.end._i);
            // console.log('jsEvent',jsEvent);
            // console.log('view',view);
            $('#fc_edit').click();
            $('#eventid').val(calEvent.id);
            $('#title2').val(calEvent.title);
            $('#descrnew2').val(calEvent.description);
            $('#start2').val(moment(calEvent.start._i).local().format('HH:mm'));
            $('#end2').val(moment(calEvent.end._i).local().format('HH:mm'));
            if(calEvent.allday)$('#allday2').prop("checked", "checked");
            
            $('#clienta').val('');
            $('#project2').val('');
            $('#tecnico2').val('');

            var visitid=calEvent.visitid;

                ul="";
                //alert(visitid);
            if(visitid!=''){
                  $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: 'searchvisitdetail', // This is the url we gave in the route
                      data: {'visitid' : visitid}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      // console.log('event info: ',response.teclider.name);
                        
                         $('#clienta').val(response.client[0]['name']);
                         $('#project2').val(response.client[0]['process_number']);
                         $('#teclider2').val(response.teclider.name);
                         $.each(response.tecnico, function(i, item) {
                            ul +=response.tecnico[i]['name']+", ";
                           
                            });
                         
                          $('#tecnico2').val(ul);
                        
                      }
                  }); 
                        $('.client2').show();
                        $('.project2').show();
                        $('.tecnician2').show();
            }else{
                        $('.client2').hide();
                        $('.project2').hide();
                        $('.tecnician2').hide();
            }

            categoryClass = $("#event_type").val();

            $(".antosubmit2").on("click", function() {

              calEvent.title =$('#title2').val();
              calEvent.description =$('#descrnew2').val();
              calEvent.start =$('#start2').val();
              calEvent.end =$('#end2').val();
              calEvent.allday =$('#allday2').val();

              var startdate=moment(calEvent.start._i).format('YYYY-MM-DD');
              var enddate=moment(calEvent.end._i).format('YYYY-MM-DD');
              
              var title =$('#title2').val();
              var description =$('#descrnew2').val();
              var start  = moment(startdate+' '+$('#start2').val()).local().format('YYYY-MM-DD HH:mm');
              var end    = moment(enddate+' '+$('#end2').val()).local().format('YYYY-MM-DD HH:mm');
              var allday ='';
              var id =$('#eventid').val();

              if($('#allday2').val()=='')allday =false;
              console.log('event to updated: '+title+' '+description+' '+start+' '+end+' '+allday+' '+id);
              $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: 'updateevent', // This is the url we gave in the route
                      data: {'id' : id,'title':title,'description':description,'start':start,'end':end,'allday':allday}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      console.log('event updated: ',response);
                        calendar.fullCalendar('updateEvent', calEvent);
                        $('.antoclose2').click();
                        
                      }
                  }); 
              
            });

            calendar.fullCalendar('unselect');
          },//eventClick

          editable: false,
          //-----LOAD EVENTS----------------------------------------
          events: {url: APP_URL+'/visits/calendarscheduled'},
          //---ADD DELETE BOTTOM -----------------------------------
          eventRender: function(event, element) { 
            element.append( "<span class='closeon' title='Delete event' ><i class='fa fa-close'> Delete</i></span>" );
            
          //---DELETE EVENT --------------------------------------
            element.find(".closeon").click(function(e){

              var check = moment(event.start).format('YYYY-MM-DD');
              var today = moment(new Date()).format('YYYY-MM-DD');
              var deleteurl='deletevent';
                    id=event.id;
                //console.log('DELETE check '+check+' today'+today);
                //console.log(' EVENTINFO ',event.start);
              if(check >= today){
                 if(confirm('Do you want to delete this event?')){
                  $('#calendar').fullCalendar('removeEvents',event._id);
                  $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: deleteurl, // This is the url we gave in the route
                      data: {'id' : id}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      //console.log('event deleted:',response.deleted);
                        
                      }
                  }); 
                 }//confirm
                 return false;
              }else{
                $.confirm({
                    title: '<strong>Atention!!</strong>',
                    content: "You can't delete events in previous dates..",
                    confirmButton:false,
                    cancelButton:'Ok',
                    cancel:function(){}
                    
                });
                return false;
              } 
            });//element.find
          
          }//eventrender
          
        });

      });
    </script>
    <!-- /FullCalendar -->
@endsection