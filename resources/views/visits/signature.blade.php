<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>{{ env('APP_TITLE')}} |</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  {!! Html::style('assets/signature_pad/signature-pad.css') !!}
      <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}

  <!-- <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-39365077-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script> -->
</head>
<body onselectstart="return false">
  <input type="hidden" name="visitid" id="visitid" value="{{@$visit->id}}">
  <input type="hidden" name="processid" id="processid" value="{{@$process_id}}">
  <input type="hidden" name="type" id="type" value="{{@$type}}">
  
  <div id="signature-pad" class="m-signature-pad">
    <div class="m-signature-pad--body">
      <canvas></canvas>
    </div>
    <div class="m-signature-pad--footer">
      
      <div class="description"> 
      <span class="labelsign">{!!trans('form.label.signinfo')!!}</span>
       
        <i class="fa fa-spinner fa-spin fa-3x fa-fw wait" style="display:none"></i>
        <span class="sr-only wait" style="display:none">Loading...</span>
      
      </div>

      <button type="button" class="button clear" data-action="clear">{!!trans('form.label.clear')!!}</button>
      <button type="button" class="button save" data-action="save">{!!trans('form.label.save')!!}</button>
    </div>
  </div>

   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    {!! Html::script('assets/signature_pad/signature_pad.js') !!}
    {!! Html::script('assets/signature_pad/app.js') !!}
<script type="text/javascript">
  var APP_URL = {!! json_encode(url('/')) !!};
  $('.save').click(function(){
    
    $(".wait").show();
    $(".clear").hide();
    $(".save").hide();
     $(".labelsign").hide();
  });
</script>
</body>
</html>



  