@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}
        <!-- Select2 -->
    {!! Html::style('/vendors/select2/dist/css/select2.min.css') !!}

  
    <!-- Custom Theme Style -->
    {!! Html::style('/build/css/custom.min.css') !!}
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
@endsection
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>{!!trans('form.label.visit')!!}<small> {!!trans('form.label.visitlist')!!}</small></h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                              <th>#</th>
                                              <th width="10%">{!!trans('form.table.project')!!}</th>
                                              <th width="20%">{!!trans('form.table.client')!!}</th>
                                              <th width="10%">{!!trans('form.table.leader')!!}</th>
                                              <th width="10%">{!!trans('form.table.tecnicians')!!}</th>
                                              <th width="5%">{!!trans('form.table.date')!!}</th>
                                              <th width="30%">{!!trans('form.table.description')!!}</th>
                                              <th width="5%">{!!trans('form.table.status')!!}</th>
                                              <!--<th>Exit_hr</th> -->
                                              <th width="10%">{!!trans('form.table.action')!!}</th>
                                              

                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count(@$visits)>0)
                                        @foreach($visits as $visit)
                                         <tr class="odd gradeX">

                                            <td>{{$visit->visitid}}</td>
                                            <td>{{$visit->process_number}}</td> 
                                            <td>{{$visit->name}}</td> 
                                            <td><span>{{$visit->lider}}</span></td>
                                            <td>
                                              @foreach($visittec as $tec)
                                                @if($visit->visitid==$tec->id)
                                                <span>{{$tec->name}}</span>
                                                @endif
                                              @endforeach
                                            </td>
                                            <td>{{date("d/M/Y",strtotime($visit->date))}}</td>
                                            <td width="30%">{{$visit->description}}</td>
                                            <td>{{@$visit->status}}<br>
                                                @if($visit->close_type!='')
                                                <span>{{$visit->close_type}}</span>
                                                @endif
                                            </td>
                                            <!-- <td>{{@$visit->exit_hr}}</td> -->
                                            <td>
                                             
                                            @if($visit->status_id==20 && $visit->description=='')
                                              <a  class="fa fa-edit fa-2x notmade" style="color: blue" title='Observacion de Visita no Realizada' href="#" id="{{$visit->visitid}}" ></a>
                                            @elseif($visit->status_id==20 && $visit->description!='')
                                              <a  class="fa fa-edit fa-2x notmade" style="color: green" title='Editar Observacion de Visita no Realizada' href="#" id="{{$visit->visitid}}" ></a>
                                            @endif

                                            @if($visit->status_id==21)
                                              
                                                @if($visit->close_type=='' && ($visit->assigned_id == $perid || $rol < 5))
                                                <a  class="fa fa-check-square-o fa-2x endadmin" style="color: blue" title='Tipo de Cierre administrativo' href="#" id="{{$visit->visitid}}" ></a>
                                                @endif
                                              <!--Notificar Cierre administrativo de la visita-->
                                                @if(($visit->close_type=='Archivar' || $visit->close_type=='Facturar' ) && ($rol ==1 || $rol ==6 ))
                                                <a  class="fa fa-check-square-o fa-2x endadmindone" style="color: green" title='Notificar Cierre Administrativo realizado' href="#" id="{{$visit->visitid}}" data-type="{{$visit->close_type}}" ></a>
                                                @endif

                                            @endif 

                                            @if($visit->status_id==18 && ($visit->assigned_id == $perid || $rol <= 5))

                                                <a  href="{{ route('visits/findprocess',['id' => $visit->visitid] )}}" class="green" title="Add visit information">
                                                  <i class="fa fa-pencil-square fa-2x"></i>
                                                </a>

                                            @elseif($visit->status_id==19 && ($visit->assigned_id == $perid || $rol <= 5))
                                                <a  href="{{ route('visits.edit',['id' => $visit->visitid] )}}" class="blue" title="Edit visit information">
                                                  <i class="fa fa-pencil-square fa-2x"></i>
                                                </a>
                                            @endif

                                            @if($rol < 5 && $visit->status_id==18 )
                                                  <a href="#" class="change" title='Cambiar Tecnico Asignado' data-toggle="modal" data-target="#changeModal"  id="{{$visit->visitid}}" data-lider="{{$visit->lider}}"><i class="fa fa-users fa-2x"></i></a>    

                                            @endif

                                            @if( $rol < 5 && ($visit->status_id==47 || $visit->status_id==21))
                                                  <a  class="fa fa-send fa-2x send" title='Enviar Reporte' href="{{ route('visits/sendpdf',['id' => $visit->visitid] )}}" ></a> 
                                                  <a  class="fa fa-eye fa-2x" title='Ver Info' href="{{ route('visits.show',['id' => $visit->visitid] )}}" ></a>
                                                  <a  class="glyphicon glyphicon-list-alt fa-2x" title='Ver pdf' target="_blank" href="{{ route('visits/pdftest',['id' => $visit->visitid] )}}" ></a>

                                            @endif
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                     @else
                                        <tr class="odd gradeX">
                                            <td colspan="5" align="center">No hay información de visitas que mostrar</td> 
                                        </tr>
                                     @endif 
                                        
                                    </tbody>
                                </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <!-- /page content -->



<!-- MODAL PARA EL CAMBIO DE TECNICO---->

<div class="modal fade" id="changeModal" tabindex="-1" role="dialog" aria-labelledby="changeModalLabel">
              {!! Form::hidden('visitid',null,['id'=>'visitid']) !!}
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Actualizacion de Tecnicos</h4>
                  </div>
                  <div class="modal-body">
                    
                      <div class="form-group">
                      <label for="recipient-name" class="control-label">Tecnico Lider:</label>
                        <span id="lidersold"></span><br />
                        <label for="recipient-name" class="control-label">Nuevo Tecnico Lider:</label>
                        {!! Form::select('lider_id', @$personal,null,['class' => 'form-control','id'=>'lider_id']) !!}

                      </div>
                      <div class="form-group tecs">
                        <label for="recipient-name" class="control-label">Tecnicos Asignados:</label>
                        <span id="tecsold"></span><br />
                        <label for="recipient-name" class="control-label">Nuevos Tecnicos</label>
                        {!! Form::select('tecnico', @$personal,null,['class' => 'select2_multiple form-control','id'=>'tecnico','multiple'=>'multiple','style'=>'width:100%']) !!}

                      </div>


                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default cerrar" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary cambiar">Cambiar</button>
                  </div>
                </div>
              </div>
            </div>

<!---------------------------------------->


@endsection

@section('script')

    <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}
   <!--  Select2 -->
    {!! Html::script('/vendors/select2/dist/js/select2.full.min.js') !!}

    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->

    <script type="text/javascript">
$(document).ready(function() {
  var ul="";
  var APP_URL = {!! json_encode(url('/')) !!};
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
/* --------- AGREGAR OBSERVACION VISITA NOT MADE ------*/
   $('#datatable-buttons').on('click','.notmade',function(e) {
            var idvisit= $(this).attr("id");
           
            $.confirm({
                            title: '<strong>Agregar razon por la no que se hizo la visita!!</strong>',
                            content: '<textarea name="description" class="form-control description" rows="5"></textarea>',
                            confirmButton:'Guardar',
                            cancelButton:'Cancelar',
                            confirm: function(){
                              var description=this.$content.find('textarea.description').val();
                              console.log('notmade idvisit: '+idvisit+' description: '+description);
                               $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: APP_URL+'/notmade', // This is the url we gave in the route
                                        data: {'idvisit' : idvisit,'description':description}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                            console.log('Respuesta ajax: ',response);
                                            location.reload();
                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });
/* --------- CIERRE ADMINISTRATIVO ------*/
   $('#datatable-buttons').on('click','.endadmin',function(e) {
            var idvisit= $(this).attr("id");
           
            $.confirm({
                            title: '<strong>Cierre Administrativo!!</strong>',
                            content: 'Seleccione el tipo de cierre para esta visita : <select class="form-group finishas"><option value="Archivar">Archivar</option><option value="Facturar">Facturar</option></select>',
                            confirmButton:'Aceptar',
                            cancelButton:'Cancelar',
                            confirm: function(){
                              var status=this.$content.find('select.finishas').val();
                              console.log('endadmin idvisit: '+idvisit+' status: '+status);
                               $.confirm({
                                          icon:'fa fa-spinner fa-spin',
                                          title:'En Proceso..',
                                          content:'La Solicitud esta en proceso, por favor espere...',
                                          confirmButton:false,
                                          cancelButton:false,
                                          closeIcon:false,
                                        });
                                this.close(true);
                                $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: APP_URL+'/endadmin', // This is the url we gave in the route
                                        data: {'idvisit' : idvisit,'status':status}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                            console.log('Respuesta ajax: ',response);
                                            location.reload();
                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });
/* --------- CIERRE ADMINISTRATIVO REALIZADO ------*/

   $('#datatable-buttons').on('click','.endadmindone',function(e) {
            var idvisit= $(this).attr("id");
            var type = $(this).data('type');
           console.log('Type:' +type);
            $.confirm({
                            title: '<strong>Cierre Administrativo Realizado!!</strong>',
                            content: 'Se actualizara el estado del Cierre administrativo y se informara al Lider del proyecto...',
                            confirmButton:'Aceptar',
                            cancelButton:'Cancelar',
                            confirm: function(){
                              if(type=='Archivar') status = 'Archivado';
                              else if(type=='Facturar') status = 'Facturado';
                              console.log('endadmindone idvisit: '+idvisit+' status: '+status);
                               $.confirm({
                                          icon:'fa fa-spinner fa-spin',
                                          title:'En Proceso..',
                                          content:'La Solicitud esta en proceso, por favor espere...',
                                          confirmButton:false,
                                          cancelButton:false,
                                          closeIcon:false,
                                        });
                                this.close(true);
                                $.ajax({
                                        type: 'POST', // Type of response and matches what we said in the route
                                        url: APP_URL+'/endadmindone', // This is the url we gave in the route
                                        data: {'idvisit' : idvisit,'status':status}, // a JSON object to send back
                                        success: function(response){ // What to do if we succeed
                                            location.reload();
                                        }
                                    });
                            },
                            cancel:function(){
                               
                            }
                            
                        });
             
            
        });

 /*--------- CAMBIO DE TECNICO ---------------------*/  
       $('#datatable-buttons').on('click','.change',function(){
                var idvisit= $(this).attr("id");
                    lider=$(this).data("lider");
                    ul="";
                console.log('visitid: '+idvisit);
                $("#visitid").val(idvisit);
                $("#lidersold").text(lider);


                $.ajax({
                        type: 'POST', // Type of response and matches what we said in the route
                        url: APP_URL+'/findtec', // This is the url we gave in the route
                        data: {'idvisit' : idvisit}, // a JSON object to send back
                        success: function(response){ // What to do if we succeed
                            console.log('tecs',response);
                            $.each(response.tecs, function(i, item) {
                            ul +=response.tecs[i]['name']+", ";
                           
                            });
                          console.log('tecnico response:'+ul);
                          $('#tecsold').text(ul);

                        }
                });
               
                
        });
       $('.cambiar').click(function(){
            var idvisit=$("#visitid").val();
                lider_id=$("#lider_id").val();
                tecnico=$("#tecnico").val();
                console.log('send',idvisit+' '+lider_id+' '+tecnico);
            $.ajax({
                type: 'POST', // Type of response and matches what we said in the route
                url: APP_URL+'/visits/updatetec', // This is the url we gave in the route
                data: {' cambiar idvisit' : idvisit,'lider_id' :lider_id,'tecnico' :tecnico}, // a JSON object to send back
                success: function(response){ // What to do if we succeed
                    console.log('visit',response);
                    location.reload();
                  

                }
            });
       });
/* --------- ENVIO DE REPORTE DE VISITA AL CLIENTE POR CORREO------*/

        $('#datatable-buttons').on('click','.send',function(){
            $.confirm({
                            icon: 'fa fa-spinner fa-spin fa-3x fa-fw',
                            title: '<strong>Espere mientres se envia el reporte al Cliente!!</strong>',
                            confirmButton:false,
                            cancelButton:false,
                            closeIcon: false, // hides the close icon.
                            content: false, // hides content block.
                            
                        });
       });

});
    </script>

@endsection