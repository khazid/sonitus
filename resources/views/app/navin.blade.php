<!DOCTYPE html>
<html lang="en">
  <head>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>{{ env('APP_TITLE')}} |</title>

@yield('css')
<style type="text/css">
  .imgsize {
    max-width: 200px ;
    max-height: 300px ;
    
}
</style>
<!-- oncontextmenu='return false' ondragstart='return false' -->
  </head>
  <body class="nav-md"  >
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{route('app/dashboard')}}" class="site_title"><img src="{{ url('images/img/logo_sonitus_transparente.png')}}" alt="logo_header"></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
              
                <img src='{{(Auth::rol()->roleid != 8 ? url(Auth::personal()->img) : url("images/personal/user.png"))}}' alt="..." class="img-circle profile_img imgsize" style="width:60px;height: 60px;">
              </div>
              <div class="profile_info">
                <span>{!!trans('nav.menu.Welcome')!!}</span>
                <h2>{{ Auth::user()->name }}</h2>
                <span>{{ Auth::rol()->rol }}</span>
              </div>
            </div>
            <!-- /menu profile quick info -->

             <div class="clearfix"></div>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
              
                
                <ul class="nav side-menu">
                @if(Auth::rol()->roleid != 8)
                <li><a href="{{route('app/dashboard')}}"><i class="fa fa-home"></i>{!!trans('nav.menu.Home')!!}</a></li>

                  @foreach(Auth::options() as $opt) 
                            
                    <li role="presentation" class="dropdown">
                     <a href="#"><i class="{{$opt->icon}}"></i> {!!trans('nav.menu.'.$opt->menuname)!!}<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">

                            @foreach(Auth::permission() as $p)
                                @if($p->menu_id === $opt->idmenu)           

                                    <li><a href="{!! ($p->slug=='#' ? '#' : route($p->slug)) !!}">{!!trans('nav.submenu.'.$p->permname)!!}</a></li>    

                                @endif
                            @endforeach           
                        </ul>
                    </li>  
                 @endforeach             
                 @else
                <li><a href="{{ route('client.show',['id' => Auth::client()->id] )}}"><i class="fa fa-home"></i>{!!trans('nav.menu.Home')!!} </a></li>
                @endif 
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>-->
              <a href="{{route('auth/logout')}}" data-toggle="tooltip" data-placement="top" title="Logout" style="width:100%">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a> 
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div><!-- /col-md-3 left_col -->

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <!-- <img src="{{(Auth::rol()->roleid != 8 ? url(Auth::personal()->img) : url('images/personal/user.png'))}}" alt="" style="width:30px;height: 30px;"> -->
                    {{ Auth::user()->name }}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                  @if(Auth::rol()->roleid != 8)
                    <li><a href="{{ route('personal.show',['id' => Auth::personal()->id] )}}"> <span>{!!trans('nav.menu.Profile')!!}</span></a></li>
                  @endif  
                   <li><a href="{{route('auth/logout')}}" ><i class="fa fa-sign-out pull-right"></i>{!!trans('nav.menu.Logout')!!}</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">{{count(Auth::message())}}</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                  @if(count(Auth::message()) > 0)
                  
                      @foreach(Auth::message() as $mess) 
                                <li>
                                  <a>
                                    <span class="image"><img src="{{(Auth::rol()->roleid != 8 ? url(Auth::personal()->img) : url('images/personal/user.png'))}}" alt="Profile Image" /></span>
                                    <span>
                                      <span>{{$mess->name}}</span>
                                      <span class="time">{{date('d-m-Y:H:m',strtotime($mess->created_at))}}</span>
                                    </span>
                                    <span class="message">{{$mess->tittle}}</span>
                                  </a>
                                </li>
                                
                       @endforeach
                    <li>
                      <div class="text-center">
                        <a href="{{route('mess.index')}}">
                          <strong>{!!trans('nav.menu.allalerts')!!}</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                    </ul>
                   @else
                   <li>{!!trans('nav.menu.nomess')!!}</li> 
                  </ul>
                  @endif
                </li>

                <li class="">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false" title="Language">
                    <i class="fa fa-language"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li><a href="{{ url('lang', ['en']) }}"><img src="{{ url('images/img/usa.png')}}" alt="EN" ></a></li>
                  <li><a href="{{ url('lang', ['es']) }}"><img src="{{ url('images/img/spain.png')}}" alt="ES" ></a></li>
                  </ul>
                </li>      

              </ul>
              
            </nav>
          </div>
        </div>
    <!-- /top navigation -->

  @yield('content')      
  
     <!-- footer content -->
        <footer>
          <div class="pull-right">
            Inversiones Sonitus
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
@yield('script')

  </body>
</html>

   
