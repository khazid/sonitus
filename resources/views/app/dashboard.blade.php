@extends('app/navin')
@section('css')
    <!-- Bootstrap -->
    {!! Html::style('/vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('/vendors/font-awesome/css/font-awesome.min.css') !!}
    <!-- NProgress -->
    {!! Html::style('/vendors/nprogress/nprogress.css') !!}
    <!-- iCheck -->
    {!! Html::style('/vendors/iCheck/skins/flat/green.css') !!}
    <!-- bootstrap-progressbar -->
    {!! Html::style('/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') !!}
    
    {!! Html::style('assets/dist/css/jquery-confirm.min.css') !!}
   
   <!-- Custom Theme Style -->
     {!! Html::style('/build/css/custom.min.css') !!}

        
@endsection
@section('content')
<!-- page content -->
        <div class="right_col" role="main">
          <!-- Personla Tecnico -->
          <div class="row tile_count col-md-14">
          @if($rol < 5 )
          <!-- Superusuario-jefe-jefe tecnico-administrador -->
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check"></i>{!!trans('nav.dashboard.newweb')!!}</span>
              <div class="count">{{$newweb[0]->total}}</div>
              <a href="{{route('client/listwebrequest')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-eye"></i> {!!trans('nav.dashboard.newvisit')!!}</span>
              <div class="count">{{$newvisit[0]->total}}</div>
              <a href="{{route('visits.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-asterisk"></i> {!!trans('nav.dashboard.newprocess')!!}</span>
              <div class="count">{{$counter[0]->asignado}}</div>
              <a href="{{route('process.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check-square-o"></i> {!!trans('nav.dashboard.finishpro')!!} </span>
              <div class="count red">{{$counter[0]->terminado}}</div>
              <a href="{{route('process.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-close"></i> {!!trans('nav.dashboard.closepro')!!}</span>
              <div class="count green">{{$counter[0]->cerrado}}</div>
              <a href="{{route('process.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
          @endif
          @if($rol ==5)
          <!-- Tecnico --> 
           <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-eye"></i> {!!trans('nav.dashboard.newvisit')!!}</span>
              <div class="count">{{$newvisit[0]->total}}</div>
              <a href="{{route('visits.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
          @endif
          @if($rol ==6 || $rol==1)
            <!-- Administrativo -->
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-cogs"></i> {!!trans('nav.dashboard.newquote')!!}</span>
              <div class="count blue">{{$quotes[0]->nuevo}}</div>
              <a href="{{route('quote.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-cogs"></i> {!!trans('nav.dashboard.acceptquote')!!}</span>
              <div class="count blue">{{$quotes[0]->aceptada}}</div>
              <a href="{{route('quote.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-cogs"></i> {!!trans('nav.dashboard.askmat')!!}</span>
              <div class="count blue">{{$quotes[0]->solicitudmat}}</div>
              <a href="{{route('quote.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-cogs"></i> {!!trans('nav.dashboard.loadmat')!!}</span>
              <div class="count blue">{{$quotes[0]->matcargados}}</div>
              <a href="{{route('quote.index')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-cogs"></i> {!!trans('nav.submenu.admin_close')!!}</span>
              <div class="count blue">{{$closeadminvisit[0]->total}}</div>
              <a href="{{route('visits.adminclose')}}"><span class="green"> {!!trans('nav.dashboard.seedetail')!!}</a>
            </div>
           @endif 
          </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">
                @include('partials.layout.errors')
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>{!!trans('nav.dashboard.graphh3')!!} <small> {!!trans('nav.dashboard.graphsmall')!!} {{date('d-m-Y',strtotime($semana['start']))}} al {{date('d-m-Y',strtotime($semana['end']))}}</small></h3>
                  </div>
                  <!-- <div class="col-md-6">
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                    </div>
                  </div> -->
                </div>
                  
                <div class="col-md-8 col-sm-9 col-xs-12" >
                  <!-- <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div> -->
                  
                    <!-- <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div> -->
                    <!-- <canvas id="canvas_dahs" ></canvas> -->
                    <div class="panel panel-success">
                        <!-- <div class="panel-heading">
                             <h3 class="panel-title">Total de Visitas</h3>

                        </div> -->
                        <div class="panel-body" align="center">
                            <canvas id="canvas_dahs" width="800" height="300"></canvas>
                        </div>
                    </div>
                  
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                  <div class="x_title">
                    <h2>{!!trans('nav.dashboard.graphh2')!!}</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-6">
                    <div>
                      <p>{!!trans('nav.dashboard.graphd1')!!}</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar" role="progressbar" data-transitiongoal="100" style="background:#9abcc3"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>{!!trans('nav.dashboard.graphd2')!!} </p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%">
                          <div class="progress-bar" role="progressbar" data-transitiongoal="100" style="background:#adc8db"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>{!!trans('nav.dashboard.graphd3')!!} </p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%;">
                          <div class="progress-bar" role="progressbar" data-transitiongoal="100" style="background:#a8e3d7"></div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>{!!trans('nav.dashboard.graphd4')!!}</p>
                      <div class="">
                        <div class="progress progress_sm" style="width: 76%">
                          <div class="progress-bar" role="progressbar" data-transitiongoal="100" style="background:#EF8282"></div>
                        </div>
                      </div>
                    </div>
                  </div>
  

                </div>

                <div class="clearfix"></div>
              </div>
            </div>

          </div>
          <br />

          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!!trans('nav.dashboard.genevent')!!}</h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    @if(count(@$events)>0)
                    @foreach(@$events as $even)
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                              <span>{{date("l jS F Y",strtotime($even->startdate))}}</span>
                            </h2>
                              <p class="excerpt">{{$even->title}}</p>
                              <div class="byline">
                              <span>Event: {{$even->event}}</span>
                              </div>
                            </p>
                          </div>
                        </div>
                      </li>
                    @endforeach
                    @endif 

                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!!trans('nav.dashboard.visits')!!}<small>{!!trans('nav.dashboard.eventdone')!!}</small></h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    @if(count(@$visitsmade)>0)
                    @foreach(@$visitsmade as $vm)
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                              @if($vm->eventtype == 'visit')
                              <a href="{{ route('visits.show',['id' => $vm->visitsid] )}}">{{date("d/m/Y",strtotime($vm->date)).' '.$vm->title}}</a>
                              @else
                              <span>{{date("d/m/Y",strtotime($vm->date)).' '.$vm->title}}</span>
                              @endif
                            </h2>
                            <div class="byline">
                            <span>{{$vm->clientname.' Process: '.$vm->process_number.' lider: '.$vm->staff}}</span>
                            <span>
                             @foreach($visittec as $tec)
                              @if($vm->visitsid==$tec->id)
                              {{$tec->name.' '}}
                              @endif
                             @endforeach
                              </span>
                            </div>
                              <p class="excerpt">Event: {{$vm->event}}</p>
                              @if($vm->eventtype == 'visit')
                              <p class="excerpt">Visit Observation: {{$vm->description}}</p>
                              @endif
                            </p>
                          </div>
                        </div>
                      </li>
                    @endforeach
                    @endif 

                    </ul>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!!trans('nav.dashboard.eventtomade')!!} <small>{!!trans('nav.dashboard.eventtomadesmall')!!}</small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <ul class="list-unstyled timeline">
                   @if(count(@$visits)>0)
                  @foreach(@$visits as $v)
                    <li>
                      <div class="block">
                        <div class="tags">
                        @if($v->visitstatus == '18' && ($rol < 5 || @$perid==$v->id || $rol == 6))

                          <a  href="{{ route('visits/findprocess',['id' => $v->visitsid] )}}" class="tag" title="Add visit information">
                            <span>{{date("d/m/Y",strtotime($v->date))}}</span>
                          </a>

                        @elseif($v->visitstatus == '19' && ($rol < 5 || @$perid==$v->id || $rol == 6))
                          <a  href="{{ route('visits.edit',['id' => $v->visitsid] )}}" class="tag alert alert-warning" title="Edit visit information">
                            <span>{{date("d/m/Y",strtotime($v->date))}}</span>
                          </a>
                        @else
                          <span>{{date("d/m/Y",strtotime($v->date))}}</span>
                        @endif 
                          
                        </div>
                        <div class="block_content">
                          <h2 class="title">{{$v->title}}</h2>
                          <div class="byline">
                            <span>{{$v->clientname.' Process: '.$v->process_number.' Tecnician: '.$v->staff}}</span>
                            <span>
                             @foreach($visittec as $tec)
                              @if($v->visitsid==$tec->id)
                              {{$tec->name.' '}}
                              @endif
                             @endforeach
                              </span>
                          </div>
                          <p class="excerpt">Event: {{$v->event}}</p>

                        </div>
                      </div>
                    </li>
                  @endforeach 
                  @endif
                  </ul>

                </div>
              </div>
            </div>
                
                
              </div>
              <!-- End row -->
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection        
@section('script')

   <!-- jQuery -->
    {!! Html::script('/vendors/jquery/dist/jquery.min.js') !!}
    <!-- Bootstrap -->
    {!! Html::script('/vendors/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/vendors/fastclick/lib/fastclick.js') !!}
    <!-- NProgress -->
    {!! Html::script('/vendors/nprogress/nprogress.js') !!}
    
    <!-- bootstrap-progressbar -->
    {!! Html::script('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}
    <!-- iCheck -->
    {!! Html::script('/vendors/iCheck/icheck.min.js') !!}
    
    <!-- Chart.js -->
    {!! Html::script('/vendors/Chart.js/dist/Chart.min.js') !!}

    <!-- Flot -->
    {!! Html::script('/vendors/Flot/jquery.flot.js') !!}
    {!! Html::script('/vendors/Flot/jquery.flot.pie.js') !!}
    {!! Html::script('/vendors/Flot/jquery.flot.time.js') !!}
    {!! Html::script('/vendors/Flot/jquery.flot.stack.js') !!}
    {!! Html::script('/vendors/Flot/jquery.flot.resize.js') !!}
    <!-- Flot plugins -->
    {!! Html::script('/vendors/flot.orderbars/js/jquery.flot.orderBars.js') !!}
    {!! Html::script('/vendors/flot-spline/js/jquery.flot.spline.min.js') !!}
    {!! Html::script('/vendors/flot.curvedlines/curvedLines.js') !!}
    <!-- DateJS -->
    {!! Html::script('/vendors/DateJS/build/date.js') !!}

    <!-- bootstrap-daterangepicker -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/datepicker/daterangepicker.js') !!}
    <!-- morris.js -->
    {!! Html::script('/vendors/raphael/raphael.min.js') !!}
    {!! Html::script('/vendors/morris.js/morris.min.js') !!}

        <!-- jQuery-confirm -->
    {!! Html::script('assets/dist/js/jquery-confirm.min.js') !!}
    
    
    <!-- Custom Theme Scripts -->
    {!! Html::script('build/js/custom.min.js') !!}

    <!-- bootstrap-daterangepicker -->
    <script>
    var APP_URL = {!! json_encode(url('/')) !!};
      $(document).ready(function() {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      // fillgraph('','','default');
        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2016',
          maxDate: '12/31/2021',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          // ranges: {
          //   'Today': [moment(), moment()],
          //   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          //   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          //   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          //   'This Month': [moment().startOf('month'), moment().endOf('month')],
          //   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          // },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
           var first= picker.startDate.format('Y-m-d');
               last = picker.endDate.format('Y-m-d');
               fillgraph(first,last,'datepicker');
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });

        function fillgraph(first,last,type){
         
          $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: 'creategraphajax', // This is the url we gave in the route
                      data: {'first' : first,'last':last,'type':type}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      console.log('graph updated: ',response.graphnew);
                        
                            // $('#graphnew')val(response.graphnew);
                            // $('#graphdone')val(response.graphdone);
                            // $('#graphdone')val(response.graphnotmade);
                        


                        
                      }
                  });
          
        }
        
   //Bar chart Visits total
    var options = {
          legend: true,
          responsive: false,
          maintainAspectRatio: false,
          scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    suggestedMax:5,
                    stepSize: 1
                }
            }]
        }

        };
      var vnuevas=[{!!$visita['nuevas']!!}];
       var vnodone=[{!!$visita['nodone']!!}];
       var vdone=[{!!$visita['done']!!}];
       var all=[{!!$visita['total']!!}];
      // var colores=randomColor({count: 3,hue: 'blue'});
      var ctx = document.getElementById("canvas_dahs");
     
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        options: options,
        data: {
          labels: ["Total de Visitas"],
          datasets: [{
            label: '# Total',
            backgroundColor: ["rgb(154, 188, 195)"], 
            data: all
          },{
            label: '# Nuevas',
            backgroundColor: ["rgba(3, 88, 106, 0.38)"], 
            data: vnuevas
          }, {
            label: '# Realizadas',
            backgroundColor: ["rgba(38, 185, 154, 0.38)"],
            data: vdone
          }, {
            label: '# No Realizadas',
            backgroundColor: ["rgb(239, 130, 130)"],
            data: vnodone
          }]
        },
     });   
  //-------FLOT CHART -------------------------      
         
        //   data2={!! $graphnew !!};
        //   data3={!! $graphdone !!};
        //   data4={!! $graphnotmade !!};

        // $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
        //   data2, data3, data4
          
        // ], {
        //   series: {
        //     lines: {
        //       show: false,
        //       fill: true
        //     },
        //     splines: {
        //       show: true,
        //       tension: 0.4,
        //       lineWidth: 1,
        //       fill: 0.4
        //     },
        //     points: {
        //       radius: 0,
        //       show: true
        //     },
        //     shadowSize: 2
        //   },
        //   grid: {
        //     verticalLines: true,
        //     hoverable: true,
        //     clickable: true,
        //     tickColor: "#d5d5d5",
        //     borderWidth: 1,
        //     color: '#fff'
        //   },
        //   colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)", "rgba(50, 79, 165, 0.8);"],
        //   xaxis: {
        //     tickColor: "rgba(51, 51, 51, 0.06)",
        //     mode: "time",
        //     tickSize: [1, "day"],
        //     //tickLength: 10,
        //     axisLabel: "Date",
        //     axisLabelUseCanvas: true,
        //     axisLabelFontSizePixels: 12,
        //     axisLabelFontFamily: 'Verdana, Arial',
        //     axisLabelPadding: 10
        //   },
        //   yaxis: {
        //     ticks: 8,
        //     tickColor: "rgba(51, 51, 51, 0.06)",
        //   },
        //   tooltip: true
        // });

        function gd(year, month, day) {
          return new Date(year, month - 1, day).getTime();
        }

//-----MENSAJE DE ALERTAS DE LA SEMANA--------------
var rol={!!$rol !!};

        $.ajax({
                      method: 'POST', // Type of response and matches what we said in the route
                      url: APP_URL+'/app/alertas', // This is the url we gave in the route
                      data: {}, // a JSON object to send back
                      success: function(response){ // What to do if we succeed
                      console.log('Alerts: ',response);
                      console.log('Rol: ',rol);
                          birth='';reqst='';pro=''; web='';newreqst='';adminc='';matreqst='';matquote='';qaccept='';qreject='';

                       
                          if(response.birthday.length>0){
                            birth='<h2>Cumpleañeros de esta semana:</h2> <br />';
                              $.each(response.birthday, function(i, item) {
                                birth += i+")"+ response.birthday[i]+"<br />";
                               
                              });   
                          }
//--------------------- Alertas Procesos y visitas -----------------------------------------
                          if(rol<=4){// super - admin - jefe - jefetec
                            console.log('alertas para super - admin - jefe - jefetec');
                              if(response.requestclient.length>0){
                                reqst='<h2>Notificar a los siguientes clientes que tienen solicitudes sin responder:</h2> ';
                                  $.each(response.requestclient, function(i, item) {
                                    reqst += i+") <strong>Cliente:</strong> "+response.requestclient[i]+"<br /> ";
                                   
                                  });   
                              }
                              if(response.process.length>0){
                                pro='<h2>Los siguientes Proyectos tienen visitas Sin cargar:</h2> <br />';
                                  $.each(response.process, function(i, item) {
                                    pro += i+") <strong>Proyecto:</strong> "+response.process[i]+"<br /> ";
                                   
                                  });   
                              }

                              if(response.webrequest.length>0){
                                web='<h2>Nuevas Solicitudes WEB:</h2> <br />';
                                  $.each(response.webrequest, function(i, item) {
                                    web += i+") <strong>Proyecto:</strong> "+response.webrequest[i]['proyecto']+
                                              " <strong>Cliente:</strong> "+response.webrequest[i]['cliente']+"<br /> "+
                                              " <strong>Descripcion:</strong> "+response.webrequest[i]['description']+"<br /> ";
                                   
                                  });   
                              }

                              if(response.matrequesttec.length>0){
                                matreqst='<h2>Solicitud de materiales para cotizar:</h2>';
                                  $.each(response.matrequesttec, function(i, item) {
                                    matreqst += i+") <strong>Solicitante:</strong> "+response.matrequesttec[i]['personal']+
                                              " <strong>Cliente:</strong> "+response.matrequesttec[i]['cliente']+"<br /> "+
                                              " <strong>Descripcion:</strong> "+response.matrequesttec[i]['description']+"<br /> ";
                                   
                                  });   
                              }
                          } 

                         
//--------------------- Alertas Administrativo -----------------------------------------
                          if(rol==6 || rol<=3){// super - admin - jefe  - administrativo
                            console.log('alertas para super - admin - jefe  - administrativo');
                              if(response.newreqst.length>0){
                                newreqst='<h2>Nuevas Solicitudes de cotizacion:</h2> ';
                                  $.each(response.newreqst, function(i, item) {
                                    newreqst +=i+") <strong>Solicitante:</strong> "+response.newreqst[i]['personal']+
                                              " <strong>Cliente:</strong> "+response.newreqst[i]['cliente']+"<br /> "+
                                              " <strong>Descripcion:</strong> "+response.newreqst[i]['description']+"<br /> ";
                                   
                                  });   
                              }

                              if(response.matrequestadm.length>0){
                                matquote='<h2>Materiales Cargados para Cotizar:</h2> ';
                                  $.each(response.matrequestadm, function(i, item) {
                                    matquote +=i+") <strong>Solicitante:</strong> "+response.matrequestadm[i]['personal']+
                                              " <strong>Cliente:</strong> "+response.matrequestadm[i]['cliente']+"<br /> "+
                                              " <strong>Descripcion:</strong> "+response.matrequestadm[i]['description']+"<br /> ";
                                   
                                  });   
                              }
                              console.log('paso matquote');
                              if(response.visitsadminclose.length>0){
                                adminc='<h2>Visitas con Cierre Administrativo pendientes:</h2>';
                                  $.each(response.visitsadminclose, function(i, item) {
                                    adminc += i+") <strong>Nro:</strong> "+response.visitsadminclose[i]['visita']+
                                              " <strong>Proyecto:</strong> "+response.visitsadminclose[i]['proyecto']+
                                              " <strong>Cliente:</strong> "+response.visitsadminclose[i]['cliente']+"<br /> ";
                                   
                                  });   
                              }
                              if(response.quoteaccept.length>0){
                                qaccept='<h2>Cotizaciones Aceptadas:</h2>';
                                  $.each(response.quoteaccept, function(i, item) {
                                    qaccept += i+") <strong>Nro:</strong> "+response.quoteaccept[i]['quote_number']+
                                              " <strong>Cliente:</strong> "+response.quoteaccept[i]['cliente']+"<br /> ";
                                   
                                  });   
                              }
                              if(response.quotereject.length>0){
                                qreject='<h2>Cotizaciones Rechazadas:</h2>';
                                  $.each(response.quotereject, function(i, item) {
                                    qreject += i+") <strong>Nro:</strong> "+response.quotereject[i]['quote_number']+
                                              " <strong>Cliente:</strong> "+response.quotereject[i]['cliente']+"<br /> ";
                                   
                                  });   
                              }
                          }
                  
                  //--- se arma el modal con la info de las alertas ----
                          if(birth!='' || reqst!='' || pro!='' || web!='' || newreqst!='' || adminc!='' || matreqst!='' || matquote!='' || qaccept!='' || qreject!='' ){
                                    $.confirm({
                                      title: '<strong>Notificaciones Generales!!</strong>',
                                      content:birth+' '+reqst+' '+pro+' '+web+' '+newreqst+' '+adminc+' '+matreqst+' '+matquote+' '+qaccept+' '+qreject,
                                      confirmButton:false,
                                      cancelButton:'OK',
                                      cancel:function(){
                                         
                                      }
                                      
                                  });
                          }
                      }
                  });


      });
    </script>
    <!-- /Flot -->
    
@endsection